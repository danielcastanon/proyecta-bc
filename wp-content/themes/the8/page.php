<?php
get_header ();

$is_blog = the8_get_page_meta_var ( 'is_blog' );
$sb = the8_get_page_meta_var( 'sb' );
$sb_class = $sb && !empty($sb['sb_layout_class']) ? $sb['sb_layout_class'] . '_sidebar' : '';
$sb1_class = $sb && $sb['sb_layout'] == 'right' ? 'sb_right' : 'sb_left';

?>
<div class="page_content <?php echo sanitize_html_class($sb_class); ?>">
	<?php
	if ( $sb && $sb['sb_exist'] ) {
		echo "<div class='container'>";
		if ( $sb['sb1_exists'] ) {
			echo "<aside class='$sb1_class'>";
			dynamic_sidebar( $sb['sidebar1'] );
			echo "</aside>";
		}
		if ( $sb['sb2_exists'] ) {
			echo "<aside class='sb_right'>";
			dynamic_sidebar( $sb['sidebar2'] );
			echo "</aside>";
		}
	}
	?>
	<main>
	<?php
	while ( have_posts() ) : the_post();
		the_content();
		the8_page_links();
	endwhile;
	wp_reset_postdata();
	if ( $is_blog ) get_template_part( 'content', 'blog' );
	comments_template();
	?>
	</main>
	<?php echo $sb && $sb['sb_exist'] ? "</div>" : ""; ?>
</div>

<?php

get_footer ();
?>