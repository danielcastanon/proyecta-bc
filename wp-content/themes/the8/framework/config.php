<?php
	/**
	* ReduxFramework Sample Config File
	* For full documentation, please visit: http://docs.reduxframework.com/
	*/
	if ( ! class_exists( 'Redux' ) ) {
		return;
	}

	// This is your option name where all the Redux data is stored.
	$theme = wp_get_theme();

	$opt_name = $theme->get( 'TextDomain' );

	// This line is only for altering the demo. Can be easily removed.
	// $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

	/*
	*
	* --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
	*
	*/

	$icons = call_user_func(THE8_SLUG . '_get_all_fa_icons', array());

	sort( $icons );
	$iconArray = array();
	foreach ( $icons as $icon ) {
	$name = ucwords( str_replace( '-', ' ', str_replace( array(
			'fa-',
			'-play',
			'-square',
			'-alt',
			'-circle'
	), '', $icon ) ) );
	$iconArray[$icon ] = $name;
	}

	$cwsfi = get_option('cwsfi');
	$isFlatIcons_plugin = !empty($cwsfi) && !empty($cwsfi['entries']);
	if ($isFlatIcons_plugin) {
		$icons = $cwsfi['entries'];
	} else {
		$icons = call_user_func(THE8_SLUG . '_get_all_flaticon_icons');
	}
	$iconArray = array (
		'FontAwesome' => array('prefix' => 'fa fa-', 'icons' => $iconArray),
		'FlatIcons' => array('prefix' => 'flaticon-', 'icons' => $icons)
	);

	$sampleHTML = '';
	if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
		Redux_Functions::initWpFilesystem();

		global $wp_filesystem;

		$sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
	}

	// Background Patterns Reader
	$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
	$sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
	$sample_patterns	= array();

	if ( is_dir( $sample_patterns_path ) ) {

		if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
			$sample_patterns = array();

			while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

				if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
					$name			= explode( '.', $sample_patterns_file );
					$name			= str_replace( '.' . end( $name ), '', $sample_patterns_file );
					$sample_patterns[] = array(
						'alt' => $name,
						'img' => $sample_patterns_url . $sample_patterns_file
					);
				}
			}
		}
	}

	/**
	* ---> SET ARGUMENTS
	* All the possible arguments for Redux.
	* For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
	* */

	$theme = wp_get_theme(); // For use with some settings. Not necessary.

	$args = array(
		// TYPICAL -> Change these values as you need/desire
		'opt_name'			=> $opt_name,
		// This is where your data is stored in the database and also becomes your global variable name.
		'display_name'		=> $theme->get( 'Name' ),
		// Name that appears at the top of your panel
		'display_version'	=> $theme->get( 'Version' ),
		// Version that appears at the top of your panel
		'menu_type'			=> 'menu',
		//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
		'allow_sub_menu'	=> true,
		// Show the sections below the admin menu item or not
		'menu_title'		=> esc_html__('Theme Options', 'the8' ),
		'page_title'		=> esc_html__('Theme Options', 'the8' ),
		// You will need to generate a Google API key to use this feature.
		// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
		'google_api_key'	=> '',
		// Set it you want google fonts to update weekly. A google_api_key value is required.
		'google_update_weekly' => false,
		// Must be defined to add google fonts to the typography module
		'async_typography'	=> true,
		// Use a asynchronous font on the front end or font string
		//'disable_google_fonts_link' => true,					// Disable this in case you want to create your own google fonts loader
		'admin_bar'			=> false,
		// Show the panel pages on the admin bar
		'admin_bar_icon'	=> 'dashicons-admin-generic',
		// Choose an icon for the admin bar menu
		'admin_bar_priority'   => 50,
		// Choose an priority for the admin bar menu
		'global_variable'	=> '',
		// Set a different name for your global variable other than the opt_name
		'dev_mode'			=> false,
		// Show the time the page took to load, etc
		'update_notice'		=> true,
		// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
		'customizer'		=> false,
		// Enable basic customizer support
		//'open_expanded'	=> true,					// Allow you to start the panel in an expanded way initially.
		//'disable_save_warn' => true,					// Disable the save warning when a user changes a field
		// OPTIONAL -> Give you extra features
		'page_priority'		=> null,
		// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
		'page_parent'		=> 'themes.php',
		// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
		'page_permissions'	=> 'manage_options',
		// Permissions needed to access the options panel.
		'menu_icon'			=> '',
		// Specify a custom URL to an icon
		'last_tab'			=> '',
		// Force your panel to always open to a specific tab (by id)
		'page_icon'			=> 'icon-themes',
		// Icon displayed in the admin panel next to your menu_title
		'page_slug'			=> '',
		// Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
		'save_defaults'		=> true,
		// On load save the defaults to DB before user clicks save or not
		'default_show'		=> false,
		// If true, shows the default value next to each field that is not the default value.
		'default_mark'		=> '',
		// What to print by the field's title if the value shown is default. Suggested: *
		'show_import_export'   => true,
		// Shows the Import/Export panel when not used as a field.

		// CAREFUL -> These options are for advanced use only
		'transient_time'	=> 60 * MINUTE_IN_SECONDS,
		'output'			=> true,
		// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
		'output_tag'		=> true,
		// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
		// 'footer_credit'	=> '',				// Disable the footer credit of Redux. Please leave if you can help it.

		// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
		'database'			=> '',
		// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
		'use_cdn'			=> true,
		// If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

		// HINTS
		'hints'				=> array(
			'icon'		=> 'el el-question-sign',
			'icon_position' => 'right',
			'icon_color'	=> 'lightgray',
			'icon_size'	=> 'normal',
			'tip_style'	=> array(
				'color'   => 'dark',
				'shadow'  => true,
				'rounded' => true,
				'style'   => '',
			),
			'tip_position'  => array(
				'my' => 'top left',
				'at' => 'bottom right',
			),
			'tip_effect'	=> array(
				'show' => array(
					'effect'   => 'slide',
					'duration' => '300',
					'event'	=> 'mouseover',
				),
				'hide' => array(
					'effect'   => 'slide',
					'duration' => '300',
					'event'	=> 'click mouseleave',
				),
			),
		)
	);

	$args['share_icons'][] = array(
		'url' => '//twitter.com/Creative_WS',
		'title' => 'Follow us on Twitter',
		'icon' => 'fa fa-twitter'
	);
	$args['share_icons'][] = array(
		'url' => '//www.facebook.com/CreaWS',
		'title' => 'Like us on Facebook',
		'icon' => 'fa fa-facebook'
	);
	$args['share_icons'][] = array(
		'url' => '//www.behance.net/CreativeWS',
		'title' => 'Find us on Behance',
		'icon' => 'fa fa-behance'
	);

	// Panel Intro text -> before the form
	$allowed_html = wp_kses_allowed_html( 'post' );

	// Add content after the form.
	/*$args['footer_text'] = esc_html__( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );*/

	Redux::setArgs( $opt_name, $args );

	$redux_img = get_template_directory_uri() . '/img/framework_added_img/';

	/*
		As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for
	*/

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('General Settings', 'the8' ),
		'id'			=> 'general_setting',
		'customizer_width' => '400px',
		'icon'			=> 'el-icon-adjust-alt'
	) );

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Logotype', 'the8' ),
		'id'			=> 'logo_cont',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id'	=> 'logo',
				'type' => 'media',
				'title' => esc_html__( 'Logo dark', 'the8' ),
				'url'	=> true,
				'compiler' => 'true',
			),
			array(
				'id' => 'logo_is_high_dpi',
				'type' => 'checkbox',
				'title' => esc_html__( 'High-Resolution logo', 'the8' ),
				'default' => '0',
				'hint' => array(
					'title'	=> 'High-Resolution logo',
					'content'   => 'This option applies two logo variants for your site: a big one that is loaded on Retina displays (this one is supposed to be uploaded above if the checkbox is checked), and one that is half the size which is loaded on non-Retina displays (it will be generated automatically).'
					),
				),
			array(
				'id' => 'logo_white',
				'type' => 'media',
				'url'=> true,
				'title' => esc_html__( 'Logo white', 'the8' ),
				'compiler' => true,
				),
			array(
				'id' => 'logo_white_is_high_dpi',
				'type' => 'checkbox',
				'title' => esc_html__( 'High-Resolution logo', 'the8' ),
				'default' => '0'
			),			
			array(
				'id'			=> 'logo-dimensions',
				'type'		=> 'dimensions',
				'units'		=> false,	// You can specify a unit value. Possible: px, em, %
				'units_extended' => 'false',  // Allow users to select any type of unit
				'title' => esc_html__('Dimensions', 'the8' ),
				'default' => array(
					'width'  => '',
					'height' => '',
				)
			),
			array(
				'id' => 'logo-position',
				'type' => 'image_select',
				'title' => esc_html__('Logo position', 'the8' ),
				'options' => array(
								'left' => array('alt' => esc_html__('Left', 'the8' ), 'img' => $redux_img .'align-left.png'),
								'center' => array('alt' => esc_html__('Center', 'the8' ), 'img' => $redux_img .'align-center.png'),
								'right' => array('alt' => esc_html__('Right', 'the8' ), 'img' => $redux_img .'align-right.png')
								),
				'default' => 'left'
				),
			array(
				'id'	=> 'logo-margin',
				'type'	=> 'spacing',
				'mode'	=> 'margin',
				'all'	=> false,
				'title' => esc_html__('Spacings', 'the8' ),
				'default'  => array(
					'margin-top'	=> '12',
					'margin-right'  => '0',
					'margin-bottom' => '12',
					'margin-left'   => '0'
				),
			),
			array(
				'id' => 'logo_sticky',
				'type' => 'media',
				'url'=> true,
				'title' => esc_html__( 'Sticky logo', 'the8' ),
				'required' => array( 'menu-stick', '=', '1' ),
				'compiler' => true,
				),
			array(
				'id' => 'logo_sticky_is_high_dpi',
				'type' => 'checkbox',
				'title' => esc_html__( 'High-Resolution sticky logo', 'the8' ),
				'required' => array( 'menu-stick', '=', '1' ),
				'default' => '0'
				),
			array(
				'id' => 'logo_mobile',
				'type' => 'media',
				'url'=> true,
				'title' => esc_html__( 'Mobile logo', 'the8' ),
				'compiler' => true,
				),
			array(
				'id' => 'logo_mobile_is_high_dpi',
				'type' => 'checkbox',
				'title' => esc_html__( 'High-Resolution logo', 'the8' ),
				'default' => '0'
			),
		)
	) );

	Redux::setSection( $opt_name, array(
	'title'			=> esc_html__('Menu', 'the8' ),
	'id'			=> 'menu_cont',
	'subsection'	=> true,
	'customizer_width' => '450px',
	'fields'		=> array(
		array(
			'id' => 'menu-position',
			'type' => 'image_select',
			'title' => esc_html__('Position', 'the8' ),
			'options' => array(
				'left' => array('title' => esc_html__('Left', 'the8' ), 'img' => $redux_img .'align-left.png'),
				'center' => array('title' => esc_html__('Center', 'the8' ), 'img' => $redux_img .'align-center.png'),
				'right' => array('title' => esc_html__('Right', 'the8' ), 'img' => $redux_img .'align-right.png')
				),//Must provide key => value(array:title|img) pairs for radio options
			'default' => 'left'
		),
		array(
			'id'	=> 'header_margin',
			'type'	=> 'spacing',
			'mode'	=> 'margin',
			'right'		=> false,
			'left'		=> false,
			'display_units' => 'false',
			'title'	=> esc_html__('Spacings', 'the8' ),
			'default'  => array(
				'margin-top'	=> '',
				'margin-bottom' => '',
			)
		),
		array(
			'id' => 'search_place',
			'type' => 'image_select',
			'compiler' => true,
			'title' => esc_html__('Search icon position', 'the8' ),
			'options' => array(
							'none' => array('alt' => esc_html__('No Sidebar', 'the8' ), 'img' => $redux_img . 'no_layout.png'),
							'top' => array('alt' => esc_html__('Top Bar', 'the8' ), 'img' => $redux_img .'search-social-right.png'),
							'left' => array('alt' => esc_html__('Left Menu', 'the8' ), 'img' => $redux_img .'search-menu-left.png'),
							'right' => array('alt' => esc_html__('Right Menu', 'the8' ), 'img' => $redux_img .'search-menu-right.png')
							),
			'default' => 'right'
		),
		array(
			'id' => 'show_header_outside_slider',
			'title' => esc_html__( 'Header hovers slider', 'the8' ),
			'type' => 'checkbox',
			'default' => '0',
			'hint' => array(
				'title'	=> 'Header hovers image slider',
				'content'   => 'Check this checkbox if you wish the homepage header to overlay image slider.',
			),
		),
		array(
			'id' => 'header_outside_slider_bg_color',
			'title' => esc_html__( 'Header Background color', 'the8' ),
			'type' => 'color',
			'transparent' => false,
			'required' => array( 'show_header_outside_slider', '=', 1 ),
			'default'   => '#ffffff',
			'validate'  => 'color',
			'customizer' => false
		),
		array(
			'id' => 'header_outside_slider_bg_opacity',
			'title' => esc_html__( 'Opacity', 'the8' ),
			'desc' => esc_html__( 'In percents', 'the8' ),
			'required' => array( 'show_header_outside_slider', '=', 1 ),
			'type' => 'text',
			'default' => '30'
		),
		array(
			'id' => 'header_outside_slider_font_color',
			'title' => esc_html__( 'Header Font color', 'the8' ),
			'type' => 'color',
			'transparent' => false,
			'required' => array( 'show_header_outside_slider', '=', 1 ),
			'default'   => '#595959',
			'validate'  => 'color',
			'customizer' => false
		),
		array(
			'id' => 'menu-stick',
			'type' => 'checkbox',
			'title' => esc_html__('Apply sticky menu', 'the8' ),
			'default' => '1'// 1 = on | 0 = off
		),
		array(
			'id' => 'enable_mob_menu',
			'title' => esc_html__( 'Enable mobile menu', 'the8' ),
			'type' => 'checkbox',
			'default' => '1',
			'hint' => array(
				'title'	=> 'Mobile Menu (Sandwich)',
				'content'   => 'Check this checkbox if you wish enable mobile menu on all touch-enabled devices. <br> Please note, it still will be applied if the menu width won\'t fit page width.',
			)
		)
		)
	) );

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('TopBar', 'the8' ),
		'id'			=> 'top_bar_cont',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id' => 'top_panel_switcher',
				'type' => 'switch',
				'title' => esc_html__( 'TopBar switcher', 'the8' ),
				"default" => 1,
			),
			array(
				'id' => 'top_panel_text',
				'type' => 'textarea',
				'title' => esc_html__( 'Content', 'the8' ),
				'default' => '',
				'required' => array(
					array('top_panel_switcher','=','1')
				),
			),
			array(
				'id' => 'toggle-share-icon',
				'type' => 'switch',
				'title' => esc_html__('Toogle Share', 'the8' ),
				"default" => 0,
				'required' => array(
					array('top_panel_switcher','=','1')
				),
			),
		)
	) );

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Maintenance', 'the8' ),
		'id'			=> 'meta_slugs',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id' => 'breadcrumbs',
				'type' => 'checkbox',
				'title' => esc_html__('Show breadcrumbs', 'the8' ),
				'default' => '1'// 1 = on | 0 = off
				),
			array(
				'id' => 'blog_author',
				'type' => 'checkbox',
				'title' => esc_html__('Show post author', 'the8' ),
				'default' => '1'// 1 = on | 0 = off
				),
			array(
				'id' => 'cws_enable_loader',
				'type' => 'checkbox',
				'title' => esc_html__('Enable Page Loader', 'the8' ),
				'default' => '1'// 1 = on | 0 = off
				),
			array(
				'id' => 'portfolio_slug',
				'type' => 'text',
				'title' => esc_html__('Portfolio slug', 'the8' ),
				'default' => 'Portfolio'
			),
			array(
				'id' => 'staff_slug',
				'type' => 'text',
				'title' => esc_html__( 'Staff slug', 'the8' ),
				'default' => 'Our team'
			),
			array(
				'id' => 'blog_title',
				'type' => 'text',
				'title' => esc_html__( 'Blog title', 'the8' ),
				'default' => 'Blog'
			),
			array(
				'id' => '_theme_purchase_code',
				'type' => 'text',
				'title' => esc_html__( 'Theme purchase code', 'the8' ),
				'desc' => esc_html__( 'Fill in this field to get automatic theme updates and Demo content import.', 'the8' ),
				'default' => '',
				'customizer' => false,
			)
		)
	) );

	// -> START Styling Options
	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Styling Options', 'the8' ),
		'id'			=> 'styling_options',
		'customizer_width' => '400px',
		'icon' => 'el-icon-brush'
	) );

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Header', 'the8' ),
		'id'			=> 'styling_options_headers',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id' => 'customize_headers',
				'title' => esc_html__( 'Customize headers', 'the8' ),
				'type' => 'switch',
				'default' => 0
			),
			array(
				'id' => 'header_bg_image',
				'title' => esc_html__( 'Header Image', 'the8' ),
				'type' => 'media',
				'required' => array(
					array( 'customize_headers', '=', 1 )
				)
			),
			array(
				'id' => 'independent_headers',
				'title' => esc_html__( 'Independent headers', 'the8' ),
				'type' => 'checkbox',
				'required' => array( 'customize_headers', '=', 1 ),
				'default' => '1',
				'hint' => array(
					'title'	=> 'Independent page headers',
					'content'   => 'This option forces page headers to show customized header only if featured image is assigned to the page. Leave this option untouched if you wish to 
					show the above header image on all pages by default. You can override this on each page separately by assigning featured image.'
				),				
			),			
			array(
				'id' => 'bg_header_color_overlay_type',
				'title' => esc_html__( 'Color overlay type', 'the8' ),
				'type' => 'select',
				'width' => '250px',
				'options' => array(
					'color' => esc_html__( 'Color', 'the8' ),
					'gradient' => esc_html__( 'Gradient', 'the8' )
				),
				'required' => array( 'customize_headers', '=', 1 ),
				'default' => 'none'
			),
			array(
				'id' => 'bg_header_overlay_color',
				'title' => esc_html__( 'Overlay color', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'required' => array( 'bg_header_color_overlay_type', '=', 'color' ),
				'default'   => THE8_COLOR,
				'validate'  => 'color',
				'customizer' => false
			),
			array(
				'id' => 'bg_header_settings',
				'type' => 'group',
				'title' => esc_html__( 'Gradient Settings', 'the8' ),
				'required' => array( 'bg_header_color_overlay_type', '=', 'gradient' ),
				'fields' => array()
			),
			array(
				'id' => 'bg_header_gradient_first_color',
				'title' => esc_html__( 'From', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'default' => THE8_COLOR,
				'validate' => 'color',
				'required' => array(
					array( 'bg_header_color_overlay_type', '=', 'gradient' )
				),
				'customizer' => false
			),
			array(
				'id' => 'bg_header_gradient_second_color',
				'title' => esc_html__( 'To', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'default' => '#0eecbd',
				'validate' => 'color',
				'required' => array(
					array( 'bg_header_color_overlay_type', '=', 'gradient' )
				),
				'customizer' => false
			),
			array(
				'id' => 'bg_header_gradient_type',
				'title' => esc_html__( 'Grdient type', 'the8' ),
				'type' => 'radio',
				'options' => array(
					'linear' => esc_html__( 'Linear', 'the8' ),
					'radial' => esc_html__( 'Radial', 'the8' )
				),
				'required' => array(
					array( 'bg_header_color_overlay_type', '=', 'gradient' )
				),
				'default' => 'linear'
			),
			array(
				'id' => 'bg_header_gradient_linear_settings',
				'title' => esc_html__( 'Linear settings', 'the8' ),
				'type' => 'group',
				'required' => array(
					array( 'bg_header_color_overlay_type', '=', 'gradient' ),
				),
				'fields' => array(
					array(
						'id' => 'angle',
						'title' => esc_html__( 'Angle', 'the8' ),
						'type' => 'text',
						'default' => '45'
					)
				)
			),
			array(
				'id' => 'bg_header_radial_settings',
				'title' => esc_html__( 'Radial settings', 'the8' ),
				'type' => 'group',
				'required' => array(
					array( 'bg_header_color_overlay_type', '=', 'gradient' ),
				),
				'fields' => array()
			),
			array(
				'id' => 'bg_header_gradient_shape_settings',
				'title' => esc_html__( 'Radial shape', 'the8' ),
				'type' => 'radio',
				'options' => array(
					'simple' => esc_html__( 'Simple', 'the8' ),
					'extended' => esc_html__( 'Extended', 'the8' )
				),
				'required' => array(
					array( 'bg_header_color_overlay_type', '=', 'gradient' ),
				),
				'default' => 'simple'
			),
			array(
				'id' => 'bg_header_gradient_shape',
				'title' => esc_html__( 'Radial simple type', 'the8' ),
				'type' => 'radio',
				'required' => array(
					array( 'bg_header_color_overlay_type', '=', 'gradient' ),
				),
				'options' => array(
					'ellipse' => esc_html__( 'Ellipse', 'the8' ),
					'circle' => esc_html__( 'Circle', 'the8' )
				),
				'default' => 'ellipse'
			),
			array(
				'id' => 'bg_header_gradient_size_keyword',
				'title' => esc_html__( 'Size keyword', 'the8' ),
				'type' => 'select',
				'width' => '250px',
				'required' => array(
					array( 'bg_header_color_overlay_type', '=', 'gradient' ),
				),
				'options' => array(
					'closest-side' => esc_html__('Closest side', 'the8' ),
					'farthest-side' => esc_html__('Farthest side', 'the8' ),
					'closest-corner' => esc_html__('Closest corner', 'the8' ),
					'farthest-corner' => esc_html__('Farthest corner', 'the8' )
				),
				'default' => 'farthest-corner'
			),
			array(
				'id' => 'bg_header_gradient_size',
				'title' => esc_html__( 'Size', 'the8' ),
				'desc' => esc_html__( 'Two space separated percent values, for example (60% 55%)', 'the8' ),
				'type' => 'text',
				'required' => array(
					array( 'bg_header_color_overlay_type', '=', 'gradient' ),
				),
				'default' => ''
			),
			array(
				'id' => 'bg_header_color_overlay_opacity',
				'title' => esc_html__( 'Opacity', 'the8' ),
				'desc' => esc_html__( 'In percents', 'the8' ),
				'required' => array( 'bg_header_color_overlay_type', '!=', '' ),
				'type' => 'text',
				'default' => '40'
			),
			array(
				'id' => 'bg_header_use_pattern',
				'title' => esc_html__( 'Add pattern', 'the8' ),
				'required' => array( 'customize_headers', '=', 1 ),
				'type' => 'checkbox',
				'default' => false
			),
			array(
				'id' => 'bg_header_pattern_image',
				'title' => esc_html__( 'Pattern image', 'the8' ),
				'type' => 'media',
				'required' => array(
					array( 'bg_header_use_pattern', '=', true ),
					array( 'customize_headers', '=', 1 ),
				)
			),
			array(
				'id' => 'bg_header_use_blur',
				'title'	=> esc_html__('Apply blur', 'the8' ),
				'type' => 'checkbox',
				'default' => '0',
				'required' => array(
					array( 'customize_headers', '=', 1 ),
				),
			),
			array(
				'id' => 'bg_header_blur_intensity',
				'title'	=> esc_html__('Intensity', 'the8' ),
				'desc' =>  esc_html__('Integer from 0 to 100', 'the8' ),
				'type' => 'text',
				'required' => array(
					array( 'bg_header_use_blur', '=', '1' ),
					array( 'customize_headers', '=', 1 ),
				),
				'default' => '8'
			),
			array(
				'id' => 'parallaxify',
				'title' => esc_html__( 'Parallaxify image', 'the8' ),
				'type' => 'checkbox',
				'required' => array( 'customize_headers', '=', 1 ),
				'default' => '0'
			),
			array(
				'id' => 'parallax_options',
				'type' => 'group',
				'title' => esc_html__( 'Parallax options', 'the8' ),
				'required' => array(
					array( 'customize_headers', '=', '1' ),
					array( 'parallaxify', '=', '1' )
				),
				'fields' => array()
			),
			array(
				'id' => 'scalar_x',
				'title' => esc_html__( 'x-axis parallax intensity', 'the8' ),
				'desc' => esc_html__( 'Integer', 'the8' ),
				'type' => 'text',
				'required' => array(
					array( 'customize_headers', '=', '1' ),
					array( 'parallaxify', '=', '1' )
				),
				'default' => '2'
			),
			array(
				'id' => 'scalar_y',
				'title' => esc_html__( 'y-axis parallax intensity', 'the8' ),
				'desc' => esc_html__( 'Integer', 'the8' ),
				'type' => 'text',
				'required' => array(
					array( 'customize_headers', '=', '1' ),
					array( 'parallaxify', '=', '1' )
				),
				'default' => '2'
			),
			array(
				'id' => 'limit_x',
				'title' => esc_html__( 'Maximum x-axis shift', 'the8' ),
				'desc' => esc_html__( 'Number in pixels', 'the8' ),
				'type' => 'text',
				'required' => array(
					array( 'customize_headers', '=', '1' ),
					array( 'parallaxify', '=', '1' )
				),
				'default' => '15'
			),
			array(
				'id' => 'limit_y',
				'title' => esc_html__( 'Maximum y-axis shift', 'the8' ),
				'desc' => esc_html__( 'Number in pixels', 'the8' ),
				'type' => 'text',
				'required' => array(
					array( 'customize_headers', '=', '1' ),
					array( 'parallaxify', '=', '1' )
				),
				'default' => '15'
			),
			array(
				'id' => 'font_color',
				'title' => esc_html__( 'Font Color', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'default' => '#ffffff',
				'validate' => 'color',
				'required' => array(
					array( 'customize_headers', '=', '1' )
				),
				'customizer' => false
			),
			array(
				'id' => 'spacings',
				'title' => esc_html__( 'Spacings', 'the8' ),
				'desc' => esc_html__( 'in pixels', 'the8' ),
				'type' => 'spacing',
				'mode' => 'padding',
				'right'		=> false,
				'left'		=> false,
				'display_units' => 'false',
				'required' => array(
					array( 'customize_headers', '=', '1' )
				),
				'default' => array( 'top' => '100', 'bottom' => '100')
			)
		)
	));

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Colors', 'the8' ),
		'id'			=> 'styling_options_color',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id'		=> 'theme-custom-color',
				'type'	=> 'color',
				'title'	=> esc_html__('Theme Color', 'the8' ),
				'transparent' => false,
				'default'   => THE8_COLOR,
				'validate'  => 'color',
				),
			array(
				'id'		=> 'theme-footer-color',
				'type'	=> 'color',
				'title'	=> esc_html__('Footer Bg Color', 'the8' ),
				'transparent' => false,
				'default'   => THE8_FOOTER_COLOR,
				'validate'  => 'color',
				),
			array(
				'id'		=> 'theme-footer-font-color',
				'type'	=> 'color',
				'title'	=> esc_html__('Footer Font Color', 'the8' ),
				'transparent' => false,
				'default'   => '#ffffff',
				'validate'  => 'color',
				),
			array(
				'id' => 'use_gradients',
				'title'	=> esc_html__('Apply gradients', 'the8' ),
				'type' => 'checkbox',
				'default' => '0'
				),
			array(
				'id' => 'gradient_settings',
				'title'	=> esc_html__('Gradient Settings', 'the8' ),
				'type' => 'group',
				'required' => array(
					array('use_gradients','=','1')
				),
				'fields' => array(
					array(
						'id' => 'first_color',
						'title'	=> esc_html__('From', 'the8' ),
						'type' => 'color',
						'transparent' => false,
						'default' => THE8_COLOR,
						'validate' => 'color',
						'customizer' => false
						),
					array(
						'id' => 'second_color',
						'title'	=> esc_html__('To', 'the8' ),
						'type' => 'color',
						'transparent' => false,
						'default' => '#0eecbd',
						'validate' => 'color',
						'customizer' => false
						),
					array(
						'id' => 'type',
						'title'	=> esc_html__('Gradient type', 'the8' ),
						'type' => 'radio',
						'options' => array(
							'linear' => esc_html__( 'Linear', 'the8' ),
							'radial' => esc_html__( 'Radial', 'the8' )
						),
						'default' => 'linear'
						),
					array(
						'id' => 'linear_settings',
						'title'	=> esc_html__('Linear settings', 'the8' ),
						'type' => 'group',
						'fields' => array(
							array(
								'id' => 'angle',
								'title'	=> esc_html__('Angle', 'the8' ),
								'type' => 'text',
								'default' => '45'
							)
						)
						),
					array(
						'id' => 'radial_settings',
						'title'	=> esc_html__('Radial settings', 'the8' ),
						'type' => 'group',
						'fields' => array()
						),
					array(
						'id' => 'shape_settings',
						'title'	=> esc_html__('Shape', 'the8' ),
						'type' => 'radio',
						'options' => array(
							'simple' => esc_html__('Simple', 'the8' ),
							'extended' => esc_html__('Extended', 'the8' )
						),
						'default' => 'simple'
						),
					array(
						'id' => 'shape',
						'title'	=> esc_html__('Type', 'the8' ),
						'type' => 'radio',
						'options' => array(
							'ellipse' => esc_html__('Ellipse', 'the8' ),
							'circle' => esc_html__('Circle', 'the8' )
						),
						'default' => 'ellipse'
						),
					array(
						'id' => 'size_keyword',
						'title'	=> esc_html__('Size keyword', 'the8' ),
						'type' => 'select',
						'width' => '250px',
						'options' => array(
							'closest-side' => esc_html__('Closest side', 'the8' ),
							'farthest-side' => esc_html__('Farthest side', 'the8' ),
							'closest-corner' => esc_html__('Closest corner', 'the8' ),
							'farthest-corner' => esc_html__('Farthest corner', 'the8' )
						),
						'default' => 'farthest-corner'
						),
					array(
						'id' => 'size',
						'title'	=> esc_html__('Size', 'the8' ),
						'desc' => esc_html__( 'Two space separated percent values, for example (60% 55%)' , 'the8' ),
						'type' => 'text',
						'default' => ''
						),
				),
				),
			array(
				'id' => 'use_blur',
				'title'	=> esc_html__('Apply blur', 'the8' ),
				'type' => 'checkbox',
				'default' => '0'
				),
			array(
				'id' => 'blur_intensity',
				'title'	=> esc_html__('Intensity', 'the8' ),
				'desc' =>  esc_html__('Integer from 0 to 30 (in pixels)', 'the8' ),
				'type' => 'text',
				'required' => array( 'use_blur', '=', '1' ),
				'default' => '8'
				)
		)
	));

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Layout', 'the8' ),
		'id'			=> 'styling_options_costomizations',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id' => 'boxed-layout',
				'type' => 'switch',
				'title' => esc_html__('Boxed Layout', 'the8' ),
				"default" => 0,
			),
			array(
				'id'	=> 'url_background',
				'type'  => 'info',
				'style' => 'info',
				'required' => array( 'boxed-layout', '=', '1' ),
				'icon'  => 'el el-icon-link',
				'title' => esc_html__('Background Settings', 'the8' ),
				'desc'  => '<a href="themes.php?page=custom-background" target="_blank">Click this link to customize your background settings</a>',
			),
		)
	) );

	// -> START Layout Options
	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Page Layouts', 'the8' ),
		'id'			=> 'layout_options',
		'customizer_width' => '400px',
		'icon' => 'el-icon-magic'
	) );

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Sidebars', 'the8' ),
		'id'			=> 'layout_options_page',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id' => 'def-page-layout',
				'type' => 'image_select',
				'compiler' => true,
				'title' => esc_html__('Page Sidebar Position', 'the8' ),
				'desc' => esc_html__('You can override these settings on each page individually.', 'the8' ),
				'options' => array(
						'none' => array('alt' => esc_html__('No Sidebar', 'the8' ), 'img' => $redux_img . 'none.png'),
						'left' => array('alt' => esc_html__('Left Sidebar', 'the8' ), 'img' => $redux_img . 'left.png'),
						'right' => array('alt' => esc_html__('Right Sidebar', 'the8' ), 'img' => $redux_img . 'right.png'),
						'both' => array('alt' => esc_html__('Double Sidebars', 'the8' ), 'img' => $redux_img . 'both.png'),
				),
				'default' => 'none'
			),
			array(
				'id' => 'def-page-sidebar1',
				'type' => 'select',
				'width' => '250px',
				'data' => 'sidebars',
				'required' => array('def-page-layout', '!=', 'none'),
				'title' => esc_html__('Sidebar', 'the8' ),
				),
			array(
				'id' => 'def-page-sidebar2',
				'type' => 'select',
				'width' => '250px',
				'data' => 'sidebars',
				'required' => array('def-page-layout', '=', 'both'),
				'title' => esc_html__('Right Sidebar', 'the8' ),
				),
			array(
				'id' => 'def-blog-layout',
				'type' => 'image_select',
				'compiler' => true,
				'title' => esc_html__('Blog Sidebar Position', 'the8' ),
				'desc' => esc_html__('You can override these settings on each page individually.', 'the8' ),
				'options' => array(
					'none' => array('alt' => esc_html__('No Sidebar', 'the8' ), 'img' => $redux_img . 'none.png'),
					'left' => array('alt' => esc_html__('Left Sidebar', 'the8' ), 'img' => $redux_img . 'left.png'),
					'right' => array('alt' => esc_html__('Right Sidebar', 'the8' ), 'img' => $redux_img . 'right.png'),
					'both' => array('alt' => esc_html__('Double Sidebars', 'the8' ), 'img' => $redux_img . 'both.png'),
				),
				'default' => 'none'
			),
			array(
				'id' => 'def-blog-sidebar1',
				'type' => 'select',
				'width' => '250px',
				'required' => array('def-blog-layout', '!=', 'none'),
				'data' => 'sidebars',
				'title' => esc_html__('Sidebar', 'the8' ),
				),
			array(
				'id' => 'def-blog-sidebar2',
				'type' => 'select',
				'width' => '250px',
				'required' => array('def-blog-layout', '=', 'both'),
				'data' => 'sidebars',
				'title' => esc_html__('Right Sidebar', 'the8' ),
				),
			array(
				'id' => 'def_blogtype',
				'type' => 'image_select',
				'title' => esc_html__('Blog Layout', 'the8' ),
				'options' => array(
					'small' => array('title' => esc_html__('Small', 'the8' ), 'img' => $redux_img .'small.png' ),
					'medium' => array('title' => esc_html__('Medium', 'the8' ), 'img' => $redux_img . 'medium.png' ),
					'large' => array('title' => esc_html__('Large', 'the8' ), 'img' => $redux_img .'large.png' ),
					'2' => array('title' =>  esc_html__('Two', 'the8' ), 'img' => $redux_img . 'pinterest_2_columns.png'),
					'3' => array('title' => esc_html__('Three', 'the8' ), 'img' => $redux_img . 'pinterest_3_columns.png'),
				),
				'default' => 'medium',
				'width' => '250px',
			)

		)
	) );

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Sidebar Generator', 'the8' ),
		'id'			=> 'layout_options_sidebar_generator',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id'=>'sidebars',
				'type' => 'multi_text',
				'title' => esc_html__('Sidebar Generator', 'the8' ),
				'desc' => wp_kses(esc_html__('Please note: dot\'t forget to remove all the <a href="widgets.php">widgets</a> within the sidebar before deleting!', 'the8' ), $allowed_html),
				'options' => array('Main Sidebar', 'Footer Sidebar'),
			)
		)
	) );

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Footer', 'the8' ),
		'id'			=> 'layout_options_footer',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id' => 'footer-sidebar-top',
				'type' => 'select',
				'width' => '250px',
				'data' => 'sidebars',
				'title' => esc_html__('Footer\'s sidebar area', 'the8' ),
				'desc' => esc_html__('This options will set the default Footer widget area, unless you override it on each page.', 'the8' ),
				),
			array(
				'id' => 'footer_copyrights_text',
				'type' => 'textarea',
				'title' => esc_html__( 'Footer Copyrights content', 'the8' ),
				'default' => ''
			)
		)
	) );

	// -> START Layout Options
	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Typography', 'the8' ),
		'id'			=> 'typography_options',
		'customizer_width' => '400px',
		'icon' => 'el-icon-font',
		'fields'		=> array(
			array(
				'id' => 'menu-font',
				'type' => 'typography',
				'title' => esc_html__('Menu Font', 'the8' ),
				'google' => true,
				'color' => true,
				'line-height' => true,
				'font-size' => true,
				'font-backup' => true,
				'text-align' => false,
				'default' => array(
					'font-size' => '16px',
					'line-height' => '34px',
					'color' => '#595959',
					'google' => true,
					'font-family' => 'Lato',
					'font-weight' => '400',
					'font-backup' => 'Helvetica, Arial, sans-serif'
				),
			),
			array(
				'id' => 'header-font',
				'type' => 'typography',
				'title' => esc_html__('Headers Font', 'the8' ),
				'google' => true,
				'font-backup' => true,
				'font-size' => true,
				'line-height' => true,
				'color' => true,
				'word-spacing' => false,
				'letter-spacing' => false,
				'text-align' => false,
				'text-transform' => true,
				'default' => array(
					'font-size' => '40px',
					'line-height' => '40px',
					'color' => '#595959',
					'google' => true,
					'font-family' => 'Open Sans',
					'font-weight' => '300',
					'font-backup' => 'Helvetica, Arial, sans-serif'
				),
			),
			array(
				'id' => 'body-font',
				'type' => 'typography',
				'title' => esc_html__('Content Text Font', 'the8' ),
				'google' => true,
				'font-backup' => true,
				'font-size' => true,
				'line-height' => true,
				'color' => true,
				'word-spacing' => false,
				'letter-spacing' => false,
				'text-align' => false,
				'default' => array(
					'font-size' => '16px',
					'line-height' => '28px',
					'color' => '#535353',
					'google' => true,
					'font-family' => 'Roboto',
					'font-weight' => '300',
					'font-backup' => 'Helvetica, Arial, sans-serif'
				),
			)
		)
	) );

	// -> START Homepage Options
	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Home Page', 'the8' ),
		'id'			=> 'homepage_options',
		'customizer_width' => '400px',
		'icon' => 'el-icon-home',
		'fields'		=> array(
			array(
				'id' => 'home-slider-type',
				'type' => 'radio',
				'title' => esc_html__('Slider', 'the8' ),
				'options' => array('none' => 'None', 'img-slider' => 'Image Slider', 'video-slider' => 'Video', 'stat-img-slider' => 'Static Image'), //Must provide key => value pairs for radio options
				'default' => 'none',
			),
			array(
				'id' => 'home-header-slider-options',
				'type' => 'text',
				'required' => array('home-slider-type', '=', 'img-slider'),
				'title' => esc_html__('Slider shortcode', 'the8' ),
				'default' => '[rev_slider homepage]',
			),
			array(
				'id'	=> 'slidersection-start',
				'type'	=> 'section',
				'required' => array( 'home-slider-type', '=', 'video-slider' ),
				'title'	=> esc_html__('Video Slider Setting', 'the8' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id' => 'slider_switch',
				'type' => 'switch',
				'title' => esc_html__('Slider', 'the8' ),
				'required' => array( 'home-slider-type', '=', 'video-slider' ),
				"default" => 0,
			),
			array(
				'id' => 'slider_shortcode',
				'title' => esc_html__( 'Slider shortcode', 'the8' ),
				'type' => 'text',
				'default' => '',
				'required' => array(
					array( 'slider_switch', '=', '1' ),
					array( 'home-slider-type', '=', 'video-slider' )
				),
			),
			array(
				'id' => 'set_video_header_height',
				'title' => esc_html__( 'Set Video height', 'the8' ),
				'type' => 'checkbox',
				'required' => array(
					array( 'home-slider-type', '=', 'video-slider' ),
					array( 'slider_switch', '=', '0' )
				),
				'default' => '0'
			),
			array(
				'id' => 'video_header_height',
				'title' => esc_html__( 'Video height', 'the8' ),
				'type' => 'text',
				'default' => '600',
				'required' => array(
					array( 'home-slider-type', '=', 'video-slider' ),
					array( 'set_video_header_height', '=', '1' )
				),
			),
			array(
				'id' => 'video_type',
				'title' => esc_html__( 'Video type', 'the8' ),
				'type' => 'radio',
				'options' => array(
					'self_hosted' => esc_html__( 'Self hosted', 'the8' ),
					'youtube' => esc_html__( 'Youtube', 'the8' ),
					'vimeo' => esc_html__( 'Vimeo', 'the8' )
				),
				'default' => 'self_hosted'
			),
			array(
				'id' => 'sh_source',
				'title' => esc_html__( 'Add video', 'the8' ),
				'type' => 'media',
				'mode' => false,
				'required' => array( 'video_type', '=', 'self_hosted' ),
				'url' => true
			),
			array(
				'id' => 'youtube_source',
				'title' => esc_html__( 'Youtube video code', 'the8' ),
				'type' => 'text',
				'required' => array( 'video_type', '=', 'youtube' ),
				'default' => ''
			),
			array(
				'id' => 'vimeo_source',
				'title' => esc_html__( 'Vimeo embed url', 'the8' ),
				'type' => 'text',
				'required' => array( 'video_type', '=', 'vimeo' ),
				'default' => ''
			),
			array(
				'id' => 'color_overlay_type',
				'title' => esc_html__( 'Color overlay type', 'the8' ),
				'type' => 'select',
				'width' => '250px',
				'options' => array(
					'color' => esc_html__( 'Color', 'the8' ),
					'gradient' => esc_html__( 'Gradient', 'the8' )
				),
				'default' => 'none'
			),
			array(
				'id' => 'overlay_color',
				'title' => esc_html__( 'Overlay color', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'required' => array( 'color_overlay_type', '=', 'color' ),
				'default'   => THE8_COLOR,
				'validate'  => 'color',
				'customizer' => false
			),
			array(
				'id' => 'slider_gradient_settings',
				'type' => 'group',
				'title' => esc_html__( 'Gradient Settings', 'the8' ),
				'required' => array( 'color_overlay_type', '=', 'gradient' ),
				'fields' => array()
			),
			array(
				'id' => 'slider_gradient_first_color',
				'title' => esc_html__( 'From', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'default' => THE8_COLOR,
				'validate' => 'color',
				'required' => array(
					array( 'color_overlay_type', '=', 'gradient' )
				),
				'customizer' => false
			),
			array(
				'id' => 'slider_gradient_second_color',
				'title' => esc_html__( 'To', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'default' => '#0eecbd',
				'validate' => 'color',
				'required' => array(
					array( 'color_overlay_type', '=', 'gradient' )
				),
				'customizer' => false
			),
			array(
				'id' => 'slider_gradient_type',
				'title' => esc_html__( 'Grdient type', 'the8' ),
				'type' => 'radio',
				'options' => array(
					'linear' => esc_html__( 'Linear', 'the8' ),
					'radial' => esc_html__( 'Radial', 'the8' )
				),
				'required' => array(
					array( 'color_overlay_type', '=', 'gradient' )
				),
				'default' => 'linear'
			),
			array(
				'id' => 'slider_gradient_linear_settings',
				'title' => esc_html__( 'Linear settings', 'the8' ),
				'type' => 'group',
				'required' => array(
					array( 'color_overlay_type', '=', 'gradient' ),
					array( 'slider_gradient_type', '=', 'linear' )
				),
				'fields' => array(
					array(
						'id' => 'angle',
						'title' => esc_html__( 'Angle', 'the8' ),
						'type' => 'text',
						'default' => '45'
					)
				)
			),
			array(
				'id' => 'slider_gradient_radial_settings',
				'title' => esc_html__( 'Radial settings', 'the8' ),
				'type' => 'group',
				'required' => array(
					array( 'color_overlay_type', '=', 'gradient' ),
					array( 'slider_gradient_type', '=', 'radial' )
				),
				'fields' => array()
			),
			array(
				'id' => 'slider_gradient_shape_settings',
				'title' => esc_html__( 'Shape', 'the8' ),
				'type' => 'radio',
				'options' => array(
					'simple' => esc_html__( 'Simple', 'the8' ),
					'extended' => esc_html__( 'Extended', 'the8' )
				),
				'required' => array(
					array( 'color_overlay_type', '=', 'gradient' ),
					array( 'slider_gradient_type', '=', 'radial' )
				),
				'default' => 'simple'
			),
			array(
				'id' => 'slider_gradient_shape',
				'title' => esc_html__( 'Type', 'the8' ),
				'type' => 'radio',
				'required' => array(
					array( 'color_overlay_type', '=', 'gradient' ),
					array( 'slider_gradient_type', '=', 'radial' ),
					array( 'slider_gradient_shape_settings', '=', 'simple' )
				),
				'options' => array(
					'ellipse' => esc_html__( 'Ellipse', 'the8' ),
					'circle' => esc_html__( 'Circle', 'the8' )
				),
				'default' => 'ellipse'
			),
			array(
				'id' => 'slider_gradient_size_keyword',
				'title' => esc_html__( 'Size keyword', 'the8' ),
				'type' => 'select',
				'width' => '250px',
				'required' => array(
					array( 'color_overlay_type', '=', 'gradient' ),
					array( 'slider_gradient_type', '=', 'radial' ),
					array( 'slider_gradient_shape_settings', '=', 'extended' )
				),
				'options' => array(
					'closest-side' => esc_html__('Closest side', 'the8' ),
					'farthest-side' => esc_html__('Farthest side', 'the8' ),
					'closest-corner' => esc_html__('Closest corner', 'the8' ),
					'farthest-corner' => esc_html__('Farthest corner', 'the8' )
				),
				'default' => 'farthest-corner'
			),
			array(
				'id' => 'slider_gradient_size',
				'title' => esc_html__( 'Size', 'the8' ),
				'desc' => esc_html__( 'Two space separated percent values, for example (60% 55%)', 'the8' ),
				'type' => 'text',
				'required' => array(
					array( 'color_overlay_type', '=', 'gradient' ),
					array( 'slider_gradient_type', '=', 'radial' ),
					array( 'slider_gradient_shape_settings', '=', 'extended' )
				),
				'default' => ''
			),
			array(
				'id' => 'color_overlay_opacity',
				'title' => esc_html__( 'Opacity', 'the8' ),
				'desc' => esc_html__( 'In percents', 'the8' ),
				'required' => array( 'color_overlay_type', '!=', '' ),
				'type' => 'text',
				'default' => '40'
			),
			array(
				'id' => 'use_pattern',
				'title' => esc_html__( 'Use pattern image', 'the8' ),
				'type' => 'checkbox',
				'default' => false
			),
			array(
				'id' => 'pattern_image',
				'title' => esc_html__( 'Pattern image', 'the8' ),
				'type' => 'media',
				'required' => array( 'use_pattern', '=', true )
			),
			array(
				'id'	=> 'slidersection-end',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id' => 'home-header-image-options',
				'type' => 'media',
				'required' => array('home-slider-type', '=', 'stat-img-slider'),
				'url'=> true,
				'readonly' => false,
				'title' => esc_html__('Static Image', 'the8' ),
				'compiler' => 'true',
				),
			array(
				'id' => 'set_static_image_height',
				'title' => esc_html__('Set Image height', 'the8' ),
				'type' => 'checkbox',
				'required' => array('home-slider-type', '=', 'stat-img-slider'),
				'default' => '0'
			),
			array(
				'id' => 'static_image_height',
				'title' => esc_html__( 'Static Image Height', 'the8' ),
				'type' => 'text',
				'default' => '600',
				'required' => array(
					array('home-slider-type', '=', 'stat-img-slider'),
					array('set_static_image_height', '=', '1')
				),
			),
			array(
				'id' => 'img_header_color_overlay_type',
				'title' => esc_html__( 'Color overlay type', 'the8' ),
				'type' => 'select',
				'width' => '250px',
				'options' => array(
					'color' => esc_html__( 'Color', 'the8' ),
					'gradient' => esc_html__( 'Gradient', 'the8' )
				),
				'required' => array(
					array('home-slider-type', '=', 'stat-img-slider')
				),
				'default' => 'none'
			),
			array(
				'id' => 'img_header_overlay_color',
				'title' => esc_html__( 'Overlay color', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'color' ),
					array('home-slider-type', '=', 'stat-img-slider')
				),
				'default'   => THE8_COLOR,
				'validate'  => 'color',
				'customizer' => false
			),
			array(
				'id' => 'img_header_gradient_settings',
				'type' => 'group',
				'title' => esc_html__( 'Gradient Settings', 'the8' ),
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider')
				),
				'fields' => array()
			),
			array(
				'id' => 'img_header_gradient_first_color',
				'title' => esc_html__( 'From', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'default' => THE8_COLOR,
				'validate' => 'color',
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider')
				),
				'customizer' => false
			),
			array(
				'id' => 'img_header_gradient_second_color',
				'title' => esc_html__( 'To', 'the8' ),
				'type' => 'color',
				'transparent' => false,
				'default' => '#0eecbd',
				'validate' => 'color',
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider')
				),
				'customizer' => false
			),
			array(
				'id' => 'img_header_gradient_type',
				'title' => esc_html__( 'Grdient type', 'the8' ),
				'type' => 'radio',
				'options' => array(
					'linear' => esc_html__( 'Linear', 'the8' ),
					'radial' => esc_html__( 'Radial', 'the8' )
				),
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider')
				),
				'default' => 'linear'
			),
			array(
				'id' => 'img_header_gradient_linear_settings',
				'title' => esc_html__( 'Linear settings', 'the8' ),
				'type' => 'group',
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_gradient_type', '=', 'linear' )
				),
				'fields' => array(
					array(
						'id' => 'angle',
						'title' => esc_html__( 'Angle', 'the8' ),
						'type' => 'text',
						'default' => '45'
					)
				)
			),
			array(
				'id' => 'img_header_gradient_radial_settings',
				'title' => esc_html__( 'Radial settings', 'the8' ),
				'type' => 'group',
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_gradient_type', '=', 'radial' )
				),
				'fields' => array()
			),
			array(
				'id' => 'img_header_gradient_shape_settings',
				'title' => esc_html__( 'Shape', 'the8' ),
				'type' => 'radio',
				'options' => array(
					'simple' => esc_html__( 'Simple', 'the8' ),
					'extended' => esc_html__( 'Extended', 'the8' )
				),
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_gradient_type', '=', 'radial' )
				),
				'default' => 'simple'
			),
			array(
				'id' => 'img_header_gradient_shape',
				'title' => esc_html__( 'Type', 'the8' ),
				'type' => 'radio',
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_gradient_type', '=', 'radial' ),
					array( 'img_header_gradient_shape_settings', '=', 'simple' )
				),
				'options' => array(
					'ellipse' => esc_html__( 'Ellipse', 'the8' ),
					'circle' => esc_html__( 'Circle', 'the8' )
				),
				'default' => 'ellipse'
			),
			array(
				'id' => 'img_header_gradient_size_keyword',
				'title' => esc_html__( 'Size keyword', 'the8' ),
				'type' => 'select',
				'width' => '250px',
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_gradient_type', '=', 'radial' ),
					array( 'img_header_gradient_shape_settings', '=', 'extended' )
				),
				'options' => array(
					'closest-side' => esc_html__('Closest side', 'the8' ),
					'farthest-side' => esc_html__('Farthest side', 'the8' ),
					'closest-corner' => esc_html__('Closest corner', 'the8' ),
					'farthest-corner' => esc_html__('Farthest corner', 'the8' )
				),
				'default' => 'farthest-corner'
			),
			array(
				'id' => 'img_header_gradient_size',
				'title' => esc_html__( 'Size', 'the8' ),
				'desc' => esc_html__( 'Two space separated percent values, for example (60% 55%)', 'the8' ),
				'type' => 'text',
				'required' => array(
					array( 'img_header_color_overlay_type', '=', 'gradient' ),
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_gradient_type', '=', 'radial' ),
					array( 'img_header_gradient_shape_settings', '=', 'extended' )
				),
				'default' => ''
			),
			array(
				'id' => 'img_header_color_overlay_opacity',
				'title' => esc_html__( 'Opacity', 'the8' ),
				'desc' => esc_html__( 'In percents', 'the8' ),
				'required' => array(
					array( 'img_header_color_overlay_type', '!=', '' ),
					array('home-slider-type', '=', 'stat-img-slider')
				),
				'type' => 'text',
				'default' => '40'
			),
			array(
				'id' => 'img_header_use_pattern',
				'title' => esc_html__( 'Use pattern image', 'the8' ),
				'required' => array(
					array('home-slider-type', '=', 'stat-img-slider')
				),
				'type' => 'checkbox',
				'default' => false
			),
			array(
				'id' => 'img_header_pattern_image',
				'title' => esc_html__( 'Pattern image', 'the8' ),
				'type' => 'media',
				'required' => array(
					array( 'img_header_use_pattern', '=', true ),
					array('home-slider-type', '=', 'stat-img-slider')
				),
			),
			array(
				'id' => 'img_header_parallaxify',
				'title' => esc_html__( 'Parallaxify image', 'the8' ),
				'type' => 'checkbox',
				'required' => array('home-slider-type', '=', 'stat-img-slider'),
				'default' => '0'
			),
			array(
				'id' => 'img_header_parallax_options',
				'type' => 'group',
				'title' => esc_html__( 'Parallax options', 'the8' ),
				'required' => array(
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_parallaxify', '=', '1' )
				),
				'fields' => array()
			),
			array(
				'id' => 'img_header_scalar_x',
				'title' => esc_html__( 'x-axis parallax intensity', 'the8' ),
				'desc' => esc_html__( 'Integer', 'the8' ),
				'type' => 'text',
				'required' => array(
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_parallaxify', '=', '1' )
				),
				'default' => '2'
			),
			array(
				'id' => 'img_header_scalar_y',
				'title' => esc_html__( 'y-axis parallax intensity', 'the8' ),
				'desc' => esc_html__( 'Integer', 'the8' ),
				'type' => 'text',
				'required' => array(
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_parallaxify', '=', '1' )
				),
				'default' => '2'
			),
			array(
				'id' => 'img_header_limit_x',
				'title' => esc_html__( 'Maximum x-axis shift', 'the8' ),
				'desc' => esc_html__( 'Number in pixels', 'the8' ),
				'type' => 'text',
				'required' => array(
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_parallaxify', '=', '1' )
				),
				'default' => '15'
			),
			array(
				'id' => 'img_header_limit_y',
				'title' => esc_html__( 'Maximum y-axis shift', 'the8' ),
				'desc' => esc_html__( 'Number in pixels', 'the8' ),
				'type' => 'text',
				'required' => array(
					array('home-slider-type', '=', 'stat-img-slider'),
					array( 'img_header_parallaxify', '=', '1' )
				),
				'default' => '15'
			),
			array(
				'id' => 'def-home-layout',
				'type' => 'image_select',
				'width' => '250px',
				'compiler' => true,
				'title' => esc_html__('HomePage Sidebar', 'the8' ),
				'options' => array(
					'none' => array('alt' => esc_html__('No Sidebar', 'the8' ), 'img' => $redux_img . 'none.png'),
					'left' => array('alt' => esc_html__('Left Sidebar', 'the8' ), 'img' => $redux_img . 'left.png'),
					'right' => array('alt' => esc_html__('Right Sidebar', 'the8' ), 'img' => $redux_img . 'right.png'),
					'both' => array('alt' => esc_html__('Double Sidebars', 'the8' ), 'img' => $redux_img . 'both.png'),
				),
				'default' => 'none'
			),
			array(
				'id' => 'def-home-sidebar1',
				'type' => 'select',
				'required' => array('def-home-layout', '!=', 'none'),
				'data' => 'sidebars',
				'title' => esc_html__('Sidebar', 'the8' ),
				'width' => '250px',
				),
			array(
				'id' => 'def-home-sidebar2',
				'type' => 'select',
				'required' => array('def-home-layout', '=', 'both'),
				'data' => 'sidebars',
				'title' => esc_html__('Left Sidebar', 'the8' ),
				'width' => '250px',
				),
		)
	) );

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) )  {
	// -> START WOOCOMMERCE Options
	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('WooCommerce', 'the8' ),
		'id'			=> 'wooCommerce_options',
		'customizer_width' => '400px',
		'icon' => 'el-icon-shopping-cart',
		'fields'		=> array(
			array(
				'id' => 'def-woo-layout',
				'type' => 'image_select',
				'compiler' => true,
				'title' => esc_html__('Default WooCommerce Sidebar', 'the8' ),
				'options' => array(
					'none' => array('alt' => esc_html__('No Sidebar', 'the8' ), 'img' => $redux_img . 'none.png'),
					'left' => array('alt' => esc_html__('Left Sidebar', 'the8' ), 'img' => $redux_img . 'left.png'),
					'right' => array('alt' => esc_html__('Right Sidebar', 'the8' ), 'img' => $redux_img . 'right.png'),
				),
				'default' => 'none'
			),
			array(
				'id' => 'def-woo-sidebar',
				'type' => 'select',
				'width' => '250px',
				'data' => 'sidebars',
				'required' => array('def-woo-layout', '!=', 'none'),
				'title' => esc_html__('WooCommerce Sidebar', 'the8' ),
				),
			array(
				'id' => 'woo-num-products',
				'type' => 'spinner',
				'title' => esc_html__('Products per page', 'the8' ),
				"default" => get_option('posts_per_page'),
				"min" => "1",
				"step" => "1",
				"max" => "200",
				),
			array(
				'id' => 'woo-resent-num-products',
				'type' => 'spinner',
				'title' => esc_html__('Recent products per page', 'the8' ),
				"default" => 3,
				"min" => "1",
				"step" => "1",
				"max" => "200",
				),
			array(
				'id' => 'woo_cart_place',
				'type' => 'image_select',
				'compiler' => true,
				'title' => esc_html__('Show woocart on', 'the8' ),
				'options' => array(
								'none' => array('alt' => esc_html__('No Sidebar', 'the8' ), 'img' => $redux_img . 'no_layout.png'),
								'top' => array('alt' => esc_html__('Top Bar', 'the8' ), 'img' => $redux_img .'align-center.png'),
								'left' => array('alt' => esc_html__('Left Menu', 'the8' ), 'img' => $redux_img .'align-left.png'),
								'right' => array('alt' => esc_html__('Right Menu', 'the8' ), 'img' => $redux_img .'align-right.png')
								),
				'default' => 'right'
			),
		)
	) );
}



// -> START Social Options
	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Social Networks', 'the8' ),
		'id'			=> 'social_options',
		'customizer_width' => '400px',
		'icon' => 'el el-icon-twitter',
		'fields'		=> array(
			array(
				'id'=>"social_group",
				'type' => 'group',//doesn't need to be called for callback fields
				'multi' => true,
				'title' => esc_html__('Social networks', 'the8' ),
				'groupname' => esc_html__('Social network', 'the8' ), // Group name
				'fields' =>
					array(
						array(
							'id' => 'title',
							'type' => 'text',
							'title' => esc_html__('Title', 'the8' ),
							'validate' => 'no_html',
							),
						array(
							'id'=>'soc-select-fa',
							'type' => 'select',
							'width' => '250px',
							'select2'  => array( 'containerCssClass' => 'fa' ),
							'class'	=> ' font-icons fa',
							'data' => 'fa-icons',
							'title' => esc_html__('Icon', 'the8' ),
							'options'  => $iconArray
							),
						array(
							'id' => 'soc-url',
							'type' => 'text',
							'title' => esc_html__('Url', 'the8' ),
							'validate' => 'url',
							),
						),
					),
			array(
				'id' => 'social_links_location',
				'title' => esc_html__( 'Icons Location', 'the8' ),
				'type' => 'select',
				'options' => array(
					'none' => esc_html__( 'None', 'the8' ),
					'top' => esc_html__( 'Top panel', 'the8' ),
					'bottom' => esc_html__( 'Copyrights area', 'the8' ),
					'top_bottom' => esc_html__( 'Top and Copyrights', 'the8' )
				),
				'width' => '250px',
				'default' => 'top_bottom'
			)
		)
	) );

if ( in_array( 'oauth-twitter-feed-for-developers/twitter-feed-for-developers.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) )  {

	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Twitter Feed', 'the8' ),
		'id'			=> 'twitter_setting',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id' => 'turn-twitter',
				'type' => 'switch',
					'title' => esc_html__('Enable Twitter Feed', 'the8' ),
				'default' => '0',
				'on' => 'On',
				'off' => 'Off',
			),
			array(
				'id' => 'tw-username',
				'type' => 'text',
				'required' => array('turn-twitter', '=', '1'),
				'title' => esc_html__( 'Twitter username', 'the8' ),
				'default' => ''
			),

			)
	) );
}else{
	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__('Twitter Feed', 'the8' ),
		'id'			=> 'twitter_setting',
		'subsection'	=> true,
		'customizer_width' => '450px',
		'fields'		=> array(
			array(
				'id'	=> 'twitter_active_plugin_msg',
				'type'  => 'info',
				'style' => 'info',
				'icon'  => 'el el-icon-link',
				'title' => esc_html__('oAuth Twitter Feed for Developers plugin needed!', 'the8' ),
				'desc'  => 'You need to install and activate <a href="https://wordpress.org/plugins/oauth-twitter-feed-for-developers/" target="_blank">oAuth Twitter Feed for Developers</a> plugin in order to display your twitter feed.',
			),

		)
	) );
}

Redux::setSection( $opt_name, array(
	'title'			=> esc_html__('Theme Manual', 'the8' ),
	'id'			=> 'the8-tuts',
	'customizer_width' => '450px',
	'icon' => 'el-icon-book',
	'fields' => array(
		array(
			'id' => 'help_online',
			'type' => 'info',
			'title' => esc_html__('Select tutorial', 'the8' ),
			'desc' => '<a class="button" href="http://the8.creaws.com/manual/" target="_blank"><i class="fa fa-life-bouy"></i>&nbsp;&nbsp;Online</a>&nbsp;<a class="button" href="https://www.youtube.com/user/cwsvideotuts/playlists" target="_blank"><i class="fa fa-video-camera"></i>&nbsp;&nbsp;Video</a>'
			)
		),
	) );



	/*
	* <--- END SECTIONS
	*/


	/*
	*
	* YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
	*
	*/

	/*
	*
	* --> Action hook examples
	*
	*/

	// If Redux is running as a plugin, this will remove the demo notice and links
	//add_action( 'redux/loaded', 'remove_demo' );

	// Function to test the compiler hook and demo CSS output.
	// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
	//add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

	// Change the arguments after they've been declared, but before the panel is created
	//add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

	// Change the default value of a field after it's been set, but before it's been useds
	//add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

	// Dynamically add a section. Can be also used to modify sections/fields
	//add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

	/**
	* This is a test function that will let you see when the compiler hook occurs.
	* It only runs if a field	set with compiler=>true is changed.
	* */
	if ( ! function_exists( 'compiler_action' ) ) {
		function compiler_action( $options, $css, $changed_values ) {
			echo '<h1>The compiler hook has run!</h1>';
			echo "<pre>";
			print_r( $changed_values ); // Values that have changed since the last save
			echo "</pre>";
			//print_r($options); //Option values
			//print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
		}
	}

	/**
	* Custom function for the callback validation referenced above
	* */
	if ( ! function_exists( 'redux_validate_callback_function' ) ) {
		function redux_validate_callback_function( $field, $value, $existing_value ) {
			$error   = false;
			$warning = false;

			//do your validation
			if ( $value == 1 ) {
				$error = true;
				$value = $existing_value;
			} elseif ( $value == 2 ) {
				$warning = true;
				$value   = $existing_value;
			}

			$return['value'] = $value;

			if ( $error == true ) {
				$return['error'] = $field;
				$field['msg']	= 'your custom error message';
			}

			if ( $warning == true ) {
				$return['warning'] = $field;
				$field['msg']	= 'your custom warning message';
			}

			return $return;
		}
	}

	/**
	* Custom function for the callback referenced above
	*/
	if ( ! function_exists( 'redux_my_custom_field' ) ) {
		function redux_my_custom_field( $field, $value ) {
			print_r( $field );
			echo '<br/>';
			print_r( $value );
		}
	}

	/**
	* Custom function for filtering the sections array. Good for child themes to override or add to the sections.
	* Simply include this function in the child themes functions.php file.
	* NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
	* so you must use get_template_directory_uri() if you want to use any of the built in icons
	* */
	if ( ! function_exists( 'dynamic_section' ) ) {
		function dynamic_section( $sections ) {
			//$sections = array();
			$sections[] = array(
				'title'  => esc_html__( 'Section via hook', 'redux-framework-demo' ),
				'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
				'icon'   => 'el el-paper-clip',
				// Leave this as a blank section, no options just some intro text set above.
				'fields' => array()
			);

			return $sections;
		}
	}

	/**
	* Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
	* */
	if ( ! function_exists( 'change_arguments' ) ) {
		function change_arguments( $args ) {
			//$args['dev_mode'] = true;

			return $args;
		}
	}

	/**
	* Filter hook for filtering the default value of any given field. Very useful in development mode.
	* */
	if ( ! function_exists( 'change_defaults' ) ) {
		function change_defaults( $defaults ) {
			$defaults['str_replace'] = 'Testing filter hook!';

			return $defaults;
		}
	}

	/**
	* Removes the demo link and the notice of integrated demo from the redux-framework plugin
	*/
	if ( ! function_exists( 'remove_demo' ) ) {
		function remove_demo() {
			// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
			if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
				remove_filter( 'plugin_row_meta', array(
					ReduxFrameworkPlugin::instance(),
					'plugin_metalinks'
				), null, 2 );

				// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
				remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
			}
		}
	}
