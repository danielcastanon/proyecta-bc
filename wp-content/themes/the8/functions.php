<?php

# CONSTANTS


define('THE8_URI', get_template_directory_uri());
define('THE8_BEFORE_CE_TITLE', '<div class="ce_title">');
define('THE8_AFTER_CE_TITLE', '</div>');
define('THE8_V_SEP', '<span class="v_sep"></span>');
define('THE8_MB_PAGE_LAYOUT_KEY', 'cws_mb_post');
define('THE8_COLOR', '#28a6ec');
define('THE8_FOOTER_COLOR', '#1a1a1a');
define('THE8_SECONDARY_COLOR', '#037cff');
define('THE8_SLUG', 'the8');

# \CONSTANTS

# TEXT DOMAIN

load_theme_textdomain( 'the8', get_template_directory() .'/languages' );

# \TEXT DOMAIN

# REDUX FRAMEWORK

require_once (get_template_directory() . '/framework/rc/framework.php');
require_once (get_template_directory() . '/framework/config.php');

# \REDUX FRAMEWORKr

# INCLUDE MODULES

# \INCLUDE MODULES

# PAGEBUILDER
$cws_theme_funcs = new The8_Funcs();

// CWS PB settings
class The8_Funcs {
	public function __construct() {
		$this->init();
	}

	public function wp_title_filter ( $title_text ) {
		$site_name = get_bloginfo( 'name' );
		$site_tagline = get_bloginfo( 'description' );
		if ( is_home() ) {
			$title_text = $site_name . " | " . $site_tagline;
		}
		else{
			$title_text .= $site_name;
		}
		return $title_text;
	}

	# UPDATE THEME
	public function check_for_update($transient) {
		if (empty($transient->checked)) { return $transient; }

		$theme_pc = trim(the8_get_option('_theme_purchase_code'));
		if (empty($theme_pc)) {
			add_action( 'admin_notices', array($this, 'cws_an_purchase_code') );
		}

		$result = wp_remote_get('http://up.creaws.com/products-updater.php?pc=' . $theme_pc . '&tname=' . 'the8');
		if (!is_wp_error( $result ) ) {
			if (200 == $result['response']['code'] && 0 != strlen($result['body']) ) {
				$resp = json_decode($result['body'], true);
				$h = isset( $resp['h'] ) ? (float) $resp['h'] : 0;
				$theme = wp_get_theme(get_template());
				if ( version_compare( $theme->get('Version'), $resp['new_version'], '<' ) ) {
					$transient->response['the8'] = $resp;
				}
			}
			else{
				unset($transient->response['the8']);
			}
		}
		return $transient;
	}

	// an stands for admin notice
	public function cws_an_purchase_code() {
		$cws_theme = wp_get_theme();
		echo "<div class='update-nag'>" . $cws_theme->get('Name') . esc_html__(' theme notice: Please insert your Item Purchase Code in Theme Options to get the latest theme updates!', 'the8') ."</div>";
	}
	# \UPDATE THEME

	public function fix_shortcodes_autop($content){
		$array = array (
			'<p>[' => '[',
			']</p>' => ']',
			']<br />' => ']'
		);

		$content = strtr($content, $array);
		return $content;
	}

	private function init() {
		include_once get_template_directory() . '/pbf.php';
		require_once get_template_directory() . '/core/plugins.php';

		// metaboxes
		require_once(get_template_directory() . '/core/bfi_thumb.php');
		include_once(get_template_directory() . '/core/breadcrumbs.php');
		include_once(get_template_directory() . '/core/shortcodes.php');
		require_once(get_template_directory() . '/core/metaboxes.php' );

		set_transient('update_themes', 24*3600);

		add_action('after_setup_theme', array($this, 'cws_after_setup_theme') );
		add_filter('wp_title', array($this, 'wp_title_filter') );
		add_filter('pre_set_site_transient_update_themes', array($this, 'check_for_update') );
		add_filter('the_content', array($this, 'fix_shortcodes_autop') );
		add_action('admin_enqueue_scripts', array($this, 'cws_admin_init' ) );
		add_action('wp_enqueue_scripts', array($this, 'cws_theme_enqueue_scripts') );
		add_action('wp_enqueue_scripts', array($this, 'cws_theme_standart_script') );
		add_action('wp_enqueue_scripts', array($this, 'cws_theme_youtube_api_init') );
		add_action('wp_enqueue_scripts', array($this, 'cws_theme_enqueue_styles') );
		add_action('wp_enqueue_scripts', array($this, 'cws_enqueue_theme_stylesheet'), 999 );
		add_action('widgets_init', array($this, 'cws_widgets_init') );
		add_filter('body_class', array($this, 'cws_layout_class') );

		add_action('menu_font_hook', array($this, 'cws_menu_font_action') );
		add_action('header_font_hook', array($this, 'cws_header_font_action') );
		add_action('body_font_hook', array($this, 'cws_body_font_action') );

		add_action('theme_color_hook', array($this, 'cws_theme_color_action') );
		add_action('theme_color_hook', array($this, 'cws_page_title_custom_color_action') );
		add_action('theme_gradient_hook', array($this, 'cws_theme_gradient_action') );
		add_filter('body_class', array($this, 'cws_gradients_body_class') );
		add_filter('cws_dbl_to_sngl_quotes', array($this, 'cws_dbl_to_sngl_quotes') );

		add_action('wp_enqueue_scripts', array($this, 'js_vars_init') );
		add_action('wp', array($this, 'cws_page_meta_vars') );
		add_action('template_redirect', array($this, 'cws_ajax_redirect') );
		add_filter('excerpt_length', array($this, 'cws_custom_excerpt_length'), 999 );
		add_action('wp_head', array($this, 'cws_ajaxurl') );
		add_filter('embed_oembed_html', array($this, 'cws_oembed_wrapper'),10,3);
		add_filter('body_class', array($this, 'cws_loading_body_class') );

	}

	public function cws_theme_youtube_api_init (){
		?>
		<script type="text/javascript">
			// Loads the IFrame Player API code asynchronously.
				var tag = document.createElement("script");
				tag.src = "https://www.youtube.com/player_api";
				var firstScriptTag = document.getElementsByTagName("script")[0];
				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		</script>
		<?php
	}

	public function cws_dbl_to_sngl_quotes ( $content ) {
		return preg_replace( "|\"|", "'", $content );
	}

	public function cws_loading_body_class ( $classes ) {
		$classes[] = 'the8-new-layout';
		return $classes;
	}

	public function cws_oembed_wrapper( $html, $url, $args ) {
		return !empty( $html ) ? "<div class='cws_oembed_wrapper'>$html</div>" : '';
	}

	public function cws_custom_excerpt_length( $length ) {
		return 1400;
	}

	public function cws_ajaxurl() {
?>
<script type="text/javascript">var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';</script>
<?php
	}

	public function cws_ajax_redirect() {
		$ajax = isset( $_POST['ajax'] ) ? (bool)$_POST['ajax'] : false;
		if ( $ajax ) {
			$template = isset( $_POST['template'] ) ? $_POST['template'] : '';
			if ( !empty( $template ) ) {
				if ( strpos( $template, '-' ) ) {
					$template_parts = explode( '-', $template );
					if ( count( $template_parts ) == 2 ) {
						get_template_part( $template_parts[0], $template_parts[1] );
					}
					else {
						return;
					}
				}	else {
					get_template_part( $template );
				}
				exit();
			}
		}
		return;
	}

	public function cws_page_meta_vars() {
		if ( is_page() ) {
			$pid = get_query_var('page_id');
			$pid = !empty($pid) ? $pid : get_queried_object_id();
			$post = get_post( $pid );
			$cws_stored_meta = get_post_meta( $pid, THE8_MB_PAGE_LAYOUT_KEY );
			$cws_stored_meta = isset( $cws_stored_meta[0] ) && !empty( $cws_stored_meta[0] ) ? $cws_stored_meta[0] : array();
			$GLOBALS['the8_page_meta'] = array();
			$GLOBALS['the8_page_meta']['sb'] = the8_get_sidebars($pid);
			$GLOBALS['the8_page_meta']['is_blog'] = isset( $cws_stored_meta['is_blog'] ) ? $cws_stored_meta['is_blog'] === '1' : ( is_home() ? true : false );
			$GLOBALS['the8_page_meta']['sb_hide_title_area'] = isset( $cws_stored_meta['sb_hide_title_area'] ) ? $cws_stored_meta['sb_hide_title_area'] === '1' : false;
			$GLOBALS['the8_page_meta']['blog'] = array( 'blogtype' => isset( $cws_stored_meta['blogtype'] ) ? $cws_stored_meta['blogtype'] : 'large',
				'cats' => isset( $cws_stored_meta['category'] ) ? implode( $cws_stored_meta['category'], ',' ) : ( is_home() ? the8_get_option('def-home-category') : '' ) );
			$GLOBALS['the8_page_meta']['footer'] = array( 'footer_sb_top' => '', 'footer_sb_bottom' => '' );
			if ( (!is_404()) && (!empty($post)) ) {
				if (isset( $cws_stored_meta['sb_foot_override'] ) && $cws_stored_meta['sb_foot_override'] === '1') {
					$GLOBALS['the8_page_meta']['footer']['footer_sb_top'] = isset( $cws_stored_meta['footer-sidebar-top'] ) ? $cws_stored_meta['footer-sidebar-top'] : '';
				} else {
					$GLOBALS['the8_page_meta']['footer']['footer_sb_top'] = the8_get_option('footer-sidebar-top');
				}
			} else {
				$GLOBALS['the8_page_meta']['footer']['footer_sb_top'] = the8_get_option('footer-sidebar-top');
			}
			$GLOBALS['the8_page_meta']['slider'] = array( 'slider_override' => '', 'slider_shortcode' => '' );
			$GLOBALS['the8_page_meta']['slider']['slider_override'] = isset($cws_stored_meta['sb_slider_override']) && $cws_stored_meta['sb_slider_override'] === '1' ? $cws_stored_meta['sb_slider_override'] : false;
			$GLOBALS['the8_page_meta']['slider']['slider_options'] = isset($cws_stored_meta['slider_shortcode']) && $cws_stored_meta['sb_slider_override'] === '1' ? $cws_stored_meta['slider_shortcode'] : '';
			return true;
		} else {
			return false;
		}
	}

	/************** JAVASCRIPT VARIABLES INIT **************/

	public function js_vars_init() {
		$is_user_logged = is_user_logged_in();
		$stick_menu = the8_get_option('menu-stick');
		$use_blur = the8_get_option('use_blur');
		$logged_var = $is_user_logged ? 'true' : 'false';
		?>
		<script type="text/javascript">
			var is_user_logged = <?php echo esc_js($logged_var)?>;
			var stick_menu = true;
			var use_blur = false;
		</script>
		<?php
	}

	/************** \JAVASCRIPT VARIABLES INIT **************/

	/******************** TYPOGRAPHY ********************/
	// MENU FONT HOOK

	private function cws_print_font_css($font_array) {
		$out = '';
		foreach ($font_array as $style=>$v) {
			if ($style != 'font-options' && $style != 'google' && $style != 'subsets' && $style != 'font-backup') {
				$out .= !empty($v) ? $style .':'.$v.';' : '';
			}
		}
		return $out;
	}

	private function cws_print_menu_font() {
		ob_start();
		do_action( 'menu_font_hook' );
		return ob_get_clean();
	}

	public function cws_menu_font_action() {
		$out = '';
		$font_array = the8_get_option('menu-font');
		if (isset($font_array)) {
			$out .= '.main-nav-container .menu-item a,
				.main-nav-container .menu-item .button_open,
				.mobile_menu_header{'. $this->cws_print_font_css($font_array) . '}';
			$out .= '.main-menu .search_menu{
						font-size : '. $font_array["font-size"] . ';
					}';
		}

		$slider_settings = the8_get_page_meta_var( "slider" );
		$cws_is_header_hovers_slider = (the8_get_option('show_header_outside_slider') === "1") ? true : false;
		$cws_is_custom_headers = (the8_get_option('customize_headers') === "1") ? true : false;
		$independent_headers = the8_get_option( 'independent_headers' );		
		if (( $cws_is_header_hovers_slider && (is_front_page() || (( isset( $slider_settings['slider_override'] ) && $slider_settings['slider_override'] ) && (!empty( $slider_settings['slider_options']) ) ) )) || (!(is_front_page() || is_single()) && is_page() && $cws_is_header_hovers_slider && $cws_is_custom_headers && ((($independent_headers == 1 ) && has_post_thumbnail()) || $independent_headers == 0 )) ) {
			$out .= '.header_wrapper_container .header_nav_part:not(.mobile_nav) .main-nav-container > .main-menu > .menu-item:not(.current-menu-ancestor):not(:hover):not(.current-menu-item) > a,
		.header_wrapper_container .mini-cart,
		.header_wrapper_container .site_header .search_menu{
						color : '. the8_get_option('header_outside_slider_font_color') . ';
					}';
		}
		echo preg_replace('/\s+/',' ', $out);
	}

	// \MENU FONT HOOK

	// HEADER FONT HOOK

	private function cws_print_header_font () {
		ob_start();
		do_action( 'header_font_hook' );
		return ob_get_clean();
	}

	public function cws_header_font_action () {
		$out = '';
		$font_array = the8_get_option('header-font');
		if (isset($font_array)) {
			$out .= '.ce_title,
				.comments-area .comment-reply-title,
				.the8-new-layout .cws-widget .widget-title,
				.woocommerce div[class^="post-"] h1.product_title.entry-title{'. $this->cws_print_font_css($font_array) . '}';
			$out .= '.testimonial .author figcaption,
				.testimonial .quote .quote_link:hover,
				.news .post_tags>*,
				.news .post_categories>*,
				.pagination a,
				.widget-title,
				a:hover,
				.ce_toggle.alt .accordion_title:hover,
				.cws_portfolio_items .item .title_part,
				.pricing_table_column .price_section,
				.cws_callout .callout_title,
				.comments-area .comments_title,
				.comments-area .comment-meta,
				.comments-area .comment-reply-title,
				.comments-area .comment-respond .comment-form input:not([type=\'submit\']),
				.comments-area .comment-respond .comment-form textarea,
				.page_title .bread-crumbs,
				.benefits_container .cws_textwidget_content .link a:hover,
				.cws_portfolio_fw .title,
				.cws_portfolio_fw .cats a:hover,
				.ourteam_item_wrapper .title,
				.ourteam_item_wrapper .title a,
				.msg_404,
				.the8-new-layout .news .item .post_info
					{color:' . $font_array['color'] . ';}';
		}
		echo preg_replace('/\s+/',' ', $out);
	}

	// \HEADER FONT HOOK

	// BODY FONT HOOK

	private function cws_print_body_font () {
		ob_start();
		do_action( 'body_font_hook' );
		return ob_get_clean();
	}

	public function cws_body_font_action () {
		$out = '';
		$font_array = the8_get_option('body-font');
		if (isset($font_array)) {
			$out .= 'body
						{'. $this->cws_print_font_css($font_array) . '}';
			$out .= '.cws-widget ul li>a,
					.comments-area .comments_nav.carousel_nav_panel a,
					.cws_img_navigation.carousel_nav_panel a,
					.cws_portfolio_fw .cats a,
					.row_bg .ce_accordion.alt .accordion_title,
					.row_bg .ce_toggle .accordion_title,
					.mini-cart .woo_mini_cart
						{color:' . $font_array['color'] . ';}';
			$out .= '.mini-cart .woo_mini_cart,
					body input,body  textarea
						{font-size:' . $font_array['font-size'] . ';}';
			$out .= 'body input,body  textarea
						{line-height:' . $font_array['line-height'] . ';}';
			$out .= 'abbr
						{border-bottom-color:' . $font_array['color'] . ';}';
			$fs_match = preg_match( '#(\d+)(.*)#', $font_array['font-size'], $fs_matches );
			$lh_match = preg_match( '#(\d+)(.*)#', $font_array['line-height'], $lh_matches );
			if ( $fs_match && $lh_match ) {
				$fs_number = (int)$fs_matches[1];
				$fs_units = $fs_matches[2];
				$lh_number = (int)$lh_matches[1];
				$lh_units = $lh_matches[2];
				$out .= ".dropcap{font-size:" . $fs_number * 2 . "$fs_units;line-height:" . $lh_number * 2 . "$lh_units;width:" . $lh_number * 2 . "$lh_units;";
			}
		}
		echo preg_replace('/\s+/',' ', $out);
	}

	// \BODY FONT HOOK

	public function cws_process_fonts() {
		$out = '<style type="text/css" id="cws-custom-fonts-css">';
		$out .= $this->cws_print_menu_font();
		$out .= $this->cws_print_header_font();
		$out .= $this->cws_print_body_font();
		$out .= '</style>';
		echo $out;
	}

	public function cws_process_blur() {
		$blur_intensity = the8_get_option('blur_intensity');
		$use_blur = the8_get_option('use_blur');
		$use_blur = isset($use_blur) && !empty($use_blur) && ($use_blur == '1') ? true : false;
		if (!$use_blur) return;
		$out = '<style type="text/css" id="cws-custom-blur-css">';
		$out .= '.pic.blured img.blured-img,
				.item .pic_alt .img_cont>img.blured-img,
				.pic .img_cont>img.blured-img,
				.cws-widget .post_item .post_thumb:hover img,
				.cws_img_frame:hover img,
				.cws-widget .portfolio_item_thumb .pic .blured-img{
					-webkit-filter: blur('.$blur_intensity.'px);
				    -moz-filter: blur('.$blur_intensity.'px);
				    -o-filter: blur('.$blur_intensity.'px);
				    -ms-filter: blur('.$blur_intensity.'px);
				    filter: blur('.$blur_intensity.'px);
				}';
		$out .= '</style>';
		echo $out;
	}

	/******************** \TYPOGRAPHY ********************/

	public function cws_layout_class ($classes=array()) {
		$boxed_layout = the8_get_option('boxed-layout');
		if ( $boxed_layout=='0' ) {
			array_push( $classes, 'wide' );
		}
		return $classes;
	}

	public function cws_widgets_init() {
		$sidebars = the8_get_option('sidebars');
		if (!empty($sidebars) && function_exists('register_sidebars')) {
			foreach ($sidebars as $sb) {
				if ($sb) {
					register_sidebar( array(
						'name' => $sb,
						'id' => strtolower(preg_replace("/[^a-z0-9\-]+/i", "_", $sb)),
						'before_widget' => '<div class="cws-widget">',
						'after_widget' => '</div>',
						'before_title' => '<div class="widget-title"><span>',
						'after_title' => '</span></div>',
						));
				}
			}
		}
	}

	public function cws_theme_enqueue_styles() {
		if((is_admin() && !is_shortcode_preview()) || 'wp-login.php' == basename($_SERVER['PHP_SELF'])) {
			return;
		}

		$styles =	array(
			'cws_loader' => 'cws_loader.css',
			'font-awesome' => 'font-awesome.css',
			'fancybox' => 'jquery.fancybox.css',
			'odometer' => 'odometer-theme-default.css',
			'select2' => 'select2.css',
			'animate' => 'animate.css'
		);

		foreach($styles as $key=>$sc){
			wp_enqueue_style( $key, THE8_URI . '/css/' . $sc);
		}

		$cwsfi = get_option('cwsfi');
		if (!empty($cwsfi) && isset($cwsfi['css'])) { 
			wp_enqueue_style( 'flaticon', $cwsfi['css'] );
		}else{
			wp_enqueue_style( 'flaticon', THE8_URI . '/fonts/flaticon/flaticon.css' );
		};

		wp_enqueue_style( 'cws-iconpack', THE8_URI . '/fonts/cws-iconpack/flaticon.css' );

		$is_custom_color = the8_get_option('is-custom-color');
		if ($is_custom_color != '1') {
			$style = the8_get_option('stylesheet');
			if (!empty($style)) {
				wp_enqueue_style( 'style-color', THE8_URI . '/css/' . $style . '.css' );
			}
		}

		wp_enqueue_style( 'main', THE8_URI . '/css/main.css' );
	}

	public function cws_enqueue_theme_stylesheet () {
		wp_enqueue_style( 'style', get_stylesheet_uri() );
	}

	public function cws_theme_enqueue_scripts() {
		$footer_scripts = array (
				'owl_carousel' => 'owl.carousel.js',
				'isotope' => 'isotope.pkgd.min.js',
				'odometer' => 'odometer.js',
				'wow' => 'wow.min.js',
				'cws_parallax' => 'cws_parallax.js',
				'parallax' => 'parallax.js',
				'cws_YT_bg' => 'cws_YT_bg.js',
				'cws_self&vimeo_bg' => 'cws_self&vimeo_bg.js',
				'vimeo' => 'jquery.vimeo.api.min.js'
				);

		foreach ($footer_scripts as $alias => $src) {
			wp_register_script ($alias, THE8_URI . "/js/$src", array(), "1.0", true);
		}

	}

	public function cws_theme_standart_script(){
		$scripts = array (
				'retina' => 'retina_1.3.0.js',
				'fancybox' => 'jquery.fancybox.js',
				'select2' => 'select2.js',
				'img_loaded' => 'imagesloaded.pkgd.min.js',
				'main' => 'scripts.js'
				);

		if ( '0' != the8_get_option('enable_mob_menu') ) {
			wp_enqueue_script ('modernizr', THE8_URI . "/js/modernizr.js", array(), "1.0", true);
		}

		wp_enqueue_script( 'tweenmax', THE8_URI . "/js/TweenMax.min.js", array( "jquery" ), "1.0", false );
		wp_enqueue_script( 'cws_loader', THE8_URI . "/js/cws_loader.js", array( "jquery", "tweenmax" ), "1.0", false );
		wp_enqueue_script( 'yt_player_api', 'https://www.youtube.com/player_api', array(), '1.0', true );

		foreach ($scripts as $alias => $src) {
			wp_enqueue_script ($alias, THE8_URI . "/js/$src", array(), "1.0", true);
		}

	}

	public function cws_admin_init( $hook ) {
		wp_enqueue_style('admin-css', THE8_URI . '/core/css/mb-post-styles.css' );
		wp_enqueue_style('wp-color-picker');
		wp_enqueue_script('the8-metaboxes-js', get_template_directory_uri() . '/core/js/metaboxes.js', array('jquery') );
		wp_enqueue_style('the8-metaboxes-css', get_template_directory_uri() . '/core/css/metaboxes.css', false, '2.0.0' );
		wp_enqueue_script('custom-admin', THE8_URI . '/core/js/custom-admin.js', array( 'jquery' ) );

		$cwsfi = get_option('cwsfi');
		if (!empty($cwsfi) && isset($cwsfi['css'])) { 
			wp_enqueue_style( 'flaticon', $cwsfi['css'] );
		}else{
			wp_enqueue_style( 'flaticon', THE8_URI . '/fonts/flaticon/flaticon.css' );
		};

		if (('toplevel_page_The8' == $hook) || ('toplevel_page_The8ChildTheme' == $hook)) {
			wp_enqueue_style( 'cws-redux-style' , THE8_URI . '/core/css/cws-redux-style.css' );
		}
	}

	private function cws_register_widgets( $cws_widgets ) {
		foreach ($cws_widgets as $w) {
			require_once (get_template_directory() . '/core/widgets/' . strtolower($w) . '.php');
			register_widget($w);
		}
	}

	public function cws_after_setup_theme() {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support(' widgets ');
		add_theme_support( 'title-tag' );

		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
		add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );

		register_nav_menu( 'header-menu', esc_html__( 'Navigation Menu', 'the8' ) );
		register_nav_menu( 'sidebar-menu', esc_html__( 'SideBar Menu', 'the8' ) );
		add_theme_support( 'woocommerce' );
		add_theme_support( 'custom-background', array('default-color' => '616262') );

		$this->cws_register_widgets( array(
			'CWS_Text',
			'CWS_Latest_Posts',
			'CWS_Portfolio',
			'CWS_Twitter'
		) );

		$user = wp_get_current_user();
		$user_nav_adv_options = get_user_option( 'managenav-menuscolumnshidden', get_current_user_id() );
		if ( is_array($user_nav_adv_options) ) {
			$css_key = array_search('css-classes', $user_nav_adv_options);
			if (false !== $css_key) {
				unset($user_nav_adv_options[$css_key]);
				update_user_option($user->ID, 'managenav-menuscolumnshidden', $user_nav_adv_options,	true);
			}
		}
	}

	// THEME COLOR HOOK

	private function cws_print_theme_color() {
		ob_start();
		do_action( 'theme_color_hook' );
		return ob_get_clean();
	}

	public function cws_theme_color_action() {
		$out = '';
		$theme_color = the8_get_option('theme-custom-color');
		if (isset($theme_color)) {
			global $wp_filesystem;
			if( empty( $wp_filesystem ) ) {
				require_once( ABSPATH .'/wp-admin/includes/file.php' );
				WP_Filesystem();
			}
			$file = get_template_directory() . '/css/theme-color.css';
			if ( $wp_filesystem->exists($file) ) {
				$file = $wp_filesystem->get_contents( $file );
				$new_css = preg_replace('|#[^\s]+#|', $theme_color, $file);
				$out .= $new_css;
			}
		}
		echo preg_replace('/\s+/',' ', $out);
	}

	public function cws_page_title_custom_color_action () {
		$header_bg_settings = the8_get_option( "header_bg_settings" );
		$header_bg_settings = isset( $header_bg_settings["@"] ) ? $header_bg_settings["@"] : array();
		$hex_color = isset( $header_bg_settings['font_color'] ) ? $header_bg_settings['font_color'] : '';
		$rgb_color = '';
		$lighter_rgba_color = '';
		if ( !empty( $hex_color ) ) {
			$rgb_color = $this->cws_Hex2RGB( $hex_color );
			$rgb_color = esc_attr($rgb_color);
			$lighter_rgba_color = $rgb_color . ",0.2";
			$lighter_rgba_color = esc_attr($lighter_rgba_color);
			echo ".page_title.customized{\ncolor: $hex_color;\n}\n.page_title.customized .bread-crumbs{\nbackground-color: rgba($lighter_rgba_color);\n}";
		}
	}

	private function cws_print_theme_gradient () {
		ob_start();
		do_action( 'theme_gradient_hook' );
		return ob_get_clean();
	}

	public function cws_theme_gradient_action () {
		$out = '';
		$use_gradients = the8_get_option('use_gradients');
		if ( $use_gradients ) {
			$gradient_settings = the8_get_option( 'gradient_settings' );
			require_once( get_template_directory() . "/css/gradient_selectors.php" );
			if ( function_exists( "get_gradient_selectors" ) ) {
				$gradient_selectors = get_gradient_selectors();
				$out .= the8_render_gradient_rules( array(
					'settings' => $gradient_settings,
					'selectors' => $gradient_selectors,
					'use_extra_rules' => true
				));
			}
		}
		echo preg_replace('/\s+/',' ', $out);
	}

	public function cws_gradients_body_class ( $classes ) {
		$use_gradients = the8_get_option('use_gradients');
		if ( $use_gradients ) {
			$classes[] = "cws_gradients";
		}
		return $classes;
	}

	public function cws_process_colors() {
		$out = '<style type="text/css" id="cws-custom-colors-css">';
		$out .= $this->cws_print_theme_color();
		$out .= $this->cws_print_theme_gradient();
		$out .= '</style>';
		echo preg_replace('/\s+/',' ', $out);
	}

	public function cws_Hex2RGB($hex) {
		$hex = str_replace('#', '', $hex);
		$color = '';

		if(strlen($hex) == 3) {
			$color = hexdec(mb_substr($hex, 0, 1)) . ',';
			$color .= hexdec(mb_substr($hex, 1, 1)) . ',';
			$color .= hexdec(mb_substr($hex, 2, 1));
		}
		else if(strlen($hex) == 6) {
			$color = hexdec(mb_substr($hex, 0, 2)) . ',';
			$color .= hexdec(mb_substr($hex, 2, 2)) . ',';
			$color .= hexdec(mb_substr($hex, 4, 2));
		}
		return $color;
	}

	// \  COLOR HOOK

}

/* End of Theme's Class */

/* THE HEADER META */

if(!function_exists('the8_theme_header_meta')) {
    /**
     * Function that echoes meta data if our seo is enabled
     */
    function the8_theme_header_meta() {
    	?>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    	<?php
	}
    add_action('the8_theme_header_meta', 'the8_theme_header_meta');
}

if (!function_exists('the8_theme_header_process_fonts')) {
	function the8_theme_header_process_fonts (){
		global $cws_theme_funcs;
		$cws_theme_funcs->cws_process_fonts();
	}
	add_action('the8_theme_header_meta', 'the8_theme_header_process_fonts');
}

if (!function_exists('the8_theme_header_process_colors')) {
	function the8_theme_header_process_colors (){
		global $cws_theme_funcs;
		$cws_theme_funcs->cws_process_colors();
	}
	add_action('the8_theme_header_meta', 'the8_theme_header_process_colors');
}

if (!function_exists('the8_theme_header_process_blur')) {
	function the8_theme_header_process_blur (){
		global $cws_theme_funcs;
		$cws_theme_funcs->cws_process_blur();
	}
	add_action('the8_theme_header_meta', 'the8_theme_header_process_blur');
}

/* END THE HEADER META */

/* HEDER LOADER */
if (!function_exists('the8_page_loader')) {
	function the8_page_loader (){
		echo '<div id="cws_page_loader_container" class="cws_loader_container">
			<div id="cws_page_loader" class="cws_loader"></div>
		</div>';
	}
}
/* END HEDER LOADER */

/* THEME HEADER */
if (!function_exists('the8_page_header')) {
	function the8_page_header (){
		$stick_menu = the8_get_option( 'menu-stick' );
		$stick_menu =  $stick_menu == 1 ? true : false;
		$use_blur = the8_get_option( 'use_blur' );
		$custom_header_bg_color = false;
		$customize_headers = the8_get_option( 'customize_headers' );
		$independent_headers = the8_get_option( 'independent_headers' );
		$header_bg_settings = the8_get_option( "header_bg_settings" );
		$use_gradient = the8_get_option( "use_gradient" );
		$bg_color_color = the8_get_option( "bg_color_color" );

		$bg_img = the8_get_option( "bg_img" );

		$bg_color_gradient_settings = array(
			'@' => array(
				'first_color' => the8_get_option( 'first_color' ),
				'second_color' => the8_get_option( 'second_color' ),
				'type' => the8_get_option( 'type' ),
				'linear_settings' => the8_get_option( 'linear_settings' ),
				'radial_settings' => array(
					'@' => array(
						'shape_settings' => the8_get_option('shape_settings'),
						'shape' => the8_get_option('shape'),
						'size_keyword' => the8_get_option('size_keyword'),
						'size' => the8_get_option('size')
						)
					)
				)
		);

		$parallax_options = array(
			'@' => array(
				'parallaxify' => the8_get_option("parallaxify"),
				'scalar_x' => the8_get_option("scalar_x"),
				'scalar_y' => the8_get_option("scalar_y"),
				'limit_x' => the8_get_option("limit_x"),
				'limit_y' => the8_get_option("limit_y")
		));


		$parallax_options = isset( $parallax_options['@'] ) ? $parallax_options['@'] : array();
		$img_section_atts = "";
		$img_section_class = "header_bg_img";
		$img_section_styles = "";

		$img_section_atts .= !empty( $img_section_class ) ? " class='$img_section_class'" : "";
		$img_section_atts .= !empty( $img_section_styles ) ? " style='$img_section_styles'" : "";
		$parallax_section_atts = "";
		$parallax_section_class = "cws_parallax_section";
		wp_enqueue_script ('parallax');
		$parallax_section_styles = "";
		$parallax_section_styles .= isset( $parallax_options['limit_x'] ) && !empty( $parallax_options['limit_x'] ) ? "margin-left:-" . $parallax_options['limit_x'] . "px;margin-right:-" . $parallax_options['limit_x'] . "px;" : "";
		$parallax_section_styles .= isset( $parallax_options['limit_y'] ) && !empty( $parallax_options['limit_y'] ) ? "margin-top:-" . $parallax_options['limit_y'] . "px;margin-bottom:-" . $parallax_options['limit_y'] . "px;" : "";
		$parallax_section_atts .= !empty( $parallax_section_class ) ? " class='$parallax_section_class'" : "";
		$parallax_section_atts .= isset( $parallax_options['scalar_x'] ) && !empty( $parallax_options['scalar_x'] ) ? " data-scalar-x='" . $parallax_options['scalar_x'] . "'" : "";
		$parallax_section_atts .= isset( $parallax_options['scalar_y'] ) && !empty( $parallax_options['scalar_y'] ) ? " data-scalar-y='" . $parallax_options['scalar_y'] . "'" : "";
		$parallax_section_atts .= isset( $parallax_options['limit_x'] ) && !empty( $parallax_options['limit_x'] ) ? " data-limit-x='" . $parallax_options['limit_x'] . "'" : "";
		$parallax_section_atts .= isset( $parallax_options['limit_y'] ) && !empty( $parallax_options['limit_y'] ) ? " data-limit-y='" . $parallax_options['limit_y'] . "'" : "";
		$parallax_section_atts .= !empty( $parallax_section_styles ) ? " style='$parallax_section_styles'" : "";

		$show_header_outside_slider = the8_get_option( 'show_header_outside_slider');

		$show_page_title = true;
		/***** Boxed Layout *****/
		$boxed_layout = ('0' != the8_get_option('boxed-layout') ) ? 'boxed' : '';
		echo $boxed_layout ? '<div class="page_boxed">' : '';
		/***** \Boxed Layout *****/

		ob_start();
			$social_links_location = the8_get_option( 'social_links_location' );
			$top_panel_text = the8_get_option( 'top_panel_text' );
			$social_toggle = the8_get_option('toggle-share-icon');

			$top_panel_switcher = the8_get_option( 'top_panel_switcher');

			$social_links = "";
			$show_wpml_header = the8_is_wpml_active() ? true : false;

			if (the8_get_option('woo_cart_place') == 'top') {
				ob_start();
					if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
						woocommerce_mini_cart();
					}
				$woo_mini_cart = ob_get_clean();

				ob_start();
					if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
						?>
							<a class="woo_icon" href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" title="<?php esc_html_e( 'View your shopping cart', 'the8' ); ?>"><i class='woo_mini-count cwsicon-shopping-carts6'><?php echo ((WC()->cart->cart_contents_count > 0) ?  '<span>' . esc_html( WC()->cart->cart_contents_count ) .'</span>' : '') ?></i></a>
							<?php
					}
				$woo_mini_icon = ob_get_clean();
			}

			if ( in_array( $social_links_location, array( 'top', 'top_bottom' ) ) ){
				$social_links = the8_render_social_links();
			}
			if ( (!empty( $social_links ) || !empty( $top_panel_text ))  && $top_panel_switcher == 1){
				echo "<div id='site_top_panel'>";
					echo "<div class='container'>";
						echo "<div class='row_text_search'>";
							echo ! empty( $top_panel_text ) ? "<div id='top_panel_text'>$top_panel_text</div>" : '';
							if(the8_get_option('search_place') == 'top'){
								get_search_form();
							}
						echo '</div>';
						if ( !empty( $social_links ) || the8_get_option('search_place') == 'top' || $show_wpml_header){
							echo "<div id='top_panel_links'>";
								if(the8_get_option('search_place') == 'top'){
									echo "<div class='search_icon'></div>";
								}
								echo !empty( $social_links ) ? "<div id='top_social_links_wrapper' class='".($social_toggle == 1 ? 'toggle-on' : 'toggle-off')."'>$social_links</div>" : "";
								if (the8_get_option('woo_cart_place') == 'top') {
									echo is_plugin_active( 'woocommerce/woocommerce.php' ) ? "<div class='mini-cart'>$woo_mini_icon$woo_mini_cart</div>" : '';
								}
								if ( $show_wpml_header ) : ?>
		                            <div class="lang_bar">
										<?php do_action( 'icl_language_selector' ); ?>
		                            </div>
								<?php 	endif;
							echo "</div>";
						}
					echo "</div>";
					echo "<div id='top_panel_curtain'></div>";
				echo "</div>";
			}
		$top_panel_content = ob_get_clean();

		ob_start();
			$spacings = the8_get_option( "spacings" );
			$custom_header_bg_spacings = (isset( $spacings ) && ($customize_headers == 1) && ((($independent_headers == 1 ) && has_post_thumbnail()) || $independent_headers == 0 ) && ( !the8_is_woo() && ! is_single() && ! (get_post_type() == 'post') && ! (get_post_type() == 'cws_staff') && ! (get_post_type() == 'cws_portfolio'))) ? $spacings : array();

			$page_title_section_atts = "";
			$page_title_section_class = "page_title";
			$page_title_section_class .= $custom_header_bg_spacings ? (!empty($custom_header_bg_spacings["padding-top"]) || !empty($custom_header_bg_spacings["padding-bottom"])) ? " custom_spacing" : "" : "";
			$page_title_section_styles = "";
			$page_title_section_styles = esc_attr( $page_title_section_styles );
			$page_title_section_atts .= !empty( $page_title_section_class ) ? " class='$page_title_section_class'" : "";
			$page_title_section_atts .= !empty( $page_title_section_styles ) ? " style='$page_title_section_styles'" : "";


			$page_title_container_styles = '';



			if ( $custom_header_bg_spacings ){
				foreach ( $custom_header_bg_spacings as $key => $value ){
					if ( !empty( $value ) || $value == '0' ){
						$page_title_container_styles .= $key . ": " . $value . "px;";
						$page_title_section_atts .= " data-init-$key='$value'";
					}
				}
			}

			$show_breadcrumbs = the8_get_option( 'breadcrumbs' ) == 1 ? true : false;


			$text['home']	 = esc_html__( 'Home', 'the8' ); // text for the 'Home' link
			$text['category'] = esc_html__( 'Category "%s"', 'the8' ); // text for a category page
			$text['search']   = esc_html__( 'Search for "%s"', 'the8' ); // text for a search results page
			$text['taxonomy'] = esc_html__( 'Archive by %s "%s"', 'the8' );
			$text['tag']	  = esc_html__( 'Posts Tagged "%s"', 'the8' ); // text for a tag page
			$text['author']   = esc_html__( 'Articles Posted by %s', 'the8' ); // text for an author page
			$text['404']	  = esc_html__( 'Error 404', 'the8' ); // text for the 404 page

			$page_title = "";

			if ( is_404() ) {
				$page_title = esc_html__( '404 Page', 'the8' );
			} else if ( is_search() ) {
				$page_title = esc_html__( 'Search', 'the8' );
			} else if ( is_front_page() ) {
				$page_title = esc_html__( 'Home', 'the8' );
			} else if ( is_category() ) {
				$cat = get_category( get_query_var( 'cat' ) );
				$cat_name = isset( $cat->name ) ? $cat->name : '';
				$page_title = sprintf( $text['category'], $cat_name );
			} else if ( is_tag() ) {
				$page_title = sprintf( $text['tag'], single_tag_title( '', false ) );
			} elseif ( is_day() ) {
				echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
				echo sprintf( $link, get_month_link( get_the_time( 'Y' ),get_the_time( 'm' ) ), get_the_time( 'F' ) ) . $delimiter;
				$page_title = get_the_time( 'd' );

			} elseif ( is_month() ) {
				$page_title = get_the_time( 'F' );

			} elseif ( is_year() ) {
				$page_title = get_the_time( 'Y' );

			} elseif ( has_post_format() && ! is_singular() ) {
				$page_title = get_post_format_string( get_post_format() );
			} else if ( is_tax( array( 'cws_portfolio_cat', 'cws_staff_member_department', 'cws_staff_member_position' ) ) ) {
				$tax_slug = get_query_var( 'taxonomy' );
				$term_slug = get_query_var( $tax_slug );
				$tax_obj = get_taxonomy( $tax_slug );
				$term_obj = get_term_by( 'slug', $term_slug, $tax_slug );

				$singular_tax_label = isset( $tax_obj->labels ) && isset( $tax_obj->labels->singular_name ) ? $tax_obj->labels->singular_name : '';
				$term_name = isset( $term_obj->name ) ? $term_obj->name : '';
				$page_title = $singular_tax_label . ' ' . $term_name ;
			} elseif ( is_archive() ) {
				$post_type = get_post_type();
				$post_type_obj = get_post_type_object( $post_type );
				$post_type_name = isset( $post_type_obj->label ) ? $post_type_obj->label : '';
				$page_title = $post_type_name ;
			} else if ( the8_is_woo() ) {
				$page_title = woocommerce_page_title( false );
			} else if (get_post_type() == 'cws_portfolio') {
				$portfolio_slug = the8_get_option('portfolio_slug');
				$post_type = get_post_type();
				$post_type_obj = get_post_type_object( $post_type );
				$post_type_name = isset( $post_type_obj->labels->menu_name ) ? $post_type_obj->labels->menu_name : '';
				$page_title = !empty($portfolio_slug) ? $portfolio_slug : $post_type_name ;
			}else if (get_post_type() == 'cws_staff') {
				$stuff_slug = the8_get_option('staff_slug');
				$post_type = get_post_type();
				$post_type_obj = get_post_type_object( $post_type );
				$post_type_name = isset( $post_type_obj->labels->menu_name ) ? $post_type_obj->labels->menu_name : '';
				$page_title = !empty($stuff_slug) ? $stuff_slug : $post_type_name ;
			}else {
				$blog_title = the8_get_option('blog_title');
				$page_title = (!is_page() && !empty($blog_title)) ? $blog_title : get_the_title();
			}
				// $page_title = wp_title( '', false );
				$breadcrumbs = "";
				if ( $show_breadcrumbs ){
					if ( function_exists( 'yoast_breadcrumb' ) ) {
						$breadcrumbs = yoast_breadcrumb( "<nav class='bread-crumbs'>", '</nav>', false );
					} else {
						ob_start();
						the8_dimox_breadcrumbs();
						$breadcrumbs = ob_get_clean();
					}
				}

				$page_title = esc_html($page_title);

				$hide_title_area = the8_get_page_meta_var("sb_hide_title_area");
				if (!($hide_title_area) && ( !empty( $page_title ) || (!empty( $breadcrumbs ) && $show_breadcrumbs ) )){
					echo "<section" . ( !empty( $page_title_section_atts ) ? $page_title_section_atts : "" ) . ">";
						echo "<div class='container'" . ( !empty( $page_title_container_styles ) ? " style='$page_title_container_styles'" : "" ) . ">";
							echo !empty( $page_title ) ? "<div class='title'><h1>$page_title</h1></div>" : "";
							echo (!empty( $breadcrumbs ) && $show_breadcrumbs) ? $breadcrumbs : "";
						echo "</div>";
					echo "</section>";
				}
		$page_title_content = ob_get_clean();

		ob_start();

			$bg_header_color_overlay_type = ($customize_headers == 1) ? the8_get_option( 'bg_header_color_overlay_type' ) : '';
			$bg_header_overlay_color = ($customize_headers == 1) ? the8_get_option( 'bg_header_overlay_color' ) : '';
			$bg_header_color_overlay_opacity = ($customize_headers == 1) ? the8_get_option( 'bg_header_color_overlay_opacity' ) : '';
			$bg_header_use_pattern = ($customize_headers == 1) ? the8_get_option( 'bg_header_use_pattern' ) : '';
			$bg_header_pattern_image = ($customize_headers == 1) ? the8_get_option( 'bg_header_pattern_image' ) : '';
			$bg_header_use_blur = ($customize_headers == 1) ? the8_get_option('bg_header_use_blur') : 0;
			$bg_header_blur_intensity = ($customize_headers == 1) ? the8_get_option('bg_header_blur_intensity') : '';
			$font_color = ($customize_headers == 1) ? the8_get_option( "font_color" ) : '';
			$bg_header_parallaxify = ($customize_headers == 1) ? the8_get_option("parallaxify") : '';
			$bg_header_scalar_x = ($customize_headers == 1) ? the8_get_option("scalar_x") : '';
			$bg_header_scalar_y = ($customize_headers == 1) ? the8_get_option("scalar_y") : '';
			$bg_header_limit_x = ($customize_headers == 1) ? the8_get_option("limit_x") : '';
			$bg_header_limit_y = ($customize_headers == 1) ? the8_get_option("limit_y") : '';
			$bg_header_options = the8_get_option('header_bg_image');


			if ($bg_header_use_blur == 1) {
				$bg_header_use_blur_style = '-webkit-filter: blur('.$bg_header_blur_intensity.'px);-moz-filter: blur('.$bg_header_blur_intensity.'px);-o-filter: blur('.$bg_header_blur_intensity.'px);-ms-filter: blur('.$bg_header_blur_intensity.'px);filter: blur('.$bg_header_blur_intensity.'px);';
			}

			$bg_header_gradient_settings = array(
				'@' => array(
					'first_color' => the8_get_option( 'bg_header_gradient_first_color' ),
					'second_color' => the8_get_option( 'bg_header_gradient_second_color' ),
					'type' => the8_get_option( 'bg_header_gradient_type' ),
					'linear_settings' => the8_get_option( 'bg_header_gradient_linear_settings' ),
					'radial_settings' => array(
						'@' => array(
							'shape_settings' => the8_get_option('bg_header_gradient_shape_settings'),
							'shape' => the8_get_option('bg_header_gradient_shape'),
							'size_keyword' => the8_get_option('bg_header_gradient_size_keyword'),
							'size' => the8_get_option('bg_header_gradient_size')
							)
						)
					)
			);
			$bg_header_gradient_settings = ($customize_headers == 1) ? $bg_header_gradient_settings : '';

			$bg_header_color_overlay_opacity = (int)$bg_header_color_overlay_opacity / 100;

			$bg_header_parallaxify_atts = ' data-scalar-x="'.esc_attr($bg_header_scalar_x).'" data-scalar-y="'.esc_attr($bg_header_scalar_y).'" data-limit-y="'.esc_attr($bg_header_limit_y).'" data-limit-x="'.esc_attr($bg_header_limit_x).'"';
			$bg_header_parallaxify_layer_atts = 'position: absolute; z-index: 1; left: -'.esc_attr($bg_header_limit_y).'px; right: -'.esc_attr($bg_header_limit_y).'px; top: -'.esc_attr($bg_header_limit_x).'px; bottom: -'.esc_attr($bg_header_limit_x).'px;';

			$bg_header = false;
			$bg_header_feature = false;
			$bg_header_url = "";
			if ( has_post_thumbnail() && ! the8_is_woo() && ! is_single() && ! (get_post_type() == 'post') && ! (get_post_type() == 'cws_staff') && ! (get_post_type() == 'cws_portfolio') ) {
				$img_object = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				$bg_header_url = isset( $img_object[0] ) ? $img_object[0] : '';
				$bg_header = true;
				$bg_header_feature = true;
			} else if ( isset( $bg_header_options['url'] ) && ! empty( $bg_header_options['url'] ) && (!has_post_thumbnail() && ! the8_is_woo() && ! is_single() && ! (get_post_type() == 'post') && ! (get_post_type() == 'cws_staff') && ! (get_post_type() == 'cws_portfolio')) ) {
				$bg_header_url = $bg_header_options['url'];
				$bg_header = true;
			}

			$bg_header_url = esc_url( $bg_header_url );

			$bg_header_html = '';

			if (($customize_headers == 1) && ((($independent_headers == 1 ) && has_post_thumbnail()) || $independent_headers == 0 ) && ! the8_is_woo() && ! is_single() && ! (get_post_type() == 'post') && ! (get_post_type() == 'cws_staff') && ! (get_post_type() == 'cws_portfolio')) {

				if ( isset( $bg_header_url ) ){

					if ( $bg_header_use_pattern && !empty( $bg_header_pattern_image ) && isset( $bg_header_pattern_image['url'] ) && !empty( $bg_header_pattern_image['url'] ) ){
						$bg_header = true;
						$bg_header_pattern_image_src = $bg_header_pattern_image['url'];
						$bg_header_html .= "<div class='bg_layer' style='background-image:url(" . $bg_header_pattern_image_src . ");".($bg_header_parallaxify ? $bg_header_parallaxify_layer_atts : '')."'></div>";
					}
					if ( $bg_header_color_overlay_type == 'color' && !empty( $bg_header_overlay_color ) ){
						$bg_header = true;
						$bg_header_html .= "<div class='bg_layer' style='background-color:" . $bg_header_overlay_color . ";" . ( !empty( $bg_header_color_overlay_opacity ) ? "opacity:$bg_header_color_overlay_opacity;" : "" ) . ";".($bg_header_parallaxify ? $bg_header_parallaxify_layer_atts : '')."'></div>";
					}
					else if ( $bg_header_color_overlay_type == 'gradient' ){
						$bg_header = true;
						$bg_header_gradient_rules = the8_render_gradient_rules( array( 'settings' => $bg_header_gradient_settings ) );
						$bg_header_html .= "<div class='bg_layer' style='$bg_header_gradient_rules" . ( !empty( $bg_header_color_overlay_opacity ) ? "opacity:$bg_header_color_overlay_opacity;" : "" ) . ";".($bg_header_parallaxify ? $bg_header_parallaxify_layer_atts : '')."'></div>";
					}
				}

				if ( !empty( $bg_header_url ) ){

					echo "<div class='bg_page_header'".(!empty($font_color) ? ' style="color:'.esc_attr($font_color).';"' : '').">";
						echo $page_title_content ? $page_title_content : '';
						echo $bg_header_parallaxify ? '<div class="cws_parallax_section" '.$bg_header_parallaxify_atts.'>' : '';
							wp_enqueue_script ('parallax');
							echo $bg_header_parallaxify ? '<div class="layer" data-depth="1.00">' : '';
								echo $bg_header_html;
								echo "<div class='stat_img_cont' style='". ($bg_header_use_blur == 1 ? $bg_header_use_blur_style : '') . ($bg_header_parallaxify ? $bg_header_parallaxify_layer_atts : '') ."background-image: url(".$bg_header_url.");background-size: cover;background-position: center center;'></div>";
							echo $bg_header_parallaxify ? '</div>' : '';
						echo $bg_header_parallaxify ? '</div>' : '';
					echo '</div>';

				}else{
					echo "<div class='bg_page_header'".(!empty($font_color) ? ' style="color:'.$font_color.';"' : '').">";
						echo $page_title_content ? $page_title_content : '';
						echo $bg_header_html;
					echo '</div>';
				}

			}else{
				if (!$bg_header_feature) {
					$bg_header = false;
				}
				echo !empty($bg_header_url) && $bg_header_feature ? "<div class='bg_page_header'".(!empty($font_color) ? ' style="color:'.esc_attr($font_color).';"' : '').">" : '';
					echo $page_title_content ? $page_title_content : '';
					echo $bg_header_html;
					echo !empty($bg_header_url) && $bg_header_feature ? "<div class='stat_img_cont' style='". ($bg_header_use_blur == 1 ? $bg_header_use_blur_style : '') . ($bg_header_parallaxify ? $bg_header_parallaxify_layer_atts : '') ."background-image: url(".esc_attr($bg_header_url).");background-size: cover;background-position: center center;'></div>" : '';
				echo !empty($bg_header_url) && $bg_header_feature ? '</div>' : '';
			}

		$page_header_content = ob_get_clean();


		ob_start();
			$is_revslider_active = is_plugin_active( 'revslider/revslider.php' );
			$cws_revslider_content = "";
			$slider_type = "none";
			if ( is_front_page() ){
				$slider_type = the8_get_option( 'home-slider-type' );
				switch( $slider_type ){
					case 'img-slider':
						$bg_header = false;
						if ( is_page() ){
							$slider_settings = the8_get_page_meta_var( "slider" );
							if ( isset( $slider_settings['slider_override'] ) && $slider_settings['slider_override'] ){
								$slider_options = isset( $slider_settings['slider_options'] ) ? $slider_settings['slider_options'] : array();
							}
							else{
								$slider_options = the8_get_option( 'home-header-slider-options' );
							}
						}
						else if ( is_home() ){
							$slider_options = the8_get_option( 'home-header-slider-options' );
						}
						$slider_options = htmlspecialchars_decode($slider_options, ENT_QUOTES);
						if (!empty($slider_options)) {
							$bg_header = true;
						}
						echo do_shortcode( $slider_options );
						$show_page_title = !empty( $slider_options ) ? false : $show_page_title;
						break;
					case 'video-slider':
						$bg_header = false;
						$slider_shortcode = the8_get_option( 'slider_shortcode' );
						$slider_switch = the8_get_option( 'slider_switch' );
						$video_type = the8_get_option( 'video_type' );
						$set_video_header_height = the8_get_option( 'set_video_header_height' );
						$video_header_height = the8_get_option( 'video_header_height' );
						$sh_source = the8_get_option( 'sh_source' );
						$youtube_source = the8_get_option( 'youtube_source' );
						$vimeo_source = the8_get_option( 'vimeo_source' );
						$color_overlay_type = the8_get_option( 'color_overlay_type' );
						$overlay_color = the8_get_option( 'overlay_color' );
						$color_overlay_opacity = the8_get_option( 'color_overlay_opacity' );
						$use_pattern = the8_get_option( 'use_pattern' );
						$pattern_image = the8_get_option( 'pattern_image' );

						$video_header_height = $set_video_header_height == "1" ? $video_header_height : false;
						$gradient_settings = array(
								'@' => array(
									'first_color' => the8_get_option( 'slider_gradient_first_color' ),
									'second_color' => the8_get_option( 'slider_gradient_second_color' ),
									'type' => the8_get_option( 'slider_gradient_type' ),
									'linear_settings' => the8_get_option( 'slider_gradient_linear_settings' ),
									'radial_settings' => array(
										'@' => array(
											'shape_settings' => the8_get_option('slider_gradient_shape_settings'),
											'shape' => the8_get_option('slider_gradient_shape'),
											'size_keyword' => the8_get_option('slider_gradient_size_keyword'),
											'size' => the8_get_option('slider_gradient_size')
											)
										)
									)
							);

						$sh_source = isset( $sh_source['url'] ) && !empty( $sh_source['url'] ) ? $sh_source['url'] : "";
						$color_overlay_opacity = (int)$color_overlay_opacity / 100;
						$has_video_src = false;
						$header_video_atts = "";
						$header_video_class = "fs_video_bg";
						$header_video_styles = "";
						$header_video_html = "";
						$uniqid = uniqid( 'video-' );
						$uniqid_esc = esc_attr( $uniqid );
						switch ( $video_type ){
							case 'self_hosted':
								if ( !empty( $sh_source ) ){
									$has_video_src = true;
									$header_video_class .= " cws_self_hosted_video";
									$header_video_html .= "<video class='self_hosted_video' src='$sh_source' autoplay='autoplay' loop='loop' muted='muted'></video>";
								}
								break;
							case 'youtube':
								if ( !empty( $youtube_source ) ){
									wp_enqueue_script ('cws_YT_bg');
									$has_video_src = true;
									$header_video_class .= " cws_Yt_video_bg loading";
									$header_video_atts .= " data-video-source='$youtube_source' data-video-id='$uniqid'";
									$header_video_html .= "<div id='$uniqid_esc'></div>";
								}
								break;
							case 'vimeo':
								if ( !empty( $vimeo_source ) ){
									wp_enqueue_script ('vimeo');
									wp_enqueue_script ('cws_self&vimeo_bg');
									$has_video_src = true;
									$header_video_class .= " cws_Vimeo_video_bg";
									$header_video_atts .= " data-video-source='$vimeo_source' data-video-id='$uniqid'";
									$header_video_html .= "<iframe id='$uniqid_esc' src='" . $vimeo_source . "?api=1&player_id=$uniqid' frameborder='0'></iframe>";
								}
								break;
						}
						if ( $has_video_src ){
							$bg_header = true;
							if ( $use_pattern && !empty( $pattern_image ) && isset( $pattern_image['url'] ) && !empty( $pattern_image['url'] ) ){
								$pattern_img_src = $pattern_image['url'];
								$header_video_html .= "<div class='bg_layer' style='background-image:url(" . $pattern_img_src . ")'></div>";
							}
							if ( $color_overlay_type == 'color' && !empty( $overlay_color ) ){
								$header_video_html .= "<div class='bg_layer' style='background-color:" . $overlay_color . ";" . ( !empty( $color_overlay_opacity ) ? "opacity:$color_overlay_opacity;" : "" ) . "'></div>";
							}
							else if ( $color_overlay_type == 'gradient' ){
								$gradient_rules = the8_render_gradient_rules( array( 'settings' => $gradient_settings ) );
								$header_video_html .= "<div class='bg_layer' style='$gradient_rules" . ( !empty( $color_overlay_opacity ) ? "opacity:$color_overlay_opacity;" : "" ) . "'></div>";
							}
						}

						$header_video_atts .= !empty( $header_video_class ) ? " class='" . trim( $header_video_class ) . "'" : "";
						$header_video_atts .= !empty( $header_video_styles ) ? " style='". esc_attr($header_video_styles) ."'" : "";


						if ( !empty( $slider_shortcode ) && $has_video_src && $slider_switch == 1 ){
							$bg_header = true;
							echo "<div class='fs_video_slider'>";
							if ( $is_revslider_active ) {
								echo  do_shortcode( $slider_shortcode );
							} else {
								echo do_shortcode( "[cws_sc_msg_box type='warning' is_closable='1' text='Install and activate Slider Revolution plugin'][/cws_sc_msg_box]" );
							}
								echo '<div ' . $header_video_atts . '>';
								echo $header_video_html;
								echo '</div>';
								echo '</div>';
						} elseif ( $has_video_src && $slider_switch == 0 ) {
							$bg_header = true;
							$header_video_fs_view = $video_header_height == false ? 'header_video_fs_view' : '';
							$video_height_coef = $video_header_height == false ? '' : " data-wrapper-height='".(960 / $video_header_height)."'";
							$video_header_height = $video_header_height == false ? '' : "style='height:" . $video_header_height ."px'";
							echo "<div class='fs_video_slider ". sanitize_html_class( $header_video_fs_view ) ."' " . $video_header_height . " ". $video_height_coef .">";
							echo '<div ' . $header_video_atts . '>';
							echo $header_video_html;
							echo '</div>';
							echo '</div>';
						}elseif ( ! empty( $slider_shortcode ) && $slider_switch == 1 && ! $has_video_src ) {
							$bg_header = true;
							if ( $is_revslider_active ) {
								echo  do_shortcode( $slider_shortcode );
							} else {
								echo do_shortcode( "[cws_sc_msg_box type='warning' is_closable='1' text='Install and activate Slider Revolution plugin'][/cws_sc_msg_box]" );
							}
						}else{
							$bg_header = true;
							if ( $has_video_src ){
								echo "<div class='fs_video_slider'></div>";
							}
						}

	 					break;
					case 'stat-img-slider':
						$bg_header = false;
						$set_img_header_height = the8_get_option( 'set_static_image_height' );
						$img_header_height = the8_get_option( 'static_image_height' );

						$color_overlay_type = the8_get_option( 'img_header_color_overlay_type' );
						$overlay_color = the8_get_option( 'img_header_overlay_color' );
						$color_overlay_opacity = the8_get_option( 'img_header_color_overlay_opacity' );
						$use_pattern = the8_get_option( 'img_header_use_pattern' );
						$pattern_image = the8_get_option( 'img_header_pattern_image' );

						$img_header_height = $set_img_header_height == "1" ? $img_header_height : false;
						$gradient_settings = array(
								'@' => array(
									'first_color' => the8_get_option( 'img_header_gradient_first_color' ),
									'second_color' => the8_get_option( 'img_header_gradient_second_color' ),
									'type' => the8_get_option( 'img_header_gradient_type' ),
									'linear_settings' => the8_get_option( 'img_header_gradient_linear_settings' ),
									'radial_settings' => array(
										'@' => array(
											'shape_settings' => the8_get_option('img_header_gradient_shape_settings'),
											'shape' => the8_get_option('img_header_gradient_shape'),
											'size_keyword' => the8_get_option('img_header_gradient_size_keyword'),
											'size' => the8_get_option('img_header_gradient_size')
											)
										)
									)
							);
						$color_overlay_opacity = (int)$color_overlay_opacity / 100;

						$img_header_parallaxify = the8_get_option("img_header_parallaxify");
						$img_header_scalar_x = the8_get_option("img_header_scalar_x");
						$img_header_scalar_y = the8_get_option("img_header_scalar_y");
						$img_header_limit_x = the8_get_option("img_header_limit_x");
						$img_header_limit_y = the8_get_option("img_header_limit_y");

						$img_header_parallaxify_atts = ' data-scalar-x="'.$img_header_scalar_x.'" data-scalar-y="'.$img_header_scalar_y.'" data-limit-y="'.$img_header_limit_y.'" data-limit-x="'.$img_header_limit_x.'"';
						$img_header_parallaxify_layer_atts = 'position: absolute; z-index: 1; left: -'.$img_header_limit_y.'px; right: -'.$img_header_limit_y.'px; top: -'.$img_header_limit_x.'px; bottom: -'.$img_header_limit_x.'px;';




						$image_options = the8_get_option("home-header-image-options");

						$default_img = false;
						$override_img = false;
						$img_url = "";



						$header_img_html = '';

						if ( isset( $image_options['url'] ) ){
							if ( $use_pattern && !empty( $pattern_image ) && isset( $pattern_image['url'] ) && !empty( $pattern_image['url'] ) ){
								$pattern_img_src = $pattern_image['url'];
								$header_img_html .= "<div class='bg_layer' style='background-image:url(" . $pattern_img_src . ");".($img_header_parallaxify ? $img_header_parallaxify_layer_atts : '')."'></div>";
							}
							if ( $color_overlay_type == 'color' && !empty( $overlay_color ) ){
								$header_img_html .= "<div class='bg_layer' style='background-color:" . $overlay_color . ";" . ( !empty( $color_overlay_opacity ) ? "opacity:$color_overlay_opacity;" : "" ) . ";".($img_header_parallaxify ? $img_header_parallaxify_layer_atts : '')."'></div>";
							}
							else if ( $color_overlay_type == 'gradient' ){
								$gradient_rules = the8_render_gradient_rules( array( 'settings' => $gradient_settings ) );
								$header_img_html .= "<div class='bg_layer' style='$gradient_rules" . ( !empty( $color_overlay_opacity ) ? "opacity:$color_overlay_opacity;" : "" ) . ";".($img_header_parallaxify ? $img_header_parallaxify_layer_atts : '')."'></div>";
							}
						}

						if ( isset( $image_options['url'] ) ) {
							$bg_header = true;
							$header_img_fs_view = $img_header_height== false ? 'header_video_fs_view' : '';
							$header_img_height_coef = $img_header_height == false ? '' : " data-wrapper-height='".(960 / $img_header_height)."'";
							$img_header_height = $img_header_height == false ? '' : "style='height:" . esc_attr($img_header_height) ."px'";

							echo "<div class='fs_img_header " . sanitize_html_class( $header_img_fs_view ) ."' " . $img_header_height . " ". $header_img_height_coef .">";

							echo $img_header_parallaxify ? '<div class="cws_parallax_section" '.$img_header_parallaxify_atts.'>' : '';
								wp_enqueue_script ('parallax');
								echo $img_header_parallaxify ? '<div class="layer" data-depth="1.00">' : '';
									echo $header_img_html;
									echo "<div class='stat_img_cont' style='". ($img_header_parallaxify ? $img_header_parallaxify_layer_atts : '') ."background-image: url(".$image_options['url'].");background-size: cover;background-position: center center;'></div>";
								echo $img_header_parallaxify ? '</div>' : '';
							echo $img_header_parallaxify ? '</div>' : '';
							echo '</div>';

						}

					break;
					default:
						echo !empty($page_header_content) ? $page_header_content : '';
				}
			}
			else if ( is_page() ){

				$slider_settings = the8_get_page_meta_var( "slider" );

				if ( $slider_settings['slider_override'] ){
					$bg_header = true;
					$slider_options = htmlspecialchars_decode($slider_settings['slider_options'], ENT_QUOTES);
					echo do_shortcode( $slider_options );
				}

				if ( !$slider_settings['slider_override'] ){
					echo !empty($page_header_content) ? $page_header_content : '';

				}
			}

		$slider_content = ob_get_clean();

		ob_start();
			header_menu_and_logo ();
		$header_content = ob_get_clean();


		if (the8_get_option( 'menu-stick' ) == 1) {
			echo "<div class='sticky_header'>";
				echo header_menu_and_logo ();
			echo "</div>";
		}
	

		echo "<script type='text/javascript'>window.header_after_slider=false;</script>";
		echo ( !empty($top_panel_content) || !empty($header_content) ) ? '<div class="header_wrapper_container'.((($show_header_outside_slider == 1) && ($bg_header == true) ) ? ' header_outside_slider' : '').'">' : '';
		echo $top_panel_content;
		echo $header_content;
		echo ( !empty($top_panel_content) || !empty($header_content) ) ? '</div>' : '';
		echo $slider_content;

		echo ( ! ( is_front_page() ) && empty( $slider_content )) ? $page_title_content :  '';
	}
}

/* END THEME HEADER */

if (!function_exists('header_menu_and_logo')) {
	function header_menu_and_logo (){
		$stick_menu = the8_get_option( 'menu-stick' );
		$stick_menu =  $stick_menu == 1 ? true : false;
		ob_start();
			if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
				woocommerce_mini_cart();
			}
		$woo_mini_cart = ob_get_clean();

		ob_start();
			if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
				?>
					<a class="woo_icon" href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" title="<?php esc_html_e( 'View your shopping cart', 'the8' ); ?>"><i class='woo_mini-count cwsicon-shopping-carts6'><?php echo ((WC()->cart->cart_contents_count > 0) ?  '<span>' . esc_html( WC()->cart->cart_contents_count ) .'</span>' : '') ?></i></a>
					<?php
			}
		$woo_mini_icon = ob_get_clean();

		/***** Logo Settings *****/
		$logo = the8_get_option('logo');
		$logo_is_high_dpi = the8_get_option('logo_is_high_dpi');


		$slider_settings = the8_get_page_meta_var( "slider" );
		$cws_is_header_hovers_slider = (the8_get_option('show_header_outside_slider') === "1") ? true : false;
		$cws_is_custom_headers = (the8_get_option('customize_headers') === "1") ? true : false;
		$independent_headers = the8_get_option( 'independent_headers' );		
		if (( $cws_is_header_hovers_slider && (is_front_page() || (( isset( $slider_settings['slider_override'] ) && $slider_settings['slider_override'] ) && (!empty( $slider_settings['slider_options']) ) ) )) || (!(is_front_page() || is_single()) && is_page() && $cws_is_header_hovers_slider && $cws_is_custom_headers && ((($independent_headers == 1 ) && has_post_thumbnail()) || $independent_headers == 0 )) ) {
				$logo_white = the8_get_option('logo_white');
				if (!empty( $logo_white['url'] )) {
					$logo = $logo_white;
					$logo_is_high_dpi = the8_get_option('logo_white_is_high_dpi');
				}
		}

		$logo_lr_spacing = "";
		$logo_tb_spacing = "";

		if ( !empty( $logo['url'] ) ) {
			$logo_hw = the8_get_option('logo-dimensions');
			$logo_m = the8_get_option('logo-margin');
			$bfi_args = array();
			if (is_array($logo_hw)){
				foreach ($logo_hw as $key => $value) {
					if ( !empty($value) ){
						$bfi_args[$key] = $value;
						$bfi_args['crop'] = true;
					}
				}
			}
			
			$logo_src = '';
			$main_logo_height = '';
			if ( is_array( $logo_m ) ) {
				$logo_lr_spacing .= ( ! empty( $logo_m['margin-left'] ) ? 'margin-left:' . (int) $logo_m['margin-left'] . 'px;' : '' ) . ( ! empty( $logo_m['margin-right'] ) ? 'margin-right:' . (int) $logo_m['margin-right'] . 'px;' : '' ) . ( ! empty( $logo_m['margin-top'] ) ? 'margin-top:' . (int) $logo_m['margin-top'] . 'px;' : '' ) . ( ! empty( $logo_m['margin-bottom'] ) ? 'margin-bottom:' . (int) $logo_m['margin-bottom'] . 'px;' : '' );
				$logo_tb_spacing .= ( ! empty( $logo_m['margin-top'] ) ? 'padding-top:' . (int) $logo_m['margin-top'] . 'px;' : '' ) . ( ! empty( $logo_m['margin-bottom'] ) ? 'padding-bottom:' . (int) $logo_m['margin-bottom'] . 'px;' : '' );
			}

			$logo_retina_thumb_exists = false;
			$logo_retina_thumb_url = "";
			if ( isset( $logo['url'] ) && ( ! empty( $logo['url'] ) ) ) {
				if ( empty( $bfi_args ) ) {
					if ( $logo_is_high_dpi ) {
						$thumb_obj = bfi_thumb( $logo['url'],array( 'width' => floor( (int) $logo['width'] / 2 ), 'crop' => true ),false );
						$main_logo_height = $thumb_obj[2];
						$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
						$logo_src = $thumb_path_hdpi;
					} else {
						$main_logo_height = $logo["height"];
						$logo_src = " src='".esc_url( $logo['url'] )."' data-no-retina";
					}
				} else {
					$thumb_obj = bfi_thumb( $logo['url'],$bfi_args,false );
					$main_logo_height =  !empty($bfi_args["height"]) ? $bfi_args["height"] : '';
					$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
					$logo_src = $thumb_path_hdpi;
				}
			}
		}

		$logo_lr_spacing .= !empty($main_logo_height) ? 'height:' . $main_logo_height . 'px;' : '';
		$main_logo_height = !empty($main_logo_height) ? ' style="height:' . $main_logo_height . 'px;"' : '';
			/***** \Logo Settings *****/

		$logo_sticky = the8_get_option( 'logo_sticky' );
		$logo_sticky_src = '';
		$logo_sticky_is_high_dpi = the8_get_option( 'logo_sticky_is_high_dpi' );
		if ( isset( $logo_sticky['url'] ) && ( ! empty( $logo_sticky['url'] ) ) ) {
			$logo_sticky_src = '';
			if ( $logo_sticky_is_high_dpi ) {
				$thumb_obj = bfi_thumb( $logo_sticky['url'],array( 'width' => floor( (int) $logo_sticky['width'] / 2 ), 'crop' => true ),false );
				$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
				$logo_sticky_src = $thumb_path_hdpi;
			} else {
				$logo_sticky_src = " src='".esc_url( $logo_sticky['url'] )."' data-no-retina";
			}
		}


		$logo_mobile = the8_get_option( 'logo_mobile' );
		$logo_mobile_src = '';
		$logo_mobile_is_high_dpi = the8_get_option( 'logo_mobile_is_high_dpi' );
		if ( isset( $logo_mobile['url'] ) && ( ! empty( $logo_mobile['url'] ) ) ) {
			$logo_mobile_src = '';
			if ( $logo_mobile_is_high_dpi ) {
				$thumb_obj = bfi_thumb( $logo_mobile['url'],array( 'width' => floor( (int) $logo_mobile['width'] / 2 ), 'crop' => true ),false );
				$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
				$logo_mobile_src = $thumb_path_hdpi;

			} else {
				$logo_mobile_src = " src='".esc_url( $logo_mobile['url'] )."' data-no-retina";
			}
		}

		/***** Logo Position *****/
		$logo_position = the8_get_option('logo-position');
		$header_class = 'site_header';
		$header_class .= $stick_menu ? ' sticky_enable' : '';
		if ( isset( $logo_position ) && ! empty( $logo_position ) ) {
			$header_class .= ' logo-' . sanitize_html_class( $logo_position );
		}
		if ( isset( $logo_sticky_src ) && ! empty( $logo_sticky_src ) ) {
			$header_class .= ' custom_sticky_logo';
		}
		if ( isset( $logo_mobile_src ) && ! empty( $logo_mobile_src ) ) {
			$header_class .= ' custom_mobile_logo';
		}
		$header_class .= !empty( $slider_content ) && ($show_header_outside_slider == '1') && ($bg_header == true) ? " with_background" : "";
		/***** \Logo Position *****/

		/***** Menu Position *****/
		global $current_user;
		$menu_locations = get_nav_menu_locations();
		$menu_position = the8_get_option("menu-position");
		/***** \Menu Position *****/
		ob_start();

			echo "<div class='header_cont'>";
				$logo_exists = isset( $logo["url"] ) && ( !empty( $logo["url"] ) );
				$logo_center = isset($logo_position) && !empty($logo_position) && $logo_position == "center";
				$header_style = '';
				$header_margin = the8_get_option( 'header_margin' );
				$header_top_margin = (int)$header_margin['margin-top'];
				$header_bot_margin = (int)$header_margin['margin-bottom'];
				$header_style .= ! empty( $header_top_margin ) ? 'padding-top: '.$header_top_margin.'px;' : '' ;
				$header_style .= ! empty( $header_bot_margin ) ? 'padding-bottom: '.$header_bot_margin.'px;' : '' ;
	
				if (( $cws_is_header_hovers_slider && (is_front_page() || (( isset( $slider_settings['slider_override'] ) && $slider_settings['slider_override'] ) && (!empty( $slider_settings['slider_options']) ) ) )) || (!(is_front_page() || is_single()) && is_page() && $cws_is_header_hovers_slider && $cws_is_custom_headers && ((($independent_headers == 1 ) && has_post_thumbnail()) || $independent_headers == 0 )) ) {
				
					global $cws_theme_funcs;	
					$header_outside_slider_style = '';
					$header_outside_slider_bg_color = $cws_theme_funcs->cws_Hex2RGB( the8_get_option('header_outside_slider_bg_color') );
					$header_outside_slider_bg_color_opacity = the8_get_option('header_outside_slider_bg_opacity')/100;
					$header_outside_slider_style .= 'background-color:rgba('.$header_outside_slider_bg_color.','.$header_outside_slider_bg_color_opacity.');';
					$header_style .= $header_outside_slider_style;
				}
				?>

				<header <?php echo !empty($header_class) ? "class='$header_class'" : ""; ?>>
				<div class="header_box tabla-nav" <?php echo ! empty( $header_style ) ? ' style="'.esc_attr($header_style).'"' : ''; ?>>
					<div>
						<?php
						if ( $logo_exists ){
							?>
							<div class="header_logotype_part" role="banner" <?php echo isset( $logo_position ) && !empty( $logo_position ) && $logo_position == 'center' && ! empty( $logo_tb_spacing ) ? " style='".esc_attr($logo_tb_spacing)."'" : ''; ?>>
								<a <?php echo ( ! empty( $logo_lr_spacing ) ? " style='".esc_attr($logo_lr_spacing)."'" : '') ?> class="logo" href="<?php echo esc_url( home_url() ); ?>" >
									<?php echo ! empty( $logo_sticky_src ) ? '<img ' . $logo_sticky_src . " class='logo_sticky' alt />" : '';?>
									<?php echo ! empty( $logo_mobile_src ) ? '<img ' . $logo_mobile_src . " class='logo_mobile' alt />" : '';?>
									<img <?php echo $logo_src; echo $main_logo_height ?> alt /></a>
		                    </div>

							<?php
						}else{
							?>
							<div class="header_logotype_part" role="banner" <?php echo isset( $logo_position ) && !empty( $logo_position ) && $logo_position == 'center' && ! empty( $logo_tb_spacing ) ? " style='".esc_attr($logo_tb_spacing)."'" : ''; ?>>
								<a <?php echo ( ! empty( $logo_lr_spacing ) ? " style='".esc_attr($logo_lr_spacing)."'" : '') ?> class="logo" href="<?php echo esc_url( home_url() ); ?>" >
									<?php echo ! empty( $logo_sticky_src ) ? '<img ' . $logo_sticky_src . " class='logo_sticky' alt />" : '';?>
									<?php echo ! empty( $logo_mobile_src ) ? '<img ' . $logo_mobile_src . " class='logo_mobile' alt />" : '';?>
									<h1 class='header_site_title'><?php echo get_bloginfo( 'name' ); ?></h1>
								</a>
		                    </div>

							<?php
						}
						if ( isset( $menu_locations['header-menu'] ) && !empty($menu_locations['header-menu']) ) {
							?>
							<div class="header_nav_part navegacion cell-nav" <?php echo !empty( $header_nav_part_atts ) ? trim( $header_nav_part_atts ) : ""; ?>>
								<nav class="main-nav-container <?php echo !empty($menu_position) ? 'a-' . $menu_position : ''; ?>">
									<div class="mobile_menu_header">
										<?php
											ob_start();
												if (the8_get_option('woo_cart_place') == 'left' || the8_get_option('woo_cart_place') == 'right') {
													echo is_plugin_active( 'woocommerce/woocommerce.php' ) ? "<div class='mini-cart'>$woo_mini_icon$woo_mini_cart</div>" : '';
												}
												if(the8_get_option('search_place') == 'left' || the8_get_option('search_place') == 'right'){
													echo "<div class='search_menu'></div>";
												}
												
											$search_and_woo_icon_start = ob_get_clean();
											echo $search_and_woo_icon_start;
										?>
										<i class="mobile_menu_switcher"></i>
									</div>
									<?php
										ob_start();
										 wp_nav_menu( array(
											'theme_location'  => 'header-menu',
											'menu_class' => 'main-menu',
											'container' => false,
											'walker' => new The8_Walker_Nav_Menu()
										) );
										$menu = ob_get_clean();
										if ( ! empty ( $menu ) ){
										    echo $menu;
										}
									?>
								</nav>
							</div>
							<?php
						}
						?>
					</div>
				</div>
				<?php
					if((the8_get_option('search_place') == 'right') || (the8_get_option('search_place') == 'left')){
						echo "<div class='search_menu_cont'>";
							echo "<div class='container'>";
								get_search_form();
								echo "<div class='search_back_button'></div>";
							echo "</div>";
						echo "</div>";
					}
				?>
			</header><!-- #masthead -->



			<?php
			echo "</div>";
		$header_content = ob_get_clean();
		if (!empty($header_content)) {
			echo $header_content;
		}
	}
}


/* THEME FOOTER */
if (!function_exists('the8_page_footer')) {

	function the8_page_footer (){

		$footer_sidebar = is_page() ? the8_get_page_meta_var( array( 'footer', 'footer_sb_top' ) ) : the8_get_option( "footer-sidebar-top" );
		if ( !empty( $footer_sidebar ) && is_active_sidebar( $footer_sidebar ) ) {
			echo "<footer class='page_footer' style='background-color:".(esc_attr(the8_get_option('theme-footer-color')))."; color:".(esc_attr(the8_get_option('theme-footer-font-color')))."'>";
				echo "<div class='container'>";
					echo "<div class='footer_container'>";
						$GLOBALS['footer_is_rendered'] = true;
						dynamic_sidebar( $footer_sidebar );
						unset( $GLOBALS['footer_is_rendered'] );
					echo "</div>";
				echo "</div>";
			echo "</footer>";
		}

		$copyrights = apply_filters( 'the_content', the8_get_option( "footer_copyrights_text" ) );

		$social_links = "";
		$social_links_location = the8_get_option( 'social_links_location' );

		$show_wpml_footer = ( the8_is_wpml_active()) ? true : false;
		if ( in_array( $social_links_location, array( 'bottom', 'top_bottom' ) ) ) {
			$social_links = the8_render_social_links();
		}
		$ret = '';
		if ( is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
			global $wpml_language_switcher;
			$slot = $wpml_language_switcher->get_slot( 'statics', 'footer' );
			$template = $slot->get_model();
			$ret = $slot->is_enabled();
		}		


		ob_start();
		if ( !empty( $copyrights ) ) {
			echo "<div class='copyrights' >$copyrights</div>";
		}
		if ( !empty( $social_links ) || ! empty( $show_wpml_footer ) ) {
			echo "<div class='copyrights_panel'>";
				echo "<div class='copyrights_panel_wrapper'>";
					echo !empty( $social_links ) ? $social_links : "";
					$class_wpml = '';
					if (!empty($template['template'])) {
						if($template['template'] == 'wpml-legacy-vertical-list'){
							$class_wpml = 'wpml_language_switch lang_bar '. $template['template'];
						}
						else {
							$class_wpml = 'wpml_language_switch horizontal_bar '.$template['template'];
						}
					} else {
						$class_wpml = 'lang_bar';
					}
					if ( $show_wpml_footer && !empty($ret) ) : ?>
						<div class="<?php echo esc_attr($class_wpml);?>">
							<?php
								do_action( 'wpml_footer_language_selector' );
							?>
						</div>
					<?php 	endif;
				echo '</div>';
			echo '</div>';
		}

		$copyrights_content = ob_get_clean();
		if ( !empty( $copyrights_content ) ) {
			echo "<div id='footer-space'></div><div class='copyrights_area'>";
					echo "<div class='copyrights_container'>";
						echo $copyrights_content;
				echo "</div>";
			echo "</div>";
		}
		echo "<div class='scroll_top animated'></div>";
		$boxed_layout = ('0' != the8_get_option('boxed-layout') ) ? 'boxed' : '';
		echo $boxed_layout ? "</div>" : "";
	}
}

/* END THEME FOOTER */

/* FA ICONS */
function the8_get_all_fa_icons() {
	$meta = get_option('cws_fa');
	if (!empty($meta) || (time() - $meta['t']) > 3600*7 ) {
		global $wp_filesystem;
		if( empty( $wp_filesystem ) ) {
			require_once( ABSPATH .'/wp-admin/includes/file.php' );
			WP_Filesystem();
		}
		$file = get_template_directory() . '/css/font-awesome.css';
		$fa_content = '';
		if ( $wp_filesystem && $wp_filesystem->exists($file) ) {
			$fa_content = $wp_filesystem->get_contents($file);
			if ( preg_match_all( "/fa-((\w+|-?)+):before/", $fa_content, $matches, PREG_PATTERN_ORDER ) ) {
				return $matches[1];
			}
		}
	} else {
		return $meta['fa'];
	}
}
/* \FA ICONS */

/* FA ICONS */
function the8_get_all_flaticon_icons() {
$cwsfi = get_option('cwsfi');
	if (!empty($cwsfi) && isset($cwsfi['entries'])) {
		return $cwsfi['entries'];
	} else {	
		global $wp_filesystem;
		if( empty( $wp_filesystem ) ) {
			require_once( ABSPATH .'/wp-admin/includes/file.php' );
			WP_Filesystem();
		}
		$file = get_template_directory() . '/fonts/flaticon/flaticon.css';
		$fi_content = '';
		$out = '';
		if ( $wp_filesystem && $wp_filesystem->exists($file) ) {
			$fi_content = $wp_filesystem->get_contents($file);
			if ( preg_match_all( "/flaticon-((\w+|-?)+):before/", $fi_content, $matches, PREG_PATTERN_ORDER ) ){
				return $matches[1];
			}
		}
	}
}
/* \FA ICONS */

/********************************** !!! **********************************/

function the8_getTweets( $count = 20 ) {
	$res = null;
	if ( '0' != the8_get_option( 'turn-twitter' ) ) {
		$twitt_name = trim(the8_get_option( 'tw-username' )) ? trim(the8_get_option( 'tw-username' )) : 'Creative_WS';
		if (function_exists('getTweets')) {
			$res = getTweets($twitt_name, $count);
		}
	}

	return $res;
}

function the8_filter_by_empty ( $arr = array() ) {
	if ( empty( $arr ) || !is_array( $arr ) ) return false;
	for ( $i=0; $i<count( $arr ); $i++ ) {
		if ( empty( $arr[$i]) ) {
			array_splice( $arr, $i, 1 );
		}
	}
	return $arr;
}

function the8_cws_switch_theme($newname, $newtheme) {
	wp_deregister_script( 'popup' );
}

add_action("switch_theme", "the8_cws_switch_theme", 10 , 2);

/******************** CUSTOM COLOR ********************/

function the8_render_gradient_rules( $args ) {

	extract( shortcode_atts( array(
		'settings' => array(),
		'selectors' => '',
		'use_extra_rules' => false
	), $args));

	$selectors_wth_pseudo = '';

	$settings = is_object( $settings ) ? get_object_vars( $settings ) : $settings;

	$settings = isset( $settings['@'] ) ? $settings['@'] : array();

	extract( shortcode_atts( array(
		'first_color' => THE8_COLOR,
		'second_color' => '#0eecbd',
		'type' => 'linear',
		'linear_settings' => array(),
		'radial_settings' => array()
	), $settings));

	$out = '';
	$rules = '';
	$border_extra_rules = "border-color: transparent;\n-moz-background-clip: border;\n-webkit-background-clip: border;\nbackground-clip: border-box;\n-moz-background-origin:border;\n-webkit-background-origin:border;\nbackground-origin:border-box;\nbackground-repeat: no-repeat;";
	$transition_extra_rules = "-webkit-transition-property: background, color, border-color, opacity;\n-webkit-transition-duration: 0s, 0s, 0s, 0.6s;\n-o-transition-property: background, color, border-color, opacity;\n-o-transition-duration: 0s, 0s, 0s, 0.6s;\n-moz-transition-property: background, color, border-color, opacity;\n-moz-transition-duration: 0s, 0s, 0s, 0.6s;\ntransition-property: background, color, border-color, opacity;\ntransition-duration: 0s, 0s, 0s, 0.6s;";
	if ( $type == 'linear' ) {

		$linear_settings = is_object( $linear_settings ) ? get_object_vars( $linear_settings ) : $linear_settings;
		$linear_settings = isset( $linear_settings['@'] ) ? $linear_settings['@'] : array();
		extract( shortcode_atts( array(
			'angle' => '45'
		), $linear_settings));

		$rules .= "background: -webkit-linear-gradient(" . $angle . "deg, $first_color, $second_color);";
		$rules .= "background: -o-linear-gradient(" . $angle . "deg, $first_color, $second_color);";
		$rules .= "background: -moz-linear-gradient(" . $angle . "deg, $first_color, $second_color);";
		$rules .= "background: linear-gradient(" . $angle . "deg, $first_color, $second_color);";
	}
	else if ( $type == 'radial' ) {
		$radial_settings = is_object( $radial_settings ) ? get_object_vars( $radial_settings ) : $radial_settings;
		$radial_settings = isset( $radial_settings['@'] ) ? $radial_settings['@'] : array();
		extract( shortcode_atts( array(
			'shape_settings' => 'simple',
			'shape' => 'ellipse',
			'size_keyword' => '',
			'size' => ''
		), $radial_settings));
		if ( $shape_settings == 'simple' ) {
			$rules .= "background: -webkit-radial-gradient(" . ( !empty( $shape ) ? " " . $shape . "," : "" ) . " $first_color, $second_color);";
			$rules .= "background: -o-radial-gradient(" . ( !empty( $shape ) ? " " . $shape . "," : "" ) . " $first_color, $second_color);";
			$rules .= "background: -moz-radial-gradient(" . ( !empty( $shape ) ? " " . $shape . "," : "" ) . " $first_color, $second_color);";
			$rules .= "background: radial-gradient(" . ( !empty( $shape ) ? " " . $shape . "," : "" ) . " $first_color, $second_color);";
		}
		else if ( $shape_settings == 'extended' ) {
			$rules .= "background: -webkit-radial-gradient(" . ( !empty( $size ) ? " " . $size . "," : "" ) . ( !empty( $size_keyword ) ? " " . $size_keyword . "," : "" ) . " $first_color, $second_color);";
			$rules .= "background: -o-radial-gradient(" . ( !empty( $size ) ? " " . $size . "," : "" ) . ( !empty( $size_keyword ) ? " " . $size_keyword . "," : "" ) . " $first_color, $second_color);";
			$rules .= "background: -moz-radial-gradient(" . ( !empty( $size ) ? " " . $size . "," : "" ) . ( !empty( $size_keyword ) ? " " . $size_keyword . "," : "" ) . " $first_color, $second_color);";
			$rules .= "background: radial-gradient(" . ( !empty( $size_keyword ) && !empty( $size ) ? " $size_keyword at $size" : "" ) . " $first_color, $second_color);";
		}
	}
	if ( !empty( $rules ) ) {
		$out .= !empty( $selectors ) ? "$selectors{\n$rules\n}" : $rules;
		if ( $use_extra_rules ) {
			$out .= !empty( $selectors ) ? "$selectors{\n$border_extra_rules\n}" : $border_extra_rules;
			$out .= !empty( $selectors ) ? "\n$selectors{\ncolor: #fff !important;\n}" : "color: #fff !important;";
			if ( !empty( $selectors ) ) {
				$selectors_wth_pseudo = str_replace( array( ":hover" ), "", $selectors );
				if ( !empty( $selectors_wth_pseudo ) ) {
					$out .= "\n$selectors_wth_pseudo{\n$transition_extra_rules\n}";
				}
			}
			else{
				$out .= $transition_extra_rules;
			}
		}
	}
	return preg_replace('/\s+/',' ', $out);
}

function the8_render_builder_gradient_rules( $options ) {
	extract(shortcode_atts(array(
		'gradient_start_color' => THE8_COLOR,
		'gradient_end_color' => '#0eecbd',
		'gradient_type' => 'linear',
		'gradient_linear_angle' => '45',
		'gradient_radial_shape_settings_type' => 'simple',
		'gradient_radial_shape' => 'ellipse',
		'gradient_radial_size_keyword' => 'farthest-corner',
		'gradient_radial_size' => '',
	), $options));
	$out = '';
	if ( $gradient_type == 'linear' ) {
		$out .= "background: -webkit-linear-gradient(" . $gradient_linear_angle . "deg, $gradient_start_color, $gradient_end_color);";
		$out .= "background: -o-linear-gradient(" . $gradient_linear_angle . "deg, $gradient_start_color, $gradient_end_color);";
		$out .= "background: -moz-linear-gradient(" . $gradient_linear_angle . "deg, $gradient_start_color, $gradient_end_color);";
		$out .= "background: linear-gradient(" . $gradient_linear_angle . "deg, $gradient_start_color, $gradient_end_color);";
	}
	else if ( $gradient_type == 'radial' ) {
		if ( $gradient_radial_shape_settings_type == 'simple' ) {
			$out .= "background: -webkit-radial-gradient(" . ( !empty( $gradient_radial_shape ) ? " " . $gradient_radial_shape . "," : "" ) . " $gradient_start_color, $gradient_end_color);";
			$out .= "background: -o-radial-gradient(" . ( !empty( $gradient_radial_shape ) ? " " . $gradient_radial_shape . "," : "" ) . " $gradient_start_color, $gradient_end_color);";
			$out .= "background: -moz-radial-gradient(" . ( !empty( $gradient_radial_shape ) ? " " . $gradient_radial_shape . "," : "" ) . " $gradient_start_color, $gradient_end_color);";
			$out .= "background: radial-gradient(" . ( !empty( $gradient_radial_shape ) ? " " . $gradient_radial_shape . "," : "" ) . " $gradient_start_color, $gradient_end_color);";
		}
		else if ( $gradient_radial_shape_settings_type == 'extended' ) {
			$out .= "background: -webkit-radial-gradient(" . ( !empty( $gradient_radial_size ) ? " " . $gradient_radial_size . "," : "" ) . ( !empty( $gradient_radial_size_keyword ) ? " " . $gradient_radial_size_keyword . "," : "" ) . " $gradient_start_color, $gradient_end_color);";
			$out .= "background: -o-radial-gradient(" . ( !empty( $gradient_radial_size ) ? " " . $gradient_radial_size . "," : "" ) . ( !empty( $gradient_radial_size_keyword ) ? " " . $gradient_radial_size_keyword . "," : "" ) . " $gradient_start_color, $gradient_end_color);";
			$out .= "background: -moz-radial-gradient(" . ( !empty( $gradient_radial_size ) ? " " . $gradient_radial_size . "," : "" ) . ( !empty( $gradient_radial_size_keyword ) ? " " . $gradient_radial_size_keyword . "," : "" ) . " $gradient_start_color, $gradient_end_color);";
			$out .= "background: radial-gradient(" . ( !empty( $gradient_radial_size_keyword ) && !empty( $gradient_radial_size ) ? " $gradient_radial_size_keyword at $gradient_radial_size" : "" ) . " $gradient_start_color, $gradient_end_color);";
		}
	}
	$out .= "border-color: transparent;-webkit-background-clip: border;-moz-background-clip: border;background-clip: border-box;-webkit-background-origin: border;-moz-background-origin: border;background-origin: border-box;";
	return preg_replace('/\s+/',' ', $out);
}

/******************** \CUSTOM COLOR ********************/

function the8_is_woo() {
	global $woocommerce;
	return ! empty( $woocommerce ) ? is_woocommerce() || is_shop() || is_product() || is_product_tag() || is_product_category() || is_account_page() ||  is_cart() || is_checkout() : false;
}

if ( ! isset( $content_width ) ) $content_width = 1170;

function the8_get_sidebars( $p_id = null ) { /*!*/
	if ($p_id){
		$post_type = get_post_type($p_id);
		if ( in_array( $post_type, array( "page" ) ) ) {
			$cws_stored_meta = get_post_meta ($p_id, THE8_MB_PAGE_LAYOUT_KEY);
			$sidebar1 = $sidebar2 = $sidebar_pos = $sb_block = '';
			$page_type = "page";
				if ( isset( $cws_stored_meta[0] ) && !empty( $cws_stored_meta[0] ) ) {
					$sidebar_pos = $cws_stored_meta[0]['sb_layout'];
					if ($sidebar_pos == 'default') {
						if($cws_stored_meta[0]['is_blog'] !== '0' ) {
							$page_type = "blog";
						}
						else if(is_front_page()) {
							$page_type = "home";
						}

						$sidebar_pos = the8_get_option("def-" . $page_type . "-layout");
						$sidebar1 = the8_get_option("def-" . $page_type . "-sidebar1");
						$sidebar2= the8_get_option("def-" . $page_type . "-sidebar2");
					}
					else{
						$sidebar1 = isset( $cws_stored_meta[0]['sidebar1'] ) ? $cws_stored_meta[0]['sidebar1'] : '';
						$sidebar2 = isset( $cws_stored_meta[0]['sidebar2'] ) ? $cws_stored_meta[0]['sidebar2'] : '';
					}
				}
				else{
					$page_type = "page";
					$sidebar_pos = the8_get_option("def-" . $page_type . "-layout");
					$sidebar1 = the8_get_option("def-" . $page_type . "-sidebar1");
					$sidebar2= the8_get_option("def-" . $page_type . "-sidebar2");
				}
			}
		else if ( in_array( $post_type, array( 'post' ) ) ) {
			$sidebar_pos = the8_get_option("def-blog-layout");
			$sidebar1 = the8_get_option("def-blog-sidebar1");
			$sidebar2 = the8_get_option("def-blog-sidebar2");
		}
		else if( in_array( $post_type, array( 'attachment', 'cws_portfolio', 'cws_staff' ) ) ) {
			$sidebar_pos = the8_get_option("def-page-layout");
			$sidebar1 = the8_get_option("def-page-sidebar1");
			$sidebar2 = the8_get_option("def-page-sidebar2");
		}
	}
	else if (is_home()) { 										/* default home page hasn't ID */
		$sidebar_pos = the8_get_option("def-home-layout");
		$sidebar1 = the8_get_option("def-home-sidebar1");
		$sidebar2 = the8_get_option("def-home-sidebar2");
	}
	else if ( is_category() || is_tag() || is_archive() ) {
		$sidebar_pos = the8_get_option("def-blog-layout");
		$sidebar1 = the8_get_option("def-blog-sidebar1");
		$sidebar2 = the8_get_option("def-blog-sidebar2");
		if (get_post_type() == 'cws_portfolio' || get_post_type() == 'cws_staff') {
			$page_type = "page";
			$sidebar_pos = the8_get_option("def-" . $page_type . "-layout");
			$sidebar1 = the8_get_option("def-" . $page_type . "-sidebar1");
			$sidebar2= the8_get_option("def-" . $page_type . "-sidebar2");
		}
	}
	else if ( is_search() ) {
		$sidebar_pos = the8_get_option("def-page-layout");
		$sidebar1 = the8_get_option("def-page-sidebar1");
		$sidebar2 = the8_get_option("def-page-sidebar2");
	}

	$ret = array();
	$ret['sb_layout'] = isset( $sidebar_pos ) ? $sidebar_pos : '';
	$ret['sidebar1'] = isset( $sidebar1 ) ? $sidebar1 : '';
	$ret['sidebar2'] = isset( $sidebar2 ) ? $sidebar2 : '';

	$sb_enabled = $ret['sb_layout'] != 'none';
	$ret['sb1_exists'] = $sb_enabled && is_active_sidebar( $ret['sidebar1'] );
	$ret['sb2_exists'] = $sb_enabled && $ret['sb_layout'] == 'both' && is_active_sidebar( $ret['sidebar2'] );

	$ret['sb_exist'] = $ret['sb1_exists'] || $ret['sb2_exists'];
	$ret['sb_layout_class'] = ( $ret['sb1_exists'] xor $ret['sb2_exists'] ) ? 'single' : ( ( $ret['sb1_exists'] && $ret['sb2_exists'] ) ? 'double' : '' );

	return $ret;
}

function the8_get_option($name) {
	$theme_options = get_option( 'the8' );
	return isset($theme_options[$name]) ? $theme_options[$name] : null;
}

function the8_widget_title_icon_rendering( $args = array() ) {
	extract( shortcode_atts(
		array(
			'icon_type' => '',
			'icon_fa' => '',
			'icon_img' => array(),
			'icon_color' => '#fff',
			'icon_bg_type' => 'color',
			'icon_bgcolor' => THE8_COLOR,
			'gradient_first_color' => THE8_COLOR,
			'gradient_second_color' => '#0eecbd',
			'gradient_type' => '',
			'gradient_linear_angle' => '',
			'gradient_radial_shape' => '',
			'gradient_radial_type' => '',
			'gradient_radial_size_key' => '',
			'gradient_radial_size' => '',
			), $args));

	$is_benefits_area_rendered = isset( $GLOBALS['benefits_area_is_rendered'] );
	$r = '';
	$icon_styles = '';
	if ( $icon_type == 'fa' && !empty( $icon_fa ) ) {
		if ( $icon_bg_type == 'none' ) {
			$icon_styles .= "border-width: 1px; border-style: solid;";
		}else if ( $icon_bg_type == 'color' ) {
			$icon_styles .= "background-color:$icon_bgcolor;border-color:$icon_bgcolor;";
		}
		else if ( $icon_bg_type == 'gradient' ) {
			$gradient_settings = the8_extract_array_prefix($args, 'gradient');


			$gradient_settings_arr = array(
				'first_color' => $gradient_settings["first_color"],
				'second_color' => $gradient_settings["second_color"],
				'type' => $gradient_settings["type"],
				'linear_settings' => array(
					'@' =>  array(
						'angle' => $gradient_settings["linear_angle"],
						),
					),
				'radial_settings' => array(
					'@' =>  array(
						'shape_settings' => $gradient_settings["radial_shape"],
						'shape' => $gradient_settings["radial_type"],
						'size_keyword' => $gradient_settings["radial_size_key"],
						'size' => $gradient_settings["radial_size"],
						),
					),
			);

			$gradient_settings = isset( $gradient_settings_arr ) ? $gradient_settings_arr : new stdClass ();
			$settings = new stdClass();

			foreach ($gradient_settings_arr as $key => $value){
			    $settings->$key = $value;
			}

			$settings_arr = array('@' => $settings);

			$icon_styles .= the8_render_gradient_rules( array( 'settings' => $settings_arr ) );
		}
		$icon_styles .= "color:$icon_color;";
		$r .= "<i class='$icon_fa'" . ( !empty( $icon_styles ) ? " style='$icon_styles'" : "" ) . "></i>";
	}
	else if ( $icon_type == 'img' && isset( $icon_img['src'] ) && !empty( $icon_img['src'] ) ) {
		$img_url = $icon_img['src'];
		$body_font_settings = the8_get_option( 'body-font' );
		$font_size = isset( $body_font_settings['font_size'] ) ? preg_replace( 'px', '', $body_font_settings['font_size'] ) : '15';
		$thumb_size = $is_benefits_area_rendered ? 102 : (int)round( (float)$font_size * 2 );
		$thumb_obj = bfi_thumb( $img_url, array( 'width' => $thumb_size, 'height' => $thumb_size ), false );
		$thumb_url = esc_url($thumb_obj[0]);
		$retina_thumb_url = '';
		$retina_thumb_exists  = false;
		if (!empty($thumb_obj[3])) {
		       extract( $thumb_obj[3] );
		       $retina_thumb_url = esc_attr($retina_thumb_url);
		}
		$icon_styles = esc_attr($icon_styles);
		if ( $retina_thumb_exists ) {
			$r .= "<img src='$thumb_url' data-at2x='$retina_thumb_url'" . ( !empty( $icon_styles ) ? " style='$icon_styles'" : "" ) . " alt />";
		}
		else{
			$r .= "<img src='$thumb_url' data-no-retina" . ( !empty( $icon_styles ) ? " style='$icon_styles'" : "" ) . " alt />";
		}
	}
	return $r;
}

function the8_extract_array_prefix($arr, $prefix) {
	$ret = array();
	$pref_len = strlen($prefix);
	foreach ($arr as $key => $value) {
		if (0 === strpos($key, $prefix . '_') ) {
			$ret[mb_substr($key, $pref_len+1)] = $value;
		}
	}
	return $ret;
}

function the8_post_info_part($custom_layout_arr = false) {
	$old_version = 0;
	$show_author = the8_get_option( "blog_author" );
	$permalink = get_permalink();
	$title = esc_html( get_the_title() );
	ob_start();
		the8_output_media_part($custom_layout_arr);
	$media_content = ob_get_clean();
	$section_class = "post_info_part";
	$section_class .= empty( $media_content ) ? " full_width" : '';
	$section_class .= $old_version == 1 ? " old_version" : '';
	$header_class = "post_info_header";
	$post_format = get_post_format();

	$date = esc_html( get_the_time( get_option( 'date_format' ) ) );
	$first_word_boundary = strpos( $date, ' ' );

	$header_class .= ( in_array( $post_format, array( 'quote', 'audio' ) ) || ( $post_format == 'link' && !has_post_thumbnail() ) ) ? " rounded" : '';
	if ($old_version) {
	?>
	<div class="<?php echo $section_class; ?>">
		<div class="post_info_box">
			<div class="<?php echo $header_class; ?>">
				<div class="date">
					<?php
					if (empty( $title )) {
						echo "<a href='".$permalink."'>";
					}
					$date = get_the_time( get_option("date_format") );
					$first_word_boundary = strpos( $date, " " );
					if ( $first_word_boundary ) {
						$first_word = esc_html(mb_substr( $date, 0, $first_word_boundary ));
						$date = "<span class='first_word'>$first_word</span>" . esc_html(trim(mb_substr( $date, $first_word_boundary + 1)));
					}
					echo $date;
					if (empty( $title )) {
						echo "</a>";
					}
					?>
				</div>
				<div class="post_info">
					<?php
						$author = '';
						$author .= $show_author ? esc_html(get_the_author()) : '';
						$special_pf = the8_is_special_post_format();
						if ( !empty($author) || $special_pf ) {
							echo "<div class='info'>";
								echo !empty($author) ? (esc_html_e('by ', 'the8'))."<span>$author</span>" : '';
								echo $special_pf ? ( !empty($author) ? THE8_V_SEP : "" ) . the8_post_format_mark() : '';
							echo "</div>";
						}
						$comments_n = get_comments_number();
						if ( (int)$comments_n > 0 ) {
							$permalink .= "#comments";
							echo "<div class='comments_link'><a href='$permalink'>$comments_n <span><i class='fa fa-comment-o'></i></span></a></div>";
						}
					?>
				</div>
			</div>
			<?php
				echo $media_content;
			?>
		</div>
	</div>
	<?php
	}else{
		if (!empty($media_content)) {
		?>
			<div class="<?php echo $section_class; ?>">
				<?php
					echo $media_content;
				?>
			</div>

		<?php

		}

		if ($custom_layout_arr['date_style'] != 'none') {
			?>

				<div class="date new_style">
					<?php
					echo "<a href='".$permalink."'>";
					if ( $first_word_boundary ) {

						$first_word_full = esc_attr( mb_substr( $date, 0, $first_word_boundary ) );

						$first_word_short = esc_attr( mb_substr( $date, 0, 3 ) );
						$date = "<div class='date-cont'><span class='day'>". esc_html( str_replace( ',','',mb_substr( $date, $first_word_boundary + 1, 2 ) ) )."</span><span class='month' title='$first_word_full'><span>$first_word_short</span></span><span class='year'>". esc_html( mb_substr( $date, -4 ) ) ."</span><i class='springs'></i></div>";
						echo $date;
					}
						echo "</a>";
					?>
				</div>
			<?php
		}
	}
}

	function the8_output_media_part ( $custom_layout_arr = false ) {
		$column_style = $custom_layout_arr['column_style'];
		$custom_layout = intval( $custom_layout_arr['custom_layout'] );
		$use_blur = the8_get_option( 'use_blur' ) == 1 ? true : false;
		$post_url = esc_url(get_the_permalink());
		$single = is_single();
		$post_format = get_post_format( );
		$eq_thumb_height = in_array( $post_format, array( 'gallery' ) );
		$media_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
		$media_meta = isset( $media_meta[0] ) ? $media_meta[0] : array();

		$thumbnail = has_post_thumbnail( ) ? wp_get_attachment_image_src( get_post_thumbnail_id( ),'full' ) : '';
		$thumbnail = ! empty( $thumbnail ) ? $thumbnail[0] : '';
		$thumbnail_dims = the8_get_post_thumbnail_dims( $eq_thumb_height );

		$real_thumbnail_dims = array();
		if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[1] ) ) $real_thumbnail_dims['width'] = $thumbnail_props[1];
		if ( !empty(  $thumbnail_props ) && isset( $thumbnail_props[2] ) ) $real_thumbnail_dims['height'] = $thumbnail_props[2];
		$thumbnail_dims = the8_get_post_thumbnail_dims( $eq_thumb_height, $real_thumbnail_dims );
		$crop_thumb = isset( $thumbnail_dims['width'] ) && $thumbnail_dims['width'] > 0;
		$thumb_media = false;

		$image_data = wp_get_attachment_metadata( get_post_thumbnail_id( get_the_ID() ) );
		if ( ! empty( $thumbnail ) ) {
			if ( $single ) {
				if ( $image_data['width'] < 1170 ) {
					$img_data['width'] = 0;
					$img_data['height'] = 0;
				} else {
					$img_data = $thumbnail_dims;
				}
			} else {
				$img_data = $thumbnail_dims;
			}
		}

		$only_link = ($post_format == 'link' && empty( $thumbnail ) ) ? ' only_link' : '';
		$quote = ('quote' === $post_format && isset( $media_meta['quote'] )) ? $media_meta['quote'] : '';
		$quote_post = ( ! empty( $quote )) ? ' quoute_post' : '';
		$video_post = ('video' === $post_format)  ? ' video_post' : '';
		$audio_post = ( 'audio' === $post_format && isset( $media_meta['audio'] ) ) ? ' audio_post' : '';
		$audio_post .= isset( $media_meta['audio'] ) ? ( is_int( strpos( $media_meta['audio'], 'https://soundcloud' ) ) ? ' soundcloud' : '') : '';
		$gallery_post = ('gallery' === $post_format && isset( $media_meta['gallery'] ) && !empty($media_meta['gallery'])) ? ' gallery_post' : '';
		$some_media = false;
		ob_start();
		?>
			<div class="media_part <?php echo $only_link;
			echo $quote_post;
			echo $video_post;
			echo $audio_post;
			echo $gallery_post;?>">
				<?php
					switch ($post_format) {
						case 'link':
							$link = isset( $media_meta['link'] ) ? esc_url( $media_meta['link'] ) : '';
							if ( !empty($thumbnail) ) {
								?>
								<div class="pic <?php echo !empty( $link ) ? 'link_post' : ''; echo $use_blur && (( ($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && (isset($media_meta['enable_lightbox'])) && $media_meta['enable_lightbox'] == '1')) ? ' blured' : ''; ?>">
									<?php
									echo !empty($link) ? "<a href='".esc_url($link)."'>" : '';
									$thumb_obj = bfi_thumb( $thumbnail,$img_data,false );
									$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
									?>
									<img <?php echo $thumb_path_hdpi;
									$some_media = true; ?> alt />

									<?php

									if ( !empty( $link ) ) {
										echo "<div class='link'><span>$link</span></div>";
									} elseif(( ($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && (isset($media_meta['enable_lightbox']) && $media_meta['enable_lightbox'] == '1')) )  {
										echo "<div class='hover-effect'></div>";
										echo "<div class='links'>
												<a class='fancy cwsicon-magnifying-glass84' href='".esc_url($thumbnail)."'></a>
							  " . ( !$single ? "<a class='cwsicon-links21' href='".esc_url($post_url)."'></a>" : "" ) . "
							  				  </div>";
									}
									echo !empty($link) ? "</a>" : '';
									?>
								</div>
								<?php
								$thumb_media = true;
								$some_media = true;
							}
							else{
								if ( !empty( $link ) ) {
									echo "<div class='link'><a href='".esc_url($link)."'>$link</a></div>";
									$some_media = true;
								}
							}
							break;
						case 'video':
							$video = isset( $media_meta['video'] ) ? $media_meta['video'] : '';
							if ( ! empty( $video ) ) {
								$unknown_video_service = false;
								$video_service = '';
								if (strpos($video, 'youtu')){
									$video_service = 'youtube';
								} elseif (strpos($video, 'vimeo')) {
									$video_service = 'vimeo';
								} else{								
									$unknown_video_service = true;
								}

								$video_dims = the8_get_post_thumbnail_dims( false );

								if ( !empty( $video_service ) ) {
									preg_match('@[^/]*$@', $video, $video_link);
									$clear_url = array("?", "&amp", "watchv=");
									$video_id = str_replace($clear_url, '', preg_replace('/[^?][a-z]*=\w+/', '', $video_link[0]));
									$video_url = ($video_service == 'youtube' ? str_replace('watch?v=', '', $video_link[0]) :  $video_link[0]);

									if ($video_service == 'youtube'){
										$thumbnail_img = "http://img.youtube.com/vi/".esc_attr($video_id)."/maxresdefault.jpg";
										$link = "http://www.youtube.com/embed/";
										(strpos($video, 't=') ? preg_match('@(t=)[0-9a-z]+@', $video, $time) : '');
										$video_time = isset($time[0]) ? $time[0] : '';
									} elseif ($video_service == 'vimeo') {								
										$json = json_decode(file_get_contents("https://vimeo.com/api/oembed.json?url=".$video));
										$vimeo_id = $json->video_id;
										$thumbnail_img = $json->thumbnail_url;
										$link = "https://player.vimeo.com/video/";
										$video_url = ($video_id != $vimeo_id ? str_replace($video_id, $vimeo_id, $video_url) : $video_url);
									}
									$embed_link = $link.esc_attr($video_url).(!$single ? (($video_id != $video_url ? '&amp;' : '?')."autoplay=1") : '');
								}								
								ob_start();
								?>
									<div class='video'>
										<?php if ( !$single && $custom_layout_arr['use_carousel'] == '1' && $media_meta['enable_lightbox'] == '1' && !$unknown_video_service) { ?>
											<div class='pic'>								
											
											<?php if (!empty($thumbnail) ){
												$thumb_obj = bfi_thumb( $thumbnail,$img_data,false );
												$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";										
												echo "<img $thumb_path_hdpi alt />";
												$some_media = true;
											} else {
											?>
												<img src='<?php echo esc_url($thumbnail_img);?>' alt=''>
											<?php } ?>
							
												<div class='hover-effect'></div>
												<div class='links'>
													<a class="fancy fancybox.iframe" href="<?php echo esc_url($embed_link);?>"><i class='play_video fa fa-play-circle'></i></a>
												</div>
											</div>
										<?php } else {
											echo apply_filters( 'the_content',"[embed width='" . $video_dims['width'] . "']" .($video_service == 'youtube' ? 'https://youtu.be/'.$video_id : $video ).(!empty($video_time) ? '?'.$video_time : '').'[/embed]' ); 											
										} ?>
									</div>
									
								<?php
								$result = ob_get_clean();
								echo $result;
								$some_media = true;
							}
							break;
						case 'audio':
							$audio = isset( $media_meta['audio'] ) ? $media_meta['audio'] : '';
							$is_sounfcloud = is_int( strpos( (string) $audio, 'https://soundcloud' ) );

							if ( $is_sounfcloud == false ) {
								if ( ! empty( $thumbnail ) ) {
									$thumb_obj = bfi_thumb( $thumbnail,$img_data,false );
									$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
									echo "<div class='pic".($use_blur ? ' blured' : '')."'>
												<img ". $thumb_path_hdpi ." alt />
												".($use_blur ? "<img ". $thumb_path_hdpi ." class='blured-img' alt />" : '' )."
											</div>";
									$thumb_media = true;
									$some_media = true;
								}
								if ( ! empty( $audio ) ) {
									echo "<div class='audio'>" . apply_filters( 'the_content','[audio src="' . esc_url( $audio ) . '"]' ) . '</div>';
									$some_media = true;
								}
							} else {
								echo apply_filters( 'the_content',"$audio" );
								$some_media = true;
							}

							break;
						case 'quote':
							$quote = isset( $media_meta['quote'] ) ? $media_meta['quote']['quote'] : '';
							$author = isset( $media_meta['quote']['author'] ) ? $media_meta['quote']['author'] : '';
							$avatar = isset( $media_meta['quote']['avatar'] ) ? $media_meta['quote']['avatar'] : '';
							if ( !empty( $quote ) ) {
								echo the8_testimonial_renderer( (!empty($avatar['src']) ? $avatar['src'] : ''), $quote, $author );
								$some_media = true;
							}
							if (!empty($thumbnail) && !empty( $quote )) {
								echo '<div class="testimonial_bg" style="background-image: url('.esc_attr($thumbnail).');background-position: center center;"></div>';
							}
							break;
						case 'gallery':
							$gallery = isset( $media_meta['gallery'] ) ? $media_meta['gallery'] : '';
							if ( !empty( $gallery ) ) {
								$match = preg_match_all("/\d+/",$gallery,$images);
								if ($match){
									$images = $images[0];
									$image_srcs = array();
									foreach ( $images as $image ) {
										$image_src = wp_get_attachment_image_src($image,'full');
										$image_url = $image_src[0];
										array_push( $image_srcs, $image_url );

									}
									$thumb_media = $some_media = count( $image_srcs ) > 0 ? true : false;
									$carousel = count($image_srcs) > 1 ? true : false;
									$gallery_id = uniqid( 'cws-gallery-' );
									if ($carousel) {
										wp_enqueue_script ('owl_carousel');
										wp_enqueue_script ('isotope');
									}
									echo  $carousel ? "<a class='carousel_nav prev'><span></span></a>
														<a class='carousel_nav next'><span></span></a>
														<div class='gallery_post_carousel'>" : '';
									foreach ( $image_srcs as $image_src ) {
										$img_obj = bfi_thumb( $image_src, $thumbnail_dims , false );
										$img_url = esc_url($img_obj[0]);
										$retina_img_exists = $img_obj[3]['retina_thumb_exists'];
										$retina_img_url = esc_attr($img_obj[3]['retina_thumb_url']);
										?>
										<div class='pic<?php echo $use_blur && (( ($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && (isset($media_meta['enable_lightbox'])) && $media_meta['enable_lightbox'] == '1')) ? ' blured' : ''  ?>'>
											<?php
											if ( $retina_img_exists ) {
												echo "<img src='".esc_url($img_url)."' data-at2x='$retina_img_url' alt />";
												echo $use_blur  && ((($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && ( isset($media_meta['enable_lightbox']) && ($media_meta['enable_lightbox'] == '1')))) ? "<img src='$img_url' data-at2x='$retina_img_url' class='blured-img' alt />" : "";
											}
											else{
												echo "<img src='".esc_url($img_url)."' data-no-retina alt />";
												echo $use_blur  && ((($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && ( isset($media_meta['enable_lightbox']) && ($media_meta['enable_lightbox'] == '1')))) ? "<img src='".esc_url($img_url)."' data-no-retina class='blured-img' alt />" : "";

											}
											if (( ($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && (isset($media_meta['enable_lightbox'])) && $media_meta['enable_lightbox'] == '1'))  {
											?>
												<div class="hover-effect"></div>
												<div class="links">
													<a href="<?php echo esc_url($image_src); ?>" <?php echo $carousel ? " data-fancybox-group='".esc_attr($gallery_id)."'" : ''; ?> class="fancy <?php echo $carousel ? 'cwsicon-photo246 fancy_gallery' : 'cwsicon-magnifying-glass84'; ?>"></a>
												</div>
											<?php } ?>
										</div>
										<?php
									}
									echo  $carousel ? "</div>" : '';
								}
								$some_media = true;
							}
							break;
					}
					if ( !$some_media && !empty( $thumbnail ) ) {
						$thumb_obj = bfi_thumb( $thumbnail, $img_data, false );
						$thumb_url = esc_url($thumb_obj[0]);
						$retina_thumb_url = '';
						if (!empty($thumb_obj[3])) {
						       extract( $thumb_obj[3] );
						       $retina_thumb_url = esc_attr($retina_thumb_url);
						}
						echo "<div class='pic".( $use_blur && (( ($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && (isset($media_meta['enable_lightbox'])) && $media_meta['enable_lightbox'] == '1')) ? ' blured' : '' )."'>";

						// add urls to the images when lightbox option is off
						$cws_add_closing_a = "";
						if ( !(( ($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && (isset($media_meta['enable_lightbox'])) && $media_meta['enable_lightbox'] == '1'))) {
							echo ("<a href='$post_url'>");
							$cws_add_closing_a = "</a>";
						}
						
							if ( $retina_thumb_exists ) {
								echo "<img src='".esc_url($thumb_url)."' data-at2x='$retina_thumb_url' alt />";
								echo $use_blur  && ((($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && ( isset($media_meta['enable_lightbox']) && ($media_meta['enable_lightbox'] == '1')))) ? "<img src='".esc_url($thumb_url)."' data-at2x='$retina_thumb_url' class='blured-img' alt />" : "";
							}
							else{
								echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
								echo $use_blur  && ((($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && ( isset($media_meta['enable_lightbox']) && ($media_meta['enable_lightbox'] == '1')))) ? "<img src='".esc_url($thumb_url)."' data-no-retina class='blured-img' alt />" : "";

							}

							if (( ($custom_layout == 1) && (intval( $custom_layout_arr['enable_lightbox'] ) == 0) ) || (($custom_layout != 1) && (isset($media_meta['enable_lightbox'])) && $media_meta['enable_lightbox'] == '1'))  {
								echo "<div class='hover-effect'></div>";
								echo "<div class='links'><a class='fancy cwsicon-magnifying-glass84' href='".esc_url($thumbnail)."'></a>" . ( !$single ? "<a class='cwsicon-links21' href='$post_url'></a>" : "" ) . "</div>";
							}

						echo ($cws_add_closing_a . "</div>");
						$thumb_media = true;
						$some_media = true;
					}
				?>
			</div>
		<?php
		$some_media ? ob_end_flush() : ob_end_clean();
	}

	if(!function_exists('the8_get_post_thumbnail_dims')) {
		function the8_get_post_thumbnail_dims ( $eq_thumb_height = false, $real_dims = array() ) {
			$p_id = get_queried_object_id();
			$blogtype = is_page() ? the8_get_page_meta_var( array( "blog", "blogtype" ) ) : the8_get_option( "def_blogtype" );

			$sb = the8_get_sidebars( $p_id );
			$sb_block = isset( $sb['sb_layout'] ) ? $sb['sb_layout'] : 'none';
			$single = is_single();
			$width_correction =  0;
			$height_correction = 0;
			$dims = array( 'width'=>0, 'height'=>0 );
			if ($single){
				if ($sb_block == 'none') {
					if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) && $real_dims['width'] > 1170 ) ) || $eq_thumb_height ) {
						$dims['width'] = 1170;
						if ( $eq_thumb_height ) $dims['height'] = 659;
					}
				}
				else if (in_array($sb_block, array('left','right'))) {
					if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) && $real_dims['width'] > 870 ) ) || $eq_thumb_height ) {
						$dims['width'] = 870;
						if ( $eq_thumb_height ) $dims['height'] = 490;
					}
				}
				else if ($sb_block == 'both') {
					if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) && $real_dims['width'] > 570 ) ) || $eq_thumb_height ) {
						$dims['width'] = 570;
						if ( $eq_thumb_height ) $dims['height'] = 321;
					}
				}
			}
			else{
				switch ($blogtype){
					case "large":
						if ($sb_block == 'none') {
							$dims['width'] = 1170;
							$dims['height'] = 659;
						}
						else if (in_array($sb_block, array('left','right'))) {
							$dims['width'] = 870;
							$dims['height'] =  490;
						}
						else if ($sb_block == 'both') {
							$dims['width'] = 570;
							$dims['height'] =  321;
						}
						break;
					case "medium":
						$dims['width'] = 570;
						$dims['height'] = 321;
						break;
					case "small":
						$dims['width'] = 370;
						$dims['height'] = 208;
						break;
					case '2':
						if ($sb_block == 'none') {
							$dims['width'] = 570;
							$dims['height'] = 321;
						}
						else if (in_array($sb_block, array('left','right'))) {
							$dims['width'] = 420;
							$dims['height'] =  237;
						}
						else if ($sb_block == 'both') {
							$dims['width'] = 270;
							$dims['height'] =  152;
						}
						break;
					case '3':
						if ($sb_block == 'none') {
							$dims['width'] = 370;
							$dims['height'] = 208;
						}
						else if (in_array($sb_block, array('left','right'))) {
							$dims['width'] = 270;
							$dims['height'] =  152;
						}
						else if ($sb_block == 'both') {
							$dims['width'] = 270;
							$dims['height'] =  152;
						}
						break;
				}
			}
			$dims['width'] = $dims['width'] != 0 ? $dims['width'] - $width_correction : $dims['width'];
			$dims['height'] = $dims['height'] != 0 ? $dims['height'] - $height_correction : $dims['height'];
			return $dims;
		}
	}

	function the8_get_page_meta_var ( $keys ) {
		$p_meta = array();
		if ( isset( $GLOBALS['the8_page_meta'] ) && !empty($keys) ) {
			$p_meta = $GLOBALS['the8_page_meta'];
			if ( is_string( $keys ) ) {
				if ( isset( $p_meta[$keys] ) ) {
					return $p_meta[$keys];
				}
			} else if ( is_array( $keys ) ) {
				for ( $i=0; $i < count($keys); $i++ ) {
					if ( isset( $p_meta[$keys[$i]] ) ) {
						if ( $i < count($keys) - 1 ) {
							if ( is_array( $p_meta[$keys[$i]] ) ) {
								$p_meta = $p_meta[$keys[$i]];
							}	else {
								return false;
							}
						}	else {
							return $p_meta[$keys[$i]];
						}
					}	else {
						return false;
					}
				}
			}
		}
		return false;
	}

	function the8_set_page_meta_var($keys, $value = '') {
		$p_meta = array();
	if (isset($GLOBALS['the8_page_meta']) && !empty($keys) ) {
		$p_meta = &$GLOBALS['the8_page_meta'];

			if ( is_string( $keys ) ) {
				if ( isset($p_meta[$keys]) ) {
					$p_meta[$keys] = $value;
					return true;
				}
			} else if ( is_array( $keys ) && !empty( $keys ) ) {
				for ( $i=0; $i < count($keys); $i++ ) {
					if ( isset( $p_meta[$keys[$i]] ) ) {
						if ( $i < count($keys) - 1 ) {
							if ( is_array( $p_meta[$keys[$i]] ) ) {
								$p_meta = &$p_meta[$keys[$i]];
							} else {
								return false;
							}
						}	else {
							$p_meta[$keys[$i]] = $value;
							return true;
						}
					}	else {
						return false;
					}
				}
			}
		}
		return false;
	}

	function the8_blog_output ( $query = false ) {
		$custom_layout_arr = array(
			'this_shortcode' => isset( $query->query_vars['this_shortcode'] ) ? $query->query_vars['this_shortcode'] : false,
			'column_style' => isset( $query->query_vars['column_style'] ) ? $query->query_vars['column_style'] : false,
			'custom_layout' => isset( $query->query_vars['custom_layout'] ) ? $query->query_vars['custom_layout'] : 0,
			'post_text_length' => ! empty( $query->query_vars['post_text_length'] ) ? $query->query_vars['post_text_length'] : '',
			'button_name' => ! empty( $query->query_vars['button_name'] ) ? $query->query_vars['button_name'] : '',
			'use_carousel' => isset( $query->query_vars['use_carousel'] ) ? $query->query_vars['use_carousel'] : 0,
			'enable_lightbox' => isset( $query->query_vars['enable_lightbox'] ) ? $query->query_vars['enable_lightbox'] : '',
			'cws_rm_image_url' => isset( $query->query_vars['cws_rm_image_url'] ) ? $query->query_vars['cws_rm_image_url'] : '',
			'hide_meta' => isset( $query->query_vars['hide_meta'] ) ? $query->query_vars['hide_meta'] : '',
			'date_style' => isset( $query->query_vars['date_style'] ) ? $query->query_vars['date_style'] : '',
			'boxed_style' => isset( $query->query_vars['boxed_style'] ) ? $query->query_vars['boxed_style'] : '',
			'column_count' => isset( $query->query_vars['column_count'] ) ? intval( $query->query_vars['column_count'] ) : 3,
		);

		global $wp_query;
		$query = $query ? $query : $wp_query;

		if ( is_page() ) {
			$blogtype = the8_get_page_meta_var( array( 'blog', 'blogtype' ) );
		} else {
			$blogtype = the8_get_option( 'def_blogtype' );
		}

		if ($blogtype == 'default') {
			$blogtype = the8_get_option( 'def_blogtype' );
		}

		if (!empty($custom_layout_arr['date_style']) && $custom_layout_arr['date_style'] == 'unwrapped') {
			$blogtype .= ' unwrapped_date';
		}

		if (!empty($custom_layout_arr['boxed_style']) && $custom_layout_arr['boxed_style'] != 'none') {
			$blogtype .= ' boxed_style';
			if ($custom_layout_arr['boxed_style'] == 'with_shadow') {
				$blogtype .= ' with_shadow';
			}
		}

		if ($query->have_posts()):
			ob_start();
			while($query->have_posts()):
				$query->the_post();
				echo "<article class='item ".$blogtype."'>";
					the8_post_output( $custom_layout_arr );
				echo "</article>";
			endwhile;
			wp_reset_postdata();
			ob_end_flush();
		endif;
	}

	function the8_load_more( $paged = 0, $template = "", $max_paged = PHP_INT_MAX ) {
		?>
			<a class="cws_button large cws_load_more icon-on" href="#" data-paged="<?php echo $paged; ?>" data-max-paged="<?php echo $max_paged; ?>" data-template="<?php echo $template; ?>"><?php echo esc_html__( "Load More", 'the8' ); ?><i class="button-icon fa fa-refresh"></i></a>
		<?php
	}

	function the8_pagination ( $paged=1, $max_paged=1 ) {
		$pagenum_link = html_entity_decode( get_pagenum_link() );
		$query_args   = array();
		$url_parts	= explode( '?', $pagenum_link );

		if ( isset( $url_parts[1] ) ) {
			wp_parse_str( $url_parts[1], $query_args );
		}

		$permalink_structure = get_option('permalink_structure');

		$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
		$pagenum_link = $permalink_structure ? trailingslashit( $pagenum_link ) . '%_%' : trailingslashit( $pagenum_link ) . '?%_%';
		$pagenum_link = add_query_arg( $query_args, $pagenum_link );

		$format  = $permalink_structure && preg_match( '#^/*index.php#', $permalink_structure ) && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
		$format .= $permalink_structure ? user_trailingslashit( 'page/%#%', 'paged' ) : 'paged=%#%';
		?>
		<div class='pagination separated'>
			<div class='page_links'>
			<?php
			$pagination_args = array( 'base' => $pagenum_link,
				'format' => $format,
				'current' => $paged,
				'total' => $max_paged,
				"prev_text" => "<i class='fa fa-angle-left'></i>",
				"next_text" => "<i class='fa fa-angle-right'></i>",
				"link_before" => "",
				"link_after" => "",
				"before" => "",
				"after" => "",
				"mid_size" => 2,
			);
			$pagination = paginate_links($pagination_args);
			echo $pagination;
			?>
			</div>
		</div>
		<?php
	}

	function the8_page_links() {
		$args = array(
		 'before'		   => ''
		,'after'			=> ''
		,'link_before'	  => '<span>'
		,'link_after'	   => '</span>'
		,'next_or_number'   => 'number'
		,'nextpagelink'	 =>  esc_html__("Next Page",'the8')
		,'previouspagelink' => esc_html__("Previous Page",'the8')
		,'pagelink'		 => '%'
		,'echo'			 => 0 );
		$pagination = wp_link_pages( $args );
		echo !empty( $pagination ) ? "<div class='pagination'><div class='page_links'>$pagination</div></div>" : '';
	}

	function the8_post_output ( $custom_layout_arr = false ) {
		$old_version = 0;
		$pid = get_the_id();
		$is_single = is_single( $pid );
		$title = esc_html( get_the_title() );
		$permalink = esc_url( get_the_permalink() );
		$show_author = the8_get_option( "blog_author" );
		if ($old_version == 1) {
			echo !empty( $title ) ?
			THE8_BEFORE_CE_TITLE . "<div>" . ( !$is_single ? "<a href='$permalink'>" : "" ) . $title . ( !$is_single ? "</a>" : "" ) . "</div>" . THE8_AFTER_CE_TITLE : '';
		}
		the8_post_info_part($custom_layout_arr);
		if ($old_version == 0) {
			echo !empty( $title ) ?
			THE8_BEFORE_CE_TITLE . "<div>" . ( !$is_single ? "<a href='$permalink'>" : "" ) . $title . ( !$is_single ? "</a>" : "" ) . "</div>" . THE8_AFTER_CE_TITLE : '';
			?>

					<?php
						$author = '';
						$author .= $show_author ? esc_html(get_the_author()) : '';
						$special_pf = the8_is_special_post_format();
						$comments_n = get_comments_number();
						if ( ! (intval( $custom_layout_arr['hide_meta'] ) == 1) ) {
							echo '<div class="post_info">';
								if ( !empty($author) || $special_pf ) {
									echo "<div class='info'>";
										echo !empty($author) ? (esc_html_e('by ', 'the8'))."<span>$author</span>" : '';
										echo $special_pf ? ( !empty($author) ? THE8_V_SEP : "" ) . the8_post_format_mark() : '';
									echo "</div>";
								}
								if ( has_category() ) {
									echo "<div class='post_categories'>".(esc_html_e('in ', 'the8'));
									the_category ( THE8_V_SEP );
									echo "</div>";
								}
								if ( has_tag() && $is_single ) {
									echo "<div class='post_tags'>".(esc_html_e('in ', 'the8'));
									the_tags ( THE8_V_SEP );
									echo "</div>";
								}
								if ( (int)$comments_n > 0 ) {
									$permalink .= "#comments";
									echo "<div class='comments_link'><a href='$permalink'>$comments_n <span><i class='fa fa-comment-o'></i></span></a></div>";
								}
							echo "</div>";
						}
					?>
			<?php
		}
		the8_post_content_output( $custom_layout_arr );
		if ($old_version == 1) {
			if ( $is_single ) {
				if ( has_category() ) {
					echo "<div class='post_categories'><i class='fa fa-bookmark'></i>";
					the_category ( THE8_V_SEP );
					echo "</div>";
				}
				if ( has_tag() ) {
					echo "<div class='post_tags'><i class='fa fa-tag'></i>";
					the_tags (  THE8_V_SEP );
					echo "</div>";
				}
			}
		}

		the8_page_links();
	}

	function the8_post_content_output ( $custom_layout_arr = false ) {
		global $post;
		global $more;
		$old_version = 0;
		$more = 0;
		$content = '';
		$button_word = '';
		$button_add = false;
		$chars_count = the8_blog_get_chars_count( $custom_layout_arr['column_count'] );
		$char_length = intval( $custom_layout_arr['post_text_length'] ) !== 0 ? intval( $custom_layout_arr['post_text_length'] ) : $chars_count;

		if ( is_single() ) {
			$content .= $post->post_content;
		} else {
			if ( ! empty( $post->post_excerpt ) ) {
				$content .= $post->post_excerpt;
			} else {
				$button_word = esc_html__( 'Read More', 'the8' );
				$pos = strpos( (string) $post->post_content, '<!--more-->' );
				if ( $pos ) {
					$button_add = true;
				}
				$content .= get_the_content( '[...]' );
			}
		}
		if ( $custom_layout_arr['this_shortcode'] ) {
			$content = ! empty( $post->post_excerpt ) ? $post->post_excerpt : $post->post_content;
			$content = trim( preg_replace( '/[\s]{2,}/u', ' ', strip_shortcodes( strip_tags( $content ) ) ) );
			$content = wptexturize( $content );
			$content_length = strlen( $content );
			if ( $content_length > $char_length ) {
				$button_add = false;
				$content = mb_substr( $content, 0, $char_length );
				if ( strlen( $custom_layout_arr['button_name'] ) !== 0 && $custom_layout_arr['custom_layout'] == 1 ) {
					$button_add = true;
					$button_word = esc_html( $custom_layout_arr['button_name'] );
				} else if ( $custom_layout_arr['custom_layout'] == 0 ) {
					$button_add = true;
					$button_word = esc_html__( 'Read More', 'the8' );
				}
				$content .= "<a class='p_cut' href='".esc_url( get_the_permalink() )."'> ...</a>";
			}
		}

		if (! empty( $content ) ) {
			echo "<div class='post_content clearfix'>" . apply_filters( 'the_content', $content );
			if ( $button_add ) {
				echo "<div class='button_cont'><a href='".esc_url( get_the_permalink() )."' class='cws_button small'>" . $button_word . '</a></div>';
			}
			echo "</div>";
		}else{
			if ( $button_add ) {
				echo "<div class='button_cont'><a href='".esc_url( get_the_permalink() )."' class='cws_button small'>" . $button_word . '</a></div>';
			}
		}
		if ($old_version == 1 && !is_single() ) {
			if ( ! (intval( $custom_layout_arr['hide_meta'] ) == 1) ) {
				if ( has_category() ) {
					echo "<div class='post_categories'><i class='fa fa-bookmark'></i>";
					the_category ( THE8_V_SEP );
					echo "</div>";
				}
				if ( has_tag() ) {
					echo "<div class='post_tags'><i class='fa fa-tag'></i>";
					the_tags ( THE8_V_SEP );
					echo "</div>";
				}
			}
		}
	}

function the8_blog_get_chars_count( $cols = null, $p_id = null ) {
	$number = 155;
	$p_id = isset( $p_id ) ? $p_id : get_queried_object_id();
	$sb = the8_get_sidebars( $p_id );
	$sb_layout = isset( $sb['sb_layout_class'] ) ? $sb['sb_layout_class'] : '';
	switch ( $cols ) {
		case '1':
			switch ( $sb_layout ) {
				case 'double':
					$number = 130;
					break;
				case 'single':
					$number = 200;
					break;
				default:
					$number = 300;
			}
			break;
		case '2':
			switch ( $sb_layout ) {
				case 'double':
					$number = 55;
					break;
				case 'single':
					$number = 90;
					break;
				default:
					$number = 130;
			}
			break;
		case '3':
			switch ( $sb_layout ) {
				case 'double':
					$number = 60;
					break;
				case 'single':
					$number = 60;
					break;
				default:
					$number = 70;
			}
			break;
		case '4':
			switch ( $sb_layout ) {
				case 'double':
					$number = 55;
					break;
				case 'single':
					$number = 55;
					break;
				default:
					$number = 55;
			}
			break;
	}
	return $number;
}

/****************** WALKER *********************/

class The8_Walker_Nav_Menu extends Walker {
	private $elements;
	private $elements_counter = 0;

	function walk ($items, $depth) {
		$this->elements = $this->get_number_of_root_elements($items);
		return parent::walk($items, $depth);
	}

	/**
	 * @see Walker::$tree_type
	 * @since 3.0.0
	 * @var string
	 */
	var $tree_type = array( 'post_type', 'taxonomy', 'custom' );

	/**
	 * @see Walker::$db_fields
	 * @since 3.0.0
	 * @todo Decouple this.
	 * @var array
	 */
	var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	/**
	 * @see Walker::start_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<span class='button_open'></span><ul class=\"sub-menu\">";
		$output .= "<li class=\"menu-item back\"><a href=\"#\">←&nbsp;" . esc_html__('BACK', 'the8') . "</a></li>";
		$output .= "\n";
	}
	/**
	 * @see Walker::end_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}
	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . sanitize_html_class( $item->ID );

		if ($item->menu_item_parent=="0") {
			$this->elements_counter += 1;
			if ($this->elements_counter>$this->elements/2){
				array_push($classes,'right');
			}
		}

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . $class_names  . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. sanitize_html_class( $item->ID ), $item, $args );
		$id = $id ? ' id="' . $id . '"' : '';

		ob_start();
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) && the8_get_option('woo_cart_place') == 'left' ) {
			woocommerce_mini_cart();
		}
		$woo_mini_cart = ob_get_clean();

		ob_start();
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) && the8_get_option('woo_cart_place') == 'left' ) {
			?>
				<a class="woo_icon" href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" title="<?php esc_html_e( 'View your shopping cart', 'the8' ); ?>"><i class='woo_mini-count cwsicon-shopping-carts6'><?php echo ((WC()->cart->cart_contents_count > 0) ?  '<span>' . esc_html( WC()->cart->cart_contents_count ) .'</span>' : '') ?></i></a>
				<?php
		}
		$woo_mini_icon = ob_get_clean();

		ob_start();
		if ( $item->menu_item_parent == '0' && $this->elements_counter == 1 ) {
			if (the8_get_option('woo_cart_place') == 'left') {
				echo is_plugin_active( 'woocommerce/woocommerce.php' ) ? "<div class='mini-cart'>$woo_mini_icon$woo_mini_cart</div>" : '';
			}
			if(the8_get_option('search_place') == 'left'){
				echo "<div class='search_menu'></div>";
			}
		}
		$search_and_woo_icon_start = ob_get_clean();


		$output .= $indent . (!empty($search_and_woo_icon_start) ? $search_and_woo_icon_start : '' ) . '<li' . $id . $value . $class_names .'>';


		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )	 ? $item->target	 : '';
		$atts['rel']	= ! empty( $item->xfn )		? $item->xfn		: '';
		$atts['href']   = ! empty( $item->url )		? $item->url		: '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$item_output = !empty($args->before) ? $args->before : '';
		$item_output .= '<a'. $attributes .'>';

		$item_output .= ( !empty($args->link_before) ? $args->link_before : "" ) . apply_filters( 'the_title', $item->title, $item->ID ) . ( !empty($args->link_after ) ? $args->link_after : "" );
		$item_output .= '</a>';
		$item_output .= ( !empty($args->after) ? $args->after : "" );

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	/**
	 * @see Walker::end_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Page data object. Not used.
	 * @param int $depth Depth of page. Not Used.
	 */


	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		ob_start();
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) && the8_get_option('woo_cart_place') == 'right' ) {
			woocommerce_mini_cart();
		}
			$woo_mini_cart = ob_get_clean();

			ob_start();
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) && the8_get_option('woo_cart_place') == 'right' ) {
			?>
				<a class="woo_icon" href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" title="<?php esc_html_e( 'View your shopping cart', 'the8' ); ?>"><i class='woo_mini-count cwsicon-shopping-carts6'><?php echo ((WC()->cart->cart_contents_count > 0) ?  '<span>' . esc_html( WC()->cart->cart_contents_count ) .'</span>' : '') ?></i></a>
				<?php
		}
		$woo_mini_icon = ob_get_clean();
		
		ob_start();
		if ( $item->menu_item_parent == '0' && $this->elements_counter == $this->elements ) {
			if (the8_get_option('woo_cart_place') == 'right') {
				echo is_plugin_active( 'woocommerce/woocommerce.php' ) ? "<div class='mini-cart'>$woo_mini_icon$woo_mini_cart</div>" : '';
			}
			if(the8_get_option('search_place') == 'right'){
				echo "<div class='search_menu'></div>";
			}
		}
		$search_and_woo_icon_end = ob_get_clean();
		$output .= "</li>\n".(!empty($search_and_woo_icon_end) ? $search_and_woo_icon_end : '');
	}
}

function the8_testimonial_renderer( $thumbnail, $text, $author, $link = "", $custom_color_settings = null ) {
	ob_start();
	if ( (isset( $custom_color_settings->fill_type ) && $custom_color_settings->fill_type == 'color') && isset( $custom_color_settings->fill_color ) ) {
		$figure_color = $custom_color_settings->fill_color;
	}
	$author_section = '';
	if ( !empty( $thumbnail ) || !empty( $author ) ) {
		$author_section .= "<figure class='author'>";
			if ( !empty( $thumbnail ) ) {
				$thumb_obj = bfi_thumb( $thumbnail, array( 'width'=>100, 'height'=>100), false );
				$thumb_url = esc_url($thumb_obj[0]);
				$retina_thumb_url = '';
				$retina_thumb_exists  = false;
				if (!empty($thumb_obj[3])) {
				       extract( $thumb_obj[3] );
				       $retina_thumb_url = esc_attr($retina_thumb_url);
				}
				if ( $retina_thumb_exists ) {
					$author_section .= "<img src='$thumb_url' data-at2x='$retina_thumb_url'".(!empty($figure_color) ? ' style="border-color:'.$figure_color.';"' : '')." alt />";
				}
				else{
					$author_section .= "<img src='$thumb_url'".(!empty($figure_color) ? ' style="border-color:'.$figure_color.';"' : '')." data-no-retina alt />";
				}
			}
			$author_section .= !empty($author) ? "<figcaption>" . esc_html($author) . "</figcaption>" : '';
		$author_section .= "</figure>";
	}
	$quote_section_class = "quote";
	$quote_section_styles = '';
	$quote_section_atts = '';
	$quote_section_class .= !empty( $link ) ? " with_link" : '';
	$link = esc_url($link);
	$custom_colors = false;

	if ( is_object( $custom_color_settings ) && !empty( $custom_color_settings ) ) {

		$custom_colors = true;
		$fill_type = isset( $custom_color_settings->fill_type ) ? $custom_color_settings->fill_type : 'color';
		$fill_color = isset( $custom_color_settings->fill_color ) ? $custom_color_settings->fill_color : THE8_COLOR;
		$font_color = isset( $custom_color_settings->font_color ) ? $custom_color_settings->font_color : '#fff';

		$gradient_settings = isset( $custom_color_settings->gradient_settings ) ? $custom_color_settings->gradient_settings : new stdClass ();
		$gradient_settings = get_object_vars( $gradient_settings );


		if ( $custom_colors && $fill_type == 'color' ) {
			$quote_section_styles .= "background-color:$fill_color;color:$font_color;";
		}
		if ( $custom_colors && $fill_type == 'gradient' ) {
			$quote_section_styles .= the8_render_gradient_rules( array('settings' => $gradient_settings));
			$quote_section_styles .= "color:$font_color;";
		}
	}
	$quote_section_class .= $custom_colors ? " custom_colors" : '';
	$quote_section_atts .= !empty( $quote_section_class ) ? " class='" . trim( $quote_section_class ) . "'" : '';
	$quote_section_atts .= !empty( $quote_section_styles ) ? " style='" . trim( $quote_section_styles ) . "'" : '';
	$text = apply_filters('the_content', $text);
	$quote_section = !empty( $text ) ? "<div" . ( !empty( $quote_section_atts ) ? $quote_section_atts : "" ) . ">$text" .
		( !empty($link) ? "<a class='quote_link' href='$link'" . ( isset( $font_color ) ? " style='color: $font_color;'" : "" ) . "></a>" : "" ) . "</div>" : '';
	?>
	<div class="testimonial clearfix <?php echo $thumbnail ? '' : 'without_image'; ?>">
		<?php
		if (!empty($thumbnail)) {
			echo $author_section . $quote_section;
		}
		else{
			echo $quote_section . $author_section;
		}
		?>
	</div>
	<?php
	return ob_get_clean();
}

function the8_get_grid_shortcodes() {
	return array( 'cws-row', 'col', 'cws-widget' );
}

function the8_get_special_post_formats() {
	return array( 'aside' );
}

function the8_is_special_post_format() {
	global $post;
	$sp_post_formats = the8_get_special_post_formats();
	if ( isset($post) ) {
		return in_array( get_post_format(), $sp_post_formats );
	}
	else{
		return false;
	}
}

function the8_post_format_mark() {
	global $post;
	$out = '';
	if ( isset( $post ) ) {
		$pf = get_post_format();
		$post_format_icons = array(
			'aside' => 'bullseye',
			'gallery' =>'bullseye',
			'link' => 'chain',
			'image' => 'image',
			'quote' => 'quote-lef',
			'status' => 'flag',
			'video' => 'video-camer',
			'audio' => 'music',
			'chat' => 'wechat',
		);
		$icon = 'book';
		if (isset($post_format_icons[$pf])) {
			$icon = $post_format_icons[$pf];
		}
		$out = "<i class='fa fa-$icon'></i> $pf";
	}
	return $out;
}

function the8_strip_grid_shortcodes($text) {
	$shortcodes = the8_get_grid_shortcodes ();
	$find = array();
	foreach ( $shortcodes as $shortcode ) {
		$shortcode = preg_replace( "|-|", "\-", $shortcode );
		$op_tag = "|\[.*" . $shortcode . ".*\]|";
		$cl_tag = "|\[/.*" . $shortcode . ".*\]|";
		array_push( $find, $op_tag, $cl_tag );
	}
	$text = preg_replace( $find, "", $text );
	return $text;
}

// Check if WooCommerce is active
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	require_once( get_template_directory() . '/woocommerce/wooinit.php' ); // WooCommerce Shop ini file
};

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	add_action( 'wp_ajax_woocommerce_remove_from_cart',array( &$this, 'woocommerce_ajax_remove_from_cart' ),1000 );
	add_action( 'wp_ajax_nopriv_woocommerce_remove_from_cart', array( &$this, 'woocommerce_ajax_remove_from_cart' ),1000 );
}

function woocommerce_ajax_remove_from_cart() {
	global $woocommerce;

	$woocommerce->cart->set_quantity( $_POST['remove_item'], 0 );

	$ver = explode( '.', WC_VERSION );

	if ( $ver[1] == 1 && $ver[2] >= 2 ) :
		$wc_ajax = new WC_AJAX();
		$wc_ajax->get_refreshed_fragments();
	else :
		woocommerce_get_refreshed_fragments();
	endif;

	die();
}
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	add_filter( 'add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
}

function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
	ob_start();
		?>
			<i class='woo_mini-count cwsicon-shopping-carts6'><?php echo ((WC()->cart->cart_contents_count > 0) ?  '<span>' . esc_html( WC()->cart->cart_contents_count ) .'</span>' : '') ?></i>
		<?php
		$fragments['.woo_mini-count'] = ob_get_clean();

		ob_start();
		woocommerce_mini_cart();
		$fragments['div.woo_mini_cart'] = ob_get_clean();

		return $fragments;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
	$args['posts_per_page'] = the8_get_option( 'woo-resent-num-products' ); // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}

// Check if WPML is active
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
	define('CWS_WPML_ACTIVE', true);
	$GLOBALS['wpml_settings'] = get_option('icl_sitepress_settings');
	global $icl_language_switcher;
}

function the8_is_wpml_active() {
	return defined('CWS_WPML_ACTIVE') && CWS_WPML_ACTIVE;
}

// shortcode json attribute conversion
function the8_json_sc_attr_conversion ( $attr ) {
	return is_string( $attr ) ? json_decode( preg_replace ( array( '/\\^\\*/', '/\\*\\$/' ), array( '[', ']' ), $attr ) ) : false;
}

// declare ajaxurl on frontend

/* Comments */

class CWS_Walker_Comment extends Walker_Comment {
	// init classwide variables
	var $tree_type = 'comment';
	var $db_fields = array( 'parent' => 'comment_parent', 'id' => 'comment_ID' );
	/** CONSTRUCTOR
	 * You'll have to use this if you plan to get to the top of the comments list, as
	 * start_lvl() only goes as high as 1 deep nested comments */
	function __construct() { ?>

		<div class="comment_list">

	<?php }

	/** START_LVL
	 * Starts the list before the CHILD elements are added. Unlike most of the walkers,
	 * the start_lvl function means the start of a nested comment. It applies to the first
	 * new level under the comments that are not replies. Also, it appear that, by default,
	 * WordPress just echos the walk instead of passing it to &$output properly. Go figure.  */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth + 1; ?>
		<div class="comments_children">
	<?php }

	/** END_LVL
	 * Ends the children list of after the elements are added. */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth + 1; ?>
		</div><!-- /.children -->

	<?php }

	/** START_EL */
	function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
		$depth++;
		$GLOBALS['comment_depth'] = $depth;
		$GLOBALS['comment'] = $comment;
		$parent_class = ( empty( $args['has_children'] ) ? '' : 'parent' );
		$old_version = 0;
		?>

		<div <?php comment_class( $parent_class ); ?> id="comment-<?php comment_ID() ?>">
			<div id="comment-body-<?php comment_ID() ?>" class="comment-body clearfix">

				<div class="avatar_section">
					<?php echo ( $args['avatar_size'] != 0 ? get_avatar( $comment, $args['avatar_size'] ) :'' ); ?>
					<?php if ($old_version == '0') : ?>
						<cite class="fn n author-name"><?php echo get_comment_author_link(); ?></cite>
					<?php endif; ?>
				</div>
				<div class="comment_info_section">
					<div class="comment_info_header">
							<?php $reply_args = array(
								'reply_text' => esc_html__( 'Reply', 'the8' ) . "&nbsp; <i class='fa fa-reply'></i>",
								'depth' => $depth,
								'max_depth' => $args['max_depth']
							);
							if ($old_version == '1') {
								echo "<div class='reply'>";
								comment_reply_link( array_merge( $args, $reply_args ) );
								echo "</div>";
							}  ?>
						<div class="comment-meta comment-meta-data">
							<?php if ($old_version == '1') : ?>
							<cite class="fn n author-name"><?php echo get_comment_author_link(); ?></cite>
							&nbsp;
							<?php endif; ?>
							<span class="comment_date"><?php
								echo "<span class='date'>";
									comment_date();
								echo "</span><span class='sep'></span>";
								echo esc_html__( 'at', 'the8' );
								echo " <span class='time'>";
									comment_time();
								echo "</span>";
								?>
							</span>
							<?php edit_comment_link( '(Edit)' ); ?>
						</div><!-- /.comment-meta -->
					</div>

					<div id="comment-content-<?php comment_ID(); ?>" class="comment-content">
						<?php if( !$comment->comment_approved ) : ?>
						<em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'the8'); ?></em>
						<?php else: comment_text(); ?>
						<?php endif; ?>
					</div><!-- /.comment-content -->
					<?php
					if ($old_version == '0') {
						echo "<div class='button-content reply'>";
						comment_reply_link( array_merge( $args, $reply_args ) );
						echo "</div>";
					}  ?>

				</div>
			</div><!-- /.comment-body -->

	<?php }

	function end_el(&$output, $comment, $depth = 0, $args = array() ) { ?>

		</div><!-- /#comment-' . get_comment_ID() . ' -->

	<?php }

	/** DESTRUCTOR
	 * I just using this since we needed to use the constructor to reach the top
	 * of the comments list, just seems to balance out :) */
	function __destruct() { ?>

	</div><!-- /#comment-list -->

	<?php }
}

function the8_comment_nav() {
	// Are there comments to navigate through?
	if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
	?>
	<div class="comments_nav carousel_nav_panel clearfix">
		<?php
			if ( $prev_link = get_previous_comments_link( "<span class='prev'></span><span>" . esc_html__( 'Older Comments', 'the8' ) . "</span>" ) ) :
				printf( '<div class="prev_section">%s</div>', esc_html($prev_link) );
			endif;

			if ( $next_link = get_next_comments_link( "<span>" . esc_html__( 'Newer Comments', 'the8' ) . "</span><span class='next'></span>" ) ) :
				printf( '<div class="next_section">%s</div>', esc_html($next_link) );
			endif;
		?>
	</div><!-- .comment-navigation -->
	<?php
	endif;
}

function the8_comment_post( $incoming_comment ) {
	$comment = strip_tags($incoming_comment['comment_content']);
	$comment = esc_html($comment);
	$incoming_comment['comment_content'] = $comment;
	return( $incoming_comment );
}
add_filter('preprocess_comment', 'the8_comment_post', '', 1);

/* \Comments */

/* Social Links */

function the8_render_social_links() {
	$out = '';
	$social_groups_option = the8_get_option( 'social_group' );
	if ( !empty( $social_groups_option ) && is_array( $social_groups_option ) ) {
		$social_groups = array();
		for ( $i=0; $i<count( $social_groups_option ); $i++ ) {
			if ( isset( $social_groups_option[$i]['soc-select-fa'] ) && !empty( $social_groups_option[$i]['soc-select-fa'] ) ) {
				$social_groups[] = $social_groups_option[$i];
			}
		}
		foreach ( $social_groups as $social_group ) {
			$title = isset( $social_group['title'] ) ? esc_attr($social_group['title']) : '';
			$icon = $social_group['soc-select-fa'];
			$url = isset( $social_group['soc-url'] ) ? $social_group['soc-url'] : '';
			$out .= "<a href='" . ( !empty( $url ) ? esc_url($url) : '#' ) . "' class='cws_social_link $icon'" . ( !empty( $title ) ? " title='$title'" : "" ) . " target='_blank'></a>";
		}
		$out = !empty( $out ) ? "<div class='cws_social_links'>$out</div>" : '';
	}
	return $out;
}

/* \Social Links */

function the8_get_date_part ( $part = '' ) {
	$part_val = '';
	$p_id = get_queried_object_id();
	$perm_struct = get_option( 'permalink_structure' );
	$use_perms = !empty( $perm_struct );
	$merge_date = get_query_var( 'm' );
	$match = preg_match( '#(\d{4})?(\d{1,2})?(\d{1,2})?#', $merge_date, $matches );
	switch ( $part ) {
		case 'y':
			$part_val = $use_perms ? get_query_var( 'year' ) : ( isset( $matches[1] ) ? $matches[1] : '' );
			break;
		case 'm':
			$part_val = $use_perms ? get_query_var( 'monthnum' ) : ( isset( $matches[2] ) ? $matches[2] : '' );
			break;
		case 'd':
			$part_val = $use_perms ? get_query_var( 'day' ) : ( isset( $matches[3] ) ? $matches[3] : '' );
			break;
	}
	return $part_val;
}

add_action( 'after_setup_theme', 'the8_after_setup_theme' );

function the8_after_setup_theme() {
	add_editor_style();
}

add_filter( 'mce_buttons_2', 'the8_mce_buttons_2' );

function the8_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}

add_filter( 'tiny_mce_before_init', 'the8_tiny_mce_before_init' );

function the8_tiny_mce_before_init( $settings ) {

	$font_array = the8_get_option( 'header-font' );

	$settings['theme_advanced_blockformats'] = 'p,h1,h2,h3,h4';

	$style_formats = array(
	array( 'title' => 'Title', 'block' => 'h3', 'classes' => 'ce_title' ),
	array( 'title' => 'Font-size', 'items' => array(
		array( 'title' => '40px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '40px' , 'line-height' => '1.2em') ),
		array( 'title' => '30px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '30px' , 'line-height' => '1.4em') ),
		array( 'title' => '20px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '20px' , 'line-height' => '1.6em') ),
		array( 'title' => '16px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '16px' , 'line-height' => '1.75em') ),
		array( 'title' => '14px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '14px' , 'line-height' => '1.75em') ),
		)
	),
	array( 'title' => 'margin-top', 'items' => array(
		array( 'title' => '0px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '0' ) ),
		array( 'title' => '10px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '10px' ) ),
		array( 'title' => '15px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '15px' ) ),
		array( 'title' => '20px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '20px' ) ),
		array( 'title' => '25px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '25px' ) ),
		array( 'title' => '30px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '30px' ) ),
		array( 'title' => '40px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '40px' ) ),
		array( 'title' => '50px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '50px' ) ),
		array( 'title' => '60px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '60px' ) ),
		)
	),
	array( 'title' => 'margin-bottom', 'items' => array(
		array( 'title' => '0px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '0px' ) ),
		array( 'title' => '10px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '10px' ) ),
		array( 'title' => '15px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '15px' ) ),
		array( 'title' => '20px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '20px' ) ),
		array( 'title' => '25px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '25px' ) ),
		array( 'title' => '30px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '30px' ) ),
		array( 'title' => '40px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '40px' ) ),
		array( 'title' => '50px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '50px' ) ),
		array( 'title' => '60px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '60px' ) ),
		)
	),
	array( 'title' => 'Underline title', 'items' => array(
		array( 'title' => 'gray line', 'selector' => '.ce_title:not(.und-title.white):not(.und-title.themecolor)', 'classes' => 'und-title gray' ),
		array( 'title' => 'white line', 'selector' => '.ce_title:not(.und-title.themecolor):not(.und-title.gray)', 'classes' => 'und-title white' ),
		array( 'title' => 'theme color line', 'selector' => '.ce_title:not(.und-title.white):not(.und-title.gray)', 'classes' => 'und-title themecolor' ),
		)
	),
	array( 'title' => 'Borderless image', 'selector' => 'img', 'classes' => 'noborder' ),
	);
	// Before 3.1 you needed a special trick to send this array to the configuration.
	// See this post history for previous versions.
	$settings['style_formats'] = str_replace( '"', "'", json_encode( $style_formats ) );

	return $settings;
}

?>