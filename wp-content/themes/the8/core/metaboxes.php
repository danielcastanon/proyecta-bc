<?php

new The8_Metaboxes();

class The8_Metaboxes {

	public function __construct() {
		$this->init();
	}

	private function init() {
		load_template( trailingslashit( get_template_directory() ) . '/core/pb.php');
		add_action( 'add_meta_boxes', array($this, 'post_addmb') );
		add_action( 'add_meta_boxes_cws_portfolio', array($this, 'portfolio_addmb') );
		add_action( 'add_meta_boxes_cws_staff', array($this, 'staff_addmb') );

		add_action( 'admin_enqueue_scripts', array($this, 'mb_script_enqueue') );
		add_action( 'save_post', array($this, 'post_metabox_save'), 11, 2 );
	}

	public function portfolio_addmb() {
		add_meta_box( 'cws-post-metabox-id', 'CWS Portfolio Options', array($this, 'mb_portfolio_callback'), 'cws_portfolio', 'normal', 'high' );
	}

	public function staff_addmb() {
		add_meta_box( 'cws-post-metabox-id', 'CWS Staff Options', array($this, 'mb_staff_callback'), 'cws_staff', 'normal', 'high' );
	}

	public function post_addmb() {
		add_meta_box( 'cws-post-metabox-id', 'CWS Post Options', array($this, 'mb_post_callback'), 'post', 'normal', 'high' );
		add_meta_box( 'cws-post-metabox-id', 'CWS Page Options', array($this, 'mb_page_callback'), 'page', 'normal', 'high' );
	}

	public function mb_staff_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = array(
			'is_clickable' => array(
				'type' => 'checkbox',
				'title' => esc_html__('Show details page', 'the8' ),
			),
			'social_group' => array(
				'type' => 'group',
				'addrowclasses' => 'group',
				'title' => esc_html__('Social networks', 'the8' ),
				'button_title' => esc_html__('Add new social network', 'the8' ),
				'layout' => array(
					'title' => array(
						'type' => 'text',
						'atts' => 'data-role="title"',
						'title' => esc_html__('Social account title', 'the8' ),
					),
					'icon' => array(
						'type' => 'select',
						'addrowclasses' => 'fai',
						'source' => 'fa',
						'title' => esc_html__('Select the icon for this social contact', 'the8' )
					),
					'url' => array(
						'type' => 'text',
						'title' => esc_html__('Url to your account', 'the8' ),
					),
				),
			),
		);

		$cws_stored_meta = get_post_meta( $post->ID, 'cws_mb_post' );
		cws_mb_fillMbAttributes($cws_stored_meta, $mb_attr);
		echo cws_mb_print_layout($mb_attr, 'cws_mb_');
	}

	public function mb_page_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = array(
			'sb_layout' => array(
				'title' => esc_html__('Sidebar Position', 'the8' ),
				'type' => 'radio',
				'subtype' => 'images',
				'value' => array(
					'default'=>	array( esc_html__('Default', 'the8' ), true, 'd:sidebar1;d:sidebar2', get_template_directory_uri() . '/core/images/default.png' ),
					'left' => 	array( esc_html__('Left', 'the8' ), false, 'e:sidebar1;d:sidebar2',	get_template_directory_uri() . '/core/images/left.png' ),
					'right' => 	array( esc_html__('Right', 'the8' ), false, 'e:sidebar1;d:sidebar2', get_template_directory_uri() . '/core/images/right.png' ),
					'both' => 	array( esc_html__('Double', 'the8' ), false, 'e:sidebar1;e:sidebar2', get_template_directory_uri() . '/core/images/both.png' ),
					'none' => 	array( esc_html__('None', 'the8' ), false, 'd:sidebar1;d:sidebar2', get_template_directory_uri() . '/core/images/none.png' )
				),
			),
			'sidebar1' => array(
				'title' => esc_html__('Select a sidebar', 'the8' ),
				'type' => 'select',
				'addrowclasses' => 'disable',
				'source' => 'sidebars',
			),
			'sidebar2' => array(
				'required' => array( 'sb_layout', '=', 'both' ),
				'title' => esc_html__('Select right sidebar', 'the8' ),
				'type' => 'select',
				'addrowclasses' => 'disable',
				'source' => 'sidebars',
			),
			'is_blog' => array(
				'type' => 'checkbox',
				'title' => esc_html__('Show Blog posts', 'the8' ),
				'atts' => 'data-options="e:blogtype;e:category"',
			),
			'blogtype' => array(
				'type' => 'radio',
				'subtype' => 'images',
				'title' => esc_html__('Blog Layout', 'the8' ),
				'addrowclasses' => 'disable',
				'value' => array(
					'default'=>	array( esc_html__('Default', 'the8' ), false, '', get_template_directory_uri() . '/core/images/default.png' ),
					'small' => array( esc_html__('Small', 'the8' ), false, '', get_template_directory_uri() . '/core/images/small.png' ),
					'medium' => array( esc_html__('Medium', 'the8' ), true, '', get_template_directory_uri() . '/core/images/medium.png' ),
					'large' => array( esc_html__('Large', 'the8' ), false, '', get_template_directory_uri() . '/core/images/large.png' ),
					'2' => array(  esc_html__('Two', 'the8' ), false, '', get_template_directory_uri() . '/core/images/pinterest_2_columns.png'),
					'3' => array( esc_html__('Three', 'the8' ), false, '', get_template_directory_uri() . '/core/images/pinterest_3_columns.png'),
				),
			),
			'category' => array(
				'title' => esc_html__('Category', 'the8' ),
				'type' => 'taxonomy',
				'addrowclasses' => 'disable',
				'atts' => 'multiple',
				'taxonomy' => 'category',
				'source' => array(),
			),
			'sb_foot_override' => array(
				'type' => 'checkbox',
				'title' => esc_html__( 'Customize footer', 'the8' ),
				'atts' => 'data-options="e:footer-sidebar-top"',
			),
			'footer-sidebar-top' => array(
				'title' => esc_html__('Sidebar area', 'the8' ),
				'type' => 'select',
				'addrowclasses' => 'disable',
				'source' => 'sidebars',
			),
			'sb_hide_title_area' => array(
				'type' => 'checkbox',
				'title' => esc_html__( 'Hide title area', 'the8' ),
			),			
			'sb_slider_override' => array(
				'type' => 'checkbox',
				'title' => esc_html__( 'Add Image Slider', 'the8' ),
				'atts' => 'data-options="e:slider_shortcode"',
			),
			'slider_shortcode' => array(
				'title' => esc_html__( 'Slider shortcode', 'the8' ),
				'addrowclasses' => 'disable',
				'type' => 'text',
				'default' => ''
			),
		);

		$cws_stored_meta = get_post_meta( $post->ID, 'cws_mb_post' );
		cws_mb_fillMbAttributes($cws_stored_meta, $mb_attr);
		echo cws_mb_print_layout($mb_attr, 'cws_mb_');
	}

	public function mb_portfolio_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = array(
			'carousel' => array(
				'title' => esc_html__( 'Display items carousel for this portfolio post', 'the8' ),
				'type' => 'checkbox',
				'atts' => 'checked',
			),
			'show_related' => array(
				'title' => esc_html__( 'Show related items', 'the8' ),
				'type' => 'checkbox',
				'atts' => 'checked data-options="e:related_projects_options;e:rpo_title;e:rpo_cols;e:rpo_items_count"',
			),
			'related_projects_options' => array(
				'type' => 'label',
				'title' => esc_html__( 'Related items options', 'the8' ),
			),
			'rpo_title' => array(
				'type' => 'text',
				'title' => esc_html__( 'Title', 'the8' ),
				'value' => esc_html__( 'Related items', 'the8' )
				),
			'rpo_cols' => array(
				'type' => 'select',
				'title' => esc_html__( 'Columns', 'the8' ),
				'source' => array(
					'1' => array(esc_html__( 'one', 'the8' ), false),
					'2' => array(esc_html__( 'two', 'the8' ), false),
					'3' => array(esc_html__( 'three', 'the8' ), false),
					'4' => array(esc_html__( 'four', 'the8' ), true),
					),
				),
			'rpo_items_count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Number of items to show', 'the8' ),
				'value' => '4'
			),
			'enable_hover' => array(
				'title' => esc_html__( 'Enable hover effect (FancyBox)', 'the8' ),
				'type' => 'checkbox',
				'atts' => 'checked data-options="e:link_options;e:link_options_url;e:link_options_fancybox"',
			),
			'link_options' => array(
				'type' => 'label',
				'title' => esc_html__( 'Add custom URL', 'the8' ),
			),
			'link_options_url' => array(
				'type' => 'text',
				'default' => ''
			),
			'link_options_fancybox' => array(
				'type' => 'checkbox',
				'title' => esc_html__( 'Open in a popup window (FancyBox)', 'the8' ),
				'atts' => 'checked'
			)
		);

		$cws_stored_meta = get_post_meta( $post->ID, 'cws_mb_post' );
		cws_mb_fillMbAttributes($cws_stored_meta, $mb_attr);
		echo cws_mb_print_layout($mb_attr, 'cws_mb_');
	}

	public function mb_post_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = array(
			'gallery' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Gallery', 'the8' ),
				'layout' => array(
					'gallery' => array(
						'type' => 'gallery'
					)
				)
			),
			'video' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Video', 'the8' ),
				'layout' => array(
					'video' => array(
						'title' => esc_html__( 'Direct URL path of a video file', 'the8' ),
						'type' => 'text'
					)
				)
			),
			'audio' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Audio', 'the8' ),
				'layout' => array(
					'audio' => array(
						'title' => esc_html__( 'A self-hosted or SoundClod audio file URL', 'the8' ),
						'subtitle' => esc_html__( 'Ex.: /wp-content/uploads/audio.mp3 or http://soundcloud.com/...', 'the8' ),
						'type' => 'text'
					)
				)
			),
			'link' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Url', 'the8' ),
				'layout' => array(
					'link' => array(
						'type' => 'text'
					)
				)
			),
			'quote' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Quote', 'the8' ),
				'layout' => array(
					'quote[quote]' => array(
						'subtitle' => esc_html__( 'Enter the quote', 'the8' ),
						'atts' => 'rows="5"',
						'type' => 'textarea'
					),
					'quote[author]' => array(
						'title' => esc_html__( 'Author', 'the8' ),
						'type' => 'text'
					),
					'quote[avatar]' => array(
						'title' => esc_html__( 'Photo', 'the8' ),
						'type' => 'media',
						'atts' => 'data-type="image"',
					)
				)
			)
		);

		$cws_stored_meta = get_post_meta( $post->ID, 'cws_mb_post' );
		cws_mb_fillMbAttributes($cws_stored_meta, $mb_attr);
		echo cws_mb_print_layout($mb_attr, 'cws_mb_');

		$mb_attr_all = array(
			'enable_lightbox' => array(
				'title' => esc_html__( 'Enable lightbox', 'the8' ),
				'type' => 'checkbox',
				'atts' => 'checked',
			),
		);
		cws_mb_fillMbAttributes($cws_stored_meta, $mb_attr_all);
		echo cws_mb_print_layout($mb_attr_all, 'cws_mb_');
	}

	public function mb_script_enqueue($a) {
		global $pagenow;
		global $typenow;
		if( ($a == 'widgets.php' || $a == 'post-new.php' || $a == 'post.php' || $a == 'edit-tags.php') && ('customize.php' !== $pagenow) ) {
			if($typenow != 'product'){
				wp_enqueue_script('select2-js', get_template_directory_uri() . '/core/js/select2/select2.js', array('jquery') );
				wp_enqueue_style('select2-css', get_template_directory_uri() . '/core/js/select2/select2.css', false, '2.0.0' );
			}
			wp_enqueue_script('the8-metaboxes-js', get_template_directory_uri() . '/core/js/metaboxes.js', array('jquery') );
			wp_enqueue_style('the8-metaboxes-css', get_template_directory_uri() . '/core/css/metaboxes.css', false, '2.0.0' );
			wp_enqueue_media();
			wp_enqueue_style( 'wp-color-picker');
			wp_enqueue_script( 'wp-color-picker');
			wp_enqueue_style( 'mb_post_css' );
		}
	}

	public function post_metabox_save( $post_id, $post )
	{
		if ( in_array($post->post_type, array('post', 'page', 'cws_portfolio', 'cws_staff')) ) {
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
				return;

			if ( !isset( $_POST['mb_nonce']) || !wp_verify_nonce($_POST['mb_nonce'], 'cws_mb_nonce') )
				return;

			if ( !current_user_can( 'edit_post', $post->ID ) )
				return;

			$save_array = array();

			foreach($_POST as $key => $value) {
				if (0 === strpos($key, 'cws_mb_')) {
					if ('on' === $value) {
						$value = '1';
					}
					if (is_array($value)) {
						foreach ($value as $k => $val) {
							if (is_array($val)) {
								$save_array[substr($key, 7)][$k] = $val;
							} else {
								$save_array[substr($key, 7)][$k] = esc_html($val);
							}
						}
					} else {
						$save_array[substr($key, 7)] = esc_html($value);
					}
				}
			}
			if (!empty($save_array)) {
				update_post_meta($post_id, 'cws_mb_post', $save_array);
			}
		}
	}
}
?>