<?php
class The8_SCG {
	private $cws_tmce_sc_settings_config = array();

	public function __construct() {
		$this->init();
	}

	private function init() {
		add_filter( 'mce_buttons_3', array($this, 'mce_sc_buttons') );
		add_filter( 'mce_external_plugins', array($this, 'mce_sc_plugin') );
		add_action( 'wp_ajax_cws_ajax_sc_settings', array($this, 'ajax_sc_settings_callback') );
		add_action( 'admin_enqueue_scripts', array($this, 'scg_scripts_enqueue'), 11 );

		$body_font = the8_get_option( 'body-font' );
		$body_font_color = $body_font['color'];

		$this->cws_tmce_sc_settings_config = array(
			'msg_box' => array(
				'title' => esc_html__( 'CWS Message Box', 'the8' ),
				'icon' => 'fa fa-exclamation-circle',
				'paired' => true,
				'fields' => array(
					'type' => array(
						'title' => esc_html__( 'Type', 'the8' ),
						'type' => 'select',
						'source' => array(
							'info' => array(esc_html__( 'Informational', 'the8' ), true),
							'warning' => array(esc_html__( 'Warning', 'the8' ), false),
							'success' => array(esc_html__( 'Success', 'the8' ), false),
							'error' => array(esc_html__( 'Error', 'the8' ), false)
							),
					),
					'title' => array(
						'title' => esc_html__( 'Title', 'the8' ),
						'type' => 'text',
					),
					'text' => array(
						'title' => esc_html__( 'Text', 'the8' ),
						'type' => 'textarea',
					),
					'is_closable' => array(
						'title' => esc_html__( 'Add close button', 'the8' ),
						'type' => 'checkbox',
					),
					'customize' => array(
						'title' => esc_html__( 'Customize colors', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:custom_options[color];e:custom_options[icon];e:custom_options[bg_color];"'
					),
					'custom_options[color]' => array(
						'title' => esc_html__( 'Text & icon color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#ffffff"',
						'addrowclasses' => 'disable',
					),
					'custom_options[bg_color]' => array(
						'title' => esc_html__( 'Background color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
					),
					'custom_options[icon]' => array(
						'title' => esc_html__( 'Icon', 'the8' ),
						'type' => 'select',
						'addrowclasses' => 'fai disable',
						'source' => 'fa'
					)
				)
			),
			'milestone' => array(
				'title' => esc_html__( 'CWS Milestone', 'the8' ),
				'icon' => 'fa fa-trophy',
				'fields' => array(
					'icon' => array(
						'title' => esc_html__( 'Icon', 'the8' ),
						'type' => 'select',
						'addrowclasses' => 'fai',
						'source' => 'fa',
					),
					'icon_color' => array(
						'title' => esc_html__( 'Icon color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						),
					'number' => array(
						'title' => esc_html__( 'Number', 'the8' ),
						'type' => 'number',
						'value' => ''
					),
					'speed' => array(
						'title' => esc_html__( 'Speed', 'the8' ),
						'desc' => esc_html__( 'Should be integer', 'the8' ),
						'type' => 'number',
						'value' => '2000'
					),
					'desc' => array(
						'title' => esc_html__( 'Description', 'the8' ),
						'type' => 'textarea',
						'atts' => 'rows="5"',
						'value' => ''
					),
					'custom_color_settings[font_color]' => array(
						'title' => esc_html__( 'Font Color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.$body_font_color.'"',
					),
					'borderless' => array(
						'title' => esc_html__( 'Hide border', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="d:custom_color_settings[border_color];"',
					),
					'custom_color_settings[border_color]' => array(
						'title' => esc_html__( 'Border color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'enable',
						),
					'item_width' => array(
						'title' => esc_html__( 'Width (px)', 'the8' ),
						'type' => 'number',
						'value' => '170'
					),
					'custom_colors' => array(
						'title' => esc_html__( 'Customize', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:custom_color_settings[fill_type];"',
					),
					'custom_color_settings[fill_type]' => array(
						'title' => esc_html__( 'Type', 'the8' ),
						'type' => 'select',
						'source' => array(
							'transparent' => array(esc_html__( 'Transparent', 'the8' ), true , 'd:custom_color_settings[fill_color];d:custom_color_settings[gradient_settings][first_color];d:custom_color_settings[gradient_settings][second_color];d:custom_color_settings[gradient_settings][type];'),
							'color' => array(esc_html__( 'Color', 'the8' ), false , 'e:custom_color_settings[fill_color];d:custom_color_settings[gradient_settings][first_color];d:custom_color_settings[gradient_settings][second_color];d:custom_color_settings[gradient_settings][type];'),
							'gradient' => array(esc_html__( 'Gradient', 'the8' ), false , 'd:custom_color_settings[fill_color];e:custom_color_settings[gradient_settings][first_color];e:custom_color_settings[gradient_settings][second_color];e:custom_color_settings[gradient_settings][type];')
							),
						'addrowclasses' => 'disable',
						),
					'custom_color_settings[fill_color]' => array(
						'title' => esc_html__( 'Color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
						),
					'custom_color_settings[gradient_settings][first_color]' => array(
						'title' => esc_html__( 'Start color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][second_color]' => array(
						'title' => esc_html__( 'End color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#0eecbd"',
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][type]' => array(
						'title' => esc_html__( 'Gradient type', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'linear' => array(esc_html__( 'Linear', 'the8' ), true , 'e:custom_color_settings[gradient_settings][linear_settings][angle];d:custom_color_settings[gradient_settings][radial_settings][shape_settings];'),
							'radial' => array(esc_html__( 'Radial', 'the8' ), false , 'd:custom_color_settings[gradient_settings][linear_settings][angle];e:custom_color_settings[gradient_settings][radial_settings][shape_settings];')
						),
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][linear_settings][angle]' => array(
						'title' => esc_html__( 'Linear gradient settings angle', 'the8' ),
						'type' => 'text',
				 		'value' => '45',
				 		'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][radial_settings][shape_settings]' =>  array(
						'title' => esc_html__( 'Shape settings type', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'simple' => array(esc_html__( 'Simple', 'the8' ), true , 'e:custom_color_settings[gradient_settings][radial_settings][shape];d:custom_color_settings[gradient_settings][radial_settings][size_keyword];d:custom_color_settings[gradient_settings][radial_settings][size]'),
							'extended' => array(esc_html__( 'Extended', 'the8' ), false , 'd:custom_color_settings[gradient_settings][radial_settings][shape];e:custom_color_settings[gradient_settings][radial_settings][size_keyword];e:custom_color_settings[gradient_settings][radial_settings][size]' )
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][shape]' =>  array(
						'title' => esc_html__( 'Shape', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'ellipse' => array(esc_html__( 'Ellipse', 'the8' ), true ),
							'circle' => array(esc_html__( 'Circle', 'the8' ), false )
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][size_keyword]' =>  array(
						'title' => esc_html__( 'Size keyword', 'the8' ),
						'type' => 'select',
						'source' => array(
							'closest-side' => array(esc_html__( 'Closest side', 'the8' ), false ),
							'farthest-side' => array(esc_html__( 'Farthest side', 'the8' ), false ),
							'closest-corner' => array(esc_html__( 'Closest corner', 'the8' ), false ),
							'farthest-corner' => array(esc_html__( 'Farthest corner', 'the8' ), true ),
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][size]' =>  array(
						'title' => esc_html__( 'Size', 'the8' ),
						'desc' => esc_html__( 'Two space separated percent values, for example (60% 55%)', 'the8' ),
						'type' => 'text',
						'required' => array( 'shape_settings', '=', 'extended' ),
						'default' => '',
						'addrowclasses' => 'disable'
					),
				)
			),
			'progress_bar' => array(
				'title' => esc_html__( 'CWS Progress Bar', 'the8' ),
				'icon' => 'fa fa-tasks',
				'fields' => array(
					'title' => array(
						'title' => esc_html__( 'Title', 'the8' ),
						'type' => 'text',
					),
					'progress' => array(
						'title' => esc_html__( 'Progress', 'the8' ),
						'desc' => esc_html__( 'In percents ( number from 0 to 100 )', 'the8' ),
						'type' => 'number',
						'atts' => 'min="1" max="100" step="1"',
					),
					'custom_colors' => array(
						'title' => esc_html__( 'Customize colors', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:custom_color_settings[fill_type];e:custom_color_settings[font_color]"',
					),
					'custom_color_settings[fill_type]' => array(
						'title' => esc_html__( 'Type', 'the8' ),
						'type' => 'select',
						'source' => array(
							'color' => array(esc_html__( 'Color', 'the8' ), true , 'e:custom_color_settings[fill_color];d:custom_color_settings[gradient_settings][first_color];d:custom_color_settings[gradient_settings][second_color];d:custom_color_settings[gradient_settings][type];'),
							'gradient' => array(esc_html__( 'Gradient', 'the8' ), false , 'd:custom_color_settings[fill_color];e:custom_color_settings[gradient_settings][first_color];e:custom_color_settings[gradient_settings][second_color];e:custom_color_settings[gradient_settings][type];')
							),
						'addrowclasses' => 'disable',
						),
					'custom_color_settings[fill_color]' => array(
						'title' => esc_html__( 'Fill color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
						),
					'custom_color_settings[gradient_settings][first_color]' => array(
						'title' => esc_html__( 'Start color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][second_color]' => array(
						'title' => esc_html__( 'End color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#0eecbd"',
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][type]' => array(
						'title' => esc_html__( 'Gradient type', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'linear' => array(esc_html__( 'Linear', 'the8' ), true , 'e:custom_color_settings[gradient_settings][linear_settings][angle];d:custom_color_settings[gradient_settings][radial_settings][shape_settings];'),
							'radial' => array(esc_html__( 'Radial', 'the8' ), false , 'd:custom_color_settings[gradient_settings][linear_settings][angle];e:custom_color_settings[gradient_settings][radial_settings][shape_settings];')
						),
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][linear_settings][angle]' => array(
						'title' => esc_html__( 'Linear gradient settings angle', 'the8' ),
						'type' => 'text',
				 		'value' => '45',
				 		'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][radial_settings][shape_settings]' =>  array(
						'title' => esc_html__( 'Shape settings type', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'simple' => array(esc_html__( 'Simple', 'the8' ), true , 'e:custom_color_settings[gradient_settings][radial_settings][shape];d:custom_color_settings[gradient_settings][radial_settings][size_keyword];d:custom_color_settings[gradient_settings][radial_settings][size]'),
							'extended' => array(esc_html__( 'Extended', 'the8' ), false , 'd:custom_color_settings[gradient_settings][radial_settings][shape];e:custom_color_settings[gradient_settings][radial_settings][size_keyword];e:custom_color_settings[gradient_settings][radial_settings][size]' )
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][shape]' =>  array(
						'title' => esc_html__( 'Shape', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'ellipse' => array(esc_html__( 'Ellipse', 'the8' ), true ),
							'circle' => array(esc_html__( 'Circle', 'the8' ), false )
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][size_keyword]' =>  array(
						'title' => esc_html__( 'Size keyword', 'the8' ),
						'type' => 'select',
						'source' => array(
							'closest-side' => array(esc_html__( 'Closest side', 'the8' ), false ),
							'farthest-side' => array(esc_html__( 'Farthest side', 'the8' ), false ),
							'closest-corner' => array(esc_html__( 'Closest corner', 'the8' ), false ),
							'farthest-corner' => array(esc_html__( 'Farthest corner', 'the8' ), true ),
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][size]' =>  array(
						'title' => esc_html__( 'Size', 'the8' ),
						'desc' => esc_html__( 'Two space separated percent values, for example (60% 55%)', 'the8' ),
						'type' => 'text',
						'required' => array( 'shape_settings', '=', 'extended' ),
						'default' => '',
						'addrowclasses' => 'disable'
					)
				)
			),
			'fa' => array(
				'title' => esc_html__( 'CWS Font Icon', 'the8' ),
				'icon' => 'fa fa-flag',
				'paired' => true,
				'fields' => array(
					'icon' => array(
						'title' => esc_html__( 'Icon', 'the8' ),
						'type' => 'select',
						'addrowclasses' => 'fai',
						'source' => 'fa',
					),
					'shape' => array(
						'title' => esc_html__( 'Shape', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'square' => array(esc_html__( 'Squared', 'the8' ), true),
							'round' => array(esc_html__( 'Round', 'the8' ), false)
						),
					),
					'bordered_icon' => array(
						'title' => esc_html__( 'Add border', 'the8' ),
						'type' => 'checkbox'
					),
					'simple_style_icon' => array(
						'title' => esc_html__( 'Simplified style', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="d:custom_color_settings[fill_type];"',
					),
					'alt' => array(
						'title' => esc_html__( 'Alternative style', 'the8' ),
						'type' => 'checkbox'
					),
					'size' => array(
						'title' => esc_html__( 'Size', 'the8' ),
						'type' => 'select',
						'source' => array(
							'lg' => array(esc_html__( 'Mini', 'the8' ), false),
							'2x' => array(esc_html__( 'Small', 'the8' ), true),
							'3x' => array(esc_html__( 'Medium', 'the8' ), false),
							'4x' => array(esc_html__( 'Large', 'the8' ), false),
							'5x' => array(esc_html__( 'Extra Large', 'the8' ), false)
						),
					),
					'align_icon' => array(
						'title' => esc_html__( 'Alignment', 'the8' ),
						'type' => 'select',
						'source' => array(
							'left' => array(esc_html__( 'Left', 'the8' ), true),
							'right' => array(esc_html__( 'Right', 'the8' ), false)
						),
					),
					'add_hover' => array(
						'title' => esc_html__( 'Add Hover', 'the8' ),
						'type' => 'checkbox'
					),
					'link' => array(
						'title' => esc_html__( 'Icon URL', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:url;"',
					),
					'url' => array(
						'title' => esc_html__( 'Url', 'the8' ),
						'type' => 'text',
						'value' => '#',
						'addrowclasses' => 'disable'
					),
					'custom_colors' => array(
						'title' => esc_html__( 'Customize', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:custom_color_settings[fill_color];e:custom_color_settings[fill_color];e:custom_color_settings[font_color]"',
					),
					'custom_color_settings[fill_color]' => array(
						'title' => esc_html__( 'Fill color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
						),
					'custom_color_settings[font_color]' => array(
						'title' => esc_html__( 'Font Color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#ffffff"',
						'addrowclasses' => 'disable',
					),
				)
			),
			'button' => array(
				'title' => esc_html__( 'CWS Button', 'the8' ),
				'icon' => 'fa fa-check-square-o',
				'paired' => true,
				'fields' => array(
					'title' => array(
						'title' => esc_html__( 'Title', 'the8' ),
						'type' => 'text',
					),
					'url' => array(
						'title' => esc_html__( 'Url', 'the8' ),
						'type' => 'text',
						'value' => '#'
					),
					'new_tab' => array(
						'title' => esc_html__( 'Open link in a new window/tab', 'the8' ),
						'type' => 'checkbox',
					),
					'size' => array(
						'title' => esc_html__( 'Size', 'the8' ),
						'type' => 'select',
						'source' => array(
							'mini' => array(esc_html__( 'Mini', 'the8' ), false),
							'small' => array(esc_html__( 'Small', 'the8' ), false),
							'regular' => array(esc_html__( 'Regular', 'the8' ), true),
							'large' => array(esc_html__( 'Large', 'the8' ), false),
							'xlarge' => array(esc_html__( 'Extra large', 'the8' ), false)
						),
					),
					'ofs' => array(
						'title' => esc_html__( 'Left & Right paddings', 'the8' ),
						'type' => 'number',
						'atts' => 'min="0" max="200" step="1"'
					),
					'icon' => array(
						'title' => esc_html__( 'Icon', 'the8' ),
						'type' => 'select',
						'addrowclasses' => 'fai',
						'source' => 'fa',
					),
					'alt' => array(
						'title' => esc_html__( 'Alternative style', 'the8' ),
						'type' => 'checkbox',
					),
					'full_width' => array(
						'title' => esc_html__( 'Full width', 'the8' ),
						'type' => 'checkbox',
					),
					'custom_colors' => array(
						'title' => esc_html__( 'Customize', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:custom_color_settings[fill_type];e:custom_color_settings[font_color]"',
					),
					'custom_color_settings[fill_type]' => array(
						'title' => esc_html__( 'Type', 'the8' ),
						'type' => 'select',
						'source' => array(
							'color' => array(esc_html__( 'Color', 'the8' ), true , 'e:custom_color_settings[fill_color];d:custom_color_settings[gradient_settings][first_color];d:custom_color_settings[gradient_settings][second_color];d:custom_color_settings[gradient_settings][type];'),
							'gradient' => array(esc_html__( 'Gradient', 'the8' ), false , 'd:custom_color_settings[fill_color];e:custom_color_settings[gradient_settings][first_color];e:custom_color_settings[gradient_settings][second_color];e:custom_color_settings[gradient_settings][type];')
							),
						'addrowclasses' => 'disable',
						),
					'custom_color_settings[fill_color]' => array(
						'title' => esc_html__( 'Fill color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
						),
					'custom_color_settings[gradient_settings][first_color]' => array(
						'title' => esc_html__( 'Start color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][second_color]' => array(
						'title' => esc_html__( 'End color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#0eecbd"',
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][type]' => array(
						'title' => esc_html__( 'Gradient type', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'linear' => array(esc_html__( 'Linear', 'the8' ), true , 'e:custom_color_settings[gradient_settings][linear_settings][angle];d:custom_color_settings[gradient_settings][radial_settings][shape_settings];'),
							'radial' => array(esc_html__( 'Radial', 'the8' ), false , 'd:custom_color_settings[gradient_settings][linear_settings][angle];e:custom_color_settings[gradient_settings][radial_settings][shape_settings];')
						),
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][linear_settings][angle]' => array(
						'title' => esc_html__( 'Linear gradient settings angle', 'the8' ),
						'type' => 'text',
				 		'value' => '45',
				 		'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][radial_settings][shape_settings]' =>  array(
						'title' => esc_html__( 'Shape settings type', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'simple' => array(esc_html__( 'Simple', 'the8' ), true , 'e:custom_color_settings[gradient_settings][radial_settings][shape];d:custom_color_settings[gradient_settings][radial_settings][size_keyword];d:custom_color_settings[gradient_settings][radial_settings][size]'),
							'extended' => array(esc_html__( 'Extended', 'the8' ), false , 'd:custom_color_settings[gradient_settings][radial_settings][shape];e:custom_color_settings[gradient_settings][radial_settings][size_keyword];e:custom_color_settings[gradient_settings][radial_settings][size]' )
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][shape]' =>  array(
						'title' => esc_html__( 'Shape', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'ellipse' => array(esc_html__( 'Ellipse', 'the8' ), true ),
							'circle' => array(esc_html__( 'Circle', 'the8' ), false )
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][size_keyword]' =>  array(
						'title' => esc_html__( 'Size keyword', 'the8' ),
						'type' => 'select',
						'source' => array(
							'closest-side' => array(esc_html__( 'Closest side', 'the8' ), false ),
							'farthest-side' => array(esc_html__( 'Farthest side', 'the8' ), false ),
							'closest-corner' => array(esc_html__( 'Closest corner', 'the8' ), false ),
							'farthest-corner' => array(esc_html__( 'Farthest corner', 'the8' ), true ),
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][size]' =>  array(
						'title' => esc_html__( 'Size', 'the8' ),
						'desc' => esc_html__( 'Two space separated percent values, for example (60% 55%)', 'the8' ),
						'type' => 'text',
						'required' => array( 'shape_settings', '=', 'extended' ),
						'default' => '',
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[font_color]' => array(
						'title' => esc_html__( 'Font Color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#ffffff"',
						'addrowclasses' => 'disable',
					)
				)
			),
			'testimonial' => array(
				'title' => esc_html__( 'CWS Testimonial', 'the8' ),
				'icon' => 'dashicons dashicons-testimonial',
				'fields' => array(
					'thumbnail' => array(
						'title' => esc_html__( 'Photo', 'the8' ),
						'type' => 'media'
					),
					'text' => array(
						'title' => esc_html__( 'Text', 'the8' ),
						'type' => 'textarea',
						'atts' => 'row="5"'
					),
					'author' => array(
						'title' => esc_html__( 'Author', 'the8' ),
						'type' => 'text',
					),
					'url' => array(
						'title' => esc_html__( 'Url', 'the8' ),
						'type' => 'text',
						'default' => ''
					),
					'custom_colors' => array(
						'title' => esc_html__( 'Customize', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:custom_color_settings[fill_type];e:custom_color_settings[font_color]"',
					),
					'custom_color_settings[fill_type]' => array(
						'title' => esc_html__( 'Type', 'the8' ),
						'type' => 'select',
						'source' => array(
							'color' => array(esc_html__( 'Color', 'the8' ), true , 'e:custom_color_settings[fill_color];d:custom_color_settings[gradient_settings][first_color];d:custom_color_settings[gradient_settings][second_color];d:custom_color_settings[gradient_settings][type];'),
							'gradient' => array(esc_html__( 'Gradient', 'the8' ), false , 'd:custom_color_settings[fill_color];e:custom_color_settings[gradient_settings][first_color];e:custom_color_settings[gradient_settings][second_color];e:custom_color_settings[gradient_settings][type];')
							),
						'addrowclasses' => 'disable',
						),
					'custom_color_settings[fill_color]' => array(
						'title' => esc_html__( 'Fill color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
						),
					'custom_color_settings[gradient_settings][first_color]' => array(
						'title' => esc_html__( 'Start color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][second_color]' => array(
						'title' => esc_html__( 'End color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#0eecbd"',
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][type]' => array(
						'title' => esc_html__( 'Gradient type', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'linear' => array(esc_html__( 'Linear', 'the8' ), true , 'e:custom_color_settings[gradient_settings][linear_settings][angle];d:custom_color_settings[gradient_settings][radial_settings][shape_settings];'),
							'radial' => array(esc_html__( 'Radial', 'the8' ), false , 'd:custom_color_settings[gradient_settings][linear_settings][angle];e:custom_color_settings[gradient_settings][radial_settings][shape_settings];')
						),
						'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][linear_settings][angle]' => array(
						'title' => esc_html__( 'Linear gradient settings angle', 'the8' ),
						'type' => 'text',
				 		'value' => '45',
				 		'addrowclasses' => 'disable',
					),
					'custom_color_settings[gradient_settings][radial_settings][shape_settings]' =>  array(
						'title' => esc_html__( 'Shape settings type', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'simple' => array(esc_html__( 'Simple', 'the8' ), true , 'e:custom_color_settings[gradient_settings][radial_settings][shape];d:custom_color_settings[gradient_settings][radial_settings][size_keyword];d:custom_color_settings[gradient_settings][radial_settings][size]'),
							'extended' => array(esc_html__( 'Extended', 'the8' ), false , 'd:custom_color_settings[gradient_settings][radial_settings][shape];e:custom_color_settings[gradient_settings][radial_settings][size_keyword];e:custom_color_settings[gradient_settings][radial_settings][size]' )
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][shape]' =>  array(
						'title' => esc_html__( 'Shape', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'ellipse' => array(esc_html__( 'Ellipse', 'the8' ), true ),
							'circle' => array(esc_html__( 'Circle', 'the8' ), false )
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][size_keyword]' =>  array(
						'title' => esc_html__( 'Size keyword', 'the8' ),
						'type' => 'select',
						'source' => array(
							'closest-side' => array(esc_html__( 'Closest side', 'the8' ), false ),
							'farthest-side' => array(esc_html__( 'Farthest side', 'the8' ), false ),
							'closest-corner' => array(esc_html__( 'Closest corner', 'the8' ), false ),
							'farthest-corner' => array(esc_html__( 'Farthest corner', 'the8' ), true ),
						),
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[gradient_settings][radial_settings][size]' =>  array(
						'title' => esc_html__( 'Size', 'the8' ),
						'desc' => esc_html__( 'Two space separated percent values, for example (60% 55%)', 'the8' ),
						'type' => 'text',
						'required' => array( 'shape_settings', '=', 'extended' ),
						'default' => '',
						'addrowclasses' => 'disable'
					),
					'custom_color_settings[font_color]' => array(
						'title' => esc_html__( 'Font Color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#ffffff"',
						'addrowclasses' => 'disable',
					)
				)
			),
			'embed' => array(
				'title' => esc_html__( 'Embed audio/video file', 'the8' ),
				'icon' => 'dashicons dashicons-format-video',
				'fields' => array(
					'url' => array(
						'title' => esc_html__( 'Url', 'the8' ),
						'desc' => esc_html__( 'Embed url', 'the8' ),
						'type' => 'text',
					),
					'width' => array(
						'title' => esc_html__( 'Width', 'the8' ),
						'desc' => esc_html__( 'Max width in pixels', 'the8' ),
						'type' => 'number',
					),
					'height' => array(
						'title' => esc_html__( 'Height', 'the8' ),
						'desc' => esc_html__( 'Max height in pixels', 'the8' ),
						'type' => 'number',
					)
				)
			),
			'divider' => array(
				'title' => esc_html__( 'CWS Divider', 'the8' ),
				'icon' => 'mce-ico mce-i-hr',
				'fields' => array(
					'divi_style' => array(
						'title' => esc_html__( 'Width', 'the8' ),
						'type' => 'select',
						'source' => array(
							'long' => array(esc_html__( 'long', 'the8' ), true, 'd:width'),
							'short' => array(esc_html__( 'short', 'the8' ), false, 'd:width'),
							'custom' => array(esc_html__( 'custom size', 'the8' ), false, 'e:width')
						),
					),
					'width' => array(
						'title' => esc_html__( 'Width', 'the8' ),
						'desc' => esc_html__( 'Max width in pixels', 'the8' ),
						'type' => 'number',
						'value' => '50'
					),
					'height' => array(
						'title' => esc_html__( 'Height', 'the8' ),
						'desc' => esc_html__( 'Max height in pixels', 'the8' ),
						'type' => 'number',
						'value' => '1'
					),
					'border_style' => array(
						'title' => esc_html__( 'Border', 'the8' ),
						'type' => 'select',
						'source' => array(
							'solid' => array(esc_html__( 'solid', 'the8' ), true),
							'dashed' => array(esc_html__( 'dashed', 'the8' ), false),
							'dotted' => array(esc_html__( 'dotted', 'the8' ), false)
						),
					),
					'color' => array(
						'title' => esc_html__( 'Color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#f2f2f2"'
					),
					'alignment' => array(
						'title' => esc_html__( 'Alignment', 'the8' ),
						'type' => 'select',
						'source' => array(
							'center' => array(esc_html__( 'center', 'the8' ), true),
							'left' => array(esc_html__( 'left', 'the8' ), false),
							'right' => array(esc_html__( 'right', 'the8' ), false)
						),
					),
					'margin_top' => array(
						'title' => esc_html__( 'Top margin', 'the8' ),
						'desc' => esc_html__( 'in pixels', 'the8' ),
						'type' => 'number',
						'value' => '20'
					),
					'margin_bottom' => array(
						'title' => esc_html__( 'Bottom margin', 'the8' ),
						'desc' => esc_html__( 'in pixels', 'the8' ),
						'type' => 'number',
						'value' => '20'
					)
				)
			),
			'dropcap' => array(
				'title' => esc_html__( 'CWS Dropcap', 'the8' ),
				'icon' => 'fa fa-font',
				'required' => 'single_char_selected'
			),
			'mark' => array(
				'title' => esc_html__( 'CWS Mark Selection', 'the8' ),
				'icon' => 'fa fa-pencil',
				'paired' => true,
				'required' => 'selection',
				'fields' => array(
					'font_color' => array(
						'title' => esc_html__( 'Font Color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="#ffffff"',
					),
					'bg_color' => array(
						'title' => esc_html__( 'Background Color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
					)
				)
			),
			'custom_list' => array(
				'title' => esc_html__( 'CWS List Selection', 'the8' ),
				'icon' => 'fa fa-list-ul',
				'required' => 'list_selection',
				'fields' => array(
					'list_style' => array(
						'title' => esc_html__( 'List Style', 'the8' ),
						'type' => 'select',
						'source' => array(
							'dot_style' => array(esc_html__( 'Dots', 'the8' ), true, 'd:icon'),
							'checkmarks_style' => array(esc_html__( 'Checkmarks', 'the8' ), false, 'd:icon'),
							'arrow_style' => array(esc_html__( 'Arrow', 'the8' ), false, 'd:icon'),
							'custom_icon_style' => array(esc_html__( 'Custom Icon', 'the8' ), false, 'e:icon'),
						),
					),
					'icon' => array(
						'title' => esc_html__( 'Icon', 'the8' ),
						'type' => 'select',
						'addrowclasses' => 'fai disable',
						'source' => 'fa',
					),
				)
			),
			'carousel' => array(
				'title' => esc_html__( 'CWS Shortcode Carousel', 'the8' ),
				'icon' => 'fa fa-arrows-h',
				'required' => 'sc_selection_or_nothing',
				'paired' => true,
				'def_content' => "<ul><li>" . esc_html__( 'Some content here', 'the8' ) . "</li><li>" . esc_html__( 'Some content here', 'the8' ) . "</li></ul>",
				'fields' => array(
					'title' => array(
						'title' => esc_html__( 'Carousel title', 'the8' ),
						'type' => 'text',
					),
					'control_color' => array(
						'title' => esc_html__( 'Controls color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="'.THE8_COLOR.'"',
					),
					'autoplay' => array(
						'title' => esc_html__( 'Autoplay', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:autoplay_speed;"',
					),	

					'autoplay_speed' => array(
						'title' => esc_html__( 'Autoplay speed', 'the8' ),
						'type' => 'number',
						'value' => '1000',
						'addrowclasses' => 'disable'
					),					
					'columns' => array(
						'title' => esc_html__( 'Columns', 'the8' ),
						'type' => 'select',
						'source' => array(
							'1' => array(esc_html__( 'One', 'the8' ), true),
							'2' => array(esc_html__( 'Two', 'the8' ), false),
							'3' => array(esc_html__( 'Three', 'the8' ), false),
							'4' => array(esc_html__( 'Four', 'the8' ), false)
						),
					)
				)
			)
		);
	}

	public function scg_scripts_enqueue($a) {
		if( $a == 'post-new.php' || $a == 'post.php' ) {
			$prefix = 'cws_sc_';
			$data = array();
			foreach ( $this->cws_tmce_sc_settings_config as $sect_name => $section ) {
				array_push( $data, array(
					'sc_name' => $prefix . $sect_name,
					'title' => isset( $section['title'] ) ? $section['title'] : '',
					'icon' => isset( $section['icon'] ) ? $section['icon'] : '',
					'required' => isset( $section['required'] ) ? $section['required'] : '',
					'def_content' => isset( $section['def_content'] ) ? $section['def_content'] : '',
					'has_options' => isset( $section['fields'] ) && is_array( $section['fields'] ) && !empty( $section['fields'] )
				));
			}
			wp_localize_script('the8-metaboxes-js', 'cws_sc_data', $data);
			wp_register_script( 'cws-redux-sc-settings', get_template_directory_uri() . '/core/js/cws_sc_settings_controller.js', array( 'jquery' ) );
			wp_enqueue_script( 'cws-redux-sc-settings' );
		}
	}

	public function mce_sc_buttons ( $buttons ) {
		$cws_sc_names = array_keys( $this->cws_tmce_sc_settings_config );
		$cws_sc_prefix = 'cws_sc_';
		foreach ($cws_sc_names as $key => $v) {
			$cws_sc_names[$key] = $cws_sc_prefix . $v;
		}
		$buttons = array_merge( $buttons, $cws_sc_names );
		return $buttons;
	}
	public function mce_sc_plugin ( $plugin_array ) {
		$plugin_array['cws_shortcodes'] = get_template_directory_uri() . '/core/js/cws_tmce.js';
		return $plugin_array;
	}
	public function ajax_sc_settings_callback () {
		$shortcode = trim( $_POST['shortcode'] );
		$prefix = 'cws_sc_';
		$selection = isset( $_POST['selection'] ) ? stripslashes( trim( $_POST['selection'] ) ) : '';
		$def_content = isset( $_POST['def_content'] ) ? trim( $_POST['def_content'] ) : '';
		$shortcode = substr($shortcode, 7);
		$paired = isset($this->cws_tmce_sc_settings_config[$shortcode]['paired']) && $this->cws_tmce_sc_settings_config[$shortcode]['paired']? '1' : '0';
		?>
		<script type='text/javascript'>
			var controller = new cws_sc_settings_controller();
		</script>
		<div class="cws_sc_settings_container">
			<input type="hidden" name="cws_sc_name" id="cws_sc_name" value="<?php echo esc_attr($shortcode); ?>" />
			<input type="hidden" name="cws_sc_selection" id="cws_sc_selection" value="<?php echo apply_filters( 'cws_dbl_to_sngl_quotes', $selection); ?>" />
			<input type="hidden" name="cws_sc_def_content" id="cws_sc_def_content" value="<?php echo esc_attr($def_content); ?>" />
			<input type="hidden" name="cws_sc_prefix" id="cws_sc_prefix" value="<?php echo esc_attr($prefix); ?>" />
			<input type="hidden" name="cws_sc_paired" id="cws_sc_paired" value="<?php echo esc_attr($paired); ?>" />
	<?php
		$meta = array(
			array (
				'text' => $selection,
				)
			);
		$sc_fields = $this->cws_tmce_sc_settings_config[$shortcode]['fields'];
		cws_mb_fillMbAttributes($meta, $sc_fields);
		echo cws_mb_print_layout($sc_fields, 'cws_sc_');
	?>
		<input type="submit" class="button button-primary button-large" id="cws_insert_button" value="<?php esc_html_e('Insert Shortcode', 'the8' ) ?>">
		</div>
	<?php
		wp_die();
	}
}
?>
