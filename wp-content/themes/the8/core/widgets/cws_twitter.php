<?php
	/**
	 * CWS Twitter Widget Class
	 */
class CWS_Twitter extends WP_Widget {
	function init_fields () {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget Title', 'the8' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
				),

			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'the8' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_type"',
			),
			'icon_type' => array(
				'title' => esc_html__( 'Icon type', 'the8' ),
				'type' => 'radio',
				'addrowclasses' => 'disable',
				'subtype' => 'images',
				'value' => array(
					'fa' => array( esc_html__( 'icon', 'the8' ), 	true, 	'e:icon_fa;e:icon_color;e:icon_bg_type;d:icon_img', get_template_directory_uri() . '/core/images/align-left.png' ),
					'img' =>array( esc_html__( 'image', 'the8' ), false,	'd:icon_fa;d:icon_color;d:icon_bg_type;e:icon_img', get_template_directory_uri() . '/core/images/align-right.png' ),
				),
			),
			'icon_fa' => array(
				'title' => esc_html__( 'Font Awesome character', 'the8' ),
				'type' => 'select',
				'addrowclasses' => 'disable fai',
				'source' => 'fa',
			),
			'icon_img' => array(
				'title' => esc_html__( 'Custom icon', 'the8' ),
				'addrowclasses' => 'disable',
				'type' => 'media',
			),
			'icon_color' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Icon color', 'the8' ),
				'addrowclasses' => 'disable',
				'atts' => 'data-default-color="'.THE8_COLOR.'"',
			),
			'icon_bg_type' => array(
				'title' => esc_html__( 'Background type', 'the8' ),
				'type' => 'radio',
				'addrowclasses' => 'disable',
				'value' => array(
					'none' => array( esc_html__( 'None', 'the8' ), 	true, 	'd:icon_bgcolor;d:gradient_first_color;d:gradient_second_color;d:gradient_type' ),
					'color' => array( esc_html__( 'Color', 'the8' ), 	true, 	'e:icon_bgcolor;d:gradient_first_color;d:gradient_second_color;d:gradient_type' ),
					'gradient' =>array( esc_html__( 'Gradient', 'the8' ), false,'d:icon_bgcolor;e:gradient_first_color;e:gradient_second_color;e:gradient_type' ),
				),
			),
			'icon_bgcolor' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Icon background color', 'the8' ),
				'addrowclasses' => 'disable',
				'atts' => 'data-default-color="'.THE8_COLOR.'"',
			),

			'gradient_first_color' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'From', 'the8' ),
				'addrowclasses' => 'disable',
				'atts' => 'data-default-color="'.THE8_COLOR.'"',
			),
			'gradient_second_color' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'To', 'the8' ),
				'addrowclasses' => 'disable',
				'atts' => 'data-default-color="#0eecbd"',
			),
			'gradient_type' => array(
				'title' => esc_html__( 'Gradient type', 'the8' ),
				'type' => 'radio',
				'addrowclasses' => 'disable',
				'value' => array(
					'linear' => array( esc_html__( 'Linear', 'the8' ), 	true, 'e:gradient_linear_angle;d:gradient_radial_shape' ),
					'radial' =>array( esc_html__( 'Radial', 'the8' ), false,	'd:gradient_linear_angle;e:gradient_radial_shape' ),
				),
			),
			'gradient_linear_angle' => array(
				'type'      => 'number',
				'title'     => esc_html__( 'Angle', 'the8' ),
				'addrowclasses' => 'disable',
				'value' => '45',
			),
			'gradient_radial_shape' => array(
				'title' => esc_html__( 'Gradient type', 'the8' ),
				'type' => 'radio',
				'addrowclasses' => 'disable',
				'value' => array(
					'simple' => array( esc_html__( 'Simple', 'the8' ), 	true, 'e:gradient_radial_type;d:gradient_radial_size_key;d:gradient_radial_size' ),
					'extended' =>array( esc_html__( 'Extended', 'the8' ), false, 'd:gradient_radial_type;e:gradient_radial_size_key;e:gradient_radial_size' ),
				),
			),
			'gradient_radial_type' => array(
				'title' => esc_html__( 'Gradient type', 'the8' ),
				'type' => 'radio',
				'addrowclasses' => 'disable',
				'value' => array(
					'ellipse' => array( esc_html__( 'Ellipse', 'the8' ), 	true ),
					'circle' =>array( esc_html__( 'Cirle', 'the8' ), false ),
				),
			),
			'gradient_radial_size_key' => array(
				'type' => 'select',
				'title' => esc_html__( 'Size keyword', 'the8' ),
				'addrowclasses' => 'disable',
				'source' => array(
					'closest-side' => array(esc_html__( 'Closest side', 'the8' ), false),
					'farthest-side' => array(esc_html__( 'Farthest side', 'the8' ), false),
					'closest-corner' => array(esc_html__( 'Closest corner', 'the8' ), false),
					'farthest-corner' => array(esc_html__( 'Farthest corner', 'the8' ), true),
				),
			),
			'gradient_radial_size' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Size', 'the8' ),
				'addrowclasses' => 'disable',
				'atts' => 'placeholder="'.esc_html__('Two space separated percent values, for example (60% 55%)', 'the8').'"',
			),
			'items' => array(
				'title' => esc_html__( 'Tweets to extract', 'the8' ),
				'type' => 'number',
				'value' => get_option( 'posts_per_page' )
			),
			'visible' => array(
				'title' => esc_html__( 'Tweets to show', 'the8' ),
				'type' => 'number',
				'value' => get_option( 'posts_per_page' )
			),
			'showdate' => array(
				'title' => esc_html__( 'Show date', 'the8' ),
				'type' => 'checkbox',
			)
		);
	}

	function __construct(){
		$widget_ops = array('classname' => 'widget_cws_twitter', 'description' => esc_html__( 'CWS Twitter Widget', 'the8' ) );
		parent::__construct('cws-twitter', esc_html__('CWS Twitter', 'the8' ), $widget_ops);
	}

	function widget ( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'show_icon_opts' => '0',
		), $instance));

		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;
		$widget_title_icon = $show_icon_opts === '1' ? the8_widget_title_icon_rendering( $instance ) : '';

		echo $before_widget;
			if ( !empty( $widget_title_icon ) ){
				if ( !empty( $title ) ){
					echo $before_title . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
				}
				else{
					echo $before_title . $widget_title_icon . $after_title;
				}
			}
			else if ( !empty( $title ) ){
				echo $before_title . $title . $after_title;
			}

			$twitter_args = array(
				'in_widget' => true
			);
			if ( isset( $instance['items'] ) ) $twitter_args['items'] = $instance['items'];
			if ( isset( $instance['visible'] ) ) $twitter_args['visible'] = $instance['visible'];
			if ( isset( $instance['showdate'] ) ) $twitter_args['showdate'] = $instance['showdate'];

			echo the8_twitter_renderer( $twitter_args );

		echo $after_widget;
	}

	function form ( $instance ){
		if (function_exists('getTweets')) {
		$this->init_fields();
		$args[0] = $instance;
		cws_mb_fillMbAttributes( $args, $this->fields );
		echo cws_mb_print_layout( $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		} else {
			echo 'You need to install and activate <b>oAuth Twitter Feed for Developers</b> plugin';
		}
	}
}
?>