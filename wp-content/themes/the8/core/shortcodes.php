<?php
load_template( trailingslashit( get_template_directory() ) . '/core/scg.php');
new The8_SCG();
/* THE 8 TINYMCE SHORTCODES */

function add_cws_shortcode($name, $callback)  {
	$short = 'shortcode';
	call_user_func('add_' . $short, $name, $callback);
}

function the8_shortcode_msg_box ( $atts, $content ) {
	extract( shortcode_atts( array(
		'type' => 'info',
		'title' => '',
		'text' => '',
		'is_closable' => '0',
		'customize' => '0',
		'custom_options' => array()
	), $atts));

	$custom_options = the8_json_sc_attr_conversion ( $custom_options );
	$custom_options = is_object( $custom_options ) ? get_object_vars( $custom_options ) : array();
	$custom_options = isset( $custom_options['@'] ) ? $custom_options['@'] : new stdClass();
	$custom_color = isset( $custom_options->color ) ? $custom_options->color : '';
	$custom_bg_color = isset( $custom_options->bg_color ) ? $custom_options->bg_color : '';
	$custom_icon = isset( $custom_options->icon ) ? $custom_options->icon : '';

	$out = "";
	$icon = $customize == '1' && !empty( $custom_icon ) ? $custom_icon : '';
	if ( empty( $icon ) ) {
		switch ($type){
			case 'info':
				$icon = 'cwsicon-info31';
				break;
			case 'warning':
				$icon = 'cwsicon-lightning24';
				break;
			case 'success':
				$icon = 'cwsicon-check-mark';
				break;
			case 'error':
				$icon = 'cwsicon-exclamation-mark1';
				break;
		}
	}

	ob_start();
		echo ! empty( $title ) ? '<div class="msg_box_title">'.esc_html( $title ).'</div>' : '';
		echo ! empty( $text ) ? '<div class="msg_box_text">'. $text .'</div>' : '';
	$content_box = ob_get_clean();

	$custom_styles = $customize == '1' && !empty( $custom_color ) ? "color:$custom_color;background-color:$custom_bg_color" : "";
	$container_class = "cws_msg_box $type-box clearfix";
	$container_class .= $is_closable == '1' ? " closable" : "";

	if ( !empty( $content_box ) ) {
		$out .= "<div class='$container_class'" . ( !empty( $custom_styles ) ? " style='$custom_styles'" : "" ) . ">";
			$out .= "<div class='icon_section'><i class='$icon'></i></div>";
			$out .= "<div class='content_section'>$content_box</div>";
			$out .= $is_closable == '1' ? "<div class='cls_btn'></div>" : "";
		$out .= "</div>";
	}

	return $out;
}
add_cws_shortcode( 'cws_sc_msg_box', 'the8_shortcode_msg_box' );

function the8_shortcode_milestone ( $atts, $content ) {
	extract( shortcode_atts( array(
		'icon' => '',
		'icon_color' => '',
		'number' => '',
		'speed' => '',
		'desc' => '',
		'borderless' => '',
		'item_width' => '',
 		'custom_colors' => '0',
		'custom_color_settings' => array()
	), $atts));
	wp_enqueue_script ('odometer');
	$number = (int) $number;
	$speed = (int) $speed;
	$desc = esc_html( $desc );

	$item_width = (int) $item_width;
	$icon_color = esc_html( $icon_color );

	$custom_color_settings = the8_json_sc_attr_conversion ( $custom_color_settings );
	$custom_color_settings = is_object( $custom_color_settings ) ? get_object_vars( $custom_color_settings ) : $custom_color_settings;
	$custom_color_settings = isset( $custom_color_settings['@'] ) ? $custom_color_settings['@'] : new stdClass ();
	$fill_type = isset( $custom_color_settings->fill_type ) ? $custom_color_settings->fill_type : 'transparent';
	$fill_color = isset( $custom_color_settings->fill_color ) ? $custom_color_settings->fill_color : THE8_COLOR;
	$font_color = isset( $custom_color_settings->font_color ) ? $custom_color_settings->font_color : '#fff';
	$border_color = isset( $custom_color_settings->border_color ) ? $custom_color_settings->border_color : THE8_COLOR;
	$border_color = $borderless == '1' ? 'transparent' :  $border_color;

	$gradient_settings = isset( $custom_color_settings->gradient_settings ) ? $custom_color_settings->gradient_settings : new stdClass ();
	$gradient_settings = is_object( $gradient_settings ) ? get_object_vars( $gradient_settings ) : array();
	//$alt_style = $custom_colors && $fill_type == 'color' && $fill_color == 'transparent' ? true : false;

	$el_atts = "";
	$el_class = "";
	$el_styles = "";
	$el_class = "cws_milestone";
	$el_class .= $borderless == '1' ? " borderless" : "";
	$el_class .= $borderless == '1' && $custom_colors ? " borderless_vs_bg" : "";
	$el_class .= $custom_colors && $fill_type == 'color' ? " kill_gradient" : "";
	$el_atts .= !empty( $el_class ) ? " class='$el_class'" : "";

	$el_styles .= !empty( $font_color ) ? "color:".$font_color.";" : "";
	$el_styles .= !empty($item_width) ? 'width:'.$item_width.'px;' : "";
	$el_styles .= $borderless == '1' ? "border-color:transparent;" : "border-color:$border_color;";

	if ( $custom_colors && $fill_type == 'transparent' ) {
		$el_styles .= "background-color:transparent;";
	}
	if ( $custom_colors && $fill_type == 'color' ) {
		$el_styles .= "background-color:$fill_color;";
	}
	if ( $custom_colors && $fill_type == 'gradient' ) {
		$el_styles .= the8_render_gradient_rules( array(
			'settings' => $gradient_settings,
			'use_extra_rules' => true
		));

		$el_styles .= "color:$font_color !important;";
		$el_class .= " custom_gradient";
	}

	$el_atts .= !empty( $el_styles ) ? " style='$el_styles'" : "";

	$out = "";
	$out .= !empty( $icon ) ? "<span class='milestone_icon' style='color:".$icon_color.";'><i class='$icon'></i></span>" : "";
	$out .= !empty( $number ) ? "<span class='milestone_number'" . ( !empty( $speed ) ? " data-speed='$speed'" : "" ) . ">$number</span>" : "";
	$out .= !empty( $desc ) ? "<span class='milestone_desc'>$desc</span>" : "";
	$out = !empty( $out ) ? "<span" . ( !empty( $el_atts ) ? $el_atts : '' ) . ">$out</span>" : "";
	return $out;
}
add_cws_shortcode( 'cws_sc_milestone', 'the8_shortcode_milestone' );

function the8_shortcode_progress_bar ( $atts, $content ) {
	extract( shortcode_atts( array(
		'title' => '',
		'progress' => '',
		'custom_colors' => '0',
		'custom_color_settings' => array()
	), $atts));
	if ( !intval( $progress ) || $progress < 0 || $progress > 100 ) return;
	$gradient_settings = array();
	$fill_type = "color";
	$fill_color = "";
	$gradient_color_1 = "";
	$gradient_color_2 = "";
	$progress_atts = "";
	$progress_class = "progress";
	$progress_styles = "";

	$progress = (int) $progress;
	$title = esc_html( $title );

	if ( $custom_colors ) {
		$custom_color_settings = the8_json_sc_attr_conversion ( $custom_color_settings );
		$custom_color_settings = is_object( $custom_color_settings ) ? get_object_vars( $custom_color_settings ) : array();
		$custom_color_settings = isset( $custom_color_settings['@'] ) ? $custom_color_settings['@'] : new stdClass();

		$fill_type = isset( $custom_color_settings->fill_type ) ? $custom_color_settings->fill_type : $fill_type;
		$fill_color = isset( $custom_color_settings->fill_color ) ? $custom_color_settings->fill_color : $fill_color;
		$progress_class .= " custom_colors";
		$gradient_settings = isset( $custom_color_settings->gradient_settings ) && is_object( $custom_color_settings->gradient_settings ) ? $custom_color_settings->gradient_settings : new stdClass ();
		$gradient_settings = is_object( $gradient_settings ) ? get_object_vars( $gradient_settings ) : array();
		$gradient_settings = isset( $gradient_settings['@'] ) ? $gradient_settings['@'] : new stdClass ();
		$gradient_color_1 = isset( $gradient_settings->first_color ) ? $gradient_settings->first_color : $gradient_color_1;
		$gradient_color_2 = isset( $gradient_settings->second_color ) ? $gradient_settings->second_color : $gradient_color_2;

		if ( $fill_type == 'color' && !empty( $fill_color ) ) {
			$progress_styles .= "background-color:$fill_color;";
			$progress_class .= " kill_gradient";
		}
		else if ( $fill_type == 'gradient' && !empty( $gradient_color_1 ) && !empty( $gradient_color_2 ) ) {
			$progress_styles .= "background-image:-webkit-linear-gradient(left,$gradient_color_1,$gradient_color_2);";
			$progress_styles .= "background-image:-o-linear-gradient(left,$gradient_color_1,$gradient_color_2);";
			$progress_styles .= "background-image:-moz-linear-gradient(left,$gradient_color_1,$gradient_color_2);";
			$progress_styles .= "background-image:linear-gradient(to right,$gradient_color_1,$gradient_color_2);";
		}
	}
	$progress_atts .= " data-value='$progress'";
	$progress_atts .= !empty( $progress_class ) ? " class='$progress_class'" : "";
	$progress_atts .= !empty( $progress_styles ) ? " style='$progress_styles'" : "";
	$out = "";
	$out .= "<div class='cws_progress_bar'>";
		$out .= !empty( $title ) ? "<div class='pb_title'>$title <span class='indicator'></span></div>" : "";
		$out .= "<div class='bar'><div " . trim( $progress_atts ) . "></div></div>";
	$out .= "</div>";
	return $out;
}
add_cws_shortcode( 'cws_sc_progress_bar', 'the8_shortcode_progress_bar' );

function the8_shortcode_fa ( $atts, $content ) {
	extract( shortcode_atts( array(
		'icon' => '',
		'shape' => 'round',
		'bordered_icon' => '',
		'simple_style_icon' => '',
		'alt' => '',
		'size' => '2x',
		'align_icon' => 'left',
		'custom_colors' => '',
		'custom_color_settings' => array(),
		'link' => '0',
		'add_hover' => '0',
		'url' => ''
	), $atts));

	if ( empty( $icon ) ) return;

	$alt = $alt == '1' ? true : false;
	$link = $link == '1' ? true : false;
	$add_hover = $add_hover == '1' ? true : false;
	$bordered_icon = $bordered_icon == '1' ? true : false;
	$simple_style_icon = $simple_style_icon == '1' ? true : false;
	$custom_color_settings = the8_json_sc_attr_conversion ( $custom_color_settings );
	$custom_color_settings = is_object( $custom_color_settings ) ? get_object_vars( $custom_color_settings ) : array();
	$custom_color_settings = isset( $custom_color_settings['@'] ) ? $custom_color_settings['@'] : new stdClass ();
	$fill_type = 'color';
	$fill_color = isset( $custom_color_settings->fill_color ) ? esc_attr($custom_color_settings->fill_color) : THE8_COLOR;
	$font_color = isset( $custom_color_settings->font_color ) ? esc_attr($custom_color_settings->font_color) : '#fff';
	$gradient_settings = isset( $custom_color_settings->gradient_settings ) ? $custom_color_settings->gradient_settings : new stdClass ();
	$gradient_settings = is_object( $gradient_settings ) ? get_object_vars( $gradient_settings ) : array();

	//$alt_style = $custom_colors && $fill_type == 'color' && $fill_color == 'transparent' ? true : false;
	//$alt = $fill_type == 'color' && $fill_color == 'transparent' ? true : false;

	$title_match = preg_match( "#(?s)<h\d[^>]*>.*<\/h\d>#", $content, $title_matches, PREG_OFFSET_CAPTURE );
	$title_match_array = $title_match && isset( $title_matches[0] ) ? $title_matches[0] : array();
	$title = isset( $title_match_array[0] ) ? $title_match_array[0] : "";
	$title_ind = isset( $title_match_array[1] ) ? $title_match_array[1] : null;

	$desc = $title_match ? substr_replace( $content, "", $title_ind, strlen( $title ) ) : $content;

	$el_class = "cws_fa";
	$el_styles = "";
	$ring_gradient_color = "";
	$el_atts = "";
	$el_class .= " $icon";
	$el_class .= !empty( $size ) ? " fa-$size" : "";
	$el_class .= $custom_colors ? " custom_colors" : "";

	$el_class .= $shape == "square" ? " square" : "";
	$el_class .= $alt ? " alt" : "";
	$el_class .= $bordered_icon ? " bordered_icon" : "";
	$el_class .= $simple_style_icon ? " simple_icon" : "";
	$el_class .= !$add_hover ? " dis_hover" : "";
	if ( $custom_colors && $fill_type == 'color' && !empty( $fill_color ) && !empty( $font_color ) ) {
		$el_atts .= " data-font-color='$font_color'";
		$el_atts .= " data-bg-color='$fill_color'";
		$el_class .= " kill_gradient";
		$el_styles .= "color: $font_color;";
	}

	$el_atts .= !empty( $el_class ) ? " class='" . trim( $el_class ) . "'" : "";

	$el_atts .= !empty( $el_styles ) ? " style='" . trim( $el_styles ) . "'" : "";


	$out = "";
	if ( !empty( $title ) || !empty( $desc ) ) {
	$out .= $link ? '<a href="'.$url.'" class="icon_link">' : '';
		$out .= "<div class='cws_fa_tbl".(!empty($align_icon) ? ' icon-'.$align_icon : '')."'>";
			$out .= "<div class='cws_fa_tbl_row'>";
				if (!empty($align_icon) && $align_icon != 'right') {
					$out .= "<div class='cws_fa_tbl_cell size_$size''>";
						$out .= $bordered_icon ? "<span class='cws_fa_wrapper". ( $shape == "square" ? " square" : "" ) . ($simple_style_icon ? " simple_icon" : "" ) . ( $add_hover ? "" : " no_hover") ."'>" : "";
							$out .="<i $el_atts></i>";
							$out .= $bordered_icon ? "<span class='ring'></span>" : "";
						$out .= $bordered_icon ? "</span>" : "";
					$out .= "</div>";
				}
				$out .= "<div class='cws_fa_tbl_cell'>";
					$out .= $title;
					$out .= $desc;
				$out .= "</div>";
				if (!empty($align_icon) && $align_icon == 'right') {
					$out .= "<div class='cws_fa_tbl_cell size_$size''>";
						$out .= $bordered_icon ? "<span class='cws_fa_wrapper". ( $shape == "square" ? " square" : "" ) . ($simple_style_icon ? " simple_icon" : "" ) . ( $add_hover ? "" : " no_hover") ."'>" : "";
							$out .="<i $el_atts></i>";
							$out .= $bordered_icon ? "<span class='ring'></span>" : "";
						$out .= $bordered_icon ? "</span>" : "";
					$out .= "</div>";
				}
			$out .= "</div>";
		$out .= "</div>";
	$out .= $link ? '</a>' : '';
	}else{
	$out .= $link ? '<a href="'.$url.'" class="icon_link">' : '';
		$out .= ($bordered_icon && !$simple_style_icon) ? "<span class='cws_fa_wrapper". ( $shape == "square" ? " square" : "" ) . ( $add_hover ? "" : " no_hover") ."'>" : "";
			$out .="<i $el_atts></i>";
			$out .= ($bordered_icon && !$simple_style_icon) ? "<span class='ring'".(!empty($ring_gradient_color) ? $ring_gradient_color : '')."></span>" : "";
		$out .= ($bordered_icon && !$simple_style_icon) ? "</span>" : "";
	$out .= $link ? '</a>' : '';
	}
	return $out;
}
add_cws_shortcode( 'cws_sc_fa', 'the8_shortcode_fa' );

function the8_shortcode_button ( $atts, $content ) {

	extract( shortcode_atts( array(
		'title' => '',
		'url' => '#',
		'size' => 'regular',
		'ofs' => '',
		'alt' => '0',
		'icon' => '',
		'full_width' => '0',
		'custom_colors' => '0',
		'new_tab' => '0',
		'custom_color_settings' => array()
	), $atts));

	if ( empty( $title ) ) { return; }

	$ofs = (int) $ofs;
	$custom_color_settings = the8_json_sc_attr_conversion ( $custom_color_settings );
	$custom_color_settings = is_object( $custom_color_settings ) ? get_object_vars( $custom_color_settings ) : array();
	$custom_color_settings = isset( $custom_color_settings['@'] ) ? $custom_color_settings['@'] : new stdClass ();
	$fill_type = isset( $custom_color_settings->fill_type ) ? esc_attr($custom_color_settings->fill_type) : 'color';
	$fill_color = isset( $custom_color_settings->fill_color ) ? esc_attr($custom_color_settings->fill_color) : THE8_COLOR;
	$font_color = isset( $custom_color_settings->font_color ) ? esc_attr($custom_color_settings->font_color) : '#fff';
	$gradient_settings = isset( $custom_color_settings->gradient_settings ) ? $custom_color_settings->gradient_settings : new stdClass ();
	$gradient_settings = is_object( $gradient_settings ) ? get_object_vars( $gradient_settings ) : array();
	$alt_class = ($alt === '1');

	$size = esc_html( $size );
	$alt_style = $custom_colors && $fill_type == 'color' && $fill_color == 'transparent' ? true : false;
	$alt = $fill_type == 'color' && $fill_color == 'transparent' ? true : false;

	$icon_content = !empty($icon) ? '<i class="button-icon '.$icon.'"></i>' : '' ;

	$el_class = "cws_button";
	$el_class .= $full_width ? " full_width" : "";
	$el_class .= !empty($icon) ? " icon-on" : "";
	$el_class .= $size != " regular" ? " $size" : "";
	$el_class .= $alt_class ? " alt" : "";
	$el_styles = "";
	$el_atts = "";
	$icons_padding = "";
	$el_atts .= !empty( $url ) ? " href='$url'" : "";
	$el_atts .= $new_tab ? " target='_blank'" : "";
	$el_class .= $custom_colors ? " custom_colors" : "";
	$el_class .= $alt ? " alt" : "";
	$icons_padding = 0;
	if (!empty($icon) && isset($ofs) && !empty($ofs)) {
		switch ($size) {
			case 'mini':
				$icons_padding = 30;
				break;
			case 'small':
				$icons_padding = 35;
				break;
			case 'regular':
				$icons_padding = 40;
				break;
			case 'large':
				$icons_padding = 45;
				break;
			case 'xlarge':
				$icons_padding = 52;
				break;
			default:
				$icons_padding = 40;
				break;
		}

	}
	if (isset($ofs) && !empty($ofs)) {
		$el_styles .= 'padding-left:'.($ofs).'px; padding-right:'.($ofs + $icons_padding).'px;';
	}
	if ( $custom_colors && $fill_type == 'color' && !empty( $fill_color ) && !empty( $font_color ) ) {
		$el_atts .= " data-font-color='$font_color'";
		$el_atts .= " data-bg-color='$fill_color'";
		$el_class .= " kill_gradient";
		$el_styles .= "color: $font_color;";
	}
	if ( $custom_colors && $fill_type == 'gradient' ) {
		$el_styles .= the8_render_gradient_rules(array(
			'settings' => $gradient_settings,
			'use_extra_rules' => true
		));
		$el_styles .= "color: $font_color !important;";
		$el_class .= " custom_gradient";
	}
	$el_atts .= !empty( $el_class ) ? " class='" . trim( $el_class ) . "'" : "";
	$el_atts .= !empty( $el_styles ) ? " style='" . trim( $el_styles ) . "'" : "";

	$out = "";
	$out .= "<a" . ( !empty( $el_atts ) ? $el_atts : "" ) . ">$title".(!empty($icon) ? $icon_content : '')."</a>";
	return $out;
}
add_cws_shortcode( 'cws_sc_button', 'the8_shortcode_button' );

function the8_shortcode_testimonial ( $atts, $content ) {
	extract( shortcode_atts( array(
		'thumbnail' => '',
		'text' => '',
		'author' => '',
		'url' => '',
		'custom_colors' => '0',
		'custom_color_settings' => array()
	), $atts));

	$custom_colors = $custom_colors == '1' ? true : false;
	$custom_color_settings = the8_json_sc_attr_conversion ( $custom_color_settings );
	$custom_color_settings = is_object( $custom_color_settings ) ? get_object_vars( $custom_color_settings ) : array();
	$custom_color_settings = isset( $custom_color_settings['@'] ) ? $custom_color_settings['@'] : new stdClass();

	$thumbnail = the8_json_sc_attr_conversion ( $thumbnail );
	$thumbnail = is_object( $thumbnail ) ? get_object_vars( $thumbnail ) : array();
	$thumbnail = isset( $thumbnail['@'] ) ? $thumbnail['@'] : new stdClass();
	$thumb_url = isset( $thumbnail->src ) ? $thumbnail->src : '';

	$args_array = array(
		$thumb_url,
		$text,
		$author,
		$url
	);

	if ( $custom_colors && is_object( $custom_color_settings ) && !empty( $custom_color_settings ) ) array_push( $args_array, $custom_color_settings );
	if ( !$custom_colors && is_object( $custom_color_settings ) ) {
		$custom_color_settings = (array)$custom_color_settings;
		$custom_color_settings["fill_type"] = "color";
		$custom_color_settings["fill_color"] = THE8_COLOR;
		$custom_color_settings["font_color"] = '#ffffff';
		$custom_color_settings = (object)$custom_color_settings;
		array_push( $args_array, $custom_color_settings );
	}
	return call_user_func_array( "the8_testimonial_renderer", $args_array );
}
add_cws_shortcode( 'cws_sc_testimonial', 'the8_shortcode_testimonial' );

function the8_shortcode_embed ( $atts, $content ) {
	extract( shortcode_atts( array(
		'url' => '',
		'width' => '',
		'height' => ''
	), $atts));
	$url = esc_url( $url );
	return !empty( $url ) ? apply_filters( "the_content", "[embed" . ( !empty( $width ) && is_numeric( $width ) ? " width='$width'" : "" ) . ( !empty( $height ) && is_numeric( $height ) ? " height='$height'" : "" ) . "]" . $url . "[/embed]" ) : "";
}
add_cws_shortcode( 'cws_sc_embed', 'the8_shortcode_embed' );

function the8_shortcode_dropcap ( $atts, $content ) {
	return !empty( $content ) ? "<span class='dropcap'>$content</span>" : "";
}
add_cws_shortcode( 'cws_sc_dropcap', 'the8_shortcode_dropcap' );

function the8_shortcode_mark ( $atts, $content ) {
	extract( shortcode_atts( array(
		'font_color' => '#fff',
		'bg_color' => THE8_COLOR
	), $atts));
	$font_color = esc_attr( $font_color );
	$bg_color = esc_attr( $bg_color );
	return !empty( $content ) ? "<mark style='color:$font_color;background-color:$bg_color;'>$content</mark>" : "";
}
add_cws_shortcode( 'cws_sc_mark', 'the8_shortcode_mark' );

function the8_shortcode_divider ( $atts, $content ) {
	extract( shortcode_atts( array(
		'divi_style' => '',
		'width' => '',
		'height' => '',
		'border_style' => '',
		'color' => '',
		'alignment' => 'center',
		'margin_top' => '',
		'margin_bottom' => ''
	), $atts));

	$divi_class = '';
	$divi_css = '';
	$divi_wrapp_class = '';

	$divi_style = esc_attr( $divi_style );
	$border_style = esc_attr( $border_style );
	$color = esc_attr( $color );
	$alignment = esc_attr( $alignment );
	$margin_top = esc_attr( $margin_top );
	$margin_bottom = esc_attr( $margin_bottom );


	$height = !empty($height) ? esc_attr( $height ) : '';
	$width = !empty($width) ? esc_attr( $width ) : '';

	$divi_css .= $divi_style == 'custom' ? 'width:'.$width.'px;' : '';
	$divi_css .= !empty($color) ? 'border-bottom-color:'.$color.';' : '';
	$divi_css .= !empty($height) ? 'border-bottom-width:'.$height.'px;' : '';
	$divi_css .= !empty($margin_top) ? 'margin-top:'.$margin_top.'px;' : '';
	$divi_css .= !empty($margin_bottom) ? 'margin-bottom:'.$margin_bottom.'px;' : '';

	$divi_class .= 'cws_divider';
	$divi_class .= $divi_style != 'custom' ? ' ' . $divi_style : '';
	$divi_class .= ' ' . $border_style;
	$divi_wrapp_class .= !empty($alignment) ? ' align-' . $alignment : '';

	$divi_attr = " class='".$divi_class."' ".(!empty($divi_css) ? 'style="'.$divi_css.'"' : '')."";

	return "<div".((!empty($divi_attr) && $divi_style == 'long') ? $divi_attr : ' class="cws_divider_wrapper'.$divi_wrapp_class.'"').">".($divi_style != 'long' ? '<span'.(!empty($divi_attr) ? $divi_attr : '').'></span>' : '')."</div>";
}
add_cws_shortcode( 'cws_sc_divider', 'the8_shortcode_divider' );

function the8_shortcode_carousel ( $atts, $content ) {
	extract( shortcode_atts( array(
		'title' => '',
		'control_color' => '',
		'autoplay' => '',
		'autoplay_speed' => '1000',		
		'columns' => '1'
	), $atts));
	$control_color = esc_html($control_color);

	$has_title = !empty( $title );
	$section_class = "cws_sc_carousel";
	$section_class .= $control_color != THE8_COLOR ? " custom-control-color" : "";
	$section_class .= !$has_title ? " bullets_nav" : "";
	$section_class .= ($autoplay == '1') ? " autoplay" : "";	

	$title = esc_html( $title );
	$section_atts = ' data-columns='. (int) $columns;
	$section_atts .= $control_color != THE8_COLOR ? ' data-customcontrol="'.$control_color.'"' : '';
	$uniqid = uniqid('id-for-ccc-');
	$section_atts .= (!$has_title && $control_color != THE8_COLOR) ? ' id="'.$uniqid.'"' : '';
	$section_atts .= ($autoplay == '1' && !empty( $autoplay_speed )) ? ' data-autoplay="'.$autoplay_speed.'"' : '';
	$out = "";


	if (!$has_title && $control_color != THE8_COLOR) {
		$style_out = '<style type="text/css">';
			$style_out .= '#'.$uniqid.' .owl-pagination .owl-page{
				-webkit-box-shadow: 0px 0px 0px 1px '.$control_color.';
				-moz-box-shadow: 0px 0px 0px 1px '.$control_color.';
				box-shadow: 0px 0px 0px 1px '.$control_color.';
			}';
			$style_out .= '#'.$uniqid.' .owl-pagination .owl-page.active{
				background-color: '.$control_color.';
			}';
		$style_out .= '</style>';

	}

	if ( !empty( $content ) ) {
		wp_enqueue_script ('owl_carousel');
		$out .= "<div class='$section_class'" . ( !empty( $section_atts ) ? $section_atts : "" ) . ">";
			if ( $has_title ) {
				$out .= "<div class='cws_sc_carousel_header clearfix'>";
					$out .= "<div class='carousel_nav_panel'>";
						$out .= "<span class='prev'></span>";
						$out .= "<span class='next'></span>";
					$out .= "</div>";
					$out .= $has_title ? "<div class='ce_title'>$title</div>" : "";
				$out .= "</div>";
			}
			$out .= "<div class='cws_wrapper'>";
				$out .= do_shortcode( $content );
			$out .= "</div>";
			$out .= (!$has_title && $control_color != THE8_COLOR) ? $style_out : '';
		$out .= "</div>";
	}
	return $out;
}
add_cws_shortcode( 'cws_sc_carousel', 'the8_shortcode_carousel' );

/* THE 8 PB SHORTCODES */

function the8_shortcode_row ( $sc_atts, $content ) {
	extract( shortcode_atts( array(
		'flags' => '0',
		'cols' => '1',
		'render' => '',
		'atts' => ''
	), $sc_atts));
	$out = "";
	$row_bg_atts = "";
	$row_atts = "";
	$row_bg_class = "row_bg";
	$row_class = "grid_row";
	$row_bg_styles = "";
	$row_styles = "";
	$padding_styles = "";
	$row_bg_html = "";
	$row_content = "";
	$has_bg = false;

	$atts_arr = json_decode( $atts, true );

	extract( shortcode_atts( array(
		'margins' => array(),
		'paddings' => array(),
		'section_border' => '',
		'section_border_width' => '',
		'section_border_style' => '',
		'section_border_color' => '',
		'customize_bg' => '0',
		'bg_media_type' => 'none',
		'bg_img' => array(),
		'bg_attach' => '',
		'bg_possition' => '',
		'bg_repeat' => '',
		'is_bg_img_high_dpi' => '0',
		'bg_video_type' => '1',
		'sh_bg_video_source' => array(),
		'yt_bg_video_source' => '',
		'vimeo_bg_video_source' => '',
		'bg_color_type' => 'none',
		'bg_color' => THE8_COLOR,
		'bg_color_opacity' => '100',
		'bg_pattern' => array(),
		'font_color' => '',
		'animate' => '0',
		'ani_duration' => '2',
		'ani_delay' => '0',
		'ani_offset' => '10',
		'ani_iteration' => '1',
		'ani_effect' => '',
		'use_prlx' => '0',
		'prlx_speed' => '0',
		'eclass' => '',
		'row_style' => 'def',
		'equal_height' => '0',
		'content_position' => ''
	), $atts_arr));


	$margin_styles = '';
	if (! empty( $margins ) ) {
		$margin_styles .= isset( $margins['left'] )  ? 'margin-left: '.esc_attr( $margins['left'] ). 'px;' : '';
		$margin_styles .= isset( $margins['top'] ) ? 'margin-top: '.esc_attr( $margins['top'] ). 'px;' : '';
		$margin_styles .= isset( $margins['right'] ) ? 'margin-right: '.esc_attr( $margins['right'] ). 'px;' : '';
		$margin_styles .= isset( $margins['bottom'] ) ? 'margin-bottom: '.esc_attr( $margins['bottom'] ). 'px;' : '';
	}


	if (! empty( $paddings ) ) {
		$padding_styles .= isset($paddings['left']) ? 'padding-left: '.esc_attr( $paddings['left'] ). 'px;' : '';
		$padding_styles .= isset($paddings['top']) ? 'padding-top: '.esc_attr( $paddings['top'] ). 'px;' : '';
		$padding_styles .= isset($paddings['right']) ? 'padding-right: '.esc_attr( $paddings['right'] ). 'px;' : '';
		$padding_styles .= isset($paddings['bottom']) ? 'padding-bottom: '.esc_attr( $paddings['bottom'] ). 'px;' : '';
	}


	if ($row_style == 'benefits') {
		$row_class .= ' benefits';
		$row_bg_class .= ' benefits_bg';
	}else if ($row_style == 'fullwidth_item') {
		$row_class .= ' fullwidth_items';
		$row_bg_class .= ' fullwidth_items_bg';
	}else if($row_style == 'fullwidth_item_no_padding'){
		$row_class .= ' fullwidth_items no_paddings';
		$row_bg_class .= ' fullwidth_items_bg';
	}else if($row_style == 'fullwidth_background'){
		$row_class .= ' fullwidth_background';
		$row_bg_class .= ' fullwidth_background_bg';
	}

	if ($equal_height == '1' || (!empty($content_position) &&  $content_position == 'middle') || (!empty($content_position) &&  $content_position == 'bottom')) {
		$row_class .= ' cws_flex_row';
	}

	if ($equal_height == '1') {
		$row_class .= ' cws_equal_height';
	}
	if (!empty($content_position) &&  $content_position == 'top') {
		$row_class .= ' cws_content_top';
	}

	if (!empty($content_position) &&  $content_position == 'middle') {
		$row_class .= ' cws_content_middle';
	}

	if (!empty($content_position) &&  $content_position == 'bottom') {
		$row_class .= ' cws_content_bottom';
	}


	$row_bg_img = isset( $bg_img['row'] ) ? esc_url( $bg_img['row'] ) : '';
	$row_bg_img_w = isset( $bg_img['w'] ) ?  $bg_img['w'] : '';
	$row_bg_img_h = isset( $bg_img['h'] ) ? $bg_img['h'] : '';
	$img_style = '';

	if ( ! empty( $row_bg_img ) ) {
		$img_src = $row_bg_img;
		$img_style = '';
	} else {
		$img_src = '';
	}

	if ($use_prlx) {
		$img_style .= 'width:'.$row_bg_img_w.'px;';
		$img_style .= 'height:'.$row_bg_img_h.'px;';
	}

	if (!empty($bg_possition)) {
		$img_style .= 'background-position:'.$bg_possition.';';
	}


	if (!empty($bg_repeat)) {
		if ($bg_repeat == "no-repeat" || $bg_repeat == "cover" || $bg_repeat == "contain") {
			$img_style .= 'background-repeat: no-repeat;';
		}
		if ($bg_repeat == "no-repeat") {
			$img_style .= 'background-size: inherit;';
		}
		if ($bg_repeat == "repeat") {
			$img_style .= 'background-repeat: repeat;';
			$img_style .= 'background-size: inherit;';
		}
		if ($bg_repeat === "cover") {
			$img_style .= 'background-size: cover;';
		}
		if ($bg_repeat === "contain") {
			$img_style .= 'background-size: contain;';
		}
	}

	if (!empty($bg_attach) && !$use_prlx) {
		$img_style .= 'background-attachment:'.$bg_attach.';';
	}

	$prlx_speed = (int) $prlx_speed;

	if ( ! empty( $img_src ) ) {
		$row_bg_class .= ' cws_wrapper';
		$row_bg_class .= $use_prlx ? ' cws_prlx_section' : '';
		$row_bg_html .= "<div  style='background-image:url(".$img_src.");".(!empty($img_style) ? $img_style : '')."' class='" . ( $use_prlx ? 'cws_prlx_layer' : 'row_bg_img' ). "'" . ( ! empty( $prlx_speed ) ? " data-scroll-speed='$prlx_speed'" : '' ) . '></div>';
		$has_bg = true;
	}
    if ($use_prlx) {
		wp_enqueue_script ('cws_parallax');
	}

	$video_src = "";
	if ( $bg_media_type == 'video' ) {
		switch ( $bg_video_type ) {
			case '1':
				$video_src = isset( $sh_bg_video_source['row'] ) ? esc_url( $sh_bg_video_source['row'] ) : '';
				$row_bg_class .= ' cws_self_hosted_video';
				$row_bg_html .= "<video class='self_hosted_video" . ( $use_prlx ? ' cws_prlx_layer' : '' ) . "' src='$video_src' autoplay='autoplay' loop='loop' muted='muted'" . ( ! empty( $prlx_speed ) ? " data-scroll-speed='$prlx_speed'" : '' ) . '></video>';
				break;
			case '2':
                wp_enqueue_script ('cws_YT_bg');
				$video_src =  $yt_bg_video_source;
				$uniqid = uniqid( 'video-' );
				$row_bg_class .= ' cws_Yt_video_bg loading';
				$row_bg_atts .= " data-video-source='$video_src' data-video-id='$uniqid'";
				$row_bg_html .= "<div id='$uniqid'" . ( $use_prlx ? " class='cws_prlx_layer'" : '' ) . ( ! empty( $prlx_speed ) ? " data-scroll-speed='$prlx_speed'" : '' ) . '></div>';
				break;
			case '3':
                wp_enqueue_script ('vimeo');
				wp_enqueue_script ('cws_self&vimeo_bg');
				$video_src = $vimeo_bg_video_source;
				$uniqid = uniqid( 'video-' );
				$row_bg_class .= ' cws_Vimeo_video_bg';
				$row_bg_atts .= " data-video-source='$video_src' data-video-id='$uniqid'";
				$row_bg_html .= "<iframe id='$uniqid'" . ( $use_prlx ? " class='cws_prlx_layer'" : '' ) . ( ! empty( $prlx_speed ) ? " data-scroll-speed='$prlx_speed'" : '' ) . " src='" . $video_src . "?api=1&player_id=$uniqid' frameborder='0'></iframe>";
				break;
		}
		if ( !empty( $video_src ) ) {
			$row_bg_class .= " row_bg_video";
			$has_bg = true;
		}
	}

	if ( ( !empty( $img_src ) || !empty( $video_src ) ) && ( !empty( $bg_pattern ) && isset( $bg_pattern['row'] ) && !empty( $bg_pattern['row'] ) ) ) {
		$bg_pattern_src = esc_attr( $bg_pattern['row'] );
		$layer_styles = "background-image:url($bg_pattern_src);";
		$row_bg_html .= "<div class='row_bg_layer' style='$layer_styles'></div>";
		$has_bg = true;
	}

	if ( in_array( $bg_color_type, array( 'color', 'gradient' ) ) ) {
		$layer_styles = "";
		$bg_color = esc_attr( $bg_color );
		if ( $bg_color_type == 'color' ) {
			$layer_styles .= "background-color:$bg_color;";
		}
		else if ( $bg_color_type == 'gradient' ) {
			$layer_styles .= the8_render_builder_gradient_rules( $atts_arr );
		}
		$layer_styles .= !empty( $bg_color_opacity ) ? "opacity:" . (float)$bg_color_opacity / 100 . ";" : "";
		if ( !empty( $layer_styles ) ) {
			$row_bg_html .= "<div class='row_bg_layer' style='$layer_styles'></div>";
			$has_bg = true;
		}
	}

	$section_border_css = '';

	if (!empty($section_border) && $section_border != 'none') {
		if ($section_border == 'top-border' || $section_border == 'top-bottom') {
			$section_border_css .= 'border-top: '.$section_border_width.'px '.$section_border_style.' '.$section_border_color.';' ;
		}
		if ($section_border == 'bottom-border' || $section_border == 'top-bottom') {
			$section_border_css .= 'border-bottom: '.$section_border_width.'px '.$section_border_style.' '.$section_border_color.';' ;
		}
	}

	if ( $has_bg && ($row_style == 'fullwidth_background') ) {
		$font_color = esc_attr( $font_color );
		$row_bg_styles .= $padding_styles;
		$row_bg_styles .= $margin_styles;
		$row_bg_styles .= $section_border_css;
		$row_bg_styles .= "color:$font_color;";
	}
	else{
		$font_color = esc_attr( $font_color );
		$row_styles .= $padding_styles;
		$row_styles .= $margin_styles;
		$row_styles .= !empty($font_color) ? "color:$font_color;" : "";
		if ($has_bg && !empty( $row_bg_html ) || ($row_style == 'fullwidth_background' )) {
			$row_bg_styles .= $section_border_css;
		}else{
			$row_styles .= $section_border_css;
		}
	}




	$row_class .= (bool) ($flags & 1) ? ' cws_equal_height cws_flex_row' : ' clearfix';
	$row_class .= in_array( $render, array( 'portfolio_fw' ) ) ? ' full_width' : '';
	$row_class .= ! empty( $eclass ) ? ' ' .  trim( $eclass )  : '';
	$row_bg_class .= $use_prlx ? ' cws_prlx_section' : '';
	$row_bg_class .= $bg_attach == "fixed" ? " fixed-bg" : "";

	$row_bg_atts .= !empty( $row_bg_class ) ? " class='$row_bg_class'" : "";
	$row_bg_atts .= !empty( $row_bg_styles ) ? " style='$row_bg_styles'" : "";


	if ( $animate ) {
		wp_enqueue_script ('wow');
		$ani_effect = sanitize_html_class( $ani_effect );
		$row_class .= ! empty( $ani_effect ) ? " wow $ani_effect" : '';
		$row_atts .= ! empty( $ani_duration ) ? " data-wow-duration='" . (int) $ani_duration . "s'" : '';
		$row_atts .= ! empty( $ani_delay ) ? " data-wow-delay='" . (int) $ani_delay . "s'" : '';
		$row_atts .= ! empty( $ani_offset ) ? " data-wow-offset='" . (int) $ani_offset . "'" : '';
		$row_atts .= ! empty( $ani_iteration ) ? " data-wow-iteration='" . (int) $ani_iteration . "'" : '';
	}

	$row_atts .= !empty( $row_class ) ? " class='$row_class'" : "";
	$row_atts .= (!empty( $row_styles ) && $row_style != 'benefits') ? " style='$row_styles'" : "";

	switch ( $render ) {
		case "portfolio":
			if ( is_plugin_active( 'cws-portfolio-staff/cws-portfolio-staff.php' ) ) {
				wp_enqueue_script ('isotope');
				$row_content .= the8_cws_portfolio( $atts_arr );
			}
			break;
		case "portfolio_fw":
			if ( is_plugin_active( 'cws-portfolio-staff/cws-portfolio-staff.php' ) ) {
				wp_enqueue_script ('isotope');
				$row_content .= the8_cws_portfolio_fw( $atts_arr );
			}
			break;
		case "ourteam":
			if ( is_plugin_active( 'cws-portfolio-staff/cws-portfolio-staff.php' ) ) {
				wp_enqueue_script ('isotope');
				$row_content .= the8_cws_ourteam( $atts_arr  );
			}
			break;
		case "blog":
			wp_enqueue_script ('isotope');
			$row_content .= the8_blog( $atts_arr );
			break;
		default:
			$row_content .= do_shortcode( $content );
	}
	if ($row_style == 'fullwidth_background' ) {
		$has_bg = true;
	}
	$out .= $row_style == 'benefits' ? '<div class="benefits_cont"'.(!empty( $row_styles ) ? " style='$row_styles'" : "").'>' : ( (!empty($row_styles) && $row_style != 'fullwidth_item' && $render != 'portfolio_fw' && $row_style != 'fullwidth_item_no_padding' ) && !$has_bg ? '<div class="grid_row_cont">' : '' );
	$out .= $has_bg ? "<div" . ( !empty( $row_bg_atts ) ? $row_bg_atts : "" ) . ">" : "";
		$out .= $has_bg && !empty( $row_bg_html ) ? $row_bg_html : "";
		$out .= "<div" . ( !empty( $row_atts ) ? $row_atts : "" ) . ">";
		$out .= $row_content;
		$out .= "</div>";
	$out .= $has_bg ? "</div>" : "";
	$out .= $row_style == 'benefits' ? '</div>' : ( (!empty($row_styles) && $row_style != 'fullwidth_item' && $render != 'portfolio_fw' && $row_style != 'fullwidth_item_no_padding' ) && !$has_bg ? '</div>' : '' );

	return $out;
}
add_cws_shortcode( 'cws-row', "the8_shortcode_row" );

function the8_shortcode_col ( $sc_atts, $content ) {
	extract( shortcode_atts( array(
		'flags' => '0',
		'span' => '12',
		'atts' => '',
		'_pcol' => '',
	), $sc_atts));


	if (!empty($_pcol)) {

		$pcol_arr = !empty($_pcol) ? json_decode( $_pcol, true ) : array();

		extract( shortcode_atts( array(
			'color' => '',
		), $pcol_arr));
	}

	$atts_arr = !empty($atts) ? json_decode( $atts, true ) : array();
	extract( shortcode_atts( array(
		'customize' => '0',
		'margins' => array(),
		'paddings' => array(),
		'customize_bg' => '0',
		'bg_media_type' => 'none',
		'bg_img' => array(),
		'bg_attach' => '',
		'bg_possition' => '',
		'bg_repeat' => '',
		'is_bg_img_high_dpi' => '0',
		'bg_color_type' => 'none',
		'bg_color' => THE8_COLOR,
		'bg_color_opacity' => '100',
		'bg_pattern' => array(),
		'font_color' => '',
		'eclass' => '',
		'animate' => '0',
		'ani_effect' => '',
		'ani_duration' => '',
		'ani_delay' => '',
		'ani_offset' => '',
		'ani_iteration' => ''
		), $atts_arr));

	$padding_styles = '';
	$margin_styles = '';

	$has_bg = false;
	$row_bg_html = '';
	$row_bg_styles = '';
	$row_styles = '';
	$row_bg_atts = '';
	$row_bg_class = 'widget_cont';
	$col_bg_styles = '';
	$row_bg_img = isset( $bg_img['row'] ) ? esc_url( $bg_img['row'] ) : '';
	$row_bg_img_w = isset( $bg_img['w'] ) ? esc_url( $bg_img['w'] ) : '';

	foreach ($margins as $k => $v) {
	    $margin_styles .= 'margin-'.$k.':'.($v).'px;';
	}

	foreach ($paddings as $k => $v) {
	    $padding_styles .= 'padding-'.$k.':'.($v).'px;';
	}


	if ( ! empty( $row_bg_img ) ) {
		if ( $is_bg_img_high_dpi && ! empty( $row_bg_img_w ) && is_numeric( $row_bg_img_w ) ) {
			$thumb_obj = bfi_thumb( $row_bg_img,array( 'width' => floor( (float) $row_bg_img_w / 2 ), 'crop' => true ), false );
			$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ?  esc_url( $thumb_obj[0] )  :  esc_url( $thumb_obj[0] ) ;
			$img_src = $thumb_path_hdpi;
		} else {
			$img_src = $row_bg_img;
		}
	} else {
		$img_src = '';
	}

	if (!empty($bg_possition)) {
		$col_bg_styles .= 'background-position:'.$bg_possition.';';
	}

	if (!empty($bg_repeat)) {
		if ($bg_repeat == 'no-repeat' || $bg_repeat == 'cover' || $bg_repeat == 'contain') {
			$col_bg_styles .= 'background-repeat: no-repeat;';
		}
		if ($bg_repeat == "no-repeat") {
			$col_bg_styles .= 'background-size: inherit;';
		}
		if ($bg_repeat == "repeat") {
			$col_bg_styles .= 'background-repeat: repeat;';
			$col_bg_styles .= 'background-size: inherit;';
		}
		if ($bg_repeat == 'cover') {
			$col_bg_styles .= 'background-size: cover;';
		}
		if ($bg_repeat == 'contain') {
			$col_bg_styles .= 'background-size: contain;';
		}
	}

	if (!empty($bg_attach)) {
		$col_bg_styles .= 'background-attachment:'.$bg_attach.';';
	}

	if ( ! empty( $img_src ) ) {
		$row_bg_html .= "<div class='row_bg_img_wrapper' style='background-image:url(".$img_src.");".(!empty($col_bg_styles) ? $col_bg_styles : '')."'></div>";
		$has_bg = true;
	}

	if ( ( !empty( $img_src ) ) && ( !empty( $bg_pattern ) && isset( $bg_pattern['row'] ) && !empty( $bg_pattern['row'] ) ) ) {
		$bg_pattern_src = esc_attr( $bg_pattern['row'] );
		$layer_styles = "background-image:url($bg_pattern_src);";
		$row_bg_html .= "<div class='row_bg_layer' style='$layer_styles'></div>";
		$has_bg = true;
	}

	if ( in_array( $bg_color_type, array( 'color', 'gradient' ) ) ) {
		$layer_styles = "";
		$bg_color = esc_attr( $bg_color );
		if ( $bg_color_type == 'color' ) {
			$layer_styles .= "background-color:$bg_color;";
		}
		else if ( $bg_color_type == 'gradient' ) {
			$layer_styles .= the8_render_builder_gradient_rules( $atts_arr );
		}
		$layer_styles .= !empty( $bg_color_opacity ) ? "opacity:" . (float)$bg_color_opacity / 100 . ";" : "";
		if ( !empty( $layer_styles ) ) {
			$row_bg_html .= "<div class='row_bg_layer' style='$layer_styles'></div>";
			$has_bg = true;
		}
	}


		$font_color = esc_attr( $font_color );
		$row_styles .= $padding_styles;
		$row_styles .= $margin_styles;
		$row_styles .= !empty($font_color) ? "color:$font_color;" : '';


	$row_bg_atts .= !empty( $row_bg_class ) ? " class='$row_bg_class'" : "";
	$row_bg_atts .= !empty( $row_bg_styles ) ? " style='$row_bg_styles'" : "";


	$out = "";
	$section_atts = "";
	$section_class = "grid_col grid_col_$span";

	$section_class .= (bool)($flags & 1) ? ' pricing_table_column' : '';
	$section_class .= (bool)($flags & 2) ? ' active_table_column' : '';

	$section_class .= $has_bg ? ' section-has-bg' : '';

	if ( $animate ) {
		wp_enqueue_script ('wow');
		$ani_effect = sanitize_html_class( $ani_effect );
		$section_class .= ! empty( $ani_effect ) ? " wow $ani_effect" : '';
		$section_atts .= ! empty( $ani_duration ) ? " data-wow-duration='" . (int) $ani_duration . "s'" : '';
		$section_atts .= ! empty( $ani_delay ) ? " data-wow-delay='" . (int) $ani_delay . "s'" : '';
		$section_atts .= ! empty( $ani_offset ) ? " data-wow-offset='" . (int) $ani_offset . "'" : '';
		$section_atts .= ! empty( $ani_iteration ) ? " data-wow-iteration='" . (int) $ani_iteration . "'" : '';
	}

	$section_atts .= ($customize == '1' && !empty($color)) ? " style='border-color:".($color).";'" : '';
	$section_atts .= ! empty( $section_class ) ? " class='$section_class'" : '';
	$out .= '<div' . ( ! empty( $section_atts ) ? $section_atts : '' ) . '>';
		$out .= '<div class="cols_wrapper ' . (!empty($eclass) ? esc_attr($eclass) : '') . '"'.(!empty($row_styles) ? " style='".$row_styles."'" : "").'>';
			$out .= $has_bg && !empty( $row_bg_html ) ? $row_bg_html : "";
			$out .= '<div class="widget_wrapper"'.(!empty($color) ? 'style="border-color:'.$color.';"' : '').'>';
				$out .= do_shortcode( $content );
			$out .= '</div>';
		$out .= '</div>';
	$out .= '</div>';

	return $out;
}
add_cws_shortcode( 'col', 'the8_shortcode_col' );

function the8_shortcode_widget ( $sc_atts, $content ) {
	extract( shortcode_atts( array(
		'type' => 'text',
		'title' => '',
		'atts' => ''
	), $sc_atts));

	$GLOBALS['widget_type'] = $type;

	$atts_arr = json_decode( $atts, true );

	extract( shortcode_atts( array(
		'title' => '',
		'margins' => array(),
		'paddings' => array(),
		'eclass' => '',
		'centertitle' => '0',
		'animate' => '0',
		'ani_effect' => '',
		'ani_duration' => '',
		'ani_delay' => '',
		'ani_offset' => '',
		'ani_iteration' => '',
		'customize_bg' => '0',
		'bg_media_type' => 'none',
		'bg_img' => array(),
		'bg_attach' => '',
		'bg_possition' => '',
		'bg_repeat' => '',
		'is_bg_img_high_dpi' => '0',
		'bg_color_type' => 'none',
		'bg_color' => THE8_COLOR,
		'bg_color_opacity' => '100',
		'bg_pattern' => array(),
		'font_color' => '',
	), $atts_arr));

	$padding_styles = '';
	$margin_styles = '';

	foreach ($margins as $k => $v) {
	    $margin_styles .= 'margin-'.$k.':'.($v).'px;';
	}

	foreach ($paddings as $k => $v) {
	    $padding_styles .= 'padding-'.$k.':'.($v).'px;';
	}

	$has_bg = false;
	$row_bg_html = '';
	$row_bg_styles = '';
	$row_styles = '';
	$row_bg_atts = '';
	$row_bg_class = 'widget_cont';
	$col_bg_styles = '';
	$row_bg_img = isset( $bg_img['row'] ) ? esc_url( $bg_img['row'] ) : '';
	$row_bg_img_w = isset( $bg_img['w'] ) ? esc_url( $bg_img['w'] ) : '';

	if (!empty($eclass)) {
		$row_bg_class .= ' '.esc_attr($eclass);

	}


	if ( ! empty( $row_bg_img ) ) {
		if ( $is_bg_img_high_dpi && ! empty( $row_bg_img_w ) && is_numeric( $row_bg_img_w ) ) {
			$thumb_obj = bfi_thumb( $row_bg_img,array( 'width' => floor( (float) $row_bg_img_w / 2 ), 'crop' => true ), false );
			$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ?  esc_url( $thumb_obj[0] )  :  esc_url( $thumb_obj[0] ) ;
			$img_src = $thumb_path_hdpi;
		} else {
			$img_src = $row_bg_img;
		}
	} else {
		$img_src = '';
	}

	if (!empty($bg_possition)) {
		$col_bg_styles .= 'background-position:'.$bg_possition.';';
	}

	if (!empty($bg_repeat)) {
		if ($bg_repeat == 'no-repeat' || $bg_repeat == 'cover' || $bg_repeat == 'contain') {
			$col_bg_styles .= 'background-repeat: no-repeat;';
		}
		if ($bg_repeat == "no-repeat") {
			$col_bg_styles .= 'background-size: inherit;';
		}
		if ($bg_repeat == "repeat") {
			$col_bg_styles .= 'background-repeat: repeat;';
			$col_bg_styles .= 'background-size: inherit;';
		}
		if ($bg_repeat == 'cover') {
			$col_bg_styles .= 'background-size: cover;';
		}
		if ($bg_repeat == 'contain') {
			$col_bg_styles .= 'background-size: contain;';
		}
	}

	if (!empty($bg_attach)) {
		$col_bg_styles .= 'background-attachment:'.$bg_attach.';';
	}

	if ( ! empty( $img_src ) ) {
		$row_bg_html .= "<div class='row_bg_img_wrapper' style='background-image:url(".$img_src.");".(!empty($col_bg_styles) ? $col_bg_styles : '')."'></div>";
		$has_bg = true;
	}


	if ( ( !empty( $img_src ) ) && ( !empty( $bg_pattern ) && isset( $bg_pattern['row'] ) && !empty( $bg_pattern['row'] ) ) ) {
		$bg_pattern_src = esc_attr( $bg_pattern['row'] );
		$layer_styles = "background-image:url($bg_pattern_src);";
		$row_bg_html .= "<div class='row_bg_layer' style='$layer_styles'></div>";
		$has_bg = true;
	}

	if ( in_array( $bg_color_type, array( 'color', 'gradient' ) ) ) {
		$layer_styles = "";
		$bg_color = esc_attr( $bg_color );
		if ( $bg_color_type == 'color' ) {
			$layer_styles .= "background-color:$bg_color;";
		}
		else if ( $bg_color_type == 'gradient' ) {
			$layer_styles .= the8_render_builder_gradient_rules( $atts_arr );
		}
		$layer_styles .= !empty( $bg_color_opacity ) ? "opacity:" . (float)$bg_color_opacity / 100 . ";" : "";
		if ( !empty( $layer_styles ) ) {
			$row_bg_html .= "<div class='row_bg_layer' style='$layer_styles'></div>";
			$has_bg = true;
		}
	}

	if ( $has_bg ) {
		$font_color = esc_attr( $font_color );
		$row_bg_styles .= $padding_styles;
		$row_bg_styles .= $margin_styles;
		$row_bg_styles .= "color:$font_color;";
	}
	else{
		$row_styles .= $padding_styles;
		$row_styles .= $margin_styles;
		$row_styles .= !empty($font_color) ? "color:$font_color;" : "";
	}

	$row_bg_atts .= !empty( $row_bg_class ) ? " class='$row_bg_class'" : "";
	$row_bg_atts .= !empty( $row_bg_styles ) ? " style='$row_bg_styles'" : "";



	$section_atts = "";
	$section_class = "ce clearfix";
	if ( $animate ) {
		wp_enqueue_script ('wow');
		$ani_effect = sanitize_html_class( $ani_effect );
		$section_class .= ! empty( $ani_effect ) ? " wow $ani_effect" : '';
		$section_atts .= ! empty( $ani_duration ) ? " data-wow-duration='" . (int) $ani_duration . "s'" : '';
		$section_atts .= ! empty( $ani_delay ) ? " data-wow-delay='" . (int) $ani_delay . "s'" : '';
		$section_atts .= ! empty( $ani_offset ) ? " data-wow-offset='" . (int) $ani_offset . "'" : '';
		$section_atts .= ! empty( $ani_iteration ) ? " data-wow-iteration='" . (int) $ani_iteration . "'" : '';
	}
	$section_atts .= !empty( $section_class ) ? " class='" . trim( $section_class ) . "'" : "";
	$out = "";
	if ( $type == 'tcol' ) {
		$out .= the8_pricing_table_column_renderer ( $atts_arr, $content );
	}
	else{
		if ( in_array( $type, array( 'callout', 'tweet' ) ) ) {
			switch ( $type ) {
				case 'callout':
					$out .= the8_callout_renderer( $atts_arr, $content );
					break;
				case 'tweet':
					$out .= the8_twitter_renderer( $atts_arr, $content );
					break;
			}
		}
		else{
			$title = esc_html( $title );
			$out .= !empty( $title ) ? THE8_BEFORE_CE_TITLE . "<div" . ( $centertitle ? " style='text-align:center;'" : "" ) . ">$title</div>" . THE8_AFTER_CE_TITLE : "";
			switch ( $type ) {
				case 'text':
					$out .= $has_bg ? "<div" . ( !empty( $row_bg_atts ) ? $row_bg_atts : "" ) . ">" : (!empty($row_styles) ? "<div style='".$row_styles."'>" : "");
						$out .= $has_bg && !empty( $row_bg_html ) ? $row_bg_html : "";
						$out .= ($has_bg && !empty( $row_bg_html )) ? '<div class="cws_wrapper_container">' : '';
						$out .= do_shortcode( $content );
						$out .= $has_bg && !empty( $row_bg_html ) ? "</div>" : "";
					$out .= $has_bg ? "</div>" : (!empty($row_styles) ? "</div>" : "");
					break;
				case 'accs':
					$out .= the8_accs_renderer( $atts_arr, $content );
					break;
				case 'tabs':
					$out .= the8_tabs_renderer( $atts_arr, $content );
					break;
			}
		}
		$out = !empty( $out ) ? "<div" . ( !empty( $section_atts ) ? $section_atts : "" ) . ">$out</div>" : "";
	}
	return $out;
}
add_cws_shortcode( 'cws-widget', 'the8_shortcode_widget' );

function the8_blog( $atts = array() ) {
	extract( shortcode_atts( array(
		'title' => '',
		'centertitle' => '0',
		'columns' => '1',
		'categories' => '',
		'items_per_page' => get_option( 'posts_per_page' ),
		'use_carousel' => '0',
		'custom_layout' => '0',
		'post_text_length' => '',
		'button_name' => '',
		'enable_lightbox' => '',
		'cws_rm_image_url' => '',
		'hide_meta' => '',
		'date_style' => '',
		'boxed_style' => ''
	), $atts));

	$categories = explode( ',', $categories );
	$categories = the8_filter_by_empty( $categories );

	$out = "";

	$header_content = "";
	ob_start();
	/* carousel_nav */
	$title = esc_html( $title );
	$centertitle = $centertitle && !$use_carousel;
	/* carousel_nav */
	echo !empty( $title ) ? THE8_BEFORE_CE_TITLE . "<div" . ( $centertitle ? " style='text-align:center;'" : "" ) . ">$title</div>" . THE8_AFTER_CE_TITLE : "";
	if ( $use_carousel ) {
		wp_enqueue_script ('owl_carousel');
		if (!empty( $title )) {
			echo "<div class='carousel_nav_panel_container'>
					<div class='carousel_nav_panel clearfix'>
						<span class='prev'></span>
						<span class='next'></span>
					</div>
				</div>";
		}

	}
	$header_content .= ob_get_clean();

	$column_style = ($columns >= '2');
	$query_args = array(
		'post_type' => 'post',
		'ignore_sticky_posts' => true,
		'post_status' => 'publish',
		'posts_per_page' => $items_per_page,
		'this_shortcode' => true,
		'column_style' => $column_style,
		'custom_layout' => (int) $custom_layout,
		'post_text_length' => (int) $post_text_length,
		'button_name' => $button_name,
		'enable_lightbox' => $enable_lightbox,
		'cws_rm_image_url' => $cws_rm_image_url,
		'use_carousel' => $use_carousel,
		'hide_meta' => $hide_meta,
		'date_style' => $date_style,
		'boxed_style' => $boxed_style,
		'column_count' => (int) $columns,
	);

	if ( !empty( $categories ) ) {
		$query_args['tax_query'] = array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => $categories
			)
		);
	}

	$q = new WP_Query( $query_args );

	if ( $q->have_posts() ) {
		$section_class = "news";
		$section_class .= $columns == '1' ? " news-large" : " news-pinterest";
		$grid_class = "grid";
		$grid_class .= $columns != '1' ? " grid-$columns" : "";
		$grid_class .= $use_carousel ? " news_carousel" : " isotope";

		if ($use_carousel) {
			wp_enqueue_script ('owl_carousel');
		} elseif ($columns != '1') {
			wp_enqueue_script ('isotope');
		}

		$column_style = $columns >= '2' ? true : false;

		$new_blogtype = $columns == '1' ? 'large' : $columns;
		$old_blogtype = the8_get_page_meta_var( array( 'blog', 'blogtype' ) );
		if ( !( is_bool( $old_blogtype ) && !$old_blogtype ) ) the8_set_page_meta_var( array( 'blog', 'blogtype' ), $new_blogtype );

		ob_start();
			echo "<section class='".$section_class."'>";
				echo !empty( $header_content ) ? "<div class='cws_blog_header'>$header_content</div>" : "";
				echo "<div class='cws_wrapper'>";
					echo "<div class='".$grid_class."'>";
						the8_blog_output( $q );
					echo "</div>";
				echo "</div>";
			echo "</section>";
		$out .= ob_get_clean();

		if ( !( is_bool( $old_blogtype ) && !$old_blogtype ) ) the8_set_page_meta_var( array( 'blog', 'blogtype' ), $old_blogtype );

	}

	return $out;
}
add_cws_shortcode( 'cws_sc_blog', 'the8_blog' );

function the8_cws_portfolio( $atts = array() ) {
	extract( shortcode_atts( array(
		'title' => '',
		'centertitle' => '0',
		'columns' => '4',
		'categories' => '',
		'grid_with_filter_categories' => '',
		'mode' => 'grid',
		'portcontent' => 'exerpt',
		'sel_posts_by' => 'none',
		'post_ids' => '',
		'items_per_page' => get_option( 'posts_per_page' ),
		'exclude' => array(),
		'disable_pagination' => false
	), $atts));

	$p_id = get_queried_object_id();

	$filter = "all";

	$post_ids = explode( ',', $post_ids );
	$post_ids = the8_filter_by_empty( $post_ids );

	if ( $sel_posts_by == 'titles' && !empty( $post_ids ) ) {
		$items_per_page = count( $post_ids );
	}

	if ($mode == 'grid_with_filter') {
		$categories = $grid_with_filter_categories;
	}

	$categories = explode( ',', $categories );
	$categories = the8_filter_by_empty( $categories );

	$query_args = array(
		'post_type' => 'cws_portfolio',
		'portcontent' => $portcontent,
		'ignore_sticky_posts' => true,
		'post_status' => 'publish'
	);

	if ( in_array( $mode, array( 'grid', 'grid_with_filter', 'carousel' ) ) ) $query_args['posts_per_page'] = $items_per_page;

	$tax_query = array();
	if ( ($sel_posts_by == 'cats' && !empty( $categories )) || ($mode == 'grid_with_filter' && !empty( $categories )) ) {
		$tax_query[] = array(
			'taxonomy' => 'cws_portfolio_cat',
			'field' => 'slug',
			'terms' => $categories
		);
	}else if ( $sel_posts_by == 'titles' && !empty( $post_ids ) ) {
		$query_args['post__in'] = $post_ids;
	}

	if ( !empty( $tax_query ) ) $query_args['tax_query'] = $tax_query;

	$excluded_posts = is_array( $exclude ) ? $exclude : the8_json_sc_attr_conversion( $exclude );
	$excluded_posts = $excluded_posts ? $excluded_posts : array();
	if ( !empty( $excluded_posts ) ) {
		$query_args["post__not_in"] = $excluded_posts;
	}

	$q = new WP_Query( $query_args );

	$section_class = "cws_portfolio";
	$section_class .= in_array( $columns, array( '2', '3', '4' ) ) ? " massonry" : "";

	$out = "";

	if ( $q->have_posts() ) {
		$out .= "<section class='$section_class'>";

			ob_start();

			$use_filter = false;
			$use_carousel = false;

			/* filter */
			if ( $mode == "grid_with_filter" ) {
				$avail_cats = $categories;
				if ( empty( $avail_cats ) ) {
					$avail_cats = cws_get_portfolio_cat_slugs ();
				}
				if ( !empty( $avail_cats ) ) {
					$use_filter = true;
				}
			}
			/* \filter */

			/* carousel_nav */
			else if ( $mode == "carousel" ) {
				$use_carousel = true;
				wp_enqueue_script ('owl_carousel');
			}
			/* carousel_nav */
			$title = esc_html( $title );
			$centertitle = $centertitle && ( !$use_filter && !$use_carousel );

			echo !empty( $title ) ? THE8_BEFORE_CE_TITLE . "<div" . ( $centertitle ? " style='text-align:center;'" : "" ) . ">$title</div>" . THE8_AFTER_CE_TITLE : "";

			if ( $use_filter ) {
				echo "<div class='cws_portfolio_filter_container'>";
					echo "<select class='cws_portfolio_filter'>";
						echo "<option value='".esc_attr($filter)."'>" . esc_html__( 'All', 'the8' ) . "</option>";
						foreach( $avail_cats as $avail_cat ) {
							$cat = get_term_by( 'slug', $avail_cat, 'cws_portfolio_cat' );
							$cat_name = esc_html( $cat->name );
							echo '<option value="'. esc_attr( $avail_cat ). "\">$cat_name</option>";
						}
					echo "</select>";
				echo "</div>";
			}

			if ( $use_carousel && ($items_per_page > $columns) && !empty($title) ) {
				wp_enqueue_script ('owl_carousel');
				echo "<div class='carousel_nav_panel_container'><div class='carousel_nav_panel clearfix'><span class='prev'></span><span class='next'></span></div></div>";
			}

			wp_enqueue_script ('isotope');

			$header_content = ob_get_clean();
			$out .= !empty( $header_content ) ? "<div class='cws_portfolio_header'>$header_content</div>" : "";

			$items_section_class = "cws_portfolio_items grid" . ( in_array( $columns, array( '2', '3', '4' ) ) ? " grid-$columns" : "" );
			$items_section_class .= $mode == "carousel" ? " portfolio_carousel" : " isotope";
			$out .= "<div class='cws_wrapper'>";
				$out .= "<div class='$items_section_class'>";
					ob_start();
					render_cws_portfolio( $q, $columns );
					$out .= ob_get_clean();
				$out .= "</div>";
			$out .= "</div>";

			if ( in_array( $mode, array( 'grid', 'grid_with_filter' ) ) ) {
				$out .= "<input type='hidden' class='cws_portfolio_ajax_data' value='" . esc_attr(
					json_encode(
						array(
							'p_id' => $p_id,
							'cols' => $columns,
							'mode' => $mode,
							'portcontent' => $portcontent,
							'cats' => $categories,
							'exclude' => $excluded_posts,
							'filter' => $filter,
							'posts_per_page' => $items_per_page,
							)
						)
					) . "' />";
				$max_paged = ceil( $q->found_posts / $items_per_page );
				if ( ! $disable_pagination && $max_paged > 1 ) {
					ob_start();
					the8_pagination( 1, $max_paged );
					$out .= ob_get_clean();
				}
			}

		$out .= "</section>";
	}

	return $out;
}
add_cws_shortcode( 'cws_sc_portfolio', 'the8_cws_portfolio' );

function the8_cws_portfolio_fw ( $atts = array() ) {
	extract( shortcode_atts( array(
		'title' => '',
		'centertitle' => '0',
		'columns' => '4',
		'categories' => '',
		'grid_with_filter_categories' => '',
		'mode' => 'grid',
		'portcontent' => '',
		'sel_posts_by' => 'none',
		'post_ids' => '',
		'items_per_page' => get_option( 'posts_per_page' ),
		'exclude' => array(),
		'disable_pagination' => false,
	), $atts));

	$p_id = get_queried_object_id();

	$filter = "all";

	$post_ids = explode( ',', $post_ids );
	$post_ids = the8_filter_by_empty( $post_ids );

	if ( $sel_posts_by == 'titles' && !empty( $post_ids ) ) {
		$items_per_page = count( $post_ids );
	}

	if ($mode == 'grid_with_filter') {
		$categories = $grid_with_filter_categories;
	}

	$categories = explode( ',', $categories );
	$categories = the8_filter_by_empty( $categories );

	$query_args = array(
		'post_type' => 'cws_portfolio',
		'portcontent' => $portcontent,
		'ignore_sticky_posts' => true,
		'post_status' => 'publish'
	);

	if ( in_array( $mode, array( 'grid', 'grid_with_filter', 'carousel' ) ) ) $query_args['posts_per_page'] = $items_per_page;

	$tax_query = array();
	if ( ($sel_posts_by == 'cats' && !empty( $categories )) || ($mode == 'grid_with_filter' && !empty( $categories )) ) {
		$tax_query[] = array(
			'taxonomy' => 'cws_portfolio_cat',
			'field' => 'slug',
			'terms' => $categories
		);
	}else if ( $sel_posts_by == 'titles' && !empty( $post_ids ) ) {
		$query_args['post__in'] = $post_ids;
	}

	if ( !empty( $tax_query ) ) $query_args['tax_query'] = $tax_query;

	$excluded_posts = is_array( $exclude ) ? $exclude : the8_json_sc_attr_conversion( $exclude );
	$excluded_posts = $excluded_posts ? $excluded_posts : array();
	if ( !empty( $excluded_posts ) ) {
		$query_args["post__not_in"] = $excluded_posts;
	}

	$q = new WP_Query( $query_args );


	$out = "";

	if ( $q->have_posts() ) {
		$out .= "<section class='cws_portfolio_fw'>";

		ob_start();

		$use_filter = false;
		$use_carousel = false;

		/* filter */
		if ( $mode == "grid_with_filter" ) {
			$avail_cats = $categories;
			if ( empty( $avail_cats ) ) {
				$avail_cats = cws_get_portfolio_cat_slugs ();
			}
			if ( !empty( $avail_cats ) ) {
				$use_filter = true;
			}
		}
		/* \filter */

		/* carousel_nav */
		else if ( $mode == "carousel" ) {
			$use_carousel = true;
			wp_enqueue_script ('owl_carousel');
		}
		/* carousel_nav */
		$title = esc_html( $title );
		$centertitle = $centertitle && ( !$use_filter && !$use_carousel );
		echo !empty( $title ) ? THE8_BEFORE_CE_TITLE . "<div" . ( $centertitle ? " style='text-align:center;'" : "" ) . ">$title</div>" . THE8_AFTER_CE_TITLE : "";
		if ( $use_filter ) {
			echo "<div class='cws_portfolio_filter_container'>";
				echo "<select class='cws_portfolio_filter'>";
					echo "<option value='".esc_attr($filter)."'>" . esc_html__( 'All', 'the8' ) . "</option>";
					foreach( $avail_cats as $avail_cat ) {
						$cat = get_term_by( 'slug', $avail_cat, 'cws_portfolio_cat' );
						$cat_name = esc_html( $cat->name );
						echo '<option value="'. esc_attr( $avail_cat ). "\">$cat_name</option>";
					}
				echo "</select>";
			echo "</div>";
		}

		if ( $use_carousel && ($items_per_page > $columns) && !empty($title) ) {
			wp_enqueue_script ('owl_carousel');
			echo "<div class='carousel_nav_panel_container'><div class='carousel_nav_panel clearfix'><span class='prev'></span><span class='next'></span></div></div>";
		}

		wp_enqueue_script ('isotope');

		$multiple = $q->post_count > 1;
		$gallery_id = uniqid( 'cws-gallery-' );

		$header_content = ob_get_clean();
			$out .= !empty( $header_content ) ? "<div class='fw_row_content_wrapper'><div class='cws_portfolio_header'>$header_content</div></div>" : "";

			$out .= "<div class='grid_fw col-$columns".($mode == "carousel" ? " portfolio_fw_carousel" : " isotope")."'>";
				ob_start();
					render_cws_portfolio_fw( $q, $columns );
				$out .= ob_get_clean();
			$out .= "</div>";

		if ( in_array( $mode, array( 'grid', 'grid_with_filter' ) ) ) {
			$out .= "<input type='hidden' class='cws_portfolio_fw_ajax_data' value='" . esc_attr(
				json_encode(
					array(
						'p_id' => $p_id,
						'cols' => $columns,
						'mode' => $mode,
						'portcontent' => $portcontent,
						'cats' => $categories,
						'exclude' => $excluded_posts,
						'filter' => $filter,
						'posts_per_page' => $items_per_page,
						)
					)
				) . "' />";
			$max_paged = ceil( $q->found_posts / $items_per_page );
			if ( ! $disable_pagination && $max_paged > 1 ) {
				ob_start();
				echo "<div class='fw_row_content_wrapper'>";
				the8_pagination( 1, $max_paged );
				echo "</div>";
				$out .= ob_get_clean();
			}
		}

		$out .= "</section>";

	}

	return $out;
}
add_cws_shortcode( 'cws_sc_portfolio_fw', 'the8_cws_portfolio_fw' );

function the8_cws_ourteam ( $atts = array() ) {
	extract( shortcode_atts( array(
		'title' => '',
		'centertitle' => '0',
		'mode' => 'grid',
		'centeritems' => '0',		
		'categories' => '',
		'tags' => '',
		'items_per_page' => get_option( 'posts_per_page' )
	), $atts));

	$p_id = get_queried_object_id();

	$filter = "all";

	$categories = explode( ',', $categories );
	$categories = the8_filter_by_empty( $categories );
	$tags = explode( ',', $tags );
	$tags = the8_filter_by_empty( $tags );

	$query_args = array(
		'post_type' => 'cws_staff',
		'ignore_sticky_posts' => true,
		'post_status' => 'publish',
	);

	if ( in_array( $mode, array( 'grid', 'grid_with_filter', 'carousel' ) ) ) $query_args['posts_per_page'] = $items_per_page;


	$tax_query = array();
	if ( !empty( $categories ) ) {
		$tax_query[] = array(
			'taxonomy' => 'cws_staff_member_department',
			'field' => 'slug',
			'terms' => $categories
		);
	}
	if ( !empty( $tags ) ) {
		$tax_query[] = array(
			'taxonomy' => 'cws_staff_member_position',
			'field' => 'slug',
			'terms' => $tags
		);
	}

	if ( !empty( $tax_query ) ) $query_args['tax_query'] = $tax_query;

	$q = new WP_Query( $query_args );

	$section_class = "cws_ourteam";

	$out = "";

	if ( $q->have_posts() ) {
		$out .= "<section class='$section_class'>";

			ob_start();

			$use_filter = false;
			$use_carousel = false;

			/* filter */
			if ( $mode == "grid_with_filter" ) {
				$avail_cats = $categories;
				if ( empty( $avail_cats ) ) {
					$avail_cats = cws_get_staff_cat_slugs ();
				}
				if ( !empty( $avail_cats ) ) {
					$use_filter = true;
				}
			}
			/* \filter */

			/* carousel_nav */
			else if ( $mode == "carousel" ) {
				$use_carousel = true;
				wp_enqueue_script ('owl_carousel');
			}
			/* carousel_nav */

			$centertitle = $centertitle && ( !$use_filter && !$use_carousel );
			$title = esc_html( $title );

			echo !empty( $title ) ? THE8_BEFORE_CE_TITLE . "<div" . ( $centertitle ? " style='text-align:center;'" : "" ) . ">$title</div>" . THE8_AFTER_CE_TITLE : "";

			if ( $use_filter ) {
				echo "<div class='cws_ourteam_filter_container'>";
					echo "<select class='cws_ourteam_filter'>";
						echo "<option value='".esc_attr($filter)."'>" . esc_html__( 'All', 'the8' ) . '</option>';
						foreach( $avail_cats as $avail_cat ) {
							$cat = get_term_by( 'slug', $avail_cat, 'cws_staff_member_department' );
							$cat_name = $cat->name;
							echo '<option value="'.esc_attr( $avail_cat )."\">$cat_name</option>";
						}
					echo "</select>";
				echo "</div>";
			}

			if ( $use_carousel && !empty($title) ) {
				wp_enqueue_script ('owl_carousel');
				echo "<div class='carousel_nav_panel_container'><div class='carousel_nav_panel clearfix'><span class='prev'></span><span class='next'></span></div></div>";
			}
			wp_enqueue_script ('isotope');
			$header_content = ob_get_clean();
			$out .= !empty( $header_content ) ? "<div class='cws_ourteam_header'>$header_content</div>" : "";

			$items_section_class = "cws_ourteam_items grid grid-4";

			if ($mode == "grid") {
				$items_section_class .= ' isotope';
			} else if ($mode == "grid_without_isotope"){
				$items_section_class .= ($centeritems ? ' center-items' : ''); 
			} else if ($mode == "grid_with_filter"){  
				$items_section_class .= " isotope";
			} else if ($mode == "carousel"){
				$items_section_class .= " ourteam_carousel";    
			}			

			$out .= "<div class='cws_wrapper'>";
				$out .= "<div class='$items_section_class'>";
					ob_start();
					render_cws_ourteam( $q );
					$out .= ob_get_clean();
				$out .= "</div>";
			$out .= "</div>";

			if ( in_array( $mode, array( 'grid', 'grid_with_filter' ) ) ) {
				$out .= "<input type='hidden' class='cws_ourteam_ajax_data' value='" . esc_attr( json_encode( array( 'p_id' => $p_id, 'mode' => $mode, 'cats' => $categories, 'filter' => $filter, 'posts_per_page' => $items_per_page ) ) ) . "' />";
				$max_paged = ceil( $q->found_posts / $items_per_page );
				if ( $max_paged > 1 ) {
					ob_start();
					the8_pagination( 1, $max_paged );
					$out .= ob_get_clean();
				}
			}

		$out .= "</section>";
	}

	return $out;
}
add_cws_shortcode( 'cws_sc_ourteam', 'cws_ourteam' );

/* WIDGET RENDERERS */

function the8_accs_renderer ( $atts, $content ) {
	extract( shortcode_atts( array(
		'istoggle' => '0',
		'accstyle' => '',
		'items' => '0'
	), $atts));
	$out = "";
	if ( (int)$items > 0 ) {
		$section_class = "cws_ce_content";
		$section_class .= $istoggle ? " ce_toggle" : " ce_accordion";
		$section_class .= ($accstyle !== 'first_style') ? " ".$accstyle : "";
		$out .= "<div class='$section_class'>" . do_shortcode( $content ) . "</div>";
	}
	return $out;
}

function the8_tabs_renderer ( $atts, $content ) {
	extract( shortcode_atts( array(
		'vertical' => '0',
		'items' => '0'
	), $atts));
	$out = "";
	$section_class = "cws_ce_content ce_tabs";
	$section_class .= $vertical ? " vertical" : "";
	$GLOBALS['cws_tabs_currently_rendered'] = array();
	do_shortcode( $content );
	$tab_items = $GLOBALS['cws_tabs_currently_rendered'];
	unset( $GLOBALS['cws_tabs_currently_rendered'] );
	if ( !empty( $tab_items ) ) {
		$out .= "<div class='$section_class'>";
		$out .= "<div class='tabs" . ( !$vertical ? " clearfix" : "" ) . "'>";
		foreach ( $tab_items as $tab_item ) {
			$tab_class = 'tab';
			$tab_class .= isset( $tab_item['open'] ) && $tab_item['open'] ? ' active' : '';
			$out .= "<div class='$tab_class' role='tab' tabindex='" . $tab_item['tabindex'] . "'>";
			if ( isset( $tab_item['iconimg'] ) && ! empty( $tab_item['iconimg'] ) ) {
				$thumb_obj = bfi_thumb( $tab_item['iconimg'],array( 'width' => 30, 'height' => 30, 'crop' => true ), false );
				$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
				$img_src = $thumb_path_hdpi;

				$out .= '<img ' . $img_src . " class='tab_icon' />";
			} else if ( isset( $tab_item['iconfa'] ) && ! empty( $tab_item['iconfa'] ) ) {
					$out .= "<i class='tab_icon " .  $tab_item['iconfa']  . "'></i>";
			}
				$out .= isset( $tab_item['title'] ) && ! empty( $tab_item['title'] ) ? '<span>' . $tab_item['title'] . '</span>' : '';
				$out .= '</div>';
		}
		$out .= "</div>";
		$out .= "<div class='tab_sections'>";
			foreach ( $tab_items as $tab_item ) {
				$out .= "<div class='tab_section' role='tabpanel' tabindex='" . $tab_item['tabindex'] . "'" . ( !isset( $tab_item['open'] ) || !$tab_item['open'] ? " style='display:none;'" : "" ) . ">";
					$out .= isset( $tab_item['content'] ) ? $tab_item['content'] : "";
				$out .= "</div>";
			}
		$out .= "</div>";
		$out .= "</div>";
	}
	return $out;
}

function the8_item_shortcode ( $atts, $content ) {
	extract( shortcode_atts( array(
		'title' => '',
		'open' => '0',
		'iconfa' => '',
		'iconimg' => ''
	), $atts));

	if (isset($atts['atts'])) {
		$icon = the8_json_sc_attr_conversion($atts['atts']);
		if (isset($icon->{'iconfa'})) {
			$iconfa = $icon->{'iconfa'};
		} else if (isset($icon->iconimg)) {
			$iconimg = $icon->iconimg->row;
		}
	}



	$type = $GLOBALS['widget_type'];
	$out = "";
	$item_content = do_shortcode( $content );
	if ( in_array( $type, array( 'accs', 'tabs' ) ) ) {
		if ( $type == 'accs' ) {
			$out .= "<div class='accordion_section" . ( $open ? " active" : "" ) . "'>";
				$out .= "<div class='accordion_title'>";
					if ( ! empty( $iconimg ) ) {
						$thumb_obj = bfi_thumb( $iconimg,array( 'width' => 30, 'height' => 30, 'crop' => true ), false );
						$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
						$img_src = $thumb_path_hdpi;
						$out .= '<img ' . $img_src . " class='accordion_icon custom' />";
					} else {
						$out .= "<i class='accordion_icon" . ( ! empty( $iconfa ) ? ' custom ' . $iconfa : '' ) . "'></i>";
					}
					$title = esc_html( $title );
					$out .= !empty( $title ) ? "<span>$title</span>" : "";
					$out .= "</div>";
					$out .= !empty( $item_content ) ? "<div class='accordion_content'" . ( $open ? "" : " style='display:none;'" ) . ">$item_content</div>" : "";
			$out .= "</div>";
		}
		else if ( $type == 'tabs' ) {
			if ( isset( $GLOBALS['cws_tabs_currently_rendered'] ) && is_array( $GLOBALS['cws_tabs_currently_rendered'] ) ) {
				$tab_item = $atts;
				$tab_item['content'] = $content;
				$tab_item['tabindex'] = count( $GLOBALS['cws_tabs_currently_rendered'] );
				array_push( $GLOBALS['cws_tabs_currently_rendered'], $tab_item );
			}
		}
	}
	return $out;
}
add_cws_shortcode( 'item', 'the8_item_shortcode' );

function the8_pricing_table_column_renderer ( $atts, $content ) {
	extract( shortcode_atts( array(
		'title' => '',
		'currency' => '',
		'price' => '',
		'price_description' => '',
		'order_url' => '#',
		'button_text' => '',
		'ishilited' => '0',
		'customize' => '0',
		'color' => '',
		'color_button_text' => '',
		'atts' => '',
	), $atts));
	$out = "";

	$color = esc_html( $color );
	$color_button_text = esc_html( $color_button_text );
	$currency = esc_html( $currency );



ob_start();
	$title = esc_html( $title );
	echo !empty( $title ) ? "<div class='title_section'".($customize == '1' && (!empty($color) || !empty($color_button_text)) ? 'style="background-color:'.esc_attr($color).';color:'.($color_button_text).';"' : '').">$title</div>" : "";

ob_start();
	preg_match( "/(\.|,)\d+$/", $price, $matches );
	$fract_price_part = isset( $matches[0] ) ? $matches[0] : '';
	$main_price_part = !empty( $fract_price_part ) ? esc_html( substr( $price, 0, strpos( $price, $fract_price_part ) ) ) : esc_html( $price );
	echo !empty( $currency ) ? "<span class='currency'>$currency</span>" : "";
	echo !empty( $main_price_part ) ? "<span class='main_price_part'>$main_price_part</span>" : "";

	ob_start();
	echo !empty( $fract_price_part ) ? "<span class='fract_price_part'>$fract_price_part</span>" : "";
	echo !empty( $price_description ) ? "<span class='price_description'><span>$price_description</span></span>" : "";
	$price_details = ob_get_clean();
	echo !empty( $price_details ) ? "<span class='price_details'>$price_details</span>" : "";
	$price_content = ob_get_clean();
	echo !empty( $price_content ) ? "<div class='price_section'".($customize == '1' ? ' data-bg-color="'.$color.'"' : '').($ishilited == '1' ? 'style="color:'.$color.';"' : '')."><div class='price_container'>$price_content</div></div>" : "";

	echo !empty( $content ) ? "<div class='desc_section'>" . wptexturize( do_shortcode( $content ) ) . "</div>" : "";
	$box1 = ob_get_clean();
	$out .= !empty( $box1 ) ? "<div>$box1</div>" : "";

	ob_start();
	$order_url = esc_url( $order_url );
	echo !empty( $button_text ) ? "<div class='btn_section'><a class='cws_button ".($ishilited != '1' ? "alt" : "").($customize == '1' ? ' custom_colors' : '')."' ". ( $customize == '1' ? " data-bg-color='".($ishilited != '1' ? $color_button_text : $color )."'" : "" ) . ( $customize == '1' ? " data-font-color='".($ishilited != '1' ? $color : $color_button_text )."'" : "" ) . " href='$order_url'>$button_text</a></div>" : "";
	$box2 = ob_get_clean();
	$out .= !empty( $box2 ) ? "<div>$box2</div>" : "";

	return $out;
}

function the8_callout_renderer ( $atts, $content ) {
	extract( shortcode_atts( array(
		'title' => '',
		'alt' => '',
		'c_btn_href' => '#',
		'c_btn_text' => '',
		'iconfa' => '',
		'iconimg' => '',
		'custom_colors' => '0',
		'fill_type' => 'color',
		'fill_color' => THE8_COLOR,
		'font_color' => '#fff',
		'btn_bg_color' => '#0eecbd',
		'btn_font_color' => '#fff'
	), $atts));

	$fa_icon = !empty($iconfa) ? $iconfa : '';

	$pbimage = !empty($iconimg) ? $iconimg : '';
	$out = "";

	$section_atts = "";
	$section_class = "cws_callout";
	$section_class .= $alt == '1' ? ' alt-style' : '';
	$section_styles = "";

	$font_color = esc_attr( $font_color );
	$fill_color = esc_attr( $fill_color );
	$btn_font_color = esc_url( $btn_font_color );
	$btn_bg_color = esc_url( $btn_bg_color );
	$c_btn_href = esc_url( $c_btn_href );
	$c_btn_text = esc_html( $c_btn_text );
	$title = esc_html( $title );



	if ( $custom_colors ) {
		$section_class .= " customized";
		$section_styles .= "color:$font_color;";
		if ( $fill_type == 'color' ) {
			$section_styles .= "background-color:$fill_color;";
		}
		else if ( $fill_type == 'gradient' ) {
			$section_styles .= the8_render_builder_gradient_rules( $atts );
		}
	}


	ob_start();
	if (!empty($fa_icon) || !empty($pbimage)) {
		echo "<div class='icon_section'>";
		if (!empty($fa_icon)) {
			$icon_custom_styles = $custom_colors ? ( $alt ? (' style="color:'.esc_attr($btn_bg_color).';background-color:transparent;border-color:'.esc_attr($btn_bg_color).';"') : (' style="color:'.esc_attr($btn_font_color).';background-color:'.esc_attr($btn_bg_color).';border-color:'.esc_attr($btn_bg_color).';"') ) : '';
			echo "<i class='cws_fa ".$fa_icon." fa-3x".($alt ? ' alt' : '')."'".$icon_custom_styles."></i>";
		}elseif (!empty($pbimage)) {
			$thumb_obj = bfi_thumb( $pbimage['row'],array( 'width' => 80, 'height' => 80, 'crop' => true ), false );
			$thumb_path_hdpi = $thumb_obj[3]['retina_thumb_exists'] ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3]['retina_thumb_url'] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
			$img_src = $thumb_path_hdpi;
			echo "<img ".$img_src." class='icon-image'>";
		}
		echo "</div>";
	}
	$icon_section = ob_get_clean();




	$section_atts .= !empty( $section_class ) ? " class='$section_class'" : "";
	$section_atts .= !empty( $section_styles ) ? " style='$section_styles'" : "";

	ob_start();
	echo !empty( $title ) ? "<div class='callout_title'" . ( $custom_colors ? " style='color:$font_color;'" : "" ) . ">$title</div>" : "";
	echo !empty( $content ) ? "<div class='callout_text'>" . wptexturize( do_shortcode( $content ) ) . "</div>" : "";
	$box1 = ob_get_clean();

	$out .= !empty($icon_section) ? $icon_section : '';

	$out .= !empty( $box1 ) ? "<div class='content_section'>$box1</div>" : "";

	$out .= !empty( $c_btn_text ) ? "<div class='button_section'><a href='".esc_url($c_btn_href)."' class='cws_button" . ( $custom_colors ? " custom_colors" : "" ) . ($alt ? " large'" : " xlarge'") . ( $custom_colors ? " data-bg-color='".esc_attr($btn_bg_color)."'" : "" ) . ( $custom_colors ? " data-font-color='".esc_attr($btn_font_color)."'" : "" ) . ">$c_btn_text</a></div>" : "";

	$out = !empty( $out ) ? "<div" . ( !empty( $section_atts ) ? $section_atts : "" ) . ">".($alt == '1' ? '<div class="styling-border"'.( $custom_colors ? " style='background-color: ".esc_attr($btn_bg_color)."'" : "").'></div>' : '')."$out</div>" : "";

	return $out;
}

function the8_twitter_renderer ( $atts, $content = "" ) {
	extract( shortcode_atts( array(
		'in_widget' => false,
		'title' => '',
		'centertitle' => '0',
		'items' => get_option( 'posts_per_page' ),
		'visible' => get_option( 'posts_per_page' ),
		'showdate' => '0'
	), $atts));
	$out = "";
	$tw_username = the8_get_option( "tw-username" );
	if ( !is_numeric( $items ) || !is_numeric( $visible ) ) return $out;
	$tweets = the8_getTweets( (int)$items );
	if ( is_string( $tweets ) ) {
		$out .= do_shortcode( "[cws_sc_msg_box title='" . esc_html__( 'Twitter responds:', 'the8' ) . "' text='$tweets' is_closable='1'][/cws_sc_msg_box]" );
	}
	else if ( is_array( $tweets ) && isset($tweets['error']) ){
		echo esc_html($tweets['error']);
	}
	else if ( is_array( $tweets ) ) {
		$use_carousel = count( $tweets ) > $visible;
		$section_class = "cws_tweets";
		$section_class .= $use_carousel ? " tweets_carousel" : "";
		$section_class .= $use_carousel && empty( $title ) ? " paginated" : "";
		$out .= !empty( $title ) ? THE8_BEFORE_CE_TITLE . "<div" . ( $centertitle ? " style='text-align:center;'" : "" ) . ">$title</div>" . THE8_AFTER_CE_TITLE : "";
		if ( $use_carousel && !$in_widget ) {
			$out .= "<div class='tweets_carousel_header'>";
				$out .= "<a href='http://twitter.com/$tw_username' class='follow_us fa fa-twitter' target='_blank'></a>";
			$out .= "</div>";
		}
		$out .= "<div class='$section_class'>";
			$out .= "<div class='cws_wrapper'>";
				$carousel_item_closed = false;
				for ( $i=0; $i<count( $tweets ); $i++ ) {
					$tweet = $tweets[$i];
					if ( $use_carousel && ( $i == 0 || $carousel_item_closed ) ) {
						wp_enqueue_script ('owl_carousel');
						$out .= "<div class='item'>";
						$carousel_item_closed = false;
					}
					$tweet_text = isset( $tweet['text'] ) ? $tweet['text'] : "";
					$tweet_entitties = isset( $tweet['entities'] ) ? $tweet['entities'] : array();
					$tweet_urls = isset( $tweet_entitties['urls'] ) && is_array( $tweet_entitties['urls'] ) ? $tweet_entitties['urls'] : array();
					foreach ( $tweet_urls as $tweet_url ) {
						$display_url = isset( $tweet_url['display_url'] ) ? $tweet_url['display_url'] : "";
						$received_url = isset( $tweet_url['url'] ) ? $tweet_url['url'] : "";
						$html_url = "<a href='$received_url'>$display_url</a>";
						$tweet_text = substr_replace( $tweet_text, $html_url, strpos( $tweet_text, $received_url ), strlen( $received_url ) );
					}
					$item_content = "";
					$item_content .= !empty( $tweet_text ) ? "<div class='tweet_content'>$tweet_text</div>" : "";
					if ( $showdate ) {
						$tweet_date = isset( $tweet['created_at'] ) ? $tweet['created_at'] : "";
						$tweet_date_formatted = time_elapsed_string( date( "U", strtotime( $tweet_date ) ) );
						$item_content .= "<div class='tweet_date'>$tweet_date_formatted</div>";
					}
					$out .= !empty( $item_content ) ? "<div class='cws_tweet'>$item_content</div>" : "";
					$temp1 = ( $i + 1 ) / (int)$visible;
					if ( $use_carousel && ( $temp1 - floor( $temp1 ) == 0 || $i == count( $tweets ) - 1 ) ) {
						$out .= "</div>";
						$carousel_item_closed = true;
					}
				}
			$out .= "</div>";
		$out .= "</div>";
	}
	return $out;
}

function time_elapsed_string($ptime)
{
    $etime = time() - $ptime;

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

?>