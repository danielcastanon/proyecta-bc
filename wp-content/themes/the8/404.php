<?php
	get_header ();
?>
<div class="page_content">
	<?php
	$home_url = esc_url(get_home_url());
	?>
	<main>
		<div class="grid_row clearfix">
			<div class="grid_col grid_col_12">
				<div class="ce">
					<div class="not_found">
						<div class="banner_404">
							<img src="<?php echo THE8_URI . "/img/404.png"; ?>" alt />
						</div>
						<div class="desc_404">
							<div class="msg_404">
								<?php
									echo esc_html__( 'Sorry', 'the8' ) . "<br />" . esc_html__( "This page doesn't exist.", "the8" );
								?>
							</div>
							<div class="link">
								<?php
									echo esc_html__( 'Please, proceed to our ', 'the8' ) . "<a href='$home_url'>" . esc_html__( 'Home page', 'the8' ) . "</a>";
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>
<?php
get_footer ();
?>