<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage The_8
 * @since The 8 1.0
 */
?>

	</div><!-- #main -->

	<?php 
		echo the8_page_footer();
		wp_footer();
	?>
</div>
<!-- end body cont -->
</body>
</html>