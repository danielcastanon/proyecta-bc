<?php
	$blogtype = the8_get_option( "def_blogtype" );
	$taxonomy = "category";
	$terms  = array();

if ( is_page() ) {
	$blogtype = the8_get_page_meta_var ( array( 'blog', 'blogtype' ) );
	$cats = the8_get_page_meta_var ( array( 'blog', 'cats' ) );
	$terms = !empty($cats) ? explode(',', the8_get_page_meta_var ( array( 'blog', 'cats' ) )) : '';
}
else if ( is_category() ) {
	$term_id = get_query_var( 'cat' );
	$term = get_term_by( 'id', $term_id, 'category' );
	$term_slug = $term->slug;
	$terms = array( $term_slug );
}
else if ( is_tag() ) {
	$taxonomy = 'post_tag';
	$term_slug = get_query_var( 'tag' );
	$terms = array( $term_slug );
}


	if ($blogtype == 'default') {
		$blogtype = the8_get_option( 'def_blogtype' );
	}

	$post_type_array = array("post");
	$posts_per_page = (int)get_option('posts_per_page');
	$ajax = isset( $_POST['ajax'] ) ? (bool)$_POST['ajax'] : false;
	$paged_var = get_query_var( 'paged' );
	$paged = $ajax && isset( $_POST['paged'] ) ? $_POST['paged'] : ( $paged_var ? $paged_var : 1 );
	$args = array("post_type"=>$post_type_array,
					'post_status' => 'publish',
					'posts_per_page' => $posts_per_page,
					'paged' => $paged);	

	if ( !empty( $terms ) ) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => $taxonomy,
				'field' => 'slug',
				'terms' => $terms
			)
		);
	}
	if ( is_date() ) {
		$year = the8_get_date_part( 'y' );
		$month = the8_get_date_part( 'm' );
		$day = the8_get_date_part( 'd' );
		if ( !empty( $year ) ) {
			$args['year'] = $year;
		}
		if ( !empty( $month ) ) {
			$args['monthnum'] = $month;
		}
		if ( !empty( $day ) ) {
			$args['day'] = $day;
		}
	}
	$query = new WP_Query( $args );
	$max_paged = ceil( $query->found_posts / $posts_per_page );

	$blogtype = sanitize_html_class( $blogtype );

	$news_class = !empty( $blogtype ) ? ( preg_match( '#^\d+$#', $blogtype ) ? "news-pinterest" : "news-$blogtype" ) : "news-medium";
	$grid_class = $news_class == "news-pinterest" ? "grid-$blogtype isotope" : "";

	if ($news_class == "news-pinterest") {
		wp_enqueue_script ('isotope');
	}

	if ( !$ajax ): // not ajax request

		?>
		<div class="grid_row">
			<section class="news <?php echo $news_class; ?>">
				<div class="cws_wrapper">
					<div class="grid <?php echo $grid_class; ?>">
					<?php

						endif;
						the8_blog_output( $query ); // output posts
						if ( !$ajax ): // not ajax request

					?>
					</div>
					<?php
						if ( $news_class == "news-pinterest" && $paged < $max_paged ) {
							$template = 'content-blog';
							the8_load_more( $paged + 1, $template, $max_paged );
						}
						else if ( in_array( $news_class, array( "news-small", "news-medium", "news-large" ) ) && $max_paged > 1 ) {
							the8_pagination( $paged, $max_paged );
						}
					?>
				</div>
			</section>
		</div>
		<?php

	endif;
?>