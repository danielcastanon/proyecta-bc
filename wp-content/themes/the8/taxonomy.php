<?php
	get_header ();

	$sb = the8_get_sidebars();
	$sb_class = $sb && !empty($sb['sb_layout_class']) ? sanitize_html_class($sb['sb_layout_class']) . '_sidebar' : '';
	$sb1_class = $sb && $sb['sb_layout'] == 'right' ? 'sb_right' : 'sb_left';

	$taxonomy = get_query_var( 'taxonomy' );
	$term_slug = get_query_var( $taxonomy );

?>
<div class="page_content <?php echo sanitize_html_class($sb_class); ?>">
	<?php
	if ( $sb && $sb['sb_exist'] ) {
		echo "<div class='container'>";
		if ( $sb['sb1_exists'] ) {
			echo "<aside class='$sb1_class'>";
			dynamic_sidebar( $sb['sidebar1'] );
			echo "</aside>";
		}
		if ( $sb['sb2_exists'] ) {
			echo "<aside class='sb_right'>";
			dynamic_sidebar( $sb['sidebar2'] );
			echo "</aside>";
		}
	}

	?>
	<main>
		<div class="grid_row">
			<?php
				switch( $taxonomy ) {
					case "cws_portfolio_cat":
						echo the8_cws_portfolio( array( 'columns' => '4', 'categories' => $term_slug, 'mode' => 'grid' ) );
						break;
					case "cws_staff_member_department":
						echo the8_cws_ourteam( array( 'mode' => 'grid', 'categories' => $term_slug ) );
						break;
					case "cws_staff_member_position":
						echo the8_cws_ourteam( array( 'mode' => 'grid', 'tags' => $term_slug ) );
						break;
				}
			?>
		</div>
	</main>
	<?php echo $sb && $sb['sb_exist'] ? "</div>" : ""; ?>
</div>

<?php

get_footer ();
?>