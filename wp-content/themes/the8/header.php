<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage The_8
 * @since The 8 1.0
 */ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php
		do_action('the8_theme_header_meta');
		wp_head(); 
	?>
</head>

<body <?php body_class(); ?>>
<!-- body cont -->
<div class='body-cont'>
	<?php

		$cws_enable_page_loader = the8_get_option( 'cws_enable_loader' ) == 1 ? true : false;
		if ($cws_enable_page_loader){
			echo the8_page_loader();
		}

		echo the8_page_header();
		
		?>
	<div id="main" class="site-main">
