<?php

	$pbf_redeclares = array( 'cws_callout_renderer', 'cws_tabs_renderer', 'cws_item_shortcode' );

	function the8_get_pb_options() {
		$body_font_settings = the8_get_option( 'body-font' );
		$body_font_color = isset( $body_font_settings['color'] ) ? $body_font_settings['color'] : '';
		return array(
			'modules' => array('text', 'column','tabs', 'tweet', 'accs', 'tcol', 'row_ourteam', 'callout', 'row_blog', 'row_portfolio_fw', 'row_portfolio'),
			'parallax' => true,
			'portfolio' => array(
				'options' => array (
					'post_type' => 'cws_portfolio',
					'taxonomy' => 'cws_portfolio_cat'
				)
			),
			'ourteam' => array(
				'options' => array(
					'post_type' => 'cws_staff',
					'taxonomy' => 'cws_staff_member_department'
				)
			),
			'margins_label' => 'Padding:',
			'tcol' => array(
				'layout' => array(
					'tab0' => array(
						'title' => esc_html__( 'Default', 'the8' ),
						'type' => 'tab',
						'layout' => array(
							'p_title' => array (
								'title' => esc_html__( 'Title', 'the8' ),
								'type' => 'text',
								'value' => 'START'
							),
							'p_currency' => array (
								'title' => esc_html__( 'Currency', 'the8' ),
								'type' => 'text',
								'value' => '$',
							),
							'p_price' => array (
								'title' => esc_html__( 'Price', 'the8' ),
								'type' => 'text',
								'value' => '60',
							),
							'p_price_description' => array (
								'title' => esc_html__( 'Description', 'the8' ),
								'type' => 'text',
								'value' => 'monthly',
							),
							'p_order_url' => array (
								'title' => esc_html__( 'Order url', 'the8' ),
								'type' => 'text',
								'value' => '#',
							),
							'p_button_text' => array (
								'title' => esc_html__( 'Button', 'the8' ),
								'type' => 'text',
								'value' => 'buy now',
							),
							'p_ishilited' => array (
								'title' => esc_html__( 'Highlighted', 'the8' ),
								'type' => 'checkbox'
							),
							'insertmedia' => array(
								'type' => 'insertmedia',
								'rowclasses' => 'row',
							),
							'cws-pb-tmce' => array(
								'type' => 'textarea',
								'rowclasses' => 'cws-pb-tmce',
								'atts' => 'class="wp-editor-area" id="wp-editor-area"'
							)
						),
					),
					'tab1' => array(
						'title' => esc_html__( 'Styling', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_customize' => array (
								'title' => esc_html__( 'Customize color', 'the8' ),
								'type' => 'checkbox',
								'atts' => 'data-options="e:p_color;e:p_color_button_text;"'
							),
							'p_color' => array(
								'title' => esc_html__( 'Color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="' . THE8_COLOR . '"',
								'value' => THE8_COLOR,
								'addrowclasses' => 'disable'
								),
							'p_color_button_text' => array(
								'title' => esc_html__( 'Hover button color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="#ffffff"',
								'value' => '#ffffff',
								'addrowclasses' => 'disable'
								),
							'module' => array(
								'type' => 'module',
								'name' => 'ani_add'
							),
						)
					)
				)
			),
			'col-title' => array(
				'layout' => array(
					'p_title' => array(
						'title' => esc_html__( 'Widget Title', 'the8' ),
						'type' => 'text',
					),
					'p_centertitle' => array(
						'title' => esc_html__( 'Center title', 'the8' ),
						'type' => 'checkbox'
					),
					'p_vertical' => array(
						'title' => esc_html__( 'Vertical type', 'the8' ),
						'type' => 'checkbox',
					),
					'module' => array(
						'type' => 'module',
						'name' => 'ani_add'
					),
				)
			),
			'accs-title' => array(
				'layout' => array(
					'p_title' => array(
						'title' => esc_html__( 'Title', 'the8' ),
						'type' => 'text',
					),
					'p_centertitle' => array(
						'title' => esc_html__( 'Center title', 'the8' ),
						'type' => 'checkbox'
					),
					'p_accstyle' => array(
						'title' => esc_html__( 'Style', 'the8' ),
						'type' => 'select',
						'source' => array(
							'first_style' => array( 'Colored content', true ),
							'second_style' => array( 'Colored content and caption', false ),
							'third_style' => array( 'Bordered', false )
						)
					),
					'p_istoggle' => array(
						'title' => esc_html__( 'Toggle', 'the8' ),
						'type' => 'checkbox',
					),
					'module' => array(
						'type' => 'module',
						'name' => 'ani_add'
					)
				)
			),
			'text' => array(
				'layout' => array(
					'tab0' => array(
						'title' => esc_html__( 'Content', 'the8' ),
						'type' => 'tab',
						'layout' => array(
							'insertmedia' => array(
								'type' => 'insertmedia',
								'rowclasses' => 'row',
							),
							'cws-pb-tmce' => array(
								'type' => 'textarea',
								'rowclasses' => 'cws-pb-tmce',
								'atts' => 'class="wp-editor-area" id="wp-editor-area"'
							)
						),
					),
					'tab1' => array(
						'title' => esc_html__( 'Spacings', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_margins' => array(
								'title' => esc_html__( 'Margins (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
							'p_paddings' => array(
								'title' => esc_html__( 'Paddings (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
						),
					),
					'tab2' => array(
						'title' => esc_html__( 'Styling', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_eclass' => array(
								'title' => esc_html__( 'Extra Class', 'the8' ),
								'description' => esc_html__( 'Space separated class names', 'the8' ),
								'type' => 'text',
							),
							'p_customize_bg' => array(
								'title' => esc_html__( 'Customize', 'the8' ),
								'type' => 'checkbox',
								'atts' => 'data-options="e:p_bg_media_type;e:p_bg_color_type;e:p_font_color"'
							),
							'p_bg_media_type' => array(
								'title' => esc_html__( 'Media type', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array( 'None', true, 'd:p_bg_img;d:p_is_bg_img_high_dpi;d:p_bg_video_type;d:p_bg_pattern;d:p_use_prlx;d:p_bg_possition;d:p_bg_repeat;d:p_bg_attach;' ),
									'img' => array( 'Image', false, 'e:p_bg_img;e:p_is_bg_img_high_dpi;e:p_bg_pattern;e:p_use_prlx;d:p_bg_video_type;e:p_bg_possition;e:p_bg_repeat;e:p_bg_attach;' ),
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_img' => array(
								'title' => esc_html__( 'Image', 'the8' ),
								'type' => 'media',
								'atts' => 'data-role="media"',
								'addrowclasses' => 'disable'
								),
							'p_bg_attach' => array(
								'title' => esc_html__( 'Background Attachment:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'scroll' => array( 'Scroll', true),
									'fixed' => array( 'Fixed', false ),
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_possition' => array(
								'title' => esc_html__( 'Background Position:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'left top' => array( 'left top', false),
									'left center' => array( 'left center', false ),
									'left bottom' => array( 'left bottom', false ),
									'right top' => array( 'right top', false ),
									'right center' => array( 'right center', false ),
									'right bottom' => array( 'right bottom', false ),
									'center top' => array( 'center top', false ),
									'center center' => array( 'center center', true ),
									'center bottom' => array( 'center bottom', false )
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_repeat' => array(
								'title' => esc_html__( 'Background Repeat:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'no-repeat' => array( 'No Repeat', false),
									'repeat' => array( 'Repeat', false ),
									'cover' => array( 'Cover', true ),
									'contain' => array( 'Contain', false )
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_color_type' =>  array(
								'title' => esc_html__( 'Background Color', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array( 'None', true, 'd:p_bg_color;d:p_gradient_start_color;d:p_gradient_end_color;d:p_gradient_type;d:p_bg_color_opacity;' ),
									'color' => array( 'Color', false, 'e:p_bg_color;e:p_bg_color_opacity;d:p_gradient_start_color;d:p_gradient_end_color;d:p_gradient_type;' ),
									'gradient' => array( 'Gradient', false, 'e:p_gradient_start_color;e:p_gradient_end_color;e:p_gradient_type;e:p_bg_color_opacity;d:p_bg_color;' )
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_color' => array(
								'title' => esc_html__( 'Color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="' . THE8_COLOR . '"',
								'value' => THE8_COLOR,
								'addrowclasses' => 'disable'
								),
							'p_gradient_start_color' => array(
								'title' => esc_html__( 'From', 'the8' ),
								'type' => 'text',
								'addrowclasses' => 'disable',
								'atts' => 'data-default-color="' . THE8_COLOR . '"',
								'value' => THE8_COLOR,
								'addrowclasses' => 'disable'
							),
							'p_gradient_end_color' => array(
								'title' => esc_html__( 'To', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'text',
								'atts' => 'data-default-color="#0eecbd"',
								'value' => '#0eecbd',
								'addrowclasses' => 'disable'
							),
							'p_gradient_type' => array(
								'title' => esc_html__( 'Type', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'radio',
								'value' => array(
									'linear' => array( 'Linear', true, 'e:p_gradient_linear_angle;d:p_gradient_radial_shape_settings_type;' ),
									'radial' => array( 'Radial', false, 'e:p_gradient_radial_shape_settings_type;d:p_gradient_linear_angle;' )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_linear_angle' => array(
								'title' => esc_html__( 'Angle', 'the8' ),
								'addrowclasses' => 'disable',
								'description' => esc_html__( 'Degrees: -360 to 360', 'the8' ),
								'type' => 'number',
								'atts' => ' min="-360" max="360" step="1"',
								'value' => '45',
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_shape_settings_type' => array(
								'addrowclasses' => 'disable',
								'title' => esc_html__( 'Shape variant', 'the8' ),
								'type' => 'radio',
								'value' => array(
									'simple' => array( 'Simple', true, 'e:p_gradient_radial_shape;d:p_gradient_radial_size_keyword;d:p_gradient_radial_size;' ),
									'extended' => array( 'Extended', false, 'e:p_gradient_radial_size_keyword;e:p_gradient_radial_size;d:p_gradient_radial_shape;' )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_shape' => array(
								'title' => esc_html__( 'Shape', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'radio',
								'value' => array(
									'ellipse' => array( 'Ellipse', true ),
									'circle' => array( 'Circle', false )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_size_keyword' => array(
								'title' => esc_html__( 'Size keyword', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'select',
								'source' => array(
									'closest-side' => array( 'Closest side', false ),
									'farthest-side' => array( 'Farthest side', false ),
									'closest-corner' => array( 'Closest corner', false ),
									'farthest-corner' => array( 'Farthest corner', true )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_size' => array(
								'title' => esc_html__( 'Size', 'the8' ),
								'addrowclasses' => 'disable',
								'description' => esc_html__( 'Two space separated percent values, for example (60%% 55%%)', 'the8' ),
								'type' => 'text',
								'value' => '',
								'addrowclasses' => 'disable'
							),
							'p_use_prlx' => array(
								'title' => esc_html__( 'Apply Parallax', 'the8' ),
								'type' => 'checkbox',
								'atts' => 'data-options="e:p_prlx_speed;"',
								'addrowclasses' => 'disable'
								),
							'p_prlx_speed' => array(
								'title' => esc_html__( 'Parallax speed', 'the8' ),
								'type' => 'number',
								'atts' => 'min="1" max="100" step="1"',
								'value' => '50',
								'addrowclasses' => 'disable'
							),
							'p_bg_color_opacity' => array(
								'title' => esc_html__( 'Opacity', 'the8' ),
								'description' => esc_html__( 'In percents', 'the8' ),
								'type' => 'number',
								'atts' => 'min="1" max="100" step="1"',
								'value' => '100',
								'addrowclasses' => 'disable'
							),
							'p_bg_pattern' => array(
								'title' => esc_html__( 'Pattern', 'the8' ),
								'type' => 'media',
								'addrowclasses' => 'disable'
							),
							'p_font_color' => array(
								'title' => esc_html__( 'Font color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="' . $body_font_color . '"',
								'value' => $body_font_color,
								'addrowclasses' => 'disable'
							),
							'module' => array(
								'type' => 'module',
								'name' => 'ani_add'
							),
						),
					),

				)
			),
			'callout' => array(
				'options' => array (
					'icon_selection' => true,
				),
				'layout' => array(
					'p_alt' => array(
						'title' => esc_html__( 'Alternative style', 'the8' ),
						'type' => 'checkbox',
						),
					'p_title' => array(
						'title' => esc_html__( 'Title', 'the8' ),
						'type' => 'text',
					),
					'p_c_btn_text' => array(
						'title' => esc_html__( 'Button', 'the8' ),
						'type' => 'text',
						'value' => 'Purchase Now',
					),
					'p_c_btn_href' => array(
						'title' => esc_html__( 'Button URL', 'the8' ),
						'type' => 'text',
						'value' => '#',
					),
					'p_custom_colors' => array(
						'title' => esc_html__( 'Customize', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:p_fill_type;e:p_font_color;e:p_btn_bg_color;e:p_btn_font_color;"'
					),
					'p_fill_type' => array(
						'title' => esc_html__( 'Type', 'the8' ),
						'type' => 'radio',
						'addrowclasses' => 'disable',
						'value' => array(
							'color' => array( 'Color', true, 'e:p_fill_color;d:p_gradient_start_color;d:p_gradient_end_color;d:p_gradient_type' ),
							'gradient' => array( 'Gradient', false, 'e:p_gradient_start_color;e:p_gradient_end_color;e:p_gradient_type;d:p_fill_color' )
						)
					),
					'p_fill_color' => array(
						'title' => esc_html__( 'Color', 'the8' ),
						'type' => 'text',
						'addrowclasses' => 'disable',
						'atts' => 'data-default-color="' . THE8_COLOR . '"',
						'value' => THE8_COLOR
					),
					'p_gradient_start_color' => array(
						'title' => esc_html__( 'From', 'the8' ),
						'type' => 'text',
						'addrowclasses' => 'disable',
						'atts' => 'data-default-color="' . THE8_COLOR . '"',
						'value' => THE8_COLOR
					),
					'p_gradient_end_color' => array(
						'title' => esc_html__( 'To', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'text',
						'atts' => 'data-default-color="#0eecbd"',
						'value' => '#0eecbd'
					),
					'p_gradient_type' => array(
						'title' => esc_html__( 'Type', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'radio',
						'value' => array(
							'linear' => array( 'Linear', true, 'e:p_gradient_linear_angle;d:p_gradient_radial_shape_settings_type;' ),
							'radial' => array( 'Radial', false, 'e:p_gradient_radial_shape_settings_type;d:p_gradient_linear_angle;' )
						)
					),
					'p_gradient_linear_angle' => array(
						'title' => esc_html__( 'Angle', 'the8' ),
						'addrowclasses' => 'disable',
						'description' => esc_html__( 'In degrees', 'the8' ),
						'type' => 'number',
						'atts' => ' min="-360" max="360" step="1"',
						'value' => '45'
					),
					'p_gradient_radial_shape_settings_type' => array(
						'addrowclasses' => 'disable',
						'title' => esc_html__( 'Shape variant', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'simple' => array( 'Simple', true, 'e:p_gradient_radial_shape;d:p_gradient_radial_size_keyword;d:p_gradient_radial_size;' ),
							'extended' => array( 'Extended', false, 'e:p_gradient_radial_size_keyword;e:p_gradient_radial_size;d:p_gradient_radial_shape;' )
						)
					),
					'p_gradient_radial_shape' => array(
						'title' => esc_html__( 'Shape', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'radio',
						'value' => array(
							'ellipse' => array( 'Ellipse', true ),
							'circle' => array( 'Circle', false )
						)
					),
					'p_gradient_radial_size_keyword' => array(
						'title' => esc_html__( 'Size keyword', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'select',
						'source' => array(
							'closest-side' => array( 'Closest side', false ),
							'farthest-side' => array( 'Farthest side', false ),
							'closest-corner' => array( 'Closest corner', false ),
							'farthest-corner' => array( 'Farthest corner', true )
						)
					),
					'p_gradient_radial_size' => array(
						'title' => esc_html__( 'Size', 'the8' ),
						'addrowclasses' => 'disable',
						'description' => esc_html__( 'Two space separated percent values, for example (60%% 55%%)', 'the8' ),
						'type' => 'text',
						'value' => ''
					),
					'p_font_color' => array(
						'title' => esc_html__( 'Font color', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'text',
						'atts' => 'data-default-color="#fff"',
						'value' => '#fff'
					),
					'p_btn_bg_color' => array(
						'title' => esc_html__( 'Button bg-color', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'text',
						'atts' => 'data-default-color="#0eecbd"',
						'value' => '#0eecbd'
					),
					'p_btn_font_color' => array(
						'title' => esc_html__( 'Button text color', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'text',
						'atts' => 'data-default-color="#fff"',
						'value' => '#fff'
					),
					'module' => array(
						'type' => 'module',
						'name' => 'ani_add'
					),
					'insertmedia' => array(
						'type' => 'insertmedia',
						'rowclasses' => 'row',
					),
					'cws-pb-tmce' => array(
						'type' => 'textarea',
						'rowclasses' => 'cws-pb-tmce',
						'atts' => 'class="wp-editor-area" id="wp-editor-area"',
					),
				)
			),
			'col' => array(
				'layout' => array(
					'tab0' => array(
						'title' => esc_html__( 'General', 'the8' ),
						'type' => 'tab',
						'layout' => array(
							'p_row_style' => array(
								'title' => esc_html__( 'Row type', 'the8' ),
								'type' => 'select',
								'source' => array(
									'def' => array('Default', true), // Title, isselected, data-options
									'benefits' => array('Benefits', false),
									'fullwidth_item_no_padding' => array('Full-width content', false),
									'fullwidth_item' => array('Full-width content with paddings', false),
									'fullwidth_background' => array('Full-width background only', false)
								),
							),
							'p_equal_height' => array(
								'title' => esc_html__( 'Equal height columns', 'the8' ),
								'type' => 'checkbox'
							),
							'p_content_position' => array(
								'title' => esc_html__( 'Vertical align', 'the8' ),
								'type' => 'select',
								'source' => array(
									'top' => array('Top', true),
									'middle' => array('Middle', false),
									'bottom' => array('Bottom', false)
								),
							),
						),
					),
					'tab1' => array(
						'title' => esc_html__( 'Spacings', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_margins' => array(
								'title' => esc_html__( 'Margins (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
							'p_paddings' => array(
								'title' => esc_html__( 'Paddings (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
						),
					),
					'tab3' => array(
						'title' => esc_html__( 'Styling', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_eclass' => array(
								'title' => esc_html__( 'Extra Class', 'the8' ),
								'description' => esc_html__( 'Space separated class names', 'the8' ),
								'type' => 'text',
							),
							'p_section_border' => array(
								'title' => esc_html__( 'Border:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array( 'none', true, 'd:p_section_border_width;d:p_section_border_style;d:p_section_border_color;'),
									'top-border' => array( 'top border', false, 'e:p_section_border_width;e:p_section_border_style;e:p_section_border_color;' ),
									'bottom-border' => array( 'bottom border', false, 'e:p_section_border_width;e:p_section_border_style;e:p_section_border_color;' ),
									'top-bottom' => array( 'top and bottom border', false, 'e:p_section_border_width;e:p_section_border_style;e:p_section_border_color;' )
								),
							),
							'p_section_border_width' => array(
								'title' => esc_html__( 'Border width', 'the8' ),
								'type' => 'number',
								'atts' => ' min="1" step="1"',
								'value' => '1',
								'addrowclasses' => 'disable'
							),
							'p_section_border_style' => array(
								'title' => esc_html__( 'Border style:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'solid' => array( 'solid', true),
									'dashed' => array( 'dashed', false ),
									'dotted' => array( 'dotted', false )
								),
								'addrowclasses' => 'disable'
							),
							'p_section_border_color' => array(
								'title' => esc_html__( 'Border Color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="#f2f2f2"',
								'value' => '#f2f2f2',
								'addrowclasses' => 'disable'
								),
							'p_customize_bg' => array(
								'title' => esc_html__( 'Customize', 'the8' ),
								'type' => 'checkbox',
								'atts' => 'data-options="e:p_bg_media_type;e:p_bg_color_type;e:p_font_color"'
							),
							'p_bg_media_type' => array(
								'title' => esc_html__( 'Media type', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array( 'None', true, 'd:p_bg_img;d:p_is_bg_img_high_dpi;d:p_bg_video_type;d:p_bg_pattern;d:p_use_prlx;d:p_bg_possition;d:p_bg_repeat;d:p_bg_attach;' ),
									'img' => array( 'Image', false, 'e:p_bg_img;e:p_is_bg_img_high_dpi;e:p_bg_pattern;e:p_use_prlx;d:p_bg_video_type;e:p_bg_possition;e:p_bg_repeat;e:p_bg_attach;' ),
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_img' => array(
								'title' => esc_html__( 'Image', 'the8' ),
								'type' => 'media',
								'atts' => 'data-role="media"',
								'addrowclasses' => 'disable'
								),
							'p_bg_attach' => array(
								'title' => esc_html__( 'Background Attachment:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'scroll' => array( 'Scroll', true),
									'fixed' => array( 'Fixed', false ),
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_possition' => array(
								'title' => esc_html__( 'Background Position:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'left top' => array( 'left top', false),
									'left center' => array( 'left center', false ),
									'left bottom' => array( 'left bottom', false ),
									'right top' => array( 'right top', false ),
									'right center' => array( 'right center', false ),
									'right bottom' => array( 'right bottom', false ),
									'center top' => array( 'center top', false ),
									'center center' => array( 'center center', true ),
									'center bottom' => array( 'center bottom', false )
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_repeat' => array(
								'title' => esc_html__( 'Background Repeat:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'no-repeat' => array( 'No Repeat', false),
									'repeat' => array( 'Repeat', false ),
									'cover' => array( 'Cover', true ),
									'contain' => array( 'Contain', false )
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_color_type' =>  array(
								'title' => esc_html__( 'Background Color', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array( 'None', true, 'd:p_bg_color;d:p_gradient_start_color;d:p_gradient_end_color;d:p_gradient_type;d:p_bg_color_opacity;' ),
									'color' => array( 'Color', false, 'e:p_bg_color;e:p_bg_color_opacity;d:p_gradient_start_color;d:p_gradient_end_color;d:p_gradient_type;' ),
									'gradient' => array( 'Gradient', false, 'e:p_gradient_start_color;e:p_gradient_end_color;e:p_gradient_type;e:p_bg_color_opacity;d:p_bg_color;' )
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_color' => array(
								'title' => esc_html__( 'Color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="' . THE8_COLOR . '"',
								'value' => THE8_COLOR,
								'addrowclasses' => 'disable'
								),
							'p_gradient_start_color' => array(
								'title' => esc_html__( 'From', 'the8' ),
								'type' => 'text',
								'addrowclasses' => 'disable',
								'atts' => 'data-default-color="' . THE8_COLOR . '"',
								'value' => THE8_COLOR,
								'addrowclasses' => 'disable'
							),
							'p_gradient_end_color' => array(
								'title' => esc_html__( 'To', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'text',
								'atts' => 'data-default-color="#0eecbd"',
								'value' => '#0eecbd',
								'addrowclasses' => 'disable'
							),
							'p_gradient_type' => array(
								'title' => esc_html__( 'Type', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'radio',
								'value' => array(
									'linear' => array( 'Linear', true, 'e:p_gradient_linear_angle;d:p_gradient_radial_shape_settings_type;' ),
									'radial' => array( 'Radial', false, 'e:p_gradient_radial_shape_settings_type;d:p_gradient_linear_angle;' )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_linear_angle' => array(
								'title' => esc_html__( 'Angle', 'the8' ),
								'addrowclasses' => 'disable',
								'description' => esc_html__( 'Degrees: -360 to 360', 'the8' ),
								'type' => 'number',
								'atts' => ' min="-360" max="360" step="1"',
								'value' => '45',
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_shape_settings_type' => array(
								'addrowclasses' => 'disable',
								'title' => esc_html__( 'Shape variant', 'the8' ),
								'type' => 'radio',
								'value' => array(
									'simple' => array( 'Simple', true, 'e:p_gradient_radial_shape;d:p_gradient_radial_size_keyword;d:p_gradient_radial_size;' ),
									'extended' => array( 'Extended', false, 'e:p_gradient_radial_size_keyword;e:p_gradient_radial_size;d:p_gradient_radial_shape;' )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_shape' => array(
								'title' => esc_html__( 'Shape', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'radio',
								'value' => array(
									'ellipse' => array( 'Ellipse', true ),
									'circle' => array( 'Circle', false )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_size_keyword' => array(
								'title' => esc_html__( 'Size keyword', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'select',
								'source' => array(
									'closest-side' => array( 'Closest side', false ),
									'farthest-side' => array( 'Farthest side', false ),
									'closest-corner' => array( 'Closest corner', false ),
									'farthest-corner' => array( 'Farthest corner', true )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_size' => array(
								'title' => esc_html__( 'Size', 'the8' ),
								'addrowclasses' => 'disable',
								'description' => esc_html__( 'Two space separated percent values, for example (60%% 55%%)', 'the8' ),
								'type' => 'text',
								'value' => '',
								'addrowclasses' => 'disable'
							),
							'p_use_prlx' => array(
								'title' => esc_html__( 'Apply Parallax', 'the8' ),
								'type' => 'checkbox',
								'atts' => 'data-options="e:p_prlx_speed;"',
								'addrowclasses' => 'disable'
								),
							'p_prlx_speed' => array(
								'title' => esc_html__( 'Parallax speed', 'the8' ),
								'type' => 'number',
								'atts' => 'min="1" max="100" step="1"',
								'value' => '50',
								'addrowclasses' => 'disable'
							),
							'p_bg_color_opacity' => array(
								'title' => esc_html__( 'Opacity', 'the8' ),
								'description' => esc_html__( 'In percents', 'the8' ),
								'type' => 'number',
								'atts' => 'min="1" max="100" step="1"',
								'value' => '100',
								'addrowclasses' => 'disable'
							),
							'p_bg_pattern' => array(
								'title' => esc_html__( 'Pattern', 'the8' ),
								'type' => 'media',
								'addrowclasses' => 'disable'
							),
							'p_font_color' => array(
								'title' => esc_html__( 'Font color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="' . $body_font_color . '"',
								'value' => $body_font_color,
								'addrowclasses' => 'disable'
							),
							'ani_module' => array(
								'type' => 'module',
								'name' => 'ani_add'
							),
						),
					),

				),
			),
			'row_ourteam' => array(
				'layout' => array(
					'tab0' => array(
						'title' => esc_html__( 'General', 'the8' ),
						'type' => 'tab',
						'layout' => array(
							'p_title' => array(
								'title' => esc_html__( 'Title', 'the8' ),
								'type' => 'text',
							),
							'p_centertitle' => array(
								'title' => esc_html__( 'Center title', 'the8' ),
								'type' => 'checkbox'
							),
							'p_mode' => array(
								'title' => esc_html__( 'Display as', 'the8' ),
								'type' => 'select',
								'source' => array(
									'grid' => array(esc_html__( 'Grid', 'the8' ), true, 'd:p_centeritems;'),
									'grid_without_isotope' => array(esc_html__( 'Grid without Isotope', 'the8' ), false, 'e:p_centeritems;'),
									'grid_with_filter' => array(esc_html__( 'Grid with filter', 'the8' ), false, 'd:p_centeritems;'),
									'carousel' => array(esc_html__( 'Carousel', 'the8' ), false, 'd:p_centeritems;')
								),								
							),
							'p_centeritems' => array(
								'title' => esc_html__( 'Center items', 'the8' ),
								'type' => 'checkbox'
							),	
							'p_categories' => array(
								'title' => esc_html__( 'Categories', 'the8' ),
								'type' => 'taxonomy',
								'atts' => 'multiple',
								'taxonomy' => 'cws_staff_member_department'
							),
							'p_items_per_page' => array(
								'title' => esc_html__( 'Items per page', 'the8' ),
								'type' => 'text',
							),
						)
					),
					'tab1' => array(
						'title' => esc_html__( 'Spacings', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_margins' => array(
								'title' => esc_html__( 'Margins (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
							'p_paddings' => array(
								'title' => esc_html__( 'Paddings (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
						)
					),
					'tab2' => array(
						'title' => esc_html__( 'Styling', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_eclass' => array(
								'title' => esc_html__( 'Extra Class', 'the8' ),
								'description' => esc_html__( 'Space separated class names', 'the8' ),
								'type' => 'text',
							),
							'prlx_module' => array(
								'type' => 'module',
								'name' => 'prlx_add'
							),
							'ani_module' => array(
								'type' => 'module',
								'name' => 'ani_add'
							),
						)
					)
				)
			),
			'row_portfolio' => array(
				'layout' => array(
					'tab0' => array(
						'title' => esc_html__( 'General', 'the8' ),
						'type' => 'tab',
						'layout' => array(
							'p_title' => array(
								'title' => esc_html__( 'Title', 'the8' ),
								'type' => 'text',
							),
							'p_centertitle' => array(
								'title' => esc_html__( 'Center title', 'the8' ),
								'type' => 'checkbox'
							),
							'p_columns' => array(
								'title' => esc_html__( 'Columns', 'the8' ),
								'type' => 'select',
								'source' => array(
									'1' => array('One', false), // Title, isselected, data-options
									'2' => array('Two', true),
									'3' => array('Three', false),
									'4' => array('Four', false)
									)
							),
							'p_mode' => array(
								'title' => esc_html__( 'Display as', 'the8' ),
								'type' => 'select',
								'source' => array(
									'grid' => array('Grid', true, 'e:p_sel_posts_by;d:p_grid_with_filter_categories;'), // Title, isselected, data-options
									'grid_with_filter' => array('Grid with filter', false, 'd:p_sel_posts_by;e:p_grid_with_filter_categories;'),
									'carousel' => array('Carousel', false, 'e:p_sel_posts_by;d:p_grid_with_filter_categories;')
								),
							),
							'p_sel_posts_by' => array(
								'title' => esc_html__( 'Filter by', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array('None', true, 'd:p_categories;d:p_post_ids;'),
									'cats' => array('Categories', false, 'e:p_categories;d:p_post_ids;'),
									'titles' => array('Titles', false, 'd:p_categories;e:p_post_ids;'),
								),
							),
							'p_categories' => array(
								'title' => esc_html__( 'Categories', 'the8' ),
								'type' => 'taxonomy',
								'atts' => 'multiple',
								'taxonomy' => 'cws_portfolio_cat'
							),
							'p_grid_with_filter_categories' => array(
								'title' => esc_html__( 'Categories', 'the8' ),
								'type' => 'taxonomy',
								'atts' => 'multiple',
								'taxonomy' => 'cws_portfolio_cat',
								'addrowclasses' => 'disable',
							),
							'p_post_ids' => array(
								'title' => esc_html__( 'Posts', 'the8' ),
								'type' => 'select',
								'atts' => 'multiple',
								'source' => 'titles cws_portfolio'
							),
							'p_items_per_page' => array(
								'title' => esc_html__( 'Items per page', 'the8' ),
								'type' => 'number',
								'atts' => ' min="1" step="1"',
								'value' => ''
							),
							'p_portcontent' => array(
								'title' => esc_html__( 'Show', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array( 'None' ), // Title, isselected, data-options
									'exerpt' => array( 'Exerpt',true ),
									'categories' => array( 'Categories' ),
								),
							),
						)
					),
					'tab1' => array(
						'title' => esc_html__( 'Spacings', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_margins' => array(
								'title' => esc_html__( 'Margins (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
							'p_paddings' => array(
								'title' => esc_html__( 'Paddings (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
						)
					),
					'tab2' => array(
						'title' => esc_html__( 'Styling', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_eclass' => array(
								'title' => esc_html__( 'Extra Class', 'the8' ),
								'description' => esc_html__( 'Space separated class names', 'the8' ),
								'type' => 'text',
							),
							'prlx_module' => array(
								'type' => 'module',
								'name' => 'prlx_add'
							),
							'ani_module' => array(
								'type' => 'module',
								'name' => 'ani_add'
							),
						)
					),
				)
			),
			'row_portfolio_fw' => array(
				'layout' => array(
					'tab0' => array(
						'title' => esc_html__( 'General', 'the8' ),
						'type' => 'tab',
						'layout' => array(
							'p_title' => array(
								'title' => esc_html__( 'Title', 'the8' ),
								'type' => 'text',
							),
							'p_centertitle' => array(
								'title' => esc_html__( 'Center title', 'the8' ),
								'type' => 'checkbox'
							),
							'p_columns' => array(
								'title' => esc_html__( 'Columns', 'the8' ),
								'type' => 'select',
								'source' => array(
									'1' => array('One'), // Title, isselected, data-options
									'2' => array('Two', true),
									'3' => array('Three'),
									'4' => array('Four'),
									'5' => array('Five'),
									'6' => array('Six'),
								),
							),
							'p_mode' => array(
								'title' => esc_html__( 'Display as', 'the8' ),
								'type' => 'select',
								'source' => array(
									'grid' => array('Grid', true, 'e:p_sel_posts_by;d:p_grid_with_filter_categories;'), // Title, isselected, data-options
									'grid_with_filter' => array('Grid with filter', false, 'd:p_sel_posts_by;e:p_grid_with_filter_categories;'),
									'carousel' => array('Carousel', false, 'e:p_sel_posts_by;d:p_grid_with_filter_categories;')
								),
							),
							'p_sel_posts_by' => array(
								'title' => esc_html__( 'Filter by', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array('None', true, 'd:p_categories;d:p_post_ids;'),
									'cats' => array('Categories', false, 'e:p_categories;d:p_post_ids;'),
									'titles' => array('Titles', false, 'd:p_categories;e:p_post_ids;'),
								),
							),
							'p_categories' => array(
								'title' => esc_html__( 'Categories', 'the8' ),
								'type' => 'taxonomy',
								'atts' => 'multiple',
								'taxonomy' => 'cws_portfolio_cat'
							),
							'p_grid_with_filter_categories' => array(
								'title' => esc_html__( 'Categories', 'the8' ),
								'type' => 'taxonomy',
								'atts' => 'multiple',
								'taxonomy' => 'cws_portfolio_cat',
								'addrowclasses' => 'disable',
							),
							'p_post_ids' => array(
								'title' => esc_html__( 'Posts', 'the8' ),
								'type' => 'select',
								'atts' => 'multiple',
								'source' => 'titles cws_portfolio'
							),
							'p_items_per_page' => array(
								'title' => esc_html__( 'Items per page', 'the8' ),
								'type' => 'number',
								'atts' => ' min="1" step="1"',
								'value' => ''
							),
							'p_portcontent' => array(
								'title' => esc_html__( 'Hide', 'the8' ),
								'type' => 'select',
								'atts' => 'multiple',
								'source' => array(
									'title' => array( 'Titles' ),
									'buttons' => array( 'Buttons'),
									'categories' => array( 'Categories' ),
								),
							),
						)
					),
					'tab1' => array(
						'title' => esc_html__( 'Spacings', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_margins' => array(
								'title' => esc_html__( 'Margins (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
							'p_paddings' => array(
								'title' => esc_html__( 'Paddings (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
						)
					),
					'tab2' => array(
						'title' => esc_html__( 'Styling', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_eclass' => array(
								'title' => esc_html__( 'Extra Class', 'the8' ),
								'description' => esc_html__( 'Space separated class names', 'the8' ),
								'type' => 'text',
							),
							'prlx_module' => array(
								'type' => 'module',
								'name' => 'prlx_add'
							),
							'ani_module' => array(
								'type' => 'module',
								'name' => 'ani_add'
							),
						)
					),
				),
			),
			'row_blog' => array(
				'layout' => array(
					'tab0' => array(
						'title' => esc_html__( 'General', 'the8' ),
						'type' => 'tab',
						'layout' => array(
							'p_title' => array(
								'title' => esc_html__( 'Title', 'the8' ),
								'type' => 'text',
							),
							'p_centertitle' => array(
								'title' => esc_html__( 'Center title', 'the8' ),
								'type' => 'checkbox'
							),
							'p_columns' => array(
								'title' => esc_html__( 'Columns', 'the8' ),
								'type' => 'select',
								'source' => array(
									'1' => array('One', true), // Title, isselected, data-options
									'2' => array('Two'),
									'3' => array('Three')
								),
							),
							'p_categories' => array(
								'title' => esc_html__( 'Categories', 'the8' ),
								'type' => 'taxonomy',
								'atts' => 'multiple',
								'taxonomy' => 'category'
							),
							'p_items_per_page' => array(
								'title' => esc_html__( 'Items', 'the8' ),
								'type' => 'text',
							),
							'p_boxed_style' => array(
								'title' => esc_html__( 'Posts layout', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array('Default', true),
									'with_border' => array('With border' , false),
									'with_shadow' => array('With shadow' , false)
								)
							),
							'p_use_carousel' => array(
								'title' => esc_html__( 'Display as carousel', 'the8' ),
								'type' => 'checkbox'
							),
							'p_custom_layout' => array(
								'title' => 'Customize layout',
								'type' => 'checkbox',
								'atts' => 'data-options="e:p_post_text_length;e:p_enable_lightbox;e:p_hide_meta;e:p_button_name;e:p_date_style"',
							),
							'p_post_text_length' => array(
								'title' => esc_html__( 'Text length', 'the8' ),
								'type' => 'text',
								'addrowclasses' => 'disable',
							),
							'p_button_name' => array(
								'title' => esc_html__( 'Button title', 'the8' ),
								'type' => 'text',
								'addrowclasses' => 'disable',
							),
							'p_enable_lightbox' => array(
								'title' => 'Disable Lightbox',
								'type' => 'checkbox',
								'addrowclasses' => 'disable',
							),
							'p_date_style' => array(
								'title' => esc_html__( 'Date container', 'the8' ),
								'type' => 'select',
								'source' => array(
									'default' => array('Wrapped around', true),
									'unwrapped' => array('Not wrapped around' , false),
									'none' => array('Hidden' , false)
								),
								'addrowclasses' => 'disable'
							),
							'p_hide_meta' => array(
								'title' => 'Hide cats and tags',
								'type' => 'checkbox',
								'addrowclasses' => 'disable',
							),
						),
					),
					'tab1' => array(
						'title' => esc_html__( 'Spacings', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_margins' => array(
								'title' => esc_html__( 'Margins (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
							'p_paddings' => array(
								'title' => esc_html__( 'Paddings (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
						),
					),
					'tab2' => array(
						'title' => esc_html__( 'Styling', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_eclass' => array(
								'title' => esc_html__( 'Extra Class', 'the8' ),
								'description' => esc_html__( 'Space separated class names', 'the8' ),
								'type' => 'text',
							),
							'p_section_border' => array(
								'title' => esc_html__( 'Border:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array( 'none', true, 'd:p_section_border_width;d:p_section_border_style;d:p_section_border_color;'),
									'top-border' => array( 'top border', false, 'e:p_section_border_width;e:p_section_border_style;e:p_section_border_color;' ),
									'bottom-border' => array( 'bottom border', false, 'e:p_section_border_width;e:p_section_border_style;e:p_section_border_color;' ),
									'top-bottom' => array( 'top and bottom border', false, 'e:p_section_border_width;e:p_section_border_style;e:p_section_border_color;' )
								),
							),
							'p_section_border_width' => array(
								'title' => esc_html__( 'Border width', 'the8' ),
								'type' => 'number',
								'atts' => ' min="1" step="1"',
								'value' => '1',
								'addrowclasses' => 'disable'
							),
							'p_section_border_style' => array(
								'title' => esc_html__( 'Border style:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'solid' => array( 'solid', true),
									'dashed' => array( 'dashed', false ),
									'dotted' => array( 'dotted', false )
								),
								'addrowclasses' => 'disable'
							),
							'p_section_border_color' => array(
								'title' => esc_html__( 'Border Color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="#f2f2f2"',
								'value' => '#f2f2f2',
								'addrowclasses' => 'disable'
								),
							'prlx_module' => array(
								'type' => 'module',
								'name' => 'prlx_add'
							),
							'ani_module' => array(
								'type' => 'module',
								'name' => 'ani_add'
							),
						),
					),
				)
			),
			'column' => array(
				'layout' => array(
					'tab0' => array(
						'title' => esc_html__( 'Spacings', 'the8' ),
						'type' => 'tab',
						'layout' => array(
							'p_margins' => array(
								'title' => esc_html__( 'Margins (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
							'p_paddings' => array(
								'title' => esc_html__( 'Paddings (px)', 'the8' ),
								'type' => 'input_group',
								'source' => array(
									'left' => array('number', 'Left'),
									'top' => array('number', 'Top'),
									'bottom' => array('number', 'Bottom'),
									'right' => array('number', 'Right'),
								),
							),
						),
					),
					'tab1' => array(
						'title' => esc_html__( 'Styling', 'the8' ),
						'type' => 'tab',
						'init' => 'closed',
						'layout' => array(
							'p_customize_bg' => array(
								'title' => esc_html__( 'Customize', 'the8' ),
								'type' => 'checkbox',
								'atts' => 'data-options="e:p_bg_media_type;e:p_bg_color_type;e:p_font_color;"'
							),
							'p_bg_media_type' => array(
								'title' => esc_html__( 'Media type', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array( 'None', true, 'd:p_bg_img;d:p_is_bg_img_high_dpi;d:p_bg_video_type;d:p_bg_pattern;d:p_use_prlx;d:p_bg_possition;d:p_bg_repeat;d:p_bg_attach;' ),
									'img' => array( 'Image', false, 'e:p_bg_img;e:p_is_bg_img_high_dpi;e:p_bg_pattern;e:p_use_prlx;d:p_bg_video_type;e:p_bg_possition;e:p_bg_repeat;e:p_bg_attach;' ),
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_img' => array(
								'title' => esc_html__( 'Image', 'the8' ),
								'type' => 'media',
								'addrowclasses' => 'disable'
								),
							'p_bg_attach' => array(
								'title' => esc_html__( 'Background Attachment:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'scroll' => array( 'Scroll', true),
									'fixed' => array( 'Fixed', false ),
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_possition' => array(
								'title' => esc_html__( 'Background Position:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'left top' => array( 'left top', false),
									'left center' => array( 'left center', false ),
									'left bottom' => array( 'left bottom', false ),
									'right top' => array( 'right top', false ),
									'right center' => array( 'right center', false ),
									'right bottom' => array( 'right bottom', false ),
									'center top' => array( 'center top', false ),
									'center center' => array( 'center center', true ),
									'center bottom' => array( 'center bottom', false )
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_repeat' => array(
								'title' => esc_html__( 'Background Repeat:', 'the8' ),
								'type' => 'select',
								'source' => array(
									'no-repeat' => array( 'No Repeat', false),
									'repeat' => array( 'Repeat', false ),
									'cover' => array( 'Cover', true ),
									'contain' => array( 'Contain', false )
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_color_type' =>  array(
								'title' => esc_html__( 'Background Color', 'the8' ),
								'type' => 'select',
								'source' => array(
									'none' => array( 'None', true, 'd:p_bg_color;d:p_gradient_start_color;d:p_gradient_end_color;d:p_gradient_type;d:p_bg_color_opacity;' ),
									'color' => array( 'Color', false, 'e:p_bg_color;e:p_bg_color_opacity;d:p_gradient_start_color;d:p_gradient_end_color;d:p_gradient_type;' ),
									'gradient' => array( 'Gradient', false, 'e:p_gradient_start_color;e:p_gradient_end_color;e:p_gradient_type;e:p_bg_color_opacity;d:p_bg_color;' )
									),
								'addrowclasses' => 'disable'
								),
							'p_bg_color' => array(
								'title' => esc_html__( 'Color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="' . THE8_COLOR . '"',
								'value' => THE8_COLOR,
								'addrowclasses' => 'disable'
								),
							'p_gradient_start_color' => array(
								'title' => esc_html__( 'From', 'the8' ),
								'type' => 'text',
								'addrowclasses' => 'disable',
								'atts' => 'data-default-color="' . THE8_COLOR . '"',
								'value' => THE8_COLOR,
								'addrowclasses' => 'disable'
							),
							'p_gradient_end_color' => array(
								'title' => esc_html__( 'To', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'text',
								'atts' => 'data-default-color="#0eecbd"',
								'value' => '#0eecbd',
								'addrowclasses' => 'disable'
							),
							'p_gradient_type' => array(
								'title' => esc_html__( 'Type', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'radio',
								'value' => array(
									'linear' => array( 'Linear', true, 'e:p_gradient_linear_angle;d:p_gradient_radial_shape_settings_type;' ),
									'radial' => array( 'Radial', false, 'e:p_gradient_radial_shape_settings_type;d:p_gradient_linear_angle;' )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_linear_angle' => array(
								'title' => esc_html__( 'Angle', 'the8' ),
								'addrowclasses' => 'disable',
								'description' => esc_html__( 'Degrees: -360 to 360', 'the8' ),
								'type' => 'number',
								'atts' => ' min="-360" max="360" step="1"',
								'value' => '45',
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_shape_settings_type' => array(
								'addrowclasses' => 'disable',
								'title' => esc_html__( 'Shape variant', 'the8' ),
								'type' => 'radio',
								'value' => array(
									'simple' => array( 'Simple', true, 'e:p_gradient_radial_shape;d:p_gradient_radial_size_keyword;d:p_gradient_radial_size;' ),
									'extended' => array( 'Extended', false, 'e:p_gradient_radial_size_keyword;e:p_gradient_radial_size;d:p_gradient_radial_shape;' )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_shape' => array(
								'title' => esc_html__( 'Shape', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'radio',
								'value' => array(
									'ellipse' => array( 'Ellipse', true ),
									'circle' => array( 'Circle', false )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_size_keyword' => array(
								'title' => esc_html__( 'Size keyword', 'the8' ),
								'addrowclasses' => 'disable',
								'type' => 'select',
								'source' => array(
									'closest-side' => array( 'Closest side', false ),
									'farthest-side' => array( 'Farthest side', false ),
									'closest-corner' => array( 'Closest corner', false ),
									'farthest-corner' => array( 'Farthest corner', true )
								),
								'addrowclasses' => 'disable'
							),
							'p_gradient_radial_size' => array(
								'title' => esc_html__( 'Size', 'the8' ),
								'addrowclasses' => 'disable',
								'description' => esc_html__( 'Two space separated percent values, for example (60%% 55%%)', 'the8' ),
								'type' => 'text',
								'value' => '',
								'addrowclasses' => 'disable'
							),
							'p_use_prlx' => array(
								'title' => esc_html__( 'Apply Parallax', 'the8' ),
								'type' => 'checkbox',
								'atts' => 'data-options="e:p_prlx_speed;"',
								'addrowclasses' => 'disable'
								),
							'p_prlx_speed' => array(
								'title' => esc_html__( 'Parallax speed', 'the8' ),
								'type' => 'number',
								'atts' => 'min="1" max="100" step="1"',
								'value' => '50',
								'addrowclasses' => 'disable'
							),
							'p_bg_color_opacity' => array(
								'title' => esc_html__( 'Opacity', 'the8' ),
								'description' => esc_html__( 'In percents', 'the8' ),
								'type' => 'number',
								'atts' => 'min="1" max="100" step="1"',
								'value' => '100',
								'addrowclasses' => 'disable'
							),
							'p_bg_pattern' => array(
								'title' => esc_html__( 'Pattern', 'the8' ),
								'type' => 'media',
								'addrowclasses' => 'disable'
							),
							'p_font_color' => array(
								'title' => esc_html__( 'Font color', 'the8' ),
								'type' => 'text',
								'atts' => 'data-default-color="' . $body_font_color . '"',
								'value' => $body_font_color,
								'addrowclasses' => 'disable'
							),
							'module' => array(
								'type' => 'module',
								'name' => 'ani_add'
							),
							'p_eclass' => array(
								'title' => esc_html__( 'Extra Class', 'the8' ),
								'description' => esc_html__( 'Space separated class names', 'the8' ),
								'type' => 'text',
							),
						),
					),
				)
			),
			'tweet' => array(
					'atts' => true,
					'layout' => array(
					'p_title' => array(
						'title' => esc_html__( 'Title', 'the8' ),
							'type' => 'text',
					),
					'p_centertitle' => array(
						'title' => esc_html__( 'Center title', 'the8' ),
						'type' => 'checkbox'
					),
					'p_items' => array(
						'title' => esc_html__( 'Tweets to extract', 'the8' ),
						'type' => 'text',
					),
					'p_visible' => array(
						'title' => esc_html__( 'Tweets to show', 'the8' ),
						'type' => 'text',
					),
					'p_showdate' => array(
						'title' => esc_html__( 'Show date', 'the8' ),
						'type' => 'checkbox'
					),
					'module' => array(
						'type' => 'module',
						'name' => 'ani_add'
					),
				)
			),
			'ani_add' => array(
				'layout' => array(
					'p_animate' => array(
						'title' => esc_html__( 'Add animation', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:p_ani_effect;e:p_ani_duration;e:p_ani_delay;e:p_ani_offset;e:p_ani_iteration"',
					),
					'p_ani_effect' => array(
						'title' => esc_html__( 'Effect', 'the8' ),
						'type' => 'select',
						'addrowclasses' => 'disable',
						'source' => array(
							'' => array('None', true),
							'bounce' => array('bounce'),
							'flash' => array('flash'),
							'pulse' => array('pulse'),
							'shake' => array('shake'),
							'swing' => array('swing'),
							'tada' => array('tada'),
							'wobble' => array('wobble'),
							'bounceIn' => array('bounceIn'),
							'bounceInDown' => array('bounceInDown'),
							'bounceInLeft' => array('bounceInLeft'),
							'bounceInRight' => array('bounceInRight'),
							'bounceInUp' => array('bounceInUp'),
							'bounceOut' => array('bounceOut'),
							'bounceOutDown' => array('bounceOutDown'),
							'bounceOutLeft' => array('bounceOutLeft'),
							'bounceOutRight' => array('bounceOutRight'),
							'bounceOutUp' => array('bounceOutUp'),
							'fadeIn' => array('fadeIn'),
							'fadeInDown' => array('fadeInDown'),
							'fadeInDownBig' => array('fadeInDownBig'),
							'fadeInLeft' => array('fadeInLeft'),
							'fadeInLeftBig' => array('fadeInLeftBig'),
							'fadeInRight' => array('fadeInRight'),
							'fadeInRightBig' => array('fadeInRightBig'),
							'fadeInUp' => array('fadeInUp'),
							'fadeInUpBig' => array('fadeInUpBig'),
							'fadeOut' => array('fadeOut'),
							'fadeOutDown' => array('fadeOutDown'),
							'fadeOutDownBig' => array('fadeOutDownBig'),
							'fadeOutLeft' => array('fadeOutLeft'),
							'fadeOutLeftBig' => array('fadeOutLeftBig'),
							'fadeOutRight' => array('fadeOutRight'),
							'fadeOutRightBig' => array('fadeOutRightBig'),
							'fadeOutUp' => array('fadeOutUp'),
							'fadeOutUpBig' => array('fadeOutUpBig'),
							'flipInX' => array('flipInX'),
							'flipInY' => array('flipInY'),
							'flipOutX' => array('flipOutX'),
							'flipOutY' => array('flipOutY'),
							'lightSpeedIn' => array('lightSpeedIn'),
							'lightSpeedOut' => array('lightSpeedOut'),
							'rotateIn' => array('rotateIn'),
							'rotateInDownLeft' => array('rotateInDownLeft'),
							'rotateInDownRight' => array('rotateInDownRight'),
							'rotateInUpLeft' => array('rotateInUpLeft'),
							'rotateInUpRight' => array('rotateInUpRight'),
							'rotateOut' => array('rotateOut'),
							'rotateOutDownLeft' => array('rotateOutDownLeft'),
							'rotateOutDownRight' => array('rotateOutDownRight'),
							'rotateOutUpLeft' => array('rotateOutUpLeft'),
							'rotateOutUpRight' => array('rotateOutUpRight'),
							'slideInDown' => array('slideInDown'),
							'slideInLeft' => array('slideInLeft'),
							'slideInRight' => array('slideInRight'),
							'slideOutLeft' => array('slideOutLeft'),
							'slideOutRight' => array('slideOutRight'),
							'slideOutUp' => array('slideOutUp'),
							'hinge' => array('hinge'),
							'rollIn' => array('rollIn'),
							'rollOut' => array('rollOut')
						),
					),
					'p_ani_duration' => array(
						'title' => esc_html__( 'Duration (sec)', 'the8' ),
						'type' => 'text',
						'value' => '2',
						'addrowclasses' => 'disable',
					),
					'p_ani_delay' => array(
						'title' => esc_html__( 'Delay (sec)', 'the8' ),
						'type' => 'text',
						'value' => '0',
						'addrowclasses' => 'disable',
					),
					'p_ani_offset' => array(
						'title' => esc_html__( 'Offset (px, related to bottom)', 'the8' ),
						'type' => 'text',
						'value' => '10',
						'addrowclasses' => 'disable',
					),
					'p_ani_iteration' => array(
						'title' => esc_html__( 'Iteration', 'the8' ),
						'type' => 'text',
						'value' => '1',
						'addrowclasses' => 'disable',
					),
				),
			),
			'prlx_add' => array(
				'layout' => array(
					'p_customize_bg' => array(
						'title' => esc_html__( 'Customize', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:p_bg_media_type;e:p_bg_color_type;e:p_font_color;"'
					),
					'p_bg_media_type' => array(
						'title' => esc_html__( 'Media type', 'the8' ),
						'type' => 'select',
						'source' => array(
							'none' => array( 'None', true, 'd:p_bg_img;d:p_is_bg_img_high_dpi;d:p_bg_video_type;d:p_bg_pattern;d:p_use_prlx;d:p_bg_possition;d:p_bg_repeat;d:p_bg_attach;' ),
							'img' => array( 'Image', false, 'e:p_bg_img;e:p_is_bg_img_high_dpi;e:p_bg_pattern;e:p_use_prlx;d:p_bg_video_type;e:p_bg_possition;e:p_bg_repeat;e:p_bg_attach;' ),
							),
						'addrowclasses' => 'disable'
						),
					'p_bg_img' => array(
						'title' => esc_html__( 'Image', 'the8' ),
						'type' => 'media',
						'addrowclasses' => 'disable'
						),
					'p_bg_attach' => array(
						'title' => esc_html__( 'Background Attachment:', 'the8' ),
						'type' => 'select',
						'source' => array(
							'scroll' => array( 'Scroll', true),
							'fixed' => array( 'Fixed', false ),
							),
						'addrowclasses' => 'disable'
						),
					'p_bg_possition' => array(
						'title' => esc_html__( 'Background Position:', 'the8' ),
						'type' => 'select',
						'source' => array(
							'left top' => array( 'left top', false),
							'left center' => array( 'left center', false ),
							'left bottom' => array( 'left bottom', false ),
							'right top' => array( 'right top', false ),
							'right center' => array( 'right center', false ),
							'right bottom' => array( 'right bottom', false ),
							'center top' => array( 'center top', false ),
							'center center' => array( 'center center', true ),
							'center bottom' => array( 'center bottom', false )
							),
						'addrowclasses' => 'disable'
						),
					'p_bg_repeat' => array(
						'title' => esc_html__( 'Background Repeat:', 'the8' ),
						'type' => 'select',
						'source' => array(
							'no-repeat' => array( 'No Repeat', false),
							'repeat' => array( 'Repeat', false ),
							'cover' => array( 'Cover', true ),
							'contain' => array( 'Contain', false )
							),
						'addrowclasses' => 'disable'
						),
					'p_bg_color_type' =>  array(
						'title' => esc_html__( 'Background Color', 'the8' ),
						'type' => 'select',
						'source' => array(
							'none' => array( 'None', true, 'd:p_bg_color;d:p_gradient_start_color;d:p_gradient_end_color;d:p_gradient_type;d:p_bg_color_opacity;' ),
							'color' => array( 'Color', false, 'e:p_bg_color;e:p_bg_color_opacity;d:p_gradient_start_color;d:p_gradient_end_color;d:p_gradient_type;' ),
							'gradient' => array( 'Gradient', false, 'e:p_gradient_start_color;e:p_gradient_end_color;e:p_gradient_type;e:p_bg_color_opacity;d:p_bg_color;' )
							),
						'addrowclasses' => 'disable'
						),
					'p_bg_color' => array(
						'title' => esc_html__( 'Color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="' . THE8_COLOR . '"',
						'value' => THE8_COLOR,
						'addrowclasses' => 'disable'
						),
					'p_gradient_start_color' => array(
						'title' => esc_html__( 'From', 'the8' ),
						'type' => 'text',
						'addrowclasses' => 'disable',
						'atts' => 'data-default-color="' . THE8_COLOR . '"',
						'value' => THE8_COLOR,
						'addrowclasses' => 'disable'
					),
					'p_gradient_end_color' => array(
						'title' => esc_html__( 'To', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'text',
						'atts' => 'data-default-color="#0eecbd"',
						'value' => '#0eecbd',
						'addrowclasses' => 'disable'
					),
					'p_gradient_type' => array(
						'title' => esc_html__( 'Type', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'radio',
						'value' => array(
							'linear' => array( 'Linear', true, 'e:p_gradient_linear_angle;d:p_gradient_radial_shape_settings_type;' ),
							'radial' => array( 'Radial', false, 'e:p_gradient_radial_shape_settings_type;d:p_gradient_linear_angle;' )
						),
						'addrowclasses' => 'disable'
					),
					'p_gradient_linear_angle' => array(
						'title' => esc_html__( 'Angle', 'the8' ),
						'addrowclasses' => 'disable',
						'description' => esc_html__( 'Degrees: -360 to 360', 'the8' ),
						'type' => 'number',
						'atts' => ' min="-360" max="360" step="1"',
						'value' => '45',
						'addrowclasses' => 'disable'
					),
					'p_gradient_radial_shape_settings_type' => array(
						'addrowclasses' => 'disable',
						'title' => esc_html__( 'Shape variant', 'the8' ),
						'type' => 'radio',
						'value' => array(
							'simple' => array( 'Simple', true, 'e:p_gradient_radial_shape;d:p_gradient_radial_size_keyword;d:p_gradient_radial_size;' ),
							'extended' => array( 'Extended', false, 'e:p_gradient_radial_size_keyword;e:p_gradient_radial_size;d:p_gradient_radial_shape;' )
						),
						'addrowclasses' => 'disable'
					),
					'p_gradient_radial_shape' => array(
						'title' => esc_html__( 'Shape', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'radio',
						'value' => array(
							'ellipse' => array( 'Ellipse', true ),
							'circle' => array( 'Circle', false )
						),
						'addrowclasses' => 'disable'
					),
					'p_gradient_radial_size_keyword' => array(
						'title' => esc_html__( 'Size keyword', 'the8' ),
						'addrowclasses' => 'disable',
						'type' => 'select',
						'source' => array(
							'closest-side' => array( 'Closest side', false ),
							'farthest-side' => array( 'Farthest side', false ),
							'closest-corner' => array( 'Closest corner', false ),
							'farthest-corner' => array( 'Farthest corner', true )
						),
						'addrowclasses' => 'disable'
					),
					'p_gradient_radial_size' => array(
						'title' => esc_html__( 'Size', 'the8' ),
						'addrowclasses' => 'disable',
						'description' => esc_html__( 'Two space separated percent values, for example (60%% 55%%)', 'the8' ),
						'type' => 'text',
						'value' => '',
						'addrowclasses' => 'disable'
					),
					'p_use_prlx' => array(
						'title' => esc_html__( 'Apply Parallax', 'the8' ),
						'type' => 'checkbox',
						'atts' => 'data-options="e:p_prlx_speed;"',
						'addrowclasses' => 'disable'
						),
					'p_prlx_speed' => array(
						'title' => esc_html__( 'Parallax speed', 'the8' ),
						'type' => 'number',
						'atts' => 'min="1" max="100" step="1"',
						'value' => '50',
						'addrowclasses' => 'disable'
					),
					'p_bg_color_opacity' => array(
						'title' => esc_html__( 'Opacity', 'the8' ),
						'description' => esc_html__( 'In percents', 'the8' ),
						'type' => 'number',
						'atts' => 'min="1" max="100" step="1"',
						'value' => '100',
						'addrowclasses' => 'disable'
					),
					'p_bg_pattern' => array(
						'title' => esc_html__( 'Pattern', 'the8' ),
						'type' => 'media',
						'addrowclasses' => 'disable'
					),
					'p_font_color' => array(
						'title' => esc_html__( 'Font color', 'the8' ),
						'type' => 'text',
						'atts' => 'data-default-color="' . $body_font_color . '"',
						'value' => $body_font_color,
						'addrowclasses' => 'disable'
					)
				)
			),
		);
	}
?>
