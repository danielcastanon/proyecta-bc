<?php
	get_header ();

	$sb = the8_get_sidebars();
	$sb_class = $sb && !empty($sb['sb_layout_class']) ? $sb['sb_layout_class'] . '_sidebar' : '';
	$sb1_class = $sb && $sb['sb_layout'] == 'right' ? 'sb_right' : 'sb_left';

?>
<div class="page_content <?php echo sanitize_html_class($sb_class); ?>">
	<?php
	if ( $sb && $sb['sb_exist'] ) {
		echo "<div class='container'>";
		if ( $sb['sb1_exists'] ) {
			echo "<aside class='".sanitize_html_class($sb1_class)."'>";
			dynamic_sidebar( $sb['sidebar1'] );
			echo "</aside>";
		}
		if ( $sb['sb2_exists'] ) {
			echo "<aside class='sb_right'>";
			dynamic_sidebar( $sb['sidebar2'] );
			echo "</aside>";
		}
	}

	?>
	<main>
		<div class="grid_row">
			<?php
				echo the8_cws_ourteam( array( 'mode' => 'grid_with_filter' ) );
			?>
		</div>
	</main>
	<?php echo $sb && $sb['sb_exist'] ? "</div>" : ""; ?>
</div>

<?php

get_footer ();
?>